<?php

namespace App\Listeners\User;

use App\Events\User\UserHasLoggedIn;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Carbon\Carbon;
use Log;

class LogLoginInfo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasLoggedIn  $event
     * @return void
     */
    public function handle(UserHasLoggedIn $event)
    {
        $user = User::find($event->user->id);

        // get current date
        $current_date = Carbon::now();
        
        if($user->hasSubscription()) {

            // Check last user subscription
            $last_subscription = $user->getLastSubscription();

            // Check last subscription status
            // if status is cancelled
            if($last_subscription->status == 'cancelled') {
                // get last subscription date
                $cancel_date_mollie = new Carbon($user->getLastSubscription()->createdDatetime);
                if($user->plan_id == '1') {
                    $cancel_date = $cancel_date_mollie->addMonth();
                } elseif($user->plan_id == '2') {
                    $cancel_date = $cancel_date_mollie->addYear();
                }

                // if current date is greater than last subscription date
                if($current_date > $cancel_date) {

                    // update user active to 0
                    $user->update(['active'=>0])->save();

                }
                // else 
                    // nada
            } elseif($last_subscription->status == 'active') {

                // get current user active status
                $active = $user->active;

                // get last subscription date
                $last_subscription_start_date = new Carbon($user->getLastSubscription()->startDate);

                if($last_subscription_start_date >= $current_date) {
                    if($active == 2) {
                        $user->update(['active'=>1])->save();
                    }
                }

            } else {
            // else
                // nada
            }
        }

        $user->update(['last_login'=>Carbon::now()]);

        // $event->user->last_login->touch();
        Log::info('User: '.$user->id. ' heeft ingelogd om: '.$user->last_login.'.');
    }
}
