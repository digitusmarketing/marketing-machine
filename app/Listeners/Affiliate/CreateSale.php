<?php

namespace App\Listeners\Affiliate;

use App\Events\Affiliate\AffiliateSale;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSale
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AffiliateSale  $event
     * @return void
     */
    public function handle(AffiliateSale $event)
    {
        //
    }
}
