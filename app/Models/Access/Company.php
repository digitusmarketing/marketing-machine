<?php namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Company extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'companies';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['validated', 'validate_request', 'company_name', 'kvk', 'title', 'business_email', 'address', 'postal', 'mobile_phone', 'phone', 'recidence', 'country_code', 'country_full', 'appellation', 'iban', 'bic', 'btw', 'user_id'];


	public function user() {
		return $this->belongsTo('App\User');
	}

}
