<?php

namespace App\Models\Access\Funnel;

use Illuminate\Database\Eloquent\Model;

class Funnel extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'funnels';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['active', 'slug', 'titel', 'slogan', 'body', 'end_point', 'pre_button_text', 'button_text', 'form_button_text', 'pre_button_2_text', 'button_2_text', 'form_button_2_text', 'video_link','image','content_titel', 'content_body'];


	public function funnelitems() {
		return $this->hasMany('App\Models\Access\Funnel\FunnelItem', 'funnel_id', 'id');
	}

	public function scopeIsActive($query)
	{
		return $query->where('active',1)->first();
	}
}
