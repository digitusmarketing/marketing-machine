<?php

namespace App\Models\Access\Funnel;

use Illuminate\Database\Eloquent\Model;

class FunnelItem extends Model
{
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'funnelitems';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['slug', 'titel', 'slogan', 'item_link', 'item_post', 'call_to_action_titel', 'call_to_action_button_text', 'call_to_action_delay', 'funnel_id'];


	public function funnel() {
		return $this->belongsTo('App\Models\Access\Funnel\Funnel', 'id');
	}
}
