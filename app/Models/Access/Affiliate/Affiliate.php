<?php namespace App\Models\Access\Affiliate;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Affiliate extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'affiliates';

	protected $dates = ['approve_date'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'code', 'watch_count', 'validated', 'status', 'approve_date'];

	// protected $attributes = array('status'=>'',);


	public function user() {
		return $this->belongsTo('App\User');
	}

	public function sales()
	{
		return $this->hasMany('App\Models\Access\Affiliate\Sale');
	}

	public function isValidated()
	{
		if($this->status != 2) {
			return false;
		} else {
			return true;
		}
	}

	public function countActiveSalesUsers()
	{
		$sales = $this->sales;
		$count = 0;
		if($sales) {
			foreach($sales as $sale) {
				if($sale->order->user->active == 1) {
					$count++;
				}
			}
			return $count;
		} else {
			return $count;
		}
	}

	public function getSalesAmount()
	{
		$company = $this->user->company;
		$sales = $this->sales;
		$prijs = '0.00';
		$commissie = config('affiliate.normal.commissie');
		foreach($sales as $sale) {
			if($sale->order->user->active == 1) {
				if($company->country_code != 'NL') {
					$prijs+= \App\Models\Plan::find(1)->price * $commissie;
				} else {
					$prijs+= \App\Models\Plan::find(1)->price * $commissie * 1.21;
				}
			}
		}	
		return number_format($prijs, 2, '.', ',');
	}

}
