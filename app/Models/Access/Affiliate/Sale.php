<?php namespace App\Models\Access\Affiliate;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Sale extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sales';

	protected $dates = ['paid_at', 'created_at', 'updated_at', 'will_be_paid_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['affiliate_id', 'order_id', 'paid_out', 'paid_at', 'will_be_paid_at'];


	public function affiliate()
	{
		return $this->belongsTo('App\Models\Access\Affiliate\Affiliate');
	}
	public function order() {
		return $this->hasOne('App\Models\Access\Order', 'id', 'order_id');
	}

}
