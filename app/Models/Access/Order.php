<?php namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Hyperized\Wefact\Types\Invoice;
use Carbon\Carbon;

class Order extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orders';

	protected $dates = ['payment_date'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['transaction_id', 'status', 'description', 'type', 'price', 'paymentmethod', 'order_id', 'wefact_invoice_code', 'user_id', 'plan_id', 'cursus_id', 'payment_date'];


	public function user() {
		return $this->belongsTo('App\User');
	}

	public function sale()
	{
		return $this->hasOne('App\Models\Access\Affiliate\Sale', 'order_id', 'id');
	}

	public function getPaymentDateAttribute($date)
	{
		return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
	}

	public function wefactInvoice()
	{
		$factuur = new Invoice();
		// $factuurParameter = array('Identifier'=>1);
		$defactuur = $factuur->show(['InvoiceCode' => $this->wefact_invoice_code]);
		return $defactuur;
	}

}
