<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Plan extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'plans';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'type', 'pay_description', 'show_description', 'price', 'interval', 'cursus_id'];


	public function cursus() {
		return $this->belongsTo('App\Models\Cursus\Cursus');
	}

	public function user()	{
		return $this->hasMany('App\User');
	}

}
