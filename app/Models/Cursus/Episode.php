<?php namespace App\Models\Cursus;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\User;

class Episode extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'episodes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'body', 'src', 'module_id'];


	public function module() {
		return $this->belongsTo('App\Models\Cursus\Module');
	}

	public function user() {
		return $this->belongsToMany('App\User');
	}

	public function previousEpisode($moduleId, $episodeId)
	{
		if($this->where('id','<',$episodeId)->where('module_id',$moduleId)->orderBy('id','desc')->first()) {
			return $this->where('id','<',$episodeId)->where('module_id',$moduleId)->orderBy('id','desc')->first();
		} else {
			return null;
		}
	}

	public function nextEpisode($moduleId, $episodeId)
	{
		if($this->where('id','>',$episodeId)->where('module_id',$moduleId)->first()) {
			return $this->where('id','>',$episodeId)->where('module_id',$moduleId)->first();			
		} else {
			return null;
		}
	}

	public function isCompleted($episodeId, $userId)
	{
		$user = User::find($userId);
		return $user->episodeIsComplete($episodeId);
	}

	public function isFavorite($episodeId, $userId)
	{
		$user = User::find($userId);
		return $user->episodeIsFavorite($episodeId);
	}

	public function isWatchLater($episodeId, $userId)
	{
		$user = User::find($userId);
		return $user->episodeisWatchLater($episodeId);
	}

	public function addWatchCount($episodeId, $userId)
	{
		$user = User::find($userId);
		return $user->addEpisodeCount($episodeId);
	}

}
