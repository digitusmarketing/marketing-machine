<?php

namespace App\Models\Cursus;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tools';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['titel', 'link', 'module_id'];

    public function module()
    {
    	return $this->belongsTo('App\Models\Cursus\Module');
    }
}