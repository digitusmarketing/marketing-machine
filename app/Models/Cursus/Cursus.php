<?php namespace App\Models\Cursus;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Cursus extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cursussen';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','body'];

	public function users() {
		return $this->belongsToMany('App\User');
	}

	public function modules() {
		return $this->hasMany('App\Models\Cursus\Module');
	}

}
