<?php namespace App\Models\Cursus;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Review extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'reviews';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['review_name', 'body', 'company', 'image', 'video_link'];

}