<?php namespace App\Models\Cursus;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Module extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'modules';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'slug', 'image', 'body', 'cursus_id'];


	public function cursus() {
		return $this->belongsTo('App\Models\Cursus\Cursus');
	}

	public function episodes()
	{
		return $this->hasMany('App\Models\Cursus\Episode', 'module_id');
	}

	public function tools()
	{
		return $this->hasMany('App\Models\Cursus\Tool');
	}

	public function previousModule($id)
	{
		return $this->where('id','<', $id)->orderBy('id','desc')->first();
	}

	public function nextModule($id)
	{
		return $this->where('id', '>', $id)->first();
	}

}
