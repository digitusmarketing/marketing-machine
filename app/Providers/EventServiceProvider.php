<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\User\UserHasLoggedIn' => [
            'App\Listeners\User\LogLoginInfo'
        ],
        'App\Events\Affiliate\AffiliateSale' => [
            'App\Listeners\Affiliate\AffiliateUpdateSubscription',
            'App\Listeners\Affiliate\CreateSale'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
