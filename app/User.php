<?php

namespace App;

use Carbon\Carbon;
use Hyperized\Wefact\Types\Invoice;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mollie;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active', 'first_name', 'last_name', 'username', 'profile_image', 'cover', 'email', 'password', 'role', 'plan_id', 'mollie_customer_id', 'mollie_mandate_id', 'mollie_subscription_id', 'wefact_debtor_id', 'last_login', 'company_id',
    ];

    protected $dates = [
        'last_login',
    ];

    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'mollie_customer_id', 'mollie_mandate_id', 'mollie_subscription_id', 'wefact_debtor_id', 'company_id'
    ];

    public function scopeActive($query)
    {
        return $query->where('active', '>=', 1);
    }

    public function isAffiliate()
    {
        if($this->affiliate) {
            return true;
        } else {
            return false;
        }
    }

    public function isValidatedAffiliate()
    {
        if($this->isAffiliate()) {
            if($this->affiliate->isValidated()) {
                return true;
            }
        }
        return false;
    }

    public function getCreatedAtAttribute($date)
    {
        if($date) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
        } else {
            return '-';
        }
    }

    // public function getLastLoginAttribute($date)
    // {
    //     if($date) {
    //         Carbon::setLocale('nl');
    //         return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    //     } else {
    //         return 'nog niet ingelogd';
    //     }
    // }

    public function isAdmin()
    {
        if($this->role == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function isMember()
    {
        if($this->role == 2 || $this->role == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function isCustomer()
    {
        if($this->mollie_customer_id) {
            return true;
        } else {
            return false;
        }
    }

    public function hasSubscription()
    {
        if($this->isCustomer()) {
            if($this->mollie_subscription_id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function subscriptionPrice()
    {
        if($this->hasSubscription()) {
            $subscription = Mollie::api()->customersSubscriptions()->withParentId($this->mollie_customer_id)->get($this->mollie_subscription_id);
            return $subscription->amount;
        } else {
            return false;
        }
    }

    public function nextSubscriptionPayment()
    {
        if($this->hasSubscription()) {
            Carbon::setLocale('nl');
            $payment = Mollie::api()->payments()->get($this->latestOrder()->transaction_id);
            $date = new Carbon($payment->paidDatetime);
            return $date->addMonth()->format('d-m-Y');
        } else {
            return false;
        }
    }

    public function getLastSubscription()
    {
        $subscriptions = Mollie::api()->customersSubscriptions()->withParentId($this->mollie_customer_id)->all();
        return $subscriptions[0];
    }

    public function getSubscriptionCancelDate()
    {
        Carbon::setLocale('nl');
        $cancelDate = new Carbon($this->getLastSubscription()->cancelledDatetime);
        return $cancelDate->format('d-m-Y');
    }

    public function getAccountEndDate()
    {
        if($this->isCustomer()) {
            Carbon::setLocale('nl');
            if($this->plan_id == '1') {
                $date = new Carbon($this->getLastSubscription()->createdDatetime);
                $end_date = $date->addMonth();
            } elseif($this->plan_id == '2') {
                $date = new Carbon($this->getLastSubscription()->createdDatetime);
                $end_date = $date->addYear();
            } else {
                $end_date = 'Je hebt geen abonnement bij Socialpreneurs';
            }

            return $end_date->diffForHumans();
        } else {
            return 'Je hebt geen abonnement bij Socialpreneurs';
        }
    }

    public function hasOrderWithSale()
    {
        $sale_sales = $this->orders->load('sale');
        $sale = $sale_sales->where('sale',!null)->first();        
        if($sale != null) {
            return true;
        } else {
            return false;
        }
    }

    public function orderWithSale()
    {
        $sale_sales = $this->orders->load('sale');
        $sale = $sale_sales->where('sale',!null)->first();
        return $sale;

    }

    public function courses() {
        return $this->belongsToMany('App\Models\Cursus\Cursus');
    }

    public function episodes() {
        return $this->belongsToMany('App\Models\Cursus\Episode')->withPivot('completed','favorite','watch_later','watch_count');
    }

    public function plan() {
        return $this->belongsTo('App\Models\Plan');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Access\Company');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Access\Order')->orderBy('id');
    }


    public function latestOrder()
    {
        return $this->hasMany('App\Models\Access\Order')->latest()->first();
    }

    public function affiliate()
    {
        return $this->hasOne('App\Models\Access\Affiliate\Affiliate');   
    }

    public function countOrders()
    {
        return count($this->orders()->get());
    }

    public function getEpisodeCount($episodeId)
    {
        $a = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a === null) {
            $this->checkRelation($episodeId);
        }
        $b = $this->episodes()->where('episode_id',$episodeId)->first();
        return $b->pivot->watch_count;
    }

    public function addEpisodeCount($episodeId)
    {
        $a = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a === null) {
            $this->checkRelation($episodeId);
        }
        $b = $this->episodes()->where('episode_id',$episodeId)->first();
        
        $this->episodes()->updateExistingPivot($episodeId, ['watch_count'=> $b->pivot->watch_count +1]);
        // $b->pivot->watch_count = $b->pivot->watch_count + 1;

        return true;
    }

    public function checkRelation($episodeId)
    {
        if($this->episodes->contains($episodeId) === false) {
            $this->episodes()->attach($episodeId);
        } else {
            return true;
        }
    }

    public function countCompleteEpisodes() {
        return count($this->episodes()->where('completed',1)->get());
    }

    public function countIncompleteEpisodes() {
        return count($this->episodes()->where('completed',0)->get());
    }

    public function episodeIsComplete($episodeId)
    {
        $a = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a === null) {
            $this->checkRelation($episodeId);
        }
        $b = $this->episodes()->where('episode_id',$episodeId)->first();
        if($b->pivot->completed === 0) {
            return false;
        } else {
            return true;
        }
    }

    public function episodeIsFavorite($episodeId)
    {
         $a = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a === null) {
            $this->checkRelation($episodeId);
        }
        $b = $this->episodes()->where('episode_id',$episodeId)->first();
        if($b->pivot->favorite === 0) {
            return false;
        } else {
            return true;
        }    
    }

    public function episodeIsWatchLater($episodeId)
    {
        $a = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a === null) {
            $this->checkRelation($episodeId);
        }
        $b = $this->episodes()->where('episode_id',$episodeId)->first();
        if($b->pivot->watch_later === 0) {
            return false;
        } else {
            return true;
        }
    }

    public function markComplete($episodeId)
    {
        $a = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a === null) {
            $this->checkRelation($episodeId);
        }
        $b = $this->episodes()->where('episode_id',$episodeId)->first();
        if($b->pivot->completed === 0) {
            $this->episodes()->updateExistingPivot($episodeId, ['completed'=>1]);
            return true;
        } else {
            $this->episodes()->updateExistingPivot($episodeId, ['completed'=>0]);
            return false;
        }
    }

    public function toggleEpisodeAsFavorite($episodeId)
    {
         $a = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a === null) {
            $this->checkRelation($episodeId);
        }
        $b = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a->pivot->favorite === 0) {
            return $this->episodes()->updateExistingPivot($episodeId, ['favorite'=>1]);
        } else {
            return $this->episodes()->updateExistingPivot($episodeId, ['favorite'=>0]);
        }
    }

    public function toggleWatchLaterEpisode($episodeId)
    {
         $a = $this->episodes()->where('episode_id',$episodeId)->first();
        if($a === null) {
            $this->checkRelation($episodeId);
        }
        $b = $this->episodes()->where('episode_id',$episodeId)->first();
        if($b->pivot->watch_later === 0) {
            $this->episodes()->updateExistingPivot($episodeId, ['watch_later'=>1]);
            return true;
        } else {
            $this->episodes()->updateExistingPivot($episodeId, ['watch_later'=>0]);
            return false;
        }
    }

    public function countCompletedEpisodes()
    {
        return count($this->episodes()->where('completed',1)->get());
    }

    public function countFavoriteEpisodes() {
        return count($this->episodes()->where('favorite',1)->get());
    }

    public function countWatchLaterEpisodes() {
        return count($this->episodes()->where('watch_later',1)->get());
    }

    public function favoriteEpisodes()
    {
        return $this->episodes()->where('favorite',1);
    }

    public function watchLaterEpisodes()
    {
        return $this->episodes()->where('watch_later',1);
    }

     /**
     * Sends the password reset notification.
     *
     * @param  string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomPassword($token));
    }
}

class CustomPassword extends ResetPassword
{
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Wachtwoord reset - ' . config('app.officialname'))
            ->line('We sturen je deze email omdat we een verzoek hebben ontvangen om je wachtwoord te resetten. Klik op de onderstaande knop om je wachtwoord opnieuw in te stellen.')
            ->action('Wachtwoord opnieuw instellen', url('/password/reset/'.$this->token))
            ->line('Indien je geen aanvraag hebt gedaan om je wachtwoord te resetten, neem a.u.b. contact met ons op.');
    }
}
