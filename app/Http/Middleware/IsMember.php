<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\User;

class IsMember {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// $user = User::find(1);
		// auth()->login($user);
		// dd($this->auth);
		// dd(auth());
		if ($this->auth->guest())
		{
			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
				return redirect()->guest('/login');
			}
		}
		// dd($this->auth);
		// if($this->auth->user()->active === 1) {
			if($this->auth->user()->role == '1' || $this->auth->user()->role == '2' || $this->auth->user()->role == '3')
			{
				return $next($request);
			}
		// }

		return redirect('/login')->withErrors('Je bent niet bevoegd om op deze sectie te komen.');

	}

}
