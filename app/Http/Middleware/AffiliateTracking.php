<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AffiliateTracking {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$response = $next($request);
		$minutes = 60 * 24 * 30;		

		if($request->hasCookie('aid')) {
			return $response;
		} else {
			if($request->has('a')) {
				return $response->withCookie(cookie('aid', $request->get('a'), $minutes));
			} else {
				return $response;
			}
		}

	}

}
