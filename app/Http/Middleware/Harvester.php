<?php namespace App\Http\Middleware;

use File;
use Closure;
use DateTime;

class Harvester
{

    public function handle($request, Closure $next)
    {
        if(!$request->isMethod('get')) {
            $date = (new \DateTime())->format('Y-m-d H:i:s.u');
            $data = [
                'req_url' => $_SERVER['REQUEST_URI'],
                'remote_addr' => $_SERVER['REMOTE_ADDR'],
                'time' => $date,
                'method' => $request->method(),
                'get' => $_GET,
                'post' => $request->all()
            ];

            $json_string = json_encode($data,JSON_PRETTY_PRINT);

            $file_handle = fopen(public_path("app/shit/{$date}.json"), 'w');
            fwrite($file_handle, $json_string);
            fclose($file_handle);

        }

        return $next($request);
    }
}