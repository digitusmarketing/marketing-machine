<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KoopCommunityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aanhef'            => 'required',
            'voornaam'          => 'required',
            'achternaam'        => 'required',
            'email'             => 'required|email',
            'bedrijfsnaam'      => 'required',
            'zakelijk_email'    => 'required|email',
            'adres'             => 'required',
            'postcode'          => 'required',
            'woonplaats'        => 'required',
            'land'              => 'required',
            'btwnummer'         => 'required_unless:land,NL|regex:/^[A-Z]{2,3}\s?[0-9]{7,}([A-Z]{1,3})?([0-9]{1,})?$/',
            'paymentmethod'     => 'required',
            'bedrag'            => 'required',
        ];
    }
}
