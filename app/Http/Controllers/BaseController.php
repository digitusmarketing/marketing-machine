<?php namespace App\Http\Controllers;

use App\Events\User\UserHasLoggedIn;
use App\Http\Controllers\Controller;
use App\Http\Requests\KoopCommunityRequest;
use App\Models\Access\Affiliate\Affiliate;
use App\Models\Access\Company;
use App\Models\Access\Order;
use App\Models\Access\SocialAccount;
use App\Models\Cursus\Cursus;
use App\Models\Cursus\Module;
use App\Models\Cursus\Review;
use App\Models\Plan;
use App\SocialAccountService;
use App\User;
use Auth;
use Carbon\Carbon;
use Hyperized\Wefact\Types\Debtor;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Log;
use Mollie;
use Socialite;

class BaseController extends Controller {

	public function __construct(Cursus $cursus, Module $module, Review $review, PLan $plan, User $user, Affiliate $affiliate, Order $order, Mollie $mollie, Company $company, SocialAccount $socialaccount, Guard $auth)
	{
		$this->cursus = $cursus;
		$this->module = $module;
		$this->review = $review;
		$this->user = $user;
		$this->affiliate = $affiliate;
		$this->plan = $plan;
		$this->order = $order;
		$this->mollie = Mollie::api();
		$this->company = $company;
		$this->socialaccount = $socialaccount;
		$this->auth = $auth;
	}
	
	public function index()
	{
		$cursus = $this->cursus->find(1);
		$plans = $this->plan->all()->take(2);
		$modules = $this->module->where('cursus_id',2)->get();
		$reviews = $this->review->all();
		$methods = $this->mollie->methods()->all();
		$issuers = $this->mollie->issuers()->all();

 		return view('home.index', compact('methods', 'issuers', 'cursus', 'plans', 'modules', 'reviews'));
	}

	public function webinar()
	{
		return redirect('https://www.facebook.com/groups/LiveOnlineTraining/');	
	}

	public function postLogin(Request $request)
	{
		$user = $this->user->where('email',$request->email)->first();
		if($user) {
			auth()->login($user);
			if(auth()->check() === true) {
				event(new UserHasLoggedIn($user));
				if($user->role == 1) {
					return redirect()->intended('/jandje');
				} else {
					return redirect()->intended('/member');
				}
			}
			return redirect('/login')->withErrors('We konden je helaas niet inloggen, neem aub contact met ons op.');
		} else {
			return redirect('/login')->withErrors('Geen gebruiker gevonden, neem aub contact met ons op.');
		}
	}

	public function redirect()
	{
		// dd($request->all());
		return Socialite::driver('facebook')->redirect();
	}

	public function fbCb(SocialAccountService $service, Request $request)
	{
		// dd($request->all());
		// dd(Socialite::driver('facebook')->user());
		$socialite_user = Socialite::driver('facebook')->user();

		$check = $service->checkUser($socialite_user);
		if($check === false) {
			return redirect('/login')->withErrors('We hebben geen gebruiker in ons systeem gevonden');
		} else {
			$user = $service->getUser($socialite_user);
			$usr = $this->user
			->find($user->id);
			auth()->login($usr);
			if(auth()->check() === true) {
				event(new UserHasLoggedIn($usr));
				if($usr->role == 1) {
					return redirect()->intended('/jandje');
				} else {
					return redirect()->intended('/member');
				}
			}
			return redirect('/login')->withErrors('Er is wat fout gegaan, neem aub contact met ons op.');
		}
	}

	public function getRegisterExistingUser()
	{
		return view('auth.register');
	}

	public function postRegisterExistingUser(Request $request)
	{
		$user = $this->user->where('email',$request->email)->first();
		if($user === null)
		{
			$account = new $this->socialaccount([
                'provider_user_id' => $request->fb_u_id,
                'provider' => 'facebook'
            ]);
			$user = $this->user->create([
				'first_name'	=> $request->voornaam,
				'last_name'		=> $request->achternaam,
				'email'			=> $request->email,
				'profile_image' => $request->picture_url,
				'cover'			=> $request->cover,
				'active'		=> 1,
				'role'			=> 3,
				'plan_id'		=> 3,
				'password' 		=> bcrypt('digitus2016'),
			]);
			$account->user()->associate($user);
            $account->save();
		}

		// Create company details
		if(!$user->company) {
			$company = $this->company->create([
				'validated' => 0,
				'validate_request' => 0,
				'user_id' => $user->id,
			]);
			// attach company details to user
			$user->company()->save($company);
		}

		// Maak affiliate account voor de user
		if(!$user->affiliate) {
			$a = substr(strtolower($user->first_name),0,3);
			$b = substr(strtolower($user->last_name),0,3);

			$dag = Carbon::now('Europe/Amsterdam')->format('jnyG');

			$code = ($a . $b . $dag);
			$affiliate = $this->affiliate->create([
				'code'		=> $code,
				'user_id'	=> $user->id
			]);
			$affiliate->save();
		}

		return redirect('/login')->with('message','Je kunt nu inloggen');
		// return Socialite::with('facebook')->scopes(['email'])->redirect();
	}

	public function store(KoopCommunityRequest $request)
	{
		// initiate Mollie payment
		$plan = $this->plan->where('id',$request->plan_id)->first();
		$user = $this->user->where('email',$request->email)->first();
		$cursus = $this->cursus->where('id',$plan->cursus_id)->first();

		if($request->hasCookie('aid'))
		{
			$cookie = $request->cookie('aid');
		} else {
			$cookie = null;
		}

		if($user === null)
		{
			$account = new $this->socialaccount([
                'provider_user_id' => $request->fb_u_id,
                'provider' => 'facebook'
            ]);
			$user = $this->user->create([
				'first_name'	=> $request->voornaam,
				'last_name'		=> $request->achternaam,
				'email'			=> $request->email,
				'profile_image' => $request->picture_url,
				'cover'			=> $request->cover,
				'active'		=> 0,
				'password' 		=> bcrypt('digitus2016'),
			]);
			$account->user()->associate($user);
            $account->save();
		}
		if($user) {
			// maak order_id
			$dag = Carbon::now('Europe/Amsterdam')->format('jnyGis');
			$order_id = $dag.'-'.$user->id;

			if($request->land != 'NL') {
				$bedrag = $plan->price;
			} else {
				$bedrag = $plan->price * 1.21;
			}

			if($user->mollie_customer_id === null) {
				// Maak mollie customer aan
				$customer = $this->mollie->customers()->create([
					'name'=>$request->voornaam.' '.$request->achternaam,
					'email'=>$request->email,
				]);

				// Update user met mollie customer ID
				$user->update(['mollie_customer_id' => $customer->id]);
			} else {
				// Get mollie customer (id)
				$customer = $this->mollie->customers()->get($user->mollie_customer_id);
			}

			if($user->company) {
				$user->company->update([
					'company_name' => $request->bedrijfsnaam,
					'business_email' => $request->zakelijk_email,
					'title' => $request->aanhef,
					'address' => $request->adres,
					'postal' => $request->postcode,
					'mobile_phone' => $request->mobielnummer,
					'phone' => $request->telefoonnummer,
					'recidence' => $request->woonplaats,
					'country_code' => $request->land,
					'country_full' => $request->land_full,
					'btw' => $request->btwnummer,
				]);
				$company = $user->company;
			} else {
				// Create company details
				$company = $this->company->create([
					'validated' => 0,
					'validate_request' => 0,
					'company_name' => $request->bedrijfsnaam,
					'business_email' => $request->zakelijk_email,
					'title' => $request->aanhef,
					'address' => $request->adres,
					'postal' => $request->postcode,
					'mobile_phone' => $request->mobielnummer,
					'phone' => $request->telefoonnummer,
					'recidence' => $request->woonplaats,
					'country_code' => $request->land,
					'country_full' => $request->land_full,
					'btw' => $request->btwnummer,
					'user_id' => $user->id,
				]);
				// attach company details to user
				$user->company()->save($company);
			}

			// Maak betaling aan Mollie klaar
			$payment = $this->mollie->customersPayments()->withParentId($user->mollie_customer_id)->create(
				[
					'amount'=> $bedrag,
					'issuer'=> $request->issuer,
					'description'=> $plan->pay_description,
					'redirectUrl'=> route('base.show', array('order_id='.$order_id)),
					'webhookUrl'=> route('webhook'),
					'method'=> $request->betaalmethode,
					'recurringType'=> 'first',
					'customerId'=>$customer->id,
					'metadata'=>
						array(
							'order_id'=>$order_id, 
							'cursus_id'=>$cursus->id, 
							'plan_id'=>$plan->id, 
							'user_id'=>$user->id, 
							'aid'=>$cookie, 
							'company_id'=>$company->id, 
							'bedrag'=>$bedrag,
							'fb_u_id' => $request->fb_u_id,
						),
				]
			);

			$order = new $this->order([
				'transaction_id' => $payment->id,
				'type' => $payment->recurringType,
				'status' => $payment->status,
				'price' => $bedrag,
				'description' => $plan->pay_description,
				'paymentmethod' => $request->betaalmethode,
				'order_id' => $order_id,
				'user_id' => $user->id,
				'plan_id' => $plan->id,
				'cursus_id' => $cursus->id,
			]);
			$order->save();

			$user->update(['plan_id'=>$plan->id]);

			return redirect($payment->getPaymentUrl());
		} else {
			return redirect()->route('base.index');
		}

	}

	public function show()
	{
		if(isset($_GET['order_id'])) {
			$order_id = $_GET['order_id'];
		} else {
			$order_id = null;
		}

		if($order_id != null) {

			// Get the order
			$order = $this->order->where('order_id',$order_id)->first();
			$order->update(['payment_date' => Carbon::now('Europe/Amsterdam')]);

			// Get the user that belongs to the order
			$user = $this->user->find($order->user_id);

			// get the payment at Mollie
			$payment = $this->mollie->payments()->get($order->transaction_id);

			if($payment->isPaid()) {
				// betaling is succesvol voldaan
				// Update order status
				$order->update(['status'=>$payment->status]);

				$sitetitel = 'Bedankt voor je bestelling!';
				$titel = 'Betaling succesvol ontvangen!';
				$message = 'We hebben je betaling succesvol ontvangen! Bekijk je inbox, we hebben je account gegevens toegestuurd! Je kan gelijk aan de slag met de cursus';
				$icon = '<i class="fa fa-check-circle" style="color:green;font-size:17em;"></i>';

				$fb_groep = config('app.fb_group');

				return view('home.succes', compact('sitetitel', 'titel', 'message', 'icon', 'fb_groep'));

			} elseif($payment->isOpen()) {
				// betaling staat nog open
				// Meld aan gebruiker dat betaling status nog open is
				// Gebruiker word op de hoogte gebracht van eventuele status verandering

				// Update order status
				$order->update(['status'=>$payment->status]);

				$sitetitel = 'Even wachten..';
				$titel = 'We moeten nog even wachten..';
				$message = 'We zijn je betaling nog aan het verwerken! Zodra we jouw betaling hebben ontvangen, ontvang je een email van ons met verdere instructies.';
				$icon = '<i class="fa fa-spinner fa-spin" style="color:#2daae1;font-size:17em;"></i>';

				return view('home.succes', compact('sitetitel', 'titel', 'message', 'icon'));
			} elseif($payment->isExpired()) {
				// betaling is verlopen
				// Geef gebruiker mogelijkheid om het alsnog te proberen
				// Of, keer terug naar homepage

				// Update order status
				$order->update(['status'=>$payment->status]);

				$sitetitel = 'Bestelling verlopen..';
				$titel = 'Je bestelling is verlopen..';
				$message = 'De maximale tijdsduur van je betaling is verlopen, daarom hebben we hem afgebroken.';
				$icon = '<i class="fa fa-clock-o" style="color:blue;font-size:17em;"></i>';

				return view('home.succes', compact('sitetitel', 'titel', 'message', 'icon'));
			} elseif($payment->isCancelled()) {
				// betaling is geannuleerd
				// Stel gebruiker de vraag waarom deze heeft geannulleerd

				// Update order status
				$order->update(['status'=>$payment->status]);

				$sitetitel = 'Bestelling geannuleerd!';
				$titel = 'Je hebt je betaling geannuleerd!';
				$message = 'Je hebt je betaling geannuleerd, en we zouden graag willen weten wat de reden hiervan is. <a href="mailto:jeroen@digitusmarketing.nl?subject=Reden annulering">Klik hier</a> om ons een bericht te sturen.';
				$icon = '<i class="fa fa-times-circle" style="color:red;font-size:17em;"></i>';

				return view('home.succes', compact('sitetitel', 'titel', 'message', 'icon'));
			} else {
				// Show message that i don't know what happened

				// Update order status
				$order->update(['status'=>$payment->status]);

				$sitetitel = 'Ehm...';
				$titel = 'Ehm... Er is wat raars gebeurd..';
				$message = 'Het lijkt er op dat er wat mis is gegaan met je betaling, maar we kunnen niet achterhalen wat er mis is gegaan. Zou je contact met ons willen opnemen?';
				$icon = '<i class="fa fa-question-circle" style="font-size:17em;"></i>';

				return view('home.succes', compact('sitetitel', 'titel', 'message', 'icon'));
			}
		} else {
			$sitetitel = 'Ehm...';
			$titel = 'We hebben geen transactie in ons systeem gevonden.';
			$message = 'Het lijkt er op dat je niet via een betaling op deze pagina komt, of er is wat mis gegaan bij je betaling. Probeer het opnieuw of neem contact met ons op.';
			$icon = '<i class="fa fa-question-circle" style="font-size:17em;"></i>';

			return view('home.succes', compact('sitetitel', 'titel', 'message', 'icon'));
		}
	}

}