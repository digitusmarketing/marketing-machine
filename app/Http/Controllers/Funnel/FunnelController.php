<?php namespace App\Http\Controllers\Funnel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Access\Funnel\Funnel;
use App\Models\Access\Funnel\FunnelItem;

class FunnelController extends Controller {

	public function __construct(Funnel $funnel, FunnelItem $funnelItem)
	{
		$this->funnel = $funnel;
		$this->funnelitem = $funnelItem;
	}
	
	public function index($funnelSlug)
	{
		// get currently active funnel
		$funnel = $this->funnel->isActive();
		
		// show funnel information on welcome page
		return view('funnel.index', compact('funnel'));
	}

	public function post($funnelSlug, Request $request)
	{
		// get currently active funnel
		$funnel = $this->funnel->isActive();
		if($funnel->slug === $funnelSlug) {
			// post to currently active funnel
			// check if user has ontraport cookie
			// create or update ontraport credentials of user
			if($request->cookie('contact_id')) {
				// we hebben een bestaande ontraport user
				// even de user erbij zoeken en zijn gegevens updaten
			} else {
				// is dus geen bestaande ontraport user
				// we kijken of het email adres voorkomt in ontraport
				// en updaten dit contact
				// of, we maken een nieuw contact
			}
			// send to thanks
			return redirect()->route('funnel.thanks',$funnel->slug);
		} else {
			dd('er is wat fout gegaan.. Probeer het opnieuw');
		}
	}

	public function thanks($funnelSlug)
	{
		// get currently active funnel
		$funnel = $this->funnel->isActive();
		// show thankyou page for funnel
		return view('funnel.thanks', compact('funnel'));
	}

	public function show($funnelSlug, $funnelItemSlug)
	{
		// get currently active funnel
		$funnel = $this->funnel->isActive();
		if($funnel->slug === $funnelSlug) {
			// show funnel item based on ontraport credentials
			$funnelitem = $funnel->funnelitems->where('slug',$funnelItemSlug)->first();
			return view('funnel.funnelitem', compact('funnel','funnelitem'));
		} 
	}

}