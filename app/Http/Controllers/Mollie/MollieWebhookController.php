<?php namespace App\Http\Controllers\Mollie;

use App\User;
use App\Models\Plan;
use App\Models\Access\Order;
use App\Models\Access\Company;
use App\Models\Access\Affiliate\Affiliate;
use App\Models\Access\Affiliate\Sale;
use App\Http\Controllers\Controller;
use Log;
use Mollie;
use Mailgun;
use Carbon\Carbon;
use Hyperized\Wefact\Types\Debtor;
use Hyperized\Wefact\Types\Invoice;

class MollieWebhookController extends Controller {

	private $ontraport_app_api_key;
	private $ontraport_api_app_id;
	private $ontraport_api_endpoint;
	private $verify_ssl = true;

	public function __construct(Order $order, User $user, Mollie $mollie, Plan $plan, Company $company, Affiliate $affiliate, Sale $sale)
	{
		$this->order = $order;
		$this->user = $user;
		$this->mollie = Mollie::api();
		$this->plan = $plan;
		$this->company = $company;
		$this->affiliate = $affiliate;
		$this->sale = $sale;
		$this->ontraport_app_api_key = 'EVEyd10HM6BwmkD';
		$this->ontraport_api_app_id = '2_110325_o39ngLEhZ';
		$this->ontraport_api_endpoint = 'https://api.ontraport.com/1';
	}
	
	public function webhook()
	{
		// Initiate Mollie webhook

		// Pak transactionReference uit Mollie ping
		$data = array();
		$data['id'] = $_POST['id'];

		// Fetch Payment at Mollie
		$payment = $this->mollie->payments()->get($data['id']);

		Log::info('webhook called!');
		Log::info($payment->id.' '.$payment->status);

		/*
		|----------------------------------------------------------------------
		| CHECK IF PAYMENT IS NOT NEW
		|----------------------------------------------------------------------
		*/
		if($payment->status === 'paid') {
			Log::info('paid');

			// Get the order if there is any
			$order = $this->order->where('transaction_id',$data['id'])->first();

			// Het gaat om een eerste betaling
			if($order) {


				// Get the user
				$user = $this->user->find($order->user_id);

				// Update user info
				$user->update([
					'mollie_mandate_id'=>$payment->mandateId, 
					'mollie_subscription_id'=>$payment->subscriptionId,
					'role'=>2,
					'active'=>1,
					'plan_id'=>$payment->metadata->plan_id,
				]);

				// Maak affiliate account voor de user
				if($user->affiliate) {
					Log::info('User: '.$user->id.' heeft een affiliate met id: '.$user->affiliate->id);
				} else {
					$a = substr(strtolower($user->first_name),0,3);
					$b = substr(strtolower($user->last_name),0,3);

					$dag = Carbon::now('Europe/Amsterdam')->format('jnyG');

					$code = ($a . $b . $dag);
					$affiliate = $this->affiliate->create([
						'code'		=> $code,
						'user_id'	=> $user->id
					]);
					$affiliate->save();
					Log::info('User: '.$user->id.' heeft een affiliate gekregen met id: '.$affiliate->id);
				}

				// Get the user again
				$user = $this->user->find($order->user_id);

				// Update the order status & payment date
				$order->update(['status'=>$payment->status,'payment_date' => Carbon::now('Europe/Amsterdam')]);

				// Get selected plan
				$plan = $this->plan->where('id',$payment->metadata->plan_id)->first();

				// get company credentials
				if($user->company->country_code != 'NL') {
					$btw = $user->company->btw;
					$bedrag = $plan->price;
				} else {
					$btw = '';
					$bedrag = $plan->price * 1.21;
				}

				if(!$order->wefact_invoice_code) {
					// findOrCreate debitor wefact
					$input = array(
						'CompanyName'	=> $user->company->company_name,
						'Sex'			=> $user->company->title,
						'Initials'		=> $user->first_name,
						'SurName'		=> $user->last_name,
						'Address'		=> $user->company->address,
						'ZipCode'		=> $user->company->postal,
						'City'			=> $user->company->recidence,
						'Country'		=> $user->company->country_code,
						'InvoiceCountry'=> $user->company->country_code,
						'EmailAddress'	=> $user->company->business_email,
						'Mailings'		=> 'no',
						'PhoneNumber'	=> $user->company->phone,
						'MobileNumber'	=> $user->company->mobile_phone,
						'InvoiceSex'	=> '',
						'InvoiceMethod'	=> '0',
						'TaxNumber'		=> $btw,
						'Taxable'		=> 'auto',
					);
					$wefactDebitor = $this->createOrFindDebitor($input, $user->company->business_email);
					
					// update user with wefact_debtor_id
					$user->update(['wefact_debtor_id'=>$wefactDebitor['debtor']['DebtorCode']]);

					// create new invoice to debitor
					$wefactInvoice = $this->createInvoice($user->wefact_debtor_id, $user, $plan, $order->order_id);

					// update order status with wefact_invoice_code
					$order->update(['wefact_invoice_code'=>$wefactInvoice['invoice']['InvoiceCode'], 'wefact'=>1]);
				}

				// Check if payment has affiliate
				if(isset($payment->metadata->aid)) {
					$aid = $payment->metadata->aid;
				} else {
					$aid = null;
				}

				if($aid != null) {
					Log::info('Webhook AID: '.$aid);
					$affiliate = $this->affiliate->where('code',$aid)->first();
					Log::alert('Eerste keer affiliate pakken');
					if($affiliate) {
						Log::alert($affiliate);
						Log::alert(count($affiliate->sales));
						Log::info('AffiliateId: '.$affiliate->id);

						if($affiliate->user->plan_id == 3 && $affiliate->user->role == 3) {
							$max_sales = config('affiliate.super.max_sales');
						} else {
							$max_sales = config('affiliate.normal.max_sales');
						}
						if(count($affiliate->sales) < $max_sales) {
							$sale = $this->sale->create([
								'affiliate_id'	=> $affiliate->id,
								'order_id'		=> $order->id,
							]);
							$sale->save();

							Log::info('Sale Id: '.$sale->id. ' AffiliateId: '.$affiliate->id);
						} else {
							Log::info('Affiliate: '.$affiliate->id.' heeft meer dan de toegestane sales en krijgt er dus geen sale bij');
						}

						// Fire event Affiliate nieuw abonnement
						// Met korting op de factuur
						// event(new AffiliateSale($affiliate->user, $user))
						$affiliate = $this->affiliate->where('code',$aid)->first();
						if($affiliate->user->plan_id == 3 && $affiliate->user->role == 3) {
							Log::info('Affiliate: '.$affiliate->id.' heeft een gratis plan, en moet dus geen subscription krijgen');
						} else {
							Log::info('Affiliate opnieuw gepakt:');
							Log::info($affiliate);
							Log::info(count($affiliate->sales));
							$a_user = $affiliate->user;
							$subscriptionId = $a_user->mollie_subscription_id;

							$a_order = $a_user->latestOrder();
							$transactionreference = $a_order->transaction_id;

							$affiliate_payment = $this->mollie->payments()->get($transactionreference);
							$last_payment_date = new Carbon($affiliate_payment->paidDatetime);

							if($a_user->plan_id == '1') {
								$affiliate_plan = $this->plan->where('id',$a_user->plan_id)->first();
								$new_start_date = $last_payment_date->addMonth()->format('Y-m-d');
							} elseif($a_user->plan_id == '2') {
								$affiliate_plan = $this->plan->where('id',$a_user->plan_id)->first();
								$new_start_date = $last_payment_date->addYear()->format('Y-m-d');
							} else {
								// $oud_bedrag = '0.00';
							}

							$oud_bedrag = $affiliate_plan->price;
							Log::info('Oud bedrag: '.$oud_bedrag);
							$korting_bedrag = number_format($affiliate_plan->price * config('affiliate.normal.commissie'), 2, '.', ',');
							Log::info('Korting bedrag: '.$korting_bedrag);
							$count = count($affiliate->sales);
							Log::info('Affiliate: '.$affiliate->id. ' heeft: '.count($affiliate->sales).' affiliate sales');

							$new_bedrag = number_format($oud_bedrag - ($korting_bedrag * $count), 2, '.', ',');
							Log::info('New bedrag: '.$new_bedrag);

							if($a_user->company->country_code != 'NL') {
								$btw = $a_user->company->btw;
								$bedrag = $new_bedrag;
							} else {
								$btw = '';
								$bedrag = number_format($new_bedrag * 1.21, 2, '.', ',');
							}
							Log::info('Bedrag: '.$bedrag);

							// stop oude abonnement
							if($a_user->mollie_subscription_id) {
								$this->mollie->customersSubscriptions()->withParentId($a_user->mollie_customer_id)->cancel($a_user->mollie_subscription_id);
								Log::info('Affiliate: '.$a_user->affiliate->id.' heeft zijn/haar oude subscription stopgezet.');
							} else {
								Log::info('User: '.$a_user->id.' had geen subscription bij mollie');
							}

						
							if($bedrag > '1.00') {
								// start nieuwe abonnement
								$subscription = $this->mollie->customersSubscriptions()->withParentId($a_user->mollie_customer_id)->create([
									'amount' => $bedrag,
									'times'	=> null,
									'interval' => $affiliate_plan->interval.' months',
									'description' => $affiliate_plan->pay_description,
									'startDate' => $new_start_date,
									'webhookUrl' => route('webhook.subscriptions'),
								]);
								$a_user->update(['mollie_subscription_id'=>$subscription->id]);
								Log::info('Affiliate: '.$a_user->affiliate->id.' heeft een nieuw subscription gestart');
							} else {
								Log::info('Het nieuwe bedrag is minder dan een euro');
							}
						}

						// Logica voor emailnotificatie voor sale hier
						if($affiliate->user) {
							Log::info('Affiliate->user: '.$affiliate->user);
						} else {
							Log::info('Affiliate heeft geen user?');
						}
					} else {
						Log::info('Affiliate bestaat niet, afhaken!');
					}

					// Nog meer logica voor de affiliate sale
					// Een mail bijvoorbeeld of een melding o.i.d.
				}

				if(!$user->mollie_subscription_id) {
					Log::info('User: '.$user->id.' heeft geen subscription id');
					if($plan->id === 1) {
						// Start customer subscription
						$subscription = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->create([
							'amount' => $payment->metadata->bedrag,
							'times'	=> null,
							'interval' => $plan->interval.' months',
							'description' => $plan->pay_description,
							'startDate' => Carbon::now()->addMonth()->format('Y-m-d'),
							'webhookUrl' => route('webhook.subscriptions'),
						]);
					} else {
						$subscription = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->create([
							'amount' => $payment->metadata->bedrag,
							'times'	=> null,
							'interval' => $plan->interval.' months',
							'description' => $plan->pay_description,
							'startDate' => Carbon::now()->addYear()->format('Y-m-d'),
							'webhookUrl' => route('webhook.subscriptions'),
						]);
					}

					if($subscription) {
						Log::info($user->id. ' heeft een subscription id gekregen');
						// Update user mollie subscription id
						$user->update(['mollie_subscription_id'=>$subscription->id]);
						$user->save();

						// Ik vind dat de user nu een affiliate mag zijn, aangezien het abonnement is gestart
						if($user->company) {
							$user->company->update(['validate_request'=>1,'validated'=>1]);
							Log::info('User: '.$user->id.' heeft zijn/haar company geupdate: '.$user->company->id);
						}
						if($user->affiliate) {
							$user->affiliate->update(['validated'=>1, 'status'=>2]);
							Log::info('User: '.$user->id.' heeft zijn/haar affiliate geupdate: '.$user->affiliate->id);
						}
					}
				}
				$password = substr(md5(microtime()),rand(0,26),8);
				$user->update(['password'=>bcrypt($password)]);

				// Hier kunnen we er vanuit gaan dat alles goed is gegaan,
				// dus laten we een mailtje versturen met bevestiging van de betaling en info over inloggen
				// Tegelijkertijd sturen we de user door naar Ontraport
				$payment_date = new Carbon($order->payment_date);
				$payment_format = $payment_date->format('d-m-Y');
				$data = array(
					'first_name'=>$user->first_name,
					'last_name'=>$user->last_name,
					'email'=>$user->company->business_email,
					'password'=>$password,
					'factuurnummer'=>$order->wefact_invoice_code,
					'referentie'=>$order->order_id,
					'payment_date'=>$payment_format,
					'description'=>$order->description,
					'bedrag'=>$order->price,
				);
				Mailgun::send('emails.welcome_payment', $data, function ($message) use($data) {
					$message->to($data['email'])->subject('Welkom bij de community!');
				});
				Log::info('mail met payment info is verstuurd');

				// Ontraport
				$args2create = array(
					'objectID'	=> '0',
					'firstname'	=> $user->first_name,
					'email'		=> $user->company->business_email,
				);

				$response = $this->createOrUpdateObject($args2create);

				$args4getobject = array(
					'objectID'	=> '0',
					'condition'	=> "email='".$user->company->business_email."'",
				);

				$object = $this->getObject($args4getobject);

				$userid = $object['data'][0]['id'];
				
				$args4tag = array(
					'objectID'	=> '0',
					'add_list'	=> '24',
					'ids'		=> $userid,
					'performAll'	=> true,
					'sortDir'	=> 'asc',
					'searchNotes'	=> true
				);

				$addtag = $this->addObjectTag($args4tag);

				// Temporary
				Log::info('subscription: '.$plan->id.' started for user: '.$user->id);
				Log::info($order);
			} else {
				Log::info($payment->id.' bestaat niet? '.$payment->status);
			}
		} elseif($payment->isOpen()) {
			Log::info('open');
			// Get the order if there is any
			$order = $this->order->where('transaction_id',$data['id'])->first();

			// Het gaat om een eerste betaling
			if($order) {
				// Get the user
				$user = $this->user->find($order->user_id);

				// Update user info
				$user->update([
					'role'=>2,
					'plan_id'=>$payment->metadata->plan_id,
				]);

				// Maak affiliate account voor de user
				if($user->affiliate) {

				} else {
					$a = substr(strtolower($user->first_name),0,3);
					$b = substr(strtolower($user->last_name),0,3);

					$dag = Carbon::now('Europe/Amsterdam')->format('jnyG');

					$code = ($a . $b . $dag);
					$affiliate = $this->affiliate->create([
						'code'		=> $code,
						'user_id'	=> $user->id
					]);
					$affiliate->save();
				}

				// Update the order status & payment date
				$order->update(['status'=>$payment->status]);
			}
		} elseif($payment->isCancelled()) {
			Log::info('cancelled');
			// Get the order if there is any
			$order = $this->order->where('transaction_id',$data['id'])->first();

			// Het gaat om een eerste betaling
			if($order) {
				// Get the user
				$user = $this->user->find($order->user_id);

				// Update user info
				$user->update([
					'role'=>2,
					'plan_id'=>$payment->metadata->plan_id,
				]);

				// Maak affiliate account voor de user
				if($user->affiliate) {

				} else {
					$a = substr(strtolower($user->first_name),0,3);
					$b = substr(strtolower($user->last_name),0,3);

					$dag = Carbon::now('Europe/Amsterdam')->format('jnyG');

					$code = ($a . $b . $dag);
					$affiliate = $this->affiliate->create([
						'code'		=> $code,
						'user_id'	=> $user->id
					]);
					$affiliate->save();
				}

				// Update the order status & payment date
				$order->update(['status'=>$payment->status]);
			}
		} elseif($payment->isExpired()) {
			Log::info('expired');
			// Get the order if there is any
			$order = $this->order->where('transaction_id',$data['id'])->first();

			// Het gaat om een eerste betaling
			if($order) {
				// Get the user
				$user = $this->user->find($order->user_id);

				// Update user info
				$user->update([
					'role'=>2,
					'plan_id'=>$payment->metadata->plan_id,
				]);

				// Maak affiliate account voor de user
				if($user->affiliate) {

				} else {
					$a = substr(strtolower($user->first_name),0,3);
					$b = substr(strtolower($user->last_name),0,3);

					$dag = Carbon::now('Europe/Amsterdam')->format('jnyG');

					$code = ($a . $b . $dag);
					$affiliate = $this->affiliate->create([
						'code'		=> $code,
						'user_id'	=> $user->id
					]);
					$affiliate->save();
				}

				// Update the order status & payment date
				$order->update(['status'=>$payment->status]);
			}
		} elseif($payment->isRefunded()) {
			Log::info('refunded');
			// Get the order if there is any
			$order = $this->order->where('transaction_id',$data['id'])->first();

			// Het gaat om een eerste betaling
			if($order) {
				// // Get the user
				$user = $this->user->find($order->user_id);

				// // Update user info
				// $user->update([
				// 	'mollie_mandate_id'=>$payment->mandateId, 
				// 	'mollie_subscription_id'=>$payment->subscriptionId,
				// 	'role'=>2,
				// 	'active'=>1,
				// 	'plan_id'=>$payment->metadata->plan_id,
				// ]);

				// Update the order status & payment date
				$order->update(['status'=>$payment->status]);

				// Get selected plan
				$plan = $this->plan->where('id',$payment->metadata->plan_id)->first();

				// get company credentials
				if($user->company->country_code != 'NL') {
					$btw = $user->company->btw;
					$bedrag = $plan->price;
				} else {
					$btw = '';
					$bedrag = $plan->price * 1.21;
				}

				// findOrCreate debitor wefact
				$input = array(
					'CompanyName'	=> $user->company->company_name,
					'Sex'			=> $user->company->title,
					'Initials'		=> $user->first_name,
					'SurName'		=> $user->last_name,
					'Address'		=> $user->company->address,
					'ZipCode'		=> $user->company->postal,
					'City'			=> $user->company->recidence,
					'Country'		=> $user->company->country_code,
					'InvoiceCountry'=> $user->company->country_code,
					'EmailAddress'	=> $user->company->business_email,
					'Mailings'		=> 'no',
					'PhoneNumber'	=> $user->company->phone,
					'MobileNumber'	=> $user->company->mobile_phone,
					'InvoiceSex'	=> '',
					'InvoiceMethod'	=> '0',
					'TaxNumber'		=> $btw,
					'Taxable'		=> 'auto',
				);
				$wefactDebitor = $this->createOrFindDebitor($input, $user->company->business_email);
				
				// update user with wefact_debtor_id
				$user->update(['wefact_debtor_id'=>$wefactDebitor['debtor']['DebtorCode']]);

				// maak order_id
				$dag = Carbon::now('Europe/Amsterdam')->format('jnyGis');
				$new_order_id = $dag.'-'.$user->id;

				// create new refund invoice to debitor
				// $wefactInvoice = $this->createInvoice($user->wefact_debtor_id, $user, $plan, $order->order_id);
				$wefactRefundInvoice = $this->refundInvoice($order->wefact_invoice_code);

				if($wefactRefundInvoice['status'] != 'success') {
					// Factuur is al gecrediteerd
				} else {
					// create new order for refund invoice to debitor
					$neworder = new $this->order([
						'transaction_id' => $payment->id,
						'type' => 'credit',
						'status' => $payment->status,
						'price' => '-'.$bedrag,
						'description' => 'Annulering: '.$plan->pay_description,
						'paymentmethod' => 'bankoverschrijving',
						'order_id' => $new_order_id,
						'user_id' => $user->id,
						'plan_id' => $plan->id,
						'cursus_id' => '1',
					]);
					$neworder->save();
					// update order status with wefact_invoice_code
					$neworder->update(['wefact_invoice_code'=>$wefactRefundInvoice['invoice']['InvoiceCode'], 'wefact'=>1]);
				}
			
			}			
		} else {
			// Log::error('dit is geen normale betaling');
			// $payment = $this->mollie->payments()->get($data['id']);
			Log::error($payment->status.' .. err .. '.$payment->id);
		}

	}

	public function createOrFindDebitor($input, $email){
		$newdebtor = new Debtor();
		$list = $newdebtor->list(['searchat'	=> 'EmailAddress', 'searchfor'=>$email]);

		if($list['totalresults'] == '0'){
			$debtor = $newdebtor->add($input);
		} else {
			$debtorcode = array('DebtorCode'=>$list['debtors']['0']['DebtorCode']);
			$input = $debtorcode + $input;
			$debtor = $newdebtor->edit($input);
		}
		return $debtor;
	}

	public function createInvoice($debtorcode, $user, $plan, $orderId){
		// Wanneer de gebruiker pakket nr 1 'basis' heeft gekocht
		$newinvoice = new Invoice();
		if($user->company->country_code != 'NL'){
			$invoiceParameters = array(
				'DebtorCode'	=> $debtorcode,
				'VatShift'		=> 'yes',
				'Status'		=> '4',
				'ReferenceNumber'	=> $orderId,
				'InvoiceLines'	=> array(
					array(
					'Number'		=> 1,
					'Description'	=> $plan->pay_description,
					'PriceExcl'		=> $plan->price,
					'TaxPercentage'	=> 0,
					)
				)
			);
		} else {
			$invoiceParameters = array(
				'DebtorCode'	=> $debtorcode,
				'VatShift'		=> 'no',
				'Status'		=> '4',
				'ReferenceNumber'	=> $orderId,
				'InvoiceLines'	=> array(
					array(
					'Number'		=> 1,
					'Description'	=> $plan->pay_description,
					'PriceExcl'		=> $plan->price,
					'TaxPercentage'	=> 21,
					)
				)
			);
		}
		$invoice = $newinvoice->add($invoiceParameters);

		return $invoice;
	}

	public function refundInvoice($wefactInvoiceCode){
		// Wanneer de gebruiker pakket nr 1 'basis' heeft gekocht
		$newinvoice = new Invoice();
		$invoiceParameters = array(
			'InvoiceCode'=>$wefactInvoiceCode,
		);
		$invoice = $newinvoice->credit($invoiceParameters);

		return $invoice;
	}

	public function createOrUpdateObject($args = array())
	{
		// Create or Merge an Object
		// Required objectID
		$method = 'objects/saveorupdate';
		$http_verb = 'post';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function getObject($args = array())
	{
		$method = 'objects';
		$http_verb = 'get';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function putObject($args = array())
	{
		$method = 'objects';
		$http_verb = 'put';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function addObjectTag($args = array())
	{
		// This will add an Object tag
		// Required objectID
		// Required add_list // array of tag ID's comma delimited
		$method = 'objects/tag';
		$http_verb = 'put';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function sendRequest($http_verb, $method, $args=array(), $timeout=10)
    {
        $url = $this->ontraport_api_endpoint.'/'.$method;

        $json_data = json_encode($args, JSON_FORCE_OBJECT);

        if (function_exists('curl_init') && function_exists('curl_setopt')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/vnd.api+json',
                'Content-Type: application/vnd.api+json','Api-Appid: '.$this->ontraport_api_app_id, 'Api-Key: '.$this->ontraport_app_api_key]);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

            // dd($ch);
            switch($http_verb) {
                case 'post':
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;

                case 'get':
                    $query = http_build_query($args);
                    curl_setopt($ch, CURLOPT_URL, $url.'?'.$query);
                    break;

                case 'delete':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    break;

                case 'patch':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;

                case 'put':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;
            }
            // dd($ch);
            $result = curl_exec($ch);
            // dd($result);
            curl_close($ch);
        } else {
            throw new \Exception("cURL support is required, but can't be found.");
        }

        return $result ? json_decode($result, true) : false;
    }

}