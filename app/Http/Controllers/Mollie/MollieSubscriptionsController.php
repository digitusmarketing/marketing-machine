<?php namespace App\Http\Controllers\Mollie;

use App\User;
use App\Models\Plan;
use App\Models\Access\Order;
use App\Models\Access\Company;
use App\Http\Controllers\Controller;
use Log;
use Mollie;
use Mailgun;
use Carbon\Carbon;
use Hyperized\Wefact\Types\Debtor;
use Hyperized\Wefact\Types\Invoice;

class MollieSubscriptionsController extends Controller {

	public function __construct(Order $order, User $user, Mollie $mollie, Plan $plan, Company $company)
	{
		$this->order = $order;
		$this->user = $user;
		$this->mollie = Mollie::api();
		$this->plan = $plan;
		$this->company = $company;
	}
	
	public function webhook()
	{
		Log::info('mollie subscriptions webhook called!');
		// Initiate Mollie webhook

		// Pak transactionReference uit Mollie ping
		$data = array();
		$data['id'] = $_POST['id'];

		// Fetch Payment at Mollie
		$payment = $this->mollie->payments()->get($data['id']);
		Log::info($payment->id. ' ' .$payment->status. ' ' .$payment->subscriptionId);

		// Get the user
		$user = $this->user->where('mollie_subscription_id',$payment->subscriptionId)->first();

		if($payment->status === 'paid' && $user) {
			Log::info('paid');

			// Get the plan
			if($user) {
				$plan = $this->plan->where('id',$user->plan_id)->first();
			} else {
				$plan = $this->plan->find(1);
			}

			// Create order_id
			$dag = Carbon::now('Europe/Amsterdam')->format('jnyGis');
			$order_id = $dag.'-'.$user->id;

			// Create the order
			$order = new $this->order([
				'transaction_id' => $payment->id,
				'type' => 'recurring',
				'status' => $payment->status,
				'price' => $payment->amount,
				'description' => $plan->pay_description,
				'paymentmethod' => $payment->method,
				'order_id' => $order_id,
				'user_id' => $user->id,
				'plan_id' => $plan->id,
				'cursus_id' => '1',
				'payment_date'=>Carbon::now(),
			]);
			$order->save();

			// create new invoice to debitor
			$wefactInvoice = $this->createInvoice($user->wefact_debtor_id, $user, $plan, $order->order_id);

			// update order status with wefact_invoice_code
			$order->update(['wefact_invoice_code'=>$wefactInvoice['invoice']['InvoiceCode'], 'wefact'=>1]);

			if($user->company->business_email) {

				$payment_date = new Carbon($order->payment_date);
				$payment_format = $payment_date->format('d-m-Y');
				// Send mail to user about invoice that is ready
				$data = array(
					'first_name'=>$user->first_name,
					'last_name'=>$user->last_name,
					'email'=>$user->company->business_email,
					'factuurnummer'=>$order->wefact_invoice_code,
					'referentie'=>$order->order_id,
					'payment_date'=>$payment_format,
					'next_payment'=>Carbon::now()->addMonth()->format('d-m-Y'),
					'description'=>$order->description,
					'bedrag'=>$order->price,
					);
				Mailgun::send('emails.subscription_payment', $data, function ($message) use($data) {
					$message->to($data['email'])->subject('Socialpreneurs.io betalingsbevestiging');
				});
				Log::info('mail met subscription info is verstuurd');
			}

			

			Log::info('Er is een nieuwe factuur aangemaakt voor user: '.$user->id. ' Met orderId: '.$order->order_id);
		} elseif($payment->isCancelled()) {
			Log::info('cancelled');
			// Get the order if there is any
			$order = $this->order->where('transaction_id',$data['id'])->first();

			// Het gaat om een eerste betaling
			if($order) {
				// Get the user
				$user = $this->user->find($order->user_id);

				// Update the order status & payment date
				$order->update(['status'=>$payment->status]);

				// Cancel subscription
				$subscriptions = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->all();
				foreach($subscriptions as $subscription) {
					$this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->cancel($subscription->id);
					$user->update(['mollie_subscription_id'=>'']);
				}

				Log::info('Cancelled!  '.$user->id.' heeft zijn subscription gestopt: '.$subsription->id);
			}
		} elseif($payment->isExpired()) {
			Log::info('expired');
			// Get the order if there is any
			$order = $this->order->where('transaction_id',$data['id'])->first();

			// Het gaat om een eerste betaling
			if($order) {
				// Update the order status & payment date
				$order->update(['status'=>$payment->status]);

				// Cancel subscription
				$subscriptions = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->all();
				foreach($subscriptions as $subscription) {
					$this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->cancel($subscription->id);
					$user->update(['mollie_subscription_id'=>'']);
				}

				Log::info('Expired!  '.$user->id.' heeft zijn subscription gestopt: '.$subsription->id);
			}
		} elseif($payment->isRefunded()) {
			Log::info('refunded');
			// Get the order if there is any
			$order = $this->order->where('transaction_id',$data['id'])->first();

			// Het gaat om een eerste betaling
			if($order) {
				// // Get the user
				$user = $this->user->find($order->user_id);

				// // Update user info
				// $user->update([
				// 	'mollie_mandate_id'=>$payment->mandateId, 
				// 	'mollie_subscription_id'=>$payment->subscriptionId,
				// 	'role'=>2,
				// 	'active'=>1,
				// 	'plan_id'=>$payment->metadata->plan_id,
				// ]);

				// Update the order status & payment date
				$order->update(['status'=>$payment->status]);

				// Get selected plan
				$plan = $this->plan->where('id',$user->plan_id)->first();

				// // get company credentials
				// $company = $this->company->find($user->company_id);

				if($user->company->country_code != 'NL') {
					$btw = $user->company->btw;
					$bedrag = $plan->price;
				} else {
					$btw = '';
					$bedrag = $plan->price * 1.21;
				}

				// findOrCreate debitor wefact
				$input = array(
					'CompanyName'	=> $user->company->company_name,
					'Sex'			=> $user->company->title,
					'Initials'		=> $user->first_name,
					'SurName'		=> $user->last_name,
					'Address'		=> $user->company->address,
					'ZipCode'		=> $user->company->postal,
					'City'			=> $user->company->recidence,
					'Country'		=> $user->company->country_code,
					'InvoiceCountry'=> $user->company->country_code,
					'EmailAddress'	=> $user->company->business_email,
					'Mailings'		=> 'no',
					'PhoneNumber'	=> $user->company->phone,
					'MobileNumber'	=> $user->company->mobile_phone,
					'InvoiceSex'	=> '',
					'InvoiceMethod'	=> '0',
					'TaxNumber'		=> $btw,
					'Taxable'		=> 'auto',
				);
				$wefactDebitor = $this->createOrFindDebitor($input, $user->company->business_email);
				
				// update user with wefact_debtor_id
				$user->update(['wefact_debtor_id'=>$wefactDebitor['debtor']['DebtorCode']]);

				// maak order_id
				$dag = Carbon::now('Europe/Amsterdam')->format('jnyGis');
				$new_order_id = $dag.'-'.$user->id;

				// create new refund invoice to debitor
				// $wefactInvoice = $this->createInvoice($user->wefact_debtor_id, $user, $plan, $order->order_id);
				$wefactRefundInvoice = $this->refundInvoice($order->wefact_invoice_code);

				if($wefactRefundInvoice['status'] != 'success') {
					// Factuur is al gecrediteerd
				} else {
					// create new order for refund invoice to debitor
					$neworder = new $this->order([
						'transaction_id' => $payment->id,
						'type' => 'credit',
						'status' => $payment->status,
						'price' => '-'.$bedrag,
						'description' => 'Annulering: '.$plan->pay_description,
						'paymentmethod' => 'bankoverschrijving',
						'order_id' => $new_order_id,
						'user_id' => $user->id,
						'plan_id' => $plan->id,
						'cursus_id' => '1',
					]);
					$neworder->save();
					// update order status with wefact_invoice_code
					$neworder->update(['wefact_invoice_code'=>$wefactRefundInvoice['invoice']['InvoiceCode'], 'wefact'=>1]);
				}


				// Cancel subscription
				$subscriptions = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->all();
				foreach($subscriptions as $subscription) {
					$this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->cancel($subscription->id);
					$user->update(['mollie_subscription_id'=>'']);
				}

				Log::info('Refunded!  '.$user->id.' heeft zijn subscription gestopt: '.$subscription->id);
			
			}			
		} else {
			Log::error($payment->id.' : '.$payment->status);
			// Log::error('dit is geen normale betaling');
			// $payment = $this->mollie->payments()->get($data['id']);
			$user = $this->user->where('mollie_subscription_id',$payment->subscriptionId)->first();
			$plan = $this->plan->find($user->plan_id);

			if($user->company->country_code != 'NL') {
				$bedrag = $plan->price;
			} else {
				$bedrag = $plan->price * 1.21;
			}

			// Maak betaling aan Mollie klaar
			$payment = $this->mollie->customersPayments()->withParentId($user->mollie_customer_id)->create(
				[
					'amount'=> $bedrag,
					'description'=> $plan->pay_description,
					'recurringType'=> 'recurring',
					'webhookUrl' => route('webhook.subscriptions'),
				]
			);
			// maak order_id
			$dag = Carbon::now('Europe/Amsterdam')->format('jnyGis');
			$order_id = $dag.'-'.$user->id;
			$order = new $this->order([
				'transaction_id' => $payment->id,
				'type' => $payment->recurringType,
				'status' => $payment->status,
				'price' => $bedrag,
				'description' => $plan->pay_description,
				'paymentmethod' => 'recurring',
				'order_id' => $order_id,
				'user_id' => $user->id,
				'plan_id' => $plan->id,
				'cursus_id' => '1',
			]);
			$order->save();
			Log::info('Order: '.$order->id.' en payment: '.$payment->id.'.. status: '.$payment->status);
		}

		// Check of het een affiliate is
		if($user->orders->first()->sale) {

			$affiliate = $user->orders->first()->sale->affiliate;
			$count = count($affiliate->sales);
			$a = 0;
			$i = 0;
			Log::info('Hij zegt dat Affiliate: '.$affiliate->id.', '.$count.' sales heeft..');
			foreach($affiliate->sales as $sale) {
				if($sale->order->user->active == '1') {
					$a++;
					Log::info('Active sale user: '.$sale->order->user->id);
				} else {
					$i++;
					Log::info('Inactive sale user: '.$sale->order->user->id);
				}
			}
			Log::error('Active = '.$a);
			Log::error('Inactive = '.$i);


			if($affiliate->user->company->country_code != 'NL') {
				$sale = $this->plan->find(1)->price;
				$btw = '1.00';
			} else {
				$sale = $this->plan->find(1)->price * 1.21;
				$btw = '1.21';
			}

			// // Define prices
			// $affiliate_sale_price = number_format($this->plan->find(1)->price * config('affiliate.normal.commissie') * $btw, 2, '.', ',');
			// $no_sales = number_format($sale - ($affiliate_sale_price * 0), 2, '.', ',');
			// $one_sale = number_format($sale - ($affiliate_sale_price * 1), 2, '.', ',');
			// $two_sales = number_format($sale - ($affiliate_sale_price * 2), 2, '.', ',');
			// $three_sales = number_format($sale - ($affiliate_sale_price * 3), 2, '.', ',');
			// $wat_te_betalen = number_format($sale - ($affiliate_sale_price * $affiliate->countActiveSalesUsers()), 2, '.', ',');
			// Log::alert('wat te betalen: '.$wat_te_betalen);
			// Log::info('Affiliate sale price: '.$affiliate_sale_price);
			// Log::info('no sales: '.$no_sales);
			// Log::info('one sale: '.$one_sale);
			// Log::info('two sales: '.$two_sales);
			// Log::info('three sales: '.$three_sales);

			// // doe een check of de user->affiliate->sales weg zijn
			// $customer_payments = $this->mollie->customersPayments()->withParentId($affiliate->user->mollie_customer_id)->all();
			// $current_subscriptions = $this->mollie->customersSubscriptions()->withParentId($affiliate->user->mollie_customer_id)->all();
			// $last_payed = $this->mollie->payments()->get($customer_payments[0]->id);
			// $last_subscription = $this->mollie->customersSubscriptions()->withParentId($affiliate->user->mollie_customer_id)->get($current_subscriptions[0]->id);
			// Log::error('Last payed: '.$last_payed->id.' '.$last_payed->amount);
			// Log::error('Last subscription: '.$last_subscription->id.' '.$last_subscription->amount);

			// $een_na_laatste_betaling = $this->mollie->payments()->get($customer_payments[1]->id);
			// $een_na_laatste_subscription = $this->mollie->customersSubscriptions()->withParentId($affiliate->user->mollie_customer_id)->get($current_subscriptions[1]->id);
			// Log::error('Daarvoor betaald: '.$een_na_laatste_betaling->id.' '.$een_na_laatste_betaling->amount);
			// Log::error('Daarvoor subscription: '.$een_na_laatste_subscription->id.' '.$een_na_laatste_subscription->amount);
			// if($last_payed->amount == $no_sales) {
			// 	// affiliate heeft geen sales
			// 	Log::info('blijkbaar geen sale');
			// } elseif($last_payed->amount == $one_sale) {
			// 	// affiliate heeft een sale
			// 	Log::info('blijkbaar een sale');
			// } elseif($last_payed->amount == $two_sales) {
			// 	// affiliate heeft twee sales
			// 	Log::info('blijkbaar twee sales');
			// } elseif($last_payed->amount == $three_sales) {
			// 	// affiliate heeft 3 sales
			// 	Log::info('blijkbaar 3 sales');
			// } else {
			// 	Log::info('blijkbaar klopt helemaal niks');
			// }
			
											
		}

	}

	public function createOrFindDebitor($input, $email){
		$newdebtor = new Debtor();
		$list = $newdebtor->list(['searchat'	=> 'EmailAddress', 'searchfor'=>$email]);

		if($list['totalresults'] == '0'){
			$debtor = $newdebtor->add($input);
		} else {
			$debtorcode = array('DebtorCode'=>$list['debtors']['0']['DebtorCode']);
			$input = $debtorcode + $input;
			$debtor = $newdebtor->edit($input);
		}
		return $debtor;
	}

	public function createInvoice($debtorcode, $user, $plan, $orderId){
		// Wanneer de gebruiker pakket nr 1 'basis' heeft gekocht
		$newinvoice = new Invoice();
		if($user->company->country_code != 'NL'){
			$invoiceParameters = array(
				'DebtorCode'	=> $debtorcode,
				'VatShift'		=> 'yes',
				'Status'		=> '4',
				'ReferenceNumber'	=> $orderId,
				'InvoiceLines'	=> array(
					array(
					'Number'		=> 1,
					'Description'	=> $plan->pay_description,
					'PriceExcl'		=> $plan->price,
					'TaxPercentage'	=> 0,
					)
				)
			);
		} else {
			$invoiceParameters = array(
				'DebtorCode'	=> $debtorcode,
				'VatShift'		=> 'no',
				'Status'		=> '4',
				'ReferenceNumber'	=> $orderId,
				'InvoiceLines'	=> array(
					array(
					'Number'		=> 1,
					'Description'	=> $plan->pay_description,
					'PriceExcl'		=> $plan->price,
					'TaxPercentage'	=> 21,
					)
				)
			);
		}
		$invoice = $newinvoice->add($invoiceParameters);

		return $invoice;
	}

	public function createAffiliateInvoice($debtorcode, $user, $plan, $orderId, $count, $sale_price, $sale_description){
		// Wanneer de gebruiker pakket nr 1 'basis' heeft gekocht
		$newinvoice = new Invoice();
		if($plan->type === 'maand') {
			$periodic = 'm';
			$startdate = Carbon::now()->format('Y-m-d');
		} else {
			$periodic = 'j';
			$startdate = Carbon::now()->format('Y-m-d');
		}
		if($user->company->country_code != 'NL'){
			$invoiceParameters = array(
				'DebtorCode'	=> $debtorcode,
				'VatShift'		=> 'yes',
				'Status'		=> '4',
				'ReferenceNumber'	=> $orderId,
				'InvoiceLines'	=> array(
					array(
					'Number'		=> 1,
					'Description'	=> $plan->pay_description,
					'PriceExcl'		=> $plan->price,
					'PeriodicType'	=> 'period',
					'Periods'		=> '1',
					'Periodic'		=> $periodic,
					'StartPeriod'	=> $startdate,
					'TaxPercentage'	=> 0,
					)
				)
			);
		} else {
			$invoiceParameters = array(
				'DebtorCode'	=> $debtorcode,
				'VatShift'		=> 'no',
				'Status'		=> '4',
				'ReferenceNumber'	=> $orderId,
				'InvoiceLines'	=> array(
					array(
					'Number'		=> 1,
					'Description'	=> $plan->pay_description,
					'PriceExcl'		=> $plan->price,
					'PeriodicType'	=> 'period',
					'Periods'		=> '1',
					'Periodic'		=> $periodic,
					'StartPeriod'	=> $startdate,
					'TaxPercentage'	=> 21,
					)
				)
			);
		}
		$invoice = $newinvoice->add($invoiceParameters);

		return $invoice;
	}

	public function refundInvoice($wefactInvoiceCode){
		// Wanneer de gebruiker pakket nr 1 'basis' heeft gekocht
		$newinvoice = new Invoice();
		$invoiceParameters = array(
			'InvoiceCode'=>$wefactInvoiceCode,
		);
		$invoice = $newinvoice->credit($invoiceParameters);

		return $invoice;
	}

}