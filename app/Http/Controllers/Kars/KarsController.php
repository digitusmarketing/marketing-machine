<?php namespace App\Http\Controllers\Kars;

use App\Http\Controllers\Controller;

use App\Http\Requests\KoopCommunityRequest;
use App\Models\Access\Affiliate\Affiliate;
use App\Models\Access\Company;
use App\Models\Access\Order;
use App\Models\Access\SocialAccount;
use App\Models\Cursus\Cursus;
use App\Models\Cursus\Module;
use App\Models\Cursus\Review;
use App\Models\Plan;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Mollie;
use Carbon\Carbon;

use Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KarsController extends Controller {

	public function __construct(Cursus $cursus, Module $module, Review $review, Plan $plan, User $user, Affiliate $affiliate, Order $order, Mollie $mollie, Company $company, SocialAccount $socialaccount, Guard $auth)
	{
		$this->cursus = $cursus;
		$this->module = $module;
		$this->review = $review;
		$this->user = $user;
		$this->affiliate = $affiliate;
		$this->plan = $plan;
		$this->order = $order;
		$this->mollie = Mollie::api();
		$this->company = $company;
		$this->socialaccount = $socialaccount;
		$this->auth = $auth;
	}
	
	public function index()
	{
		// Get Cursus
		$cursus = $this->cursus->find(1);
		// Get Plan(s)
		$plans = $this->plan->all()->take(2);
		// Get Modules
		$modules = $this->module->where('cursus_id',2)->get();
		// Get Reviews
		$reviews = $this->review->all();
		// Get Mollie payment Methods
		$methods = $this->mollie->methods()->all();
		// Get Mollie payment Issuers
		$issuers = $this->mollie->issuers()->all();

		// Show Cart Page
		return view('home.kars.index', compact('methods', 'issuers', 'cursus', 'plans', 'modules', 'reviews'));
	}

	public function store(KoopCommunityRequest $request)
	{
		$input = $request->all();
		Log::info('iemand wou betalen');
		Log::info($input);

		// dd($input);

		// Get platform name
		$name = 'Messenger Marketing Cursus';

		// Get user
		$user = $this->user->where('email',$request->email)->first();
		if($user) {
			dd("IS AL USER");
			return redirect()->route('kars.login');
		}

		if($user === null && $request->has('email'))
		{
			$user = $this->user->create([
				'first_name'	=> $request->voornaam,
				'last_name'		=> $request->achternaam,
				'email'			=> $request->email,
				'profile_image' => '#',
				'cover'			=> '#',
				'active'		=> 0,
				'password' 		=> bcrypt('socialpreneur2017'),
			]);
			$user->save();
		}

		if($user) {
			
			if($user->company) {
				$user->company->update([
					'company_name' => $request->bedrijfsnaam,
					'business_email' => $request->zakelijk_email,
					'title' => $request->aanhef,
					'address' => $request->adres,
					'postal' => $request->postcode,
					'mobile_phone' => $request->mobielnummer,
					'phone' => $request->telefoonnummer,
					'recidence' => $request->woonplaats,
					'country_code' => $request->land,
					'country_full' => $request->land_full,
					'btw' => $request->btwnummer,
				]);
				$company = $user->company;
			} else {
				// Create company details
				$company = $this->company->create([
					'validated' => 0,
					'validate_request' => 0,
					'company_name' => $request->bedrijfsnaam,
					'business_email' => $request->zakelijk_email,
					'title' => $request->aanhef,
					'address' => $request->adres,
					'postal' => $request->postcode,
					'mobile_phone' => $request->mobielnummer,
					'phone' => $request->telefoonnummer,
					'recidence' => $request->woonplaats,
					'country_code' => $request->land,
					'country_full' => $request->land_full,
					'btw' => $request->btwnummer,
					'user_id' => $user->id,
				]);
				// attach company details to user
				$user->company()->save($company);
			}

			// maak order_id
			$dag = Carbon::now('Europe/Amsterdam')->format('jnyGis');
			$order_id = $dag.'-'.$user->id;

			// Set right price based on country
			if($request->land != 'NL') {
				$bedrag = $request->bedrag;
			} else {
				$bedrag = $request->bedrag * 1.21;
			}

			if($user->mollie_customer_id === null) {
				// Maak mollie customer aan
				$customer = $this->mollie->customers()->create([
					'name'=>$request->voornaam.' '.$request->achternaam,
					'email'=>$request->email,
				]);

				// Update user met mollie customer ID
				$user->update(['mollie_customer_id' => $customer->id]);
			} else {
				// Get mollie customer (id)
				$customer = $this->mollie->customers()->get($user->mollie_customer_id);
			}

		
			// Initiate Mollie Payment
			$payment = $this->mollie->customersPayments()->withParentId($user->mollie_customer_id)->create(
				[
					'amount'=> $bedrag,
					'issuer'=> $request->issuer,
					'description'=> $name,
					'redirectUrl'=> route('base.show', array('order_id='.$order_id)),
					'webhookUrl'=> route('webhook'),
					'method'=> $request->betaalmethode,
					'recurringType'=> 'first',
					'customerId'=>$customer->id,
					'metadata'=>
						array(
							'order_id'=>$order_id, 
							'user_id'=>$user->id, 
							'company_id'=>$company->id, 
							'bedrag'=>$bedrag,
							'user_id'=>$user->id,
							'plan_id'=>'1',
						),
				]
			);

			$order = new $this->order([
				'transaction_id' => $payment->id,
				'type' => $payment->recurringType,
				'status' => $payment->status,
				'price' => $bedrag,
				'description' => $name,
				'paymentmethod' => $request->betaalmethode,
				'order_id' => $order_id,
				'user_id' => $user->id,
				'plan_id' => '1',
				'cursus_id' => '2',
			]);
			$order->save();

			$user->update(['plan_id'=>'1']);

			return redirect($payment->getPaymentUrl());
		} else {
			dd("ERROR ERROR ERROR");
		}
	}

	public function getLogin()
	{
		return view('home.kars.login');
	}

	public function postLogin(Request $request)
	{
		$email = $request->email;
		$password = $request->password;
		if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('member');
        }
        // Authentication failed...
        // Error.. Error..
        return redirect()->back()->withErrors(['credentials'=>'Het email of wachtwoord is onjuist.']);
	}

}