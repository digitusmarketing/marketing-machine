<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Mailgun;
use App\User;

class TestController extends Controller {
	
	public function sendMail()
	{
		$user = User::find(1);

		$data = array(
			'first_name'=>$user->first_name,
			'last_name'=>$user->last_name,
			'email'=>$user->email,
			);
		Mailgun::send('emails.welcome', $data, function ($message) use($data) {
			$message->to($data['email'])->subject('Testen met mailgun');
		});
	}

}