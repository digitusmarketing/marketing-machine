<?php namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;

class MessengerController extends Controller {
	
	public function webhook(Request $request)
	{
		Log::info($request->all());
	}

}