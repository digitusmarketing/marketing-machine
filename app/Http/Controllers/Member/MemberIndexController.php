<?php namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Cursus\Episode;
use App\User;
use Illuminate\Http\Request;
use Mailgun;

class MemberIndexController extends Controller {

	public function __construct(User $user, Episode $episode)
	{
		$this->user = $user;
		$this->episode = $episode;
	}
	
	public function index()
	{
		$user = auth()->user();
		$episodecount = count($this->episode->whereDoesntHave('user', function ($query) use ($user) {
			$query->where('id',$user->id);
		})->get());
		return view('member.index', compact('user', 'episodecount'));
	}

	public function me()
	{
		$user = auth()->user();
		return view('member.me.index', compact('user'));
	}

	public function meSettings()
	{
		$user = auth()->user();
		return view('member.me.settings', compact('user'));
	}

	public function reportBug(Request $request)
	{
		$bericht = $request->bericht;
		$user = $this->user->find($request->user_id);
		$data = array(
			'first_name'=>$user->first_name,
			'last_name'=>$user->last_name,
			'email'=>$user->email,
			'business_email'=>$user->company->business_email,
			'bericht' => $bericht,
		);
		Mailgun::send('emails.report-a-bug', $data, function ($message) use($data) {
			$message->to('jeroen@digitusmarketing.nl', 'Jeroen Venderbosch')->subject('Report a bug!');
			$message->from($data['email'], $data['first_name']);
		});

		return response()->json(['success'=>true],200);
	}
	
}