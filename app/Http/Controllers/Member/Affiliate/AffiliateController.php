<?php namespace App\Http\Controllers\Member\Affiliate;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Access\Affiliate\Affiliate;
use App\Models\Access\Affiliate\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Mailgun;

class AffiliateController extends Controller {

	public function __construct(User $user, Affiliate $affiliate, Sale $sale)
	{
		$this->user = $user;
		$this->affiliate = $affiliate;
		$this->sale = $sale;
	}
	
	public function index()
	{
		$user = auth()->user();
		return view('member.affiliate.index', compact('user'));
	}

	public function watchCount(Request $request)
	{
		$affiliate = $this->affiliate->where('code',$request->affiliate_code)->first();
		if($affiliate) {
			$affiliate->update(['watch_count'=>$affiliate->watch_count + 1]);
			$bool = true;
		} else {
			$bool = false;
		}

		Log::info('Affiliate: '.$affiliate->id.' viewcount: '.$affiliate->watch_count);

		return response()->json(['success'=>$bool], 200);
	}

	public function updateUserCompany(Request $request)
	{
		$user = $this->user->find($request->user_id);
		$user->company->update([
			'company_name'	=>	$request->bedrijfsnaam,
            'kvk'	=>	$request->kvk,
            'title'	=>	$request->aanhef,
            'business_email'	=>	$request->business_email,
            'address'	=>	$request->adres,
            'postal'	=>	$request->postcode,
            'mobile_phone'	=>	$request->mobielenummer,
            'phone'	=>	$request->telefoonnummer,
            'recidence'	=>	$request->woonplaats,
            'country_code'	=>	$request->land,
            'country_full'	=>	$request->land_full,
            'appellation'	=>	$request->tenaamstelling,
            'iban'	=>	$request->iban,
            'bic'	=>	$request->bic,
            'btw'	=>	$request->btw,
            'validate_request'	=> 1,
		]);
		Log::info('User: '.$user->id.' updates its company');

		$data = [
			'affiliate_id'=>$user->affiliate->id,
		];

		Mailgun::send('emails.affiliate.validation-request', $data, function ($message) use($data) {
			$message->to('info@digitusmarketing.nl')->subject('AffiliateId: '.$data['affiliate_id'].' heeft een validatie aanvraag gedaan.');
		});
		Log::info('AffiliateId: '.$user->affiliate->id.' mail met validation request verstuurd');
		$user->affiliate->update(['status'=>1]);

		return redirect()->back();
	}

	public function requestValidation(Request $request)
	{
		$user = $this->user->find($request->user_id);
		$user->affiliate->update([
			'status'=>1,
		]);

		$data = [
			'affiliate_id'=>$user->affiliate->id,
		];

		Mailgun::send('emails.affiliate-validation-request', $data, function ($message) use($data) {
			$message->to('info@digitusmarketing.nl')->subject('AffiliateId: '.$data['affiliate_id'].' heeft een validatie aanvraag gedaan.');
		});
		Log::info('User: '.$user->id.' requested validation for its affiliate account');

		return redirect()->back();
	}

}