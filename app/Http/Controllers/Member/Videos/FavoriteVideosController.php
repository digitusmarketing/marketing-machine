<?php namespace App\Http\Controllers\Member\Videos;

use App\Http\Controllers\Controller;
use App\Models\Cursus\Episode;
use App\Models\Cursus\Module;
use App\User;
use Illuminate\Http\Request;

class FavoriteVideosController extends Controller {

	public function __construct(Episode $episode, User $user, Module $module)
	{
		$this->episode = $episode;
		$this->user = $user;
		$this->module = $module;
	}
	
	public function index()
	{
		$user = auth()->user();
		$userepisodes = $user->favoriteEpisodes()->orderBy('module_id','ASC')->get();
		$episodes = $userepisodes->groupBy('module_id');
		// dd($episodes);
		$module = $this->module;
		return view('member.cursus.favorites.index', compact('episodes', 'module'));
	}

	public function update(Request $request)
	{
		$user = $this->user->findOrFail($request->user_id);
		$user->toggleEpisodeAsFavorite($request->episode_id);

		$epi = $user->episodes()->where('episode_id',$request->episode_id)->first();
		if($epi->pivot->favorite === 0) {
			$bool = false;
		} else {
			$bool = true;
		}
		return response()->json(['success'=>$bool], 200);
	}

}