<?php namespace App\Http\Controllers\Member\Videos;

use App\Http\Controllers\Controller;
use App\Models\Cursus\Episode;
use App\Models\Cursus\Module;
use App\User;
use Illuminate\Http\Request;

class LaterBekijkenController extends Controller {

	public function __construct(Episode $episode, User $user, Module $module)
	{
		$this->episode = $episode;
		$this->user = $user;
		$this->module = $module;
	}
	
	public function index()
	{
		$user = auth()->user();
		$userepisodes = $user->watchLaterEpisodes()->orderBy('module_id','ASC')->get();
		$episodes = $userepisodes->groupBy('module_id');
		$module = $this->module;
		return view('member.cursus.watch-later.index', compact('episodes', 'module'));
	}

	public function update(Request $request)
	{
		$user = $this->user->findOrFail($request->user_id);
		$user->toggleWatchLaterEpisode($request->episode_id);

		$epi = $user->episodes()->where('episode_id',$request->episode_id)->first();
		if($epi->pivot->watch_later === 0) {
			$bool = false;
		} else {
			$bool = true;
		}
		return response()->json(['success'=>$bool], 200);
	}

	public function removeAndRedirect(Request $request)
	{
		$user = $this->user->findOrFail($request->user_id);
		$user->toggleWatchLaterEpisode($request->episode_id);

		$episode = $this->episode->find($request->episode_id);

		$route = route('member.get.episode',[$episode->module->slug, $episode->id]);

		return response()->json(['success'=>true,'route'=>$route], 200);
	}

}