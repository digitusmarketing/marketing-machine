<?php namespace App\Http\Controllers\Member\Videos;

use App\Http\Controllers\Controller;
use App\Models\Cursus\Episode;
use App\User;
use Illuminate\Http\Request;

class CompletedVideosController extends Controller {

	public function __construct(Episode $episode, User $user)
	{
		$this->episode = $episode;
		$this->user = $user;
	}
	
	public function index()
	{

	}

	public function update(Request $request)
	{
		$user = $this->user->findOrFail($request->user_id);
		$user->markComplete($request->episode_id);
		// dd($this->episode);
		$epi = $user->episodes()->where('episode_id',$request->episode_id)->first();
		if($epi->pivot->completed === 0) {
			$bool = false;
		} else {
			$bool = true;
		}
		return response()->json(['success'=>$bool], 200);
	}

}