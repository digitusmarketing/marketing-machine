<?php namespace App\Http\Controllers\Member\Videos;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Cursus\Episode;

class AddCountVideoController extends Controller {

	public function __construct(Episode $episode, User $user)
	{
		$this->episode = $episode;
		$this->user = $user;
	}
	
	public function addCount($episodeId, $userId)
	{
		$user = $this->user->find($userId);
		$user->addEpisodeCount($episodeId);
	}

}