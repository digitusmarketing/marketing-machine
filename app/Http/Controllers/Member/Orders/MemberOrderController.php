<?php namespace App\Http\Controllers\Member\Orders;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use App\Models\Access\Order;
use App\User;
use Mollie;
use Hyperized\Wefact\Types\Invoice;
use Log;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MemberOrderController extends Controller {

	private $ontraport_app_api_key;
	private $ontraport_api_app_id;
	private $ontraport_api_endpoint;
	private $verify_ssl = true;

	public function __construct(User $user, Order $order, Mollie $mollie, Plan $plan)
	{
		$this->user = $user;
		$this->order = $order;
		$this->mollie = Mollie::api();
		$this->plan = $plan;
		$this->ontraport_app_api_key = 'EVEyd10HM6BwmkD';
		$this->ontraport_api_app_id = '2_110325_o39ngLEhZ';
		$this->ontraport_api_endpoint = 'https://api.ontraport.com/1';
	}
	
	public function index()
	{
		$user = auth()->user();
		return view('member.orders.index', compact('user'));
	}

	public function sendInvoice($factuurnummer){
		$invoice = new Invoice();
		$invoiceparameters = array(
			'InvoiceCode'=>$factuurnummer
		);
		$status = $invoice->sendByEmail($invoiceparameters);

		if($status['status'] === 'success') {
			$bool = true;
		} else {
			$bool = false;
		}

		return response()->json(['success'=>$bool], 200);
	}

	public function wefactSars()
	{
		$newdebtor = new \Hyperized\Wefact\Types\Debtor();
		$list = $newdebtor->list(['searchat'	=> 'EmailAddress', 'searchfor'=>'jeroen@digitusmarketing.nl']);
		dd($list);
	}

	public function delete($userId)
	{
		$user = $this->user->where('id',$userId)->first();

		$affiliate = null;
		
		if($user->hasOrderWithSale()) {
			Log::info('Sale: '.$user->orderWithSale()->sale->id.' wordt gedelete en affiliate: '.$user->orderWithSale()->sale->affiliate->id.' moet een nieuw subscription krijgen');
			$affiliate = $user->orderWithSale()->sale->affiliate;
			// $user->orderWithSale()->sale->delete();
		}
		if($affiliate) {
			if($affiliate->user->role != 3) {
				Log::error('affiliate delete via delete orders route!!!!!');
				$a_user = $affiliate->user;
				$subscriptionId = $a_user->mollie_subscription_id;

				$a_order = $a_user->latestOrder();
				$transactionreference = $a_order->transaction_id;

				$affiliate_payment = $this->mollie->payments()->get($transactionreference);
				$last_payment_date = new Carbon($affiliate_payment->paidDatetime);

				if($a_user->plan_id == '1') {
					$affiliate_plan = $this->plan->where('id',$a_user->plan_id)->first();
					$new_start_date = $last_payment_date->addDay()->format('Y-m-d');
				} elseif($a_user->plan_id == '2') {
					$affiliate_plan = $this->plan->where('id',$a_user->plan_id)->first();
					$new_start_date = $last_payment_date->addYear()->format('Y-m-d');
				} else {
					// $oud_bedrag = '0.00';
				}

				$oud_bedrag = $affiliate_plan->price;
				Log::info('Oud bedrag: '.$oud_bedrag);
				$korting_bedrag = number_format($affiliate_plan->price * config('affiliate.normal.commissie'), 2, '.', ',');
				Log::info('Korting bedrag: '.$korting_bedrag);
				$count = count($affiliate->sales);
				Log::info('Affiliate: '.$affiliate->id. ' heeft: '.count($affiliate->sales).' affiliate sales');

				$new_bedrag = number_format($oud_bedrag - ($korting_bedrag * $count), 2, '.', ',');
				Log::info('New bedrag: '.$new_bedrag);

				if($a_user->company->country_code != 'NL') {
					$btw = $a_user->company->btw;
					$bedrag = $new_bedrag;
				} else {
					$btw = '';
					$bedrag = number_format($new_bedrag * 1.21, 2, '.', ',');
				}
				Log::info('Bedrag: '.$bedrag);

				// stop oude abonnement
				if($a_user->mollie_subscription_id) {
					$this->mollie->customersSubscriptions()->withParentId($a_user->mollie_customer_id)->cancel($a_user->mollie_subscription_id);
					Log::info('Affiliate: '.$a_user->affiliate->id.' heeft zijn/haar oude subscription stopgezet.');
				} else {
					Log::info('User: '.$a_user->id.' had geen subscription bij mollie');
				}


				if($bedrag > '1.00') {
					// start nieuwe abonnement
					$subscription = $this->mollie->customersSubscriptions()->withParentId($a_user->mollie_customer_id)->create([
						'amount' => $bedrag,
						'times'	=> null,
						'interval' => '1 days',
						'description' => $a_order->description,
						'startDate' => $new_start_date,
						'webhookUrl' => route('webhook.subscriptions'),
					]);
					$a_user->update(['mollie_subscription_id'=>$subscription->id]);
					Log::info('Affiliate: '.$a_user->affiliate->id.' heeft een nieuw subscription gestart');
				} else {
					Log::info('Het nieuwe bedrag is minder dan een euro');
				}

				if($affiliate->user) {
					Log::info('Affiliate->user: '.$affiliate->user);
				} else {
					Log::info('Affiliate heeft geen user?');
				}
			}	
		}
		
		
		if($user) {
			$cancel = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->cancel($user->mollie_subscription_id);
			$user->update(['mollie_subscription_id'=>'', 'active'=>2]);
			Log::info('User: '.$user->id.' heeft zijn/haar subscription beeindigt op: '.Carbon::now().' en is op inactief gezet');
		}

		// Ontraport
		$args2create = array(
			'objectID'	=> '0',
			'firstname'	=> $user->first_name,
			'email'		=> $user->email,
		);

		$response = $this->createOrUpdateObject($args2create);

		$args4getobject = array(
			'objectID'	=> '0',
			'condition'	=> "email='".$user->email."'",
		);

		$object = $this->getObject($args4getobject);
		// dd($request);
		$userid = $object['data'][0]['id'];
		
		// dd($request['data'][0]['id']);
		// dd($userid);
		$args4tag = array(
			'objectID'	=> '0',
			'remove_list'	=> '24',
			'ids'		=> $userid,
			'performAll'	=> true,
			'sortDir'	=> 'asc',
			'searchNotes'	=> true
		);

		$addtag = $this->deleteObjectTag($args4tag);
		Log::info('User: '.$user->id.' is uitgeschreven van ontraport');

		return redirect()->back();
	}
	public function resumeSubscription(Request $request)
	{
		$user = $this->user->where('id',$request->user_id)->first();
		$plan = $this->plan->where('id',$request->plan_id)->first();
		if($user->company->country_code != 'NL') {
			$bedrag = $plan->price;
		} else {
			$bedrag = number_format($plan->price * 1.21, 2, '.', ',');
		}
		$last_subscription = new Carbon($user->getLastSubscription()->startDate);
		if($plan->id == '1') {
			$start_date = $last_subscription->addMonth()->format('Y-m-d');
		} else {
			$start_date = $last_subscription->addYear()->format('Y-m-d');
		}
		$subscription = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->create([
			'amount' => $bedrag,
			'times'	=> null,
			'interval' => $plan->interval.' months',
			'description' => $plan->pay_description,
			'startDate' => $start_date,
			'webhookUrl' => route('webhook.subscriptions'),
		]);
		$user->update(['mollie_subscription_id'=>$subscription->id, 'active'=>1, 'plan_id'=>$request->plan_id]);
		Log::info('User: '.$user->id.' heeft een nieuw subscription gestart');

		// Ontraport
		$args2create = array(
			'objectID'	=> '0',
			'firstname'	=> $user->first_name,
			'email'		=> $user->email,
		);

		$response = $this->createOrUpdateObject($args2create);

		$args4getobject = array(
			'objectID'	=> '0',
			'condition'	=> "email='".$user->email."'",
		);

		$object = $this->getObject($args4getobject);
		// dd($request);
		$userid = $object['data'][0]['id'];
		
		// dd($request['data'][0]['id']);
		// dd($userid);
		$args4tag = array(
			'objectID'	=> '0',
			'add_list'	=> '24',
			'ids'		=> $userid,
			'performAll'	=> true,
			'sortDir'	=> 'asc',
			'searchNotes'	=> true
		);

		$addtag = $this->addObjectTag($args4tag);
		Log::info('User: '.$user->id.' is weer ingeschreven op ontraport');

		return redirect()->back();
	}

	public function refund($orderId)
	{
		$order = $this->order->where('order_id',$orderId)->first();
		$wefactRefundInvoice = $this->refundInvoice('F0212');
		dd($wefactRefundInvoice);
	}

	public function refundInvoice($wefactInvoiceCode){
		// Wanneer de gebruiker pakket nr 1 'basis' heeft gekocht
		$newinvoice = new Invoice();
		$invoiceParameters = array(
			'InvoiceCode'=>$wefactInvoiceCode,
		);
		$invoice = $newinvoice->credit($invoiceParameters);

		return $invoice;
	}

	public function deleteAll()
	{
		$user = auth()->user();
		$subscriptions = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->all();
		foreach($subscriptions as $subscription) {
			$this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->cancel($subscription->id);
			print_r($this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->get($subscription->id));
			// dd($subscription->id);
		}
		$user->update(['mollie_subscription_id'=>'', 'active'=>0]);
		if($user->hasOrderWithSale()) {
			Log::info('Sale: '.$user->orderWithSale()->sale->id.' wordt gedelete en affiliate: '.$user->orderWithSale()->sale->affiliate->id.' moet een nieuw subscription krijgen');
			
		}
		if($user->hasOrderWithSale()) {
			$user->orderWithSale()->sale->delete();
		}
		Log::info('User: '.$user->id.' heeft zijn/haar subscription beeindigt op: '.Carbon::now(). ' en is op inactief gezet');
		print_r('successssssss!');
	}

	public function deleteEverything()
	{
		$customers = $this->mollie->customers()->all();
		foreach($customers as $customer) {
			$subscriptions = $this->mollie->customersSubscriptions()->withParentId($customer->id)->all();
			foreach($subscriptions as $subscription) {
				Log::info('Deleting '.$subscription->customerId. ' '.$subscription->id);
				$this->mollie->customersSubscriptions()->withParentId($subscription->customerId)->delete($subscription->id);
				$user = $this->user->where('mollie_customer_id',$subscription->customerId)->first();
				if($user) {
					$user->update(['mollie_subscription_id'=>'']);
					Log::info('UserId: '.$user->id.' heeft zijn/haar abonnement stopgezet');
				}
			}
		}
	}

	public function getAll()
	{
		$user = auth()->user();
		$subscriptions = $this->mollie->customersSubscriptions()->withParentId($user->mollie_customer_id)->all();
		dd($subscriptions);
	}

	public function allPayments()
	{
		$user = auth()->user();
		$payments = $this->mollie->customersPayments()->withParentId($user->mollie_customer_id)->all();
		dd($payments);
		foreach($payments as $payment) {
			$carbondate = new Carbon($payment->paidDatetime);


			// dd($payment);
			// print_r($carbondate.'<br>');
			// print_r($carbondate->format('Y-m-d').'<br>');
			// print_r($carbondate->addMonth()->format('Y-m-d').'<br>');
		}
		// dd($payments);
	}

	public function lastPayment()
	{
		$user = auth()->user();
		$order = $user->latestOrder();
		$transactionreference = $order->transaction_id;

		$payment = $this->mollie->payments()->get($transactionreference);
		dd($payment);
	}

	public function createOrUpdateObject($args = array())
	{
		// Create or Merge an Object
		// Required objectID
		$method = 'objects/saveorupdate';
		$http_verb = 'post';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function getObject($args = array())
	{
		$method = 'objects';
		$http_verb = 'get';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function putObject($args = array())
	{
		$method = 'objects';
		$http_verb = 'put';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function addObjectTag($args = array())
	{
		// This will add an Object tag
		// Required objectID
		// Required add_list // array of tag ID's comma delimited
		$method = 'objects/tag';
		$http_verb = 'put';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function deleteObjectTag($args = array())
	{
		// This will add an Object tag
		// Required objectID
		// Required add_list // array of tag ID's comma delimited
		$method = 'objects/tag';
		$http_verb = 'delete';

		return $this->sendRequest($http_verb, $method, $args);
	}

	public function sendRequest($http_verb, $method, $args=array(), $timeout=10)
    {
        $url = $this->ontraport_api_endpoint.'/'.$method;

        $json_data = json_encode($args, JSON_FORCE_OBJECT);

        if (function_exists('curl_init') && function_exists('curl_setopt')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/vnd.api+json',
                'Content-Type: application/vnd.api+json','Api-Appid: '.$this->ontraport_api_app_id, 'Api-Key: '.$this->ontraport_app_api_key]);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

            // dd($ch);
            switch($http_verb) {
                case 'post':
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;

                case 'get':
                    $query = http_build_query($args);
                    curl_setopt($ch, CURLOPT_URL, $url.'?'.$query);
                    break;

                case 'delete':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    break;

                case 'patch':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;

                case 'put':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;
            }
            // dd($ch);
            $result = curl_exec($ch);
            // dd($result);
            curl_close($ch);
        } else {
            throw new \Exception("cURL support is required, but can't be found.");
        }

        return $result ? json_decode($result, true) : false;
    }

}