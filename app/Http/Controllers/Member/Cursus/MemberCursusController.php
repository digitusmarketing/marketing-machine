<?php namespace App\Http\Controllers\Member\Cursus;

use App\Http\Controllers\Controller;
use App\Models\Cursus\Cursus;
use App\Models\Cursus\Module;
use App\Models\Cursus\Episode;

class MemberCursusController extends Controller {
	
	public function __construct(Cursus $cursus, Module $module, Episode $episode)
	{
		$this->cursus = $cursus;
		$this->module = $module;
		$this->episode = $episode;
	}

	public function getCursussen()
	{
		// $cursussen = $this->cursus->all();
		$cursussen = $this->cursus->find(1);
		return view('member.cursus.overview', compact('cursussen'));
	}

	public function getModules($cursusId)
	{
		$user = auth()->user();
		$cursus = $this->cursus->find($cursusId);
		$modules = $this->module->where('cursus_id',$cursusId)->get();
		// dd($module->episodes);
		// $cursus = $this->cursus->find(1);
		$modulestatus = $this->module->with(['episodes' => function ($episodeQuery) use ($user) {
		    // if episode has watchers with user and not completed, it means episode needs to be watched
		    $episodeQuery->whereHas('user', function ($userQuery) use ($user) {
		            $userQuery->where('id', '=', $user->id)->where('completed', false);
		        });
		}])->get();

		return view('member.cursus.index', compact('cursus', 'modules', 'modulestatus'));
	}

	public function getModuleEpisodes($cursusId, $moduleSlug)
	{
		$module = $this->module->where('slug',$moduleSlug)->first();
		$previous_module = $this->module->previousModule($module->id);
		$next_module = $this->module->nextModule($module->id);
		return view('member.cursus.module', compact('module','previous_module','next_module'));
	}

	public function showModuleEpisode($cursusId, $moduleSlug, $episodeId)
	{
		$module = $this->module->where('slug',$moduleSlug)->first();
		$episode = $this->episode->findOrFail($episodeId);
		$previous_episode = $this->episode->previousEpisode($episode->module_id, $episode->id);
		$next_episode = $this->episode->nextEpisode($episode->module_id, $episode->id);
		
		return view('member.cursus.episode', compact('module', 'episode', 'previous_episode', 'next_episode'));
	}

}