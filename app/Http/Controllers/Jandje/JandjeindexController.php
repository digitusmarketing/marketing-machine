<?php namespace App\Http\Controllers\Jandje;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Access\Order;
use Mailgun;
use Carbon\Carbon;

class JandjeIndexController extends Controller {

	public function __construct(User $user, Order $order)
	{
		$this->user = $user;
		$this->order = $order;
	}
	
	public function index()
	{
		$users = $this->user->all();

		$orders = $this->order->where('status','paid')->get();

		$last_month_revenue = 1;
		$current_month_revenue = 1;

		$filtered_last_month = $orders->filter(function($value, $key) {
			$last_now = Carbon::now()->subMonth();
			$last_month = $last_now->month;
			if($value->created_at->month == $last_month) {
				return $value;
			}
		});
		foreach($filtered_last_month as $filterlm) {
			$last_month_revenue += $filterlm->price;
		}
		$filtered_current_month = $orders->filter(function($value, $key) {
			$now = Carbon::now();
			$current_month = $now->month;
			if($value->created_at->month == $current_month) {
				return $value;
			}
		});
		foreach($filtered_current_month as $filtercm) {
			$current_month_revenue += $filtercm->price;
		}
		return view('jandje.index', compact('last_month_revenue','current_month_revenue','filtered_last_month', 'filtered_current_month'));
	}
}