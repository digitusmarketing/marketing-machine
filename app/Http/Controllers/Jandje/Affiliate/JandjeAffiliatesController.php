<?php namespace App\Http\Controllers\Jandje\Affiliate;

use App\Http\Controllers\Controller;
use App\Models\Access\Affiliate\Affiliate;
use Illuminate\Http\Request;
use Mailgun;

class JandjeAffiliatesController extends Controller {

	public function __construct(Affiliate $affiliate)
	{
		$this->affiliate = $affiliate;
	}
	
	public function index()
	{
		$affiliates = $this->affiliate->all();
		return view('jandje.affiliate.index', compact('affiliates'));
	}

	public function show($affiliateId)
	{
		$affiliate = $this->affiliate->find($affiliateId);
		// return view('jandje.affiliate.show', compact('affiliate'));
	}

	public function edit($affiliateId)
	{
		$affiliate = $this->affiliate->find($affiliateId);
		dd($affiliate);
	}

	public function update($affiliateId)
	{
		$affiliate = $this->affiliate->find($affiliateId);
		dd($affiliate);
	}

	public function showSales($affiliateId)
	{
		$affiliate = $this->affiliate->find($affiliateId);
		dd($affiliate->sales);	
	}

	public function sendJeroen()
	{
		$affiliate = $this->affiliate->find(1);
		$data = array(
			'first_name'=>$affiliate->user->first_name,
			'last_name'=>$affiliate->user->last_name,
			'business_email'=>'info@digitusmarketing.nl',
			'bericht'=>'hier komt dan dus een stukje tekst waarom de affiliate is afgekeurd',
		);
		Mailgun::send('emails.affiliate.decline', $data, function ($message) use($data) {
			$message->to($data['business_email'], $data['first_name'].' '.$data['last_name'])->subject('Je bent afgekeurd');
			$message->from('affiliates@socialpreneurs.io', 'Affiliates Socialpreneurs.io');
		});
		return 'jeej';
	}

	public function approve(Request $request)
	{
		// Get the affiliate
		$affiliate = $this->affiliate->find($request->affiliate_id);

		// mark affiliate as approved
		$affiliate->update(['status'=>2]);

		// send mail to affiliate about approving request
		$data = array(
			'first_name'=>$affiliate->user->first_name,
			'last_name'=>$affiliate->user->last_name,
			'business_email'=>$affiliate->user->company->business_email,
			'affiliate_link'=> route('base.index').'?a='.$affiliate->code,
		);
		Mailgun::send('emails.affiliate.approved', $data, function ($message) use($data) {
			$message->to($data['business_email'], $data['first_name'].' '.$data['last_name'])->subject('Je bent goedgekeurd!');
			$message->from('affiliates@socialpreneurs.io', 'Affiliates Socialpreneurs.io');
		});
		return redirect()->back();
	}

	public function decline(Request $request)
	{
		$affiliate = $this->affiliate->find($request->affiliate_id);
		// dd($affiliate);

		// mark affiliate as unapproved
		$affiliate->update(['status'=>0]);

		// send mail to affiliate about declining request
		$data = array(
			'first_name'=>$affiliate->user->first_name,
			'last_name'=>$affiliate->user->last_name,
			'business_email'=>$affiliate->user->company->business_email,
			'bericht'=>$request->decline_bericht,
		);
		Mailgun::send('emails.affiliate.decline', $data, function ($message) use($data) {
			$message->to($data['business_email'], $data['first_name'].' '.$data['last_name'])->subject('Je bent afgekeurd');
			$message->from('affiliates@socialpreneurs.io', 'Affiliates Socialpreneurs.io');
		});
		return redirect()->back();
	}

}