<?php namespace App\Http\Controllers\Jandje\Cursus\Episodes;

use App\Http\Controllers\Controller;
use App\Models\Cursus\Module;
use App\Models\Cursus\Episode;

class JandjeEpisodesController extends Controller {

	public function __construct(Episode $episode, Module $module)
	{
		$this->episode = $episode;
		$this->module = $module;
	}
	
	public function show($moduleId, $episodeId)
	{
		$episode = $this->episode->where(['module_id'=>$moduleId, 'id'=>$episodeId])->first();
		dd($episode);
	}

	public function edit($moduleId, $episodeId)
	{
		$episode = $this->episode->where(['module_id'=>$moduleId, 'id'=>$episodeId])->first();
		dd($episode);
	}

	public function update($moduleId, $episodeId)
	{
		$episode = $this->episode->where(['module_id'=>$moduleId, 'id'=>$episodeId])->first();
		dd($episode);
	}

}