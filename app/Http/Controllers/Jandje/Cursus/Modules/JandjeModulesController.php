<?php namespace App\Http\Controllers\Jandje\Cursus\Modules;

use App\Http\Controllers\Controller;
use App\Models\Cursus\Module;

class JandjeModulesController extends Controller {

	public function __construct(Module $module)
	{
		$this->module = $module;
	}

	public function index()
	{
		$modules = $this->module->all();
		dd($modules);
	}

	public function show($moduleId)
	{
		$module = $this->module->find($moduleId);
		dd($module->episodes);
	}

	public function edit($moduleId)
	{
		$module = $this->module->find($moduleId);
		dd($module);
	}

	public function update($moduleId)
	{
		$module = $this->module->find($moduleId);
		dd($module);
	}

}