<?php namespace App\Http\Controllers\Jandje\Member;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Access\Order;
use App\Models\Plan;
use Carbon\Carbon;
use Mollie;
use Log;

class JandjeMembersController extends Controller {

	public function __construct(User $user, Order $order, Plan $plan, Mollie $mollie)
	{
		$this->user = $user;
		$this->order = $order;
		$this->plan = $plan;
		$this->mollie = Mollie::api();
	}

	public function getAllOrders()
	{
		// $orders = $this->order->with('sale')->get();
		// $urders = $this->order->with('user')->get();
		// return response()->json([$orders->toArray(),$urders->toArray()]);
		$orders = $this->order->all();

		$last_month_revenue = 0;
		$current_month_revenue = 0;

		$de_array = [];

		$filtered_last_month = $orders->filter(function($value, $key) {
			$last_now = Carbon::now()->subMonth();
			$last_month = $last_now->month;
			if($value->created_at->month == $last_month) {
				return $value;
			}
		});
		$last_now = Carbon::now()->subMonth();
		$last_month = $last_now->month;
		$de_array['2017-0'.$last_month] = count($filtered_last_month);
		// dd(count($filtered_last_month));
		foreach($filtered_last_month as $filterlm) {
			$last_month_revenue += $filterlm->price;
		}
		$filtered_current_month = $orders->filter(function($value, $key) {
			$now = Carbon::now();
			$current_month = $now->month;
			if($value->created_at->month == $current_month) {
				return $value;
			}
		});
		$now = Carbon::now();
		$current_month = $now->month;
		$de_array['2017-0'.$current_month] = count($filtered_current_month);
		foreach($filtered_current_month as $filtercm) {
			$current_month_revenue += $filtercm->price;
		}

		return response()->json($de_array);
	}
	
	public function index()
	{
		return view('jandje.member.index');
	}

	public function newPayment($userId)
	{
		$user = $this->user->find($userId);
		$plan = $this->plan->find($user->plan_id);

		if($user->company->country_code != 'NL') {
			$bedrag = $plan->price;
		} else {
			$bedrag = $plan->price * 1.21;
		}

		// Maak betaling aan Mollie klaar
		$payment = $this->mollie->customersPayments()->withParentId($user->mollie_customer_id)->create(
			[
				'amount'=> $bedrag,
				'description'=> $plan->pay_description,
				'recurringType'=> 'recurring',
				'webhookUrl' => route('webhook.subscriptions'),
			]
		);
		// maak order_id
		$dag = Carbon::now('Europe/Amsterdam')->format('jnyGis');
		$order_id = $dag.'-'.$user->id;
		$order = new $this->order([
			'transaction_id' => $payment->id,
			'type' => $payment->recurringType,
			'status' => $payment->status,
			'price' => $bedrag,
			'description' => $plan->pay_description,
			'paymentmethod' => 'recurring',
			'order_id' => $order_id,
			'user_id' => $user->id,
			'plan_id' => $plan->id,
			'cursus_id' => '1',
		]);
		$order->save();
		Log::info('Order: '.$order->id.' en payment: '.$payment->id.'.. status: '.$payment->status);
		dd($payment);
	}

	public function getAll()
	{
		$users = $this->user->with('affiliate')->with('company')->with('affiliate.sales')->get();
		return response()->json($users->toArray());
	}

	public function show($memberId)
	{
		$member = $this->user->find($memberId);
		return view('jandje.member.show', compact('member'));
	}

	public function edit($memberId)
	{
		$member = $this->user->find($memberId);
		dd($member);
	}

	public function update($memberId)
	{
		$member = $this->user->find($memberId);
		dd($member);
	}

	public function loginAs($memberId)
	{
		$member = $this->user->find($memberId);
		auth()->login($member);
		return redirect()->route('member.index');
	}

	public function showOrders($memberId)
	{
		$member = $this->user->find($memberId);
		dd($member->orders);
	}

	public function showSubscriptions($memberId)
	{
		$member = $this->user->find($memberId);
		dd($member->getLastSubscription());
	}

}