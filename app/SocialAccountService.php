<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Models\Access\SocialAccount;

class SocialAccountService
{
    public function creatingUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();


        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                // dd($providerUser);
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'first_name' => $providerUser->user['first_name'],
                    'last_name' => $providerUser->user['last_name'],
                    'password' => bcrypt('digitus2016'),
                    'profile_image' => '#',
                    'role' => 3,
                    'plan_id' => 3,
                    'active' => 1,
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();


        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                // dd($providerUser);
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'first_name' => $providerUser->user['first_name'],
                    'last_name' => $providerUser->user['last_name'],
                    'password' => bcrypt('digitus2016'),
                    'profile_image' => '#',
                    'role' => 3,
                    'active' => 0,
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }

    public function checkUser(ProviderUser $providerUser)
    {
        // check if fb account has account on app
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if($account) {
            // return true if true,
            return true;
        } else {
            // return false if false   
            return false;
        }
    }

    public function getUser(ProviderUser $providerUser)
    {
        // return user object
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->user;
        } else {
            var_dump('FOUT!');
        }
    }

    public function createUser(ProviderUser $providerUser)
    {
        $account = new SocialAccount([
            'provider_user_id' => $providerUser->getId(),
            'provider' => 'facebook'
        ]);
        $user = User::whereEmail($providerUser->getEmail())->first();

        $account->user()->associate($user);
        $account->save();
    }
}