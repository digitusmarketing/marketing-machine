$(document).ready(function(){

	$(function(){
		$('section#call-to-action div.call-to-action-content h2').inview({
			'viewFactor': 1
		});
		$('section#call-to-action div.call-to-action-content button').inview({
			'viewFactor': 0.4
		});


		$('section#intro div.intro-content .logo').inview({
			'viewFactor': 1
		});
		$('section#intro div.intro-content h1').inview({
			'viewFactor': 0.7
		});


		$('section#focus div.focus-content h2').inview({
			'viewFactor': 0.3
		});
		$('section#focus div.focus-content h4').inview({
			'viewFactor': 0.3
		});


		$('section#voorwoord div.voorwoord-box').inview({
			'viewFactor': 0.3
		});


		$('section#voor-wie').inview({
			'viewFactor': 0.6
		});


		$('section#verwachting .verwachting-content h2').inview({
			'viewFactor': 0.7
		});
		$('section#verwachting .verwachting-content .verwachting-box ul li').inview({
			'viewFactor': 0.7
		});


		$('section#modules .modules-content h2').inview({
			'viewFactor': 0.7
		});
		$('section#modules .modules-content img').inview({
			'viewFactor': 0.7
		});
		$('section#modules .modules-content div.col-md-6').inview({
			'viewFactor': 0.7,
			'onEnter':function($object) {
				$object.each(function(index){
					$(this).delay(300*index).removeClass('slideOutLeft').addClass('slideInLeft');
				});
			}
		});


		$('section#bonus .besloten-content h2').inview({
			'viewFactor': 0.1
		});
		$('section#bonus .besloten-content h4').inview({
			'viewFactor': 0.3
		});
		$('section#bonus .bonus-content .bonus-info h4').inview({
			'viewFactor': 0.4
		});
		$('section#bonus .bonus-content .bonus-info ul li').inview({
			'viewFactor': 0.5
		});
		$('section#bonus .bonus-content .bonus-box').inview({
			'viewFactor': 0.1
		});
		$('section#bonus .bonus-content .bonus-box h4').inview({
			'viewFactor': 0.3
		});
		$('section#bonus .bonus-content .bonus-box p').inview({
			'viewFactor': 0.3
		});
		$('section#bonus .bonus-content .bonus-box ul li').inview({
			'viewFactor': 0.5
		});

		$('section#recensies .recensies-content h2').inview({
			'viewFactor': 0.3
		});
		$('section#recensies .recensies-content div').inview({
			'viewFactor': 0.5
		});

		$('section#video-recensies .video-recensies-content .recensie-content-video h2').inview({
			'viewFactor': 0.3
		});
		$('section#video-recensies .video-recensies-content .recensie-content-video div').inview({
			'viewFactor': 0.5
		});

		$('section#FAQ .faq-content h2').inview({
			'viewFactor': 0.3
		});
		$('section#FAQ .faq-content div.faq-box').inview({
			'viewFactor': 0.1
		});
		$('section#FAQ .faq-content div.faq-box div.question-content h4.question-title a span.holder').inview({
			'viewFactor': 0.1,
			'onEnter': function($object){
				setTimeout(function(){
					$object.removeClass('popOut').addClass('popIn');
				}, 400);
			},
			'onLeave': function($object){
				// $object.removeClass('popIn').addClass('popOut');
			}
		});

		$('section#investering .investering-content .investering-text h2').inview({
			'viewFactor': 0.3
		});
		$('section#investering .investering-content .investering-text p').inview({
			'viewFactor': 0.3
		});
		$('section#investering .investering-content .prijs-image .prijs-image-holder').inview({
			'viewFactor': 1,
			'onEnter':function($object) {
				$object.children('img').removeAttr('style');
				$object.addClass('rollIn');
			}
		});

		$('footer#footer .footer-logo img').inview();
		$('footer#footer .footer-content h2').inview();
		$('footer#footer .footer-content p').inview();

	});

});