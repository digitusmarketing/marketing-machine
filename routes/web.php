<?php



// $user = App\User::find(1);
// auth()->login($user);
// dd(auth()->check());

Auth::routes();

// Route::get('test-mail', ['uses'=>'TestController@sendMail']);

// Show Salespage
Route::get('', ['uses'=>'BaseController@index', 'as'=>'base.index', 'middleware'=>'affiliatetracking']);
// Route::get('/facebook-webinar', ['uses'=>'BaseController@webinar', 'as'=>'base.webinar', 'middleware'=>'affiliatetracking']);
// Route::get('/petra', function(){
// 	return redirect('https://www.socialpreneurs.io?a=petiul2121722');
// });

Route::get('/betalen', ['uses'=>'Kars\KarsController@index', 'as'=>'kars.index','middleware'=>'affiliatetracking']);
Route::post('/betalen', ['uses'=>'Kars\KarsController@store', 'as'=>'kars.store']);

Route::get('/kars',['uses'=>'Kars\KarsController@getLogin','as'=>'kars.get.login']);
Route::post('/kars',['uses'=>'Kars\KarsController@postLogin','as'=>'kars.post.login']);

// Route::get('/karsham/succes', ['uses'=>'Kars\KarsController@show', 'as'=>'kars.show']);

// Store credentials and initiate Payment process
Route::post('/', ['uses'=>'BaseController@store', 'as'=>'base.store']);

// blog group
Route::group(['prefix'=>'blog', 'namespace'=>'Blog'], function(){

	// Show blogposts
	Route::get('/', ['uses'=>'BlogController@index', 'as'=>'blog.index']);
	Route::get('/postslug', ['uses'=>'BlogController@show', 'as'=>'blog.show']);

});

// Bedankpagina
Route::get('/succes', ['uses'=>'BaseController@show', 'as'=>'base.show']);

// Mollie webhook
Route::post('/webhook', ['uses'=>'Mollie\MollieWebhookController@webhook', 'as'=>'webhook']);

// Mollie Subscriptions Webhook
Route::any('/webhook-subscriptions', ['uses'=>'Mollie\MollieSubscriptionsController@webhook', 'as'=>'webhook.subscriptions']);

// Facebook Messenger Webhook
Route::any('/messenger-webhook', ['uses'=>'Messenger\MessengerController@webhook', 'as'=>'messenger.webhook']);

////////
Route::post('login', ['uses'=>'BaseController@postLogin', 'as'=>'post.login']);
Route::get('redirect', ['uses'=>'BaseController@redirect', 'as'=>'redirect']);

// facebook callback
Route::get('/fb-cb', ['uses'=>'BaseController@fbCb', 'as'=>'fb.cb']);

Route::get('register-me', ['uses'=>'BaseController@getRegisterExistingUser', 'as'=>'get.register.existing.user']);
Route::post('register-me', ['uses'=>'BaseController@postRegisterExistingUser', 'as'=>'post.register.existing.user']);
Route::get('/post-naar-jeroen', ['uses'=>'Jandje\Affiliate\JandjeAffiliatesController@sendJeroen']);

/*
|----------------------------------------------------------------------
| Admin Section
|----------------------------------------------------------------------
*/

Route::group(['prefix'=>'jandje','namespace'=>'Jandje', 'middleware'=>'is.admin'], function() {

	// Overview all admin options
	Route::get('', ['uses'=>'JandjeIndexController@index', 'as'=>'jandje.index']);

	Route::group(['prefix'=>'members', 'namespace'=>'Member'], function(){

		// Lijst van members
		Route::get('/', ['uses'=>'JandjeMembersController@index', 'as'=>'jandje.members.index']);
		Route::get('/all', ['uses'=>'JandjeMembersController@getAll', 'as'=>'jandje.members.get.all']);

		// Show details of member
		Route::get('/{memberId}', ['uses'=>'JandjeMembersController@show', 'as'=>'jandje.member.show']);
		// Edit details of member
		Route::get('/{memberId}/edit', ['uses'=>'JandjeMembersController@edit', 'as'=>'jandje.member.edit']);
		// Update details of member
		Route::post('/{memberId}/update', ['uses'=>'JandjeMembersController@update', 'as'=>'jandje.member.update']);

		// Login as user
		Route::get('/{memberId}/login-as', ['uses'=>'JandjeMembersController@loginAs', 'as'=>'jandje.member.login.as']);

		// Lijst van Orders van member
		Route::get('/{memberid}/orders', ['uses'=>'JandjeMembersController@showOrders', 'as'=>'jandje.show.member.orders']);

		// Show user subscription(s)
		Route::get('/{memberid}/subscriptions', ['uses'=>'JandjeMembersController@showSubscriptions', 'as'=>'jandje.show.member.subscriptions']);

		// Initiate new payment for member
		Route::get('/{memberid}/new-payment', ['uses'=>'JandjeMembersController@newPayment', 'as'=>'jandje.member.new.payment']);


		
	});
	// show all orders ever
	Route::get('/orders', ['uses'=>'Member\JandjeMembersController@getAllOrders', 'as'=>'jandje.get.all.orders']);

	Route::group(['prefix'=>'affiliates', 'namespace'=>'Affiliate'], function(){

		// Lijst van alle affiliates
		Route::get('/', ['uses'=>'JandjeAffiliatesController@index', 'as'=>'jandje.affiliates.index']);

		// Show details of affiliate
		Route::get('/{affiliateId}', ['uses'=>'JandjeAffiliatesController@show', 'as'=>'jandje.affiliate.show']);
		// Edit details of affiliate
		Route::get('/{affiliateId}/edit', ['uses'=>'JandjeAffiliatesController@edit', 'as'=>'jandje.affiliate.edit']);
		// Update details of affiliate
		Route::post('/{affiliateId}/update', ['uses'=>'JandjeAffiliatesController@update', 'as'=>'jandje.affiliate.update']);

		// Show affiliate sales
		Route::get('/{affiliateId}/sales', ['uses'=>'JandjeAffiliatesController@showSales', 'as'=>'jandje.affiliate.show.sales']);

		// Approve affiliate request
		Route::post('/approve', ['uses'=>'JandjeAffiliatesController@approve', 'as'=>'jandje.affiliate.approve']);
		// Decline affiliate request
		Route::post('/decline', ['uses'=>'JandjeAffiliatesController@decline', 'as'=>'jandje.affiliate.decline']);

	});

	Route::group(['prefix'=>'cursus', 'namespace'=>'Cursus'], function() {

		// Show all cursussen
		Route::get('/', ['uses'=>'JandjeCursusController@index', 'as'=>'jandje.cursus.index']);

		// Create
		Route::get('/create', ['uses'=>'JandjeCursusController@create', 'as'=>'jandje.cursus.create']);

		// Store
		Route::post('/', ['uses'=>'JandjeCursusController@store', 'as'=>'jandje.cursus.store']);

		// Show specific cursus
		Route::get('/{cursusId}', ['uses'=>'JandjeCursusController@show', 'as'=>'jandje.cursus.show']);

		// Edit specific cursus
		Route::get('/{cursusId}/edit', ['uses'=>'JandjeCursusController@edit', 'as'=>'jandje.cursus.edit']);

		// Update specific cursus
		Route::post('/{cursusId}', ['uses'=>'JandjeCursusController@update', 'as'=>'jandje.cursus.update']);

		// Access modules of specific cursus
		Route::group(['prefix'=>'/{cursusId}/modules', 'namespace'=>'Modules'], function() {

			// Lijst van alle modules
			Route::get('/', ['uses'=>'JandjeModulesController@index', 'as'=>'jandje.module.index']);

			// Create
			Route::get('/create', ['uses'=>'JandjeModulesController@create', 'as'=>'jandje.module.create']);

			// Store
			Route::post('/', ['uses'=>'JandjeModulesController@store', 'as'=>'jandje.module.store']);

			// Show module met epidodes
			Route::get('/{moduleId}', ['uses'=>'JandjeModulesController@show', 'as'=>'jandje.module.show']);

			// Edit module
			Route::get('/{moduleId}/edit', ['uses'=>'JandjeModulesController@edit', 'as'=>'jandje.module.edit']);

			// Update module
			Route::post('/{modulesId}', ['uses'=>'JandjeModulesController@update', 'as'=>'jandje.module.update']);

		});

		Route::group(['prefix'=>'/{cursusId}/{modulesId}/episodes', 'namespace'=>'Episodes'], function(){

			// Show episodes of module
			Route::get('/', ['uses'=>'JandjeEpisodesController@index', 'as'=>'jandje.episode.index']);

			// Create
			Route::get('/create', ['uses'=>'JandjeEpisodesController@create', 'as'=>'jandje.episode.create']);

			// Store
			Route::post('/', ['uses'=>'JandjeEpisodesController@store', 'as'=>'jandje.episode.store']);

			// Show episode van module
			Route::get('/{episodeId}', ['uses'=>'JandjeEpisodesController@show', 'as'=>'jandje.episode.show']);

			// Edit episode van module
			Route::get('/{episodeId}/edit', ['uses'=>'JandjeEpisodesController@edit', 'as'=>'jandje.episode.edit']);

			// Update episode van module
			Route::post('/{episodeId}', ['uses'=>'JandjeEpisodesController@update', 'as'=>'jandje.episode.update']);

		});

	});

});

// Update affiliate link view
// Route::post('/my-affiliate-link', ['uses'=>'Member\Affiliate\AffiliateController@watchCount', 'as'=>'my.affiliate.link']);

Route::group(['prefix'=>'member', 'namespace'=>'Member', 'middleware'=>'is.member'], function(){

	// Member Dashboard
	Route::get('', ['uses'=>'MemberIndexController@index', 'as'=>'member.index']);

	// Report a bug
	Route::post('report-a-bug', ['uses'=>'MemberIndexController@reportBug', 'as'=>'report.bug']);

	// Member profile
	Route::get('/me', ['uses'=>'MemberIndexController@me', 'as'=>'member.profile']);

	// // Member profile settings
	// Route::get('/me/settings', ['uses'=>'MemberIndexController@meSettings', 'as'=>'member.settings']);

	Route::group(['prefix'=>'cursus', 'namespace'=>'Cursus', 'middleware'=>'is.active'], function() {

		// NIEUW
		// Show cursussen
		Route::get('/', ['uses'=>'MemberCursusController@getCursussen', 'as'=>'member.get.cursussen']);

		// EDIT
		// Show all modules for cursus
		Route::get('/{cursusid}', ['uses'=>'MemberCursusController@getModules', 'as'=>'member.get.all.modules']);

		// EDIT
		// Show all episodes
		Route::get('/{cursusid}/{moduleslug}', ['uses'=>'MemberCursusController@getModuleEpisodes', 'as'=>'member.get.module.episodes']);

		// EDIT
		Route::group(['prefix'=>'/{cursusid}/{moduleslug}/episodes'], function() {

			// Show episode
			Route::get('{episodeId}', ['uses'=>'MemberCursusController@showModuleEpisode', 'as'=>'member.get.episode']);

		});

	});

	Route::group(['prefix'=>'episode', 'namespace'=>'Videos', 'middleware'=>'is.active'], function() {

		Route::post('{episodeId}/{userId}', ['uses'=>'AddCountVideoController@addCount', 'as'=>'add.video.count']);

	});

	Route::group(['prefix'=>'episodes', 'namespace'=>'Videos', 'middleware'=>'is.active'], function() {

		// Overview of all completed videos
		Route::get('/completed', ['uses'=>'CompletedVideosController@index', 'as'=>'get.completed.videos']);

		// Mark video as completed or incomplete
		Route::post('/complete', ['uses'=>'CompletedVideosController@update', 'as'=>'set.completed.video']);

		// Overview of all videos marked with 'Watch later'
		Route::get('/later-bekijken', ['uses'=>'LaterBekijkenController@index', 'as'=>'get.watchlater.videos']);

		// Mark episode as watch-later or don't watch-later
		Route::post('/later-bekijken', ['uses'=>'LaterBekijkenController@update', 'as'=>'set.watchlater.video']);
		Route::post('/later-bekijken/remove', ['uses'=>'LaterBekijkenController@removeAndRedirect', 'as'=>'remove.watchlater.video']);

		// Overview of all videos marked as favorite
		Route::get('/favorites', ['uses'=>'FavoriteVideosController@index', 'as'=>'get.favorite.videos']);

		// Mark episode as favorite or no-favorite
		Route::post('/favorite-episode', ['uses'=>'FavoriteVideosController@update', 'as'=>'set.favorite.video']);

	});

	Route::group(['prefix'=>'transacties', 'namespace'=>'Orders'], function() {

		// List of orders
		Route::get('', ['uses'=>'MemberOrderController@index', 'as'=>'get.member.orders']);

		// test
		Route::get('/{orderId}', ['uses'=>'MemberOrderController@refund']);

		// Show specific order
		Route::post('/{invoiceCode}', ['uses'=>'MemberOrderController@sendInvoice', 'as'=>'send.member.invoice']);

	});

	Route::group(['namespace'=>'Orders'], function() {

		Route::delete('cancel-subscription/{userId}', ['uses'=>'MemberOrderController@delete', 'as'=>'delete.member.subscription']);
		Route::post('restart-subscription', ['uses'=>'MemberOrderController@resumeSubscription', 'as'=>'restart.member.subscription']);

		// Route::any('cancel-all', ['uses'=>'MemberOrderController@deleteAll', 'as'=>'delete.all.member.subscriptions']);
		// Route::any('delete-everything', ['uses'=>'MemberOrderController@deleteEverything', 'as'=>'delete.everything']);
		// Route::get('get-all', ['uses'=>'MemberOrderController@getAll', 'as'=>'get.all.member.subscriptions']);
		// Route::get('all-payments', ['uses'=>'MemberOrderController@allPayments', 'as'=>'get.all.member.payments']);
		// Route::get('last-payment', ['uses'=>'MemberOrderController@lastPayment', 'as'=>'get.last.member.payment']);

		// Route::get('wefact-sars', ['uses'=>'MemberOrderController@wefactSars', 'as'=>'wefact.sars']);

	});


	// Group for Affiliate
	Route::group(['prefix'=>'affiliate', 'namespace'=>'Affiliate'], function() {

		Route::get('/', function(){
			return redirect()->back();
		});

		// Show all affiliate sales
		// Route::get('/', ['uses'=>'AffiliateController@index', 'as'=>'member.affiliate.index']);

		// Set affiliate gegevens
		// Route::post('/company', ['uses'=>'AffiliateController@updateUserCompany', 'as'=>'member.company.update']);

	});

});

// Route::group(['namespace'=>'Funnel'], function(){
// 	// Aanmeld pagina funnel
// 	Route::get('{funnel_slug}', ['uses'=>'FunnelController@index', 'as'=>'funnel.index']);
// 	Route::post('{funnel_slug}', ['uses'=>'FunnelController@post', 'as'=>'funnel.post']);

// 	// Thankyou page funnel aanmeld
// 	Route::get('{funnel_slug}/bedankt', ['uses'=>'FunnelController@thanks', 'as'=>'funnel.thanks']);

// 	// Show funnel items
// 	Route::get('{funnel_slug}/{funnelitem_slug}', ['uses'=>'FunnelController@show', 'as'=>'funnel.show']);
// });