<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1928920830673076',
        'client_secret' => '7359dcc72844f04fd92b93ff707925c1',
        'redirect' => 'https://www.socialpreneurs.io/fb-cb',
        'page-token' => 'EAAP6r5a0218BABUQcjkB2jGwihXbTYOQhDKualZBZAZCrb3SQc4PPBAEa3uorhryBKUUrTKowoNzZCLv2B3XmHPewLfOpT8VyIWgktAZC2UmAXXLL7Cz1uIFZAZAegcJNFVKxHjfpiZBcWcW5zIKfNZC71PUicHyGgCMy1FWHAW27PQZDZD',
    ],

];
