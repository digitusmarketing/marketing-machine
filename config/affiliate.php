<?php

return [

	'normal'	=> [
		'max_sales' => '3',
		'commissie'	=> '0.30',
	],
	'super'		=> [
		'max_sales'	=> '9999999999',
		'commissie' => '0.20',
	],

];
