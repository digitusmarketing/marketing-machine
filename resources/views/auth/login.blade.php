@extends('layouts.auth-end')

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1928920830673076";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
<div class="lockscreen-wrapper animated flipInX">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
            <img src="{{ url(config('app.images.normal')) }}" alt="{{ config('app.name') }}" class="app-image" style="width:100%;">
        </div>
        <div class="clearfix"></div>
        <div class="p-t-25 p-b-25">&nbsp;</div>
        <div class="clearfix"></div>

        <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-4">
            <div id="fb-status" style="display:none;background:white;color:black;padding:15px;text-align:center;margin:15px 0;">
                <h3 id="status" style="color:black;"></h3>
                <div id="status-button"><fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button></div>
            </div>
            <div class="fetch open">
                {{-- <a class="btn btn-link" href="{{ url('/password/reset') }}">
                    Forgot Your Password?
                </a> --}}
                <button class="btn btn-login-screen btn-cons btn-fetch" id="fetch-facebook">Pak mijn Facebook gegevens</button>
            </div>
            <div class="fetched">
                {!! Form::open(array('url'=>'/login')) !!}
                    <div class="profile" style="align-items:flex-start;justify-content: center;width:100%;">
                        <div class="profile-wrapper">
                            <img width="69" height="69" data-src-retina="" data-src="" src="" alt="" class="profile-image">
                        </div>
                        <div class="profile-info">
                            <h2 class="user"><span class="first-name"></span> <span class="semi-bold"></span></h2>
                            <h4 class="user-email" style="display:flex;align-items:center;justify-content: space-between;width:100%;"><span class="label-for-span" style="margin-right:5px;">Email:</span> {!! Form::email('email', old('email'), array('class'=>'email semi-bold', 'style'=>'padding: 0 !important;margin: 0;background: transparent;border: 0 none;color: white;line-height: 1px;font-size: 18px;min-height: 22px;width:auto;min-width:240px;')) !!}</h4>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="profile-buttons">
                        <button id="fail-facebook" class="btn btn-login-screen btn-cons btn-fail"><i class="fa fa-times-circle"></i> Nee, dit ben ik niet..</button>
                        <button type="submit" class="btn btn-login-screen btn-cons btn-yes"><i class="fa fa-check-circle"></i> Yes, dit ben ik!</button>
                    </div>
                {!! Form::close() !!}
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div id="push"></div>
@stop

@section('scripts')
<script>
$(document).ready(function(){

$('button#fetch-facebook').on('click', function(){
    loginFacebook();
    // getLoginStatus();
});
$('button#fail-facebook').on('click', function(){
    setTimeout(function(){
        $('img.profile-image').attr('src','');
        $('h2.user span.first_name').text('');
        $('h2.user span.semi-bold').text('');
        $('h4.user-email span.email').text('');
        $('.fetched').removeClass('open');
        $('.fetch').addClass('open');
    }, 400);
});
// $('button#fbsrs').on('click', function(){
//     loginFacebook();
// });
function loginFacebook() {
    var FB = window.FB;
    var scopes = 'email,public_profile';
 
    FB.login(function(response) {
      if (response.status === 'connected') {
        console.log('The user has logged in!');
        // FB.api('/me', function(response) {
        //   console.log('Good to see you, ' + response.name + '.');
        // });
        testAPI();
      }
    }, { scope: scopes });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1928920830673076',
        xfbml      : true,
        version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    console.log(response);
    $('#fb-status').hide();
    // console.log('statusChangeCallback');

    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        document.getElementById('status').innerHTML = '';
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Je moet onze ' +
        'app toestemming tot je profiel geven.';
        $('#fb-status').show();
        // document.getElementById('status-button').innerHTML = '<fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Je moet op ' +
        'Facebook inloggen.';
        $('#fb-status').show();
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        console.log(response);
      statusChangeCallback(response);
    });
}

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.
function getLoginStatus() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });   
}

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
        $('div#fb-status').hide();
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me?fields=first_name,last_name,email', function(response) {
            console.log(response);
            // console.log('Successful login for: ' + response.name);
            // document.getElementById('status').innerHTML =
            // 'Thanks for logging in, ' + response.first_name + ' ' + response.last_name + '!';
            var voornaam = response.first_name;
            var achternaam = response.last_name;
            var email = response.email;
            $('h2.user span.first-name').text(voornaam);
            $('h2.user span.semi-bold').text(achternaam);
            $('h4.user-email input.email').val(email);
            $('.fetch').removeClass('open');
        });
        FB.api('/me/picture', function(response) {
            console.log(response);
            // console.log('Successful login for: ' + response.name);
            // document.getElementById('status').innerHTML =
            // 'Thanks for logging in, ' + response.first_name + ' ' + response.last_name + '!';
            if(response.data) {
                var picture_url = response.data.url;
            } else {
                var picture_url = '#';
            }
            $('img.profile-image').attr('src',picture_url);
            $('.fetched').addClass('open');
            // console.log(gender);
        });
    }
});
</script>
@stop