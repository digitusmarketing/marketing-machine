@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/webarch/messenger.min.css') }}">
<link rel="stylesheet" href="{{ url('/css/webarch/messenger-theme-flat.css') }}">
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')
<div id="page-header" class="transacties">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-6 de-cursus-content cursus">
			<div class="top-content">
				<h1>Transacties</h1>
			</div>
			<div class="mid-content">
				<p>Hier vind je het overzicht van je facturen.</p>
				<p><i class="fa fa-info-circle"></i> Klik op de enveloppe om de factuur naar ({!! $user->company->business_email !!}) te sturen.</p>
			</div>
			<div class="bottom-content">
				<div class="cursus-buttons">
					<a href="{!! route('member.profile') !!}" class="btn btn-success btn-cons btn-large btn-module"><i class="fa fa-credit-card"></i> Mijn abonnement</a> 
				</div>
				@include('includes.member.social')
			</div>
		</div>
		<div class="col-md-6 rollIn later-bekijken-header-image-holder">
			<i class="fa fa-exchange"></i>
			{{-- <img src="{{ url(config('app.image')) }}" alt="{{ config('app.name') }}" style="max-width:100%;"> --}}
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="content">
	<!-- BEGIN PAGE TITLE -->
	<div class="page-title">
		{{-- <h3>Master Page</h3> --}}
	</div>
	<div class="grid simple">
		<div class="grid-title">
			<h3>Facturen</h3>
		</div>
		<div class="grid-body">
			<table class="table table-striped no-border">
				<tr>
					<th></th>
					<th>Factuurnummer</th>
					<th>Referentie</th>
					<th>Bedrag</th>
					<th>Status</th>
					<th>Betaalmethode</th>
					<th>Datum</th>
					<th></th>
				</tr>
				@foreach($user->orders as $order)
				<tr>
					<td><span class="badge">{!! $order->type !!}</span></td>
					<td>{!! $order->wefact_invoice_code !!}</td>
					<td>{!! $order->order_id !!}</td>
					<td>&euro; {!! $order->price !!}</td>
					<td>
					@if($order->status === 'paid')
					<span class="label label-success">
					@elseif($order->status === 'open')
					<span class="label label-info">
					@elseif($order->status === 'cancelled')
					<span class="label label-important">
					@elseif($order->status === 'pending' || $order->status === 'expired')
					<span class="label label-warning">
					@else
					<span class="label label-inverse">
					@endif
					{!! $order->status !!}</span></td>
					<td><span class="label label-info">{!! $order->paymentmethod !!}</span></td>
					<td>
						{!! $order->payment_date !!}
					</td>
					<td class="text-center" style="border-left:1px solid #e8edf1;border-right:1px solid #e8edf1;">
						<button class="btn btn-success send-my-invoice" data-invoice-url="{!! route('send.member.invoice', [$order->wefact_invoice_code]) !!}"><i class="fa fa-envelope"></i></button>
					</td>
					
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{ url('js/webarch/messenger.min.js') }}"></script>
<script src="{{ url('js/webarch/messenger-theme-flat.min.js') }}"></script>
<script>
$(document).ready(function(){
	$('button.send-my-invoice').on('click', function() {
		var invoiceurl = $(this).data('invoice-url');

		$.ajax({
			type: 'POST',
			url: invoiceurl,
			beforeSend: function() {
	            $('div.ebook-errors').hide();
	        },
			success: function(data){
				if(data.success === false) {
					// episode is not completed
					Messenger().post({
						message: 'Factuur kon niet worden verstuurd',
						type: 'error',
					});
				} else {
					// episode is completed
					Messenger().post('Factuur is verzonden naar je emailadres!');
				}
			},
	    });
	});
});
</script>
@stop