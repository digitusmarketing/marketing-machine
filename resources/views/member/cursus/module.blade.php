@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
<style>
	.de-content {
	    min-height: 300px;
	}
</style>
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')
<style>
.dropdown-menu.module {
	position: absolute;
    top: 20px;
    left: 0;
    right: 0;
    z-index: 12;
    display: none;
    float: none;
    list-style: none;
    text-shadow: none;
    -webkit-box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.2);
    -moz-box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.2);
    box-shadow: 0px 0px 5px rgba(86, 96, 117, 0.2);
    border: none;
    padding: 0px;
    margin: 0;
    font-size: 13px;
    background: #faa652;
    border-radius: 0 0 20px 20px;
    padding-top: 20px;
    padding-bottom: 8px;
    box-shadow: none;
    transition: height 0.2s;
}
section.btn-module a {
	color: white;
}
section.btn-module:has(div.collapse.in) {
	background: #faa652;
	
}
.btn-module.open .dropdown-menu.module {
	display:block;
}
.btn-module a {
	position:relative;
	z-index:15;
}
.btn-module.open > a {
    outline: 0;
    display: block;
    z-index: 15;
    position: relative;
}
.btn-module {
	position:relative;
}
ul.module-tools {
	list-style-type:none;
	padding:13px 0;
	text-align:left;
	overflow:hidden;
	border-radius: 0 0 20px 20px;
	background-color: #faa652;
}
ul.module-tools li {
	padding:0;
}
ul.module-tools li a {
	position:relative;
	width:100%;
	display:block;
	font-size: 13px;
	padding:5px 20px;
}
ul.module-tools li a:before {
	content: '';
	position: absolute;
	top:50%;
	margin-top:-5px;
	left:-50px;
	display:block;
	border-left:5px solid white;
	border-top:5px solid transparent;
	border-bottom:5px solid transparent;
	transition: left 0.3s ease 0.2s;
}
ul.module-tools li a:hover:before {
	left: 8px;
	transition: left 0.4s;
}
div#module-tools {
	position:absolute;
	left:0;
	right:0;
	top: 25px;
}
</style>
<div id="page-header" class="header-module">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-6 de-content">
			<div class="top-content">
				<h1>Module: <span class="semi-bold">{!! $module->title !!}</span> <p class="pull-right"><span class="badge badge-success badge-x-large"><i class="fa fa-play-circle-o"></i></span> {!! count($module->episodes) !!} lessen</p></h1>
			</div>
			<div class="mid-content">
				<p>{!! $module->body !!}</p>
			</div>
			<hr>
			<div class="bottom-content" id="bottom-content">
				<a href="{!! route('member.get.episode',[$module->cursus_id, $module->slug, $module->episodes->first()->id]) !!}" class="btn btn-success btn-cons btn-large btn-module"><i class="fa fa-play-circle-o"></i> &nbsp;Bekijk de 1e les</a>
				<a href="{!! route('member.get.all.modules',[$module->cursus_id]) !!}" class="btn btn-primary btn-cons btn-large btn-module btn-module-inverse"><i class="fa fa-bars"></i> &nbsp;Module overzicht</a>
				@if($user->id === 1)
				<section class="btn btn-warning btn-cons btn-large btn-module btn-module-inverse">
					<a data-parent="#bottom-content" data-toggle="collapse" aria-expanded="false" href="#module-tools">
						<i class="fa fa-cogs"></i> &nbsp;Module Tools <i class="fa fa-caret-down"></i>
					</a>
					<div id="module-tools" class="collapse">
						<ul class="module-tools">
							<li><a href="#">Worksheets</a></li>
							<li><a href="#">Pixeltool</a></li>
							<li><a href="#">Advertentiebeheer</a></li>
							<li><a href="#">Voorbeeld pixel</a></li>
							<li><a href="#">FB Pixel helper</a></li>
							<li><a href="#">FB Helpcentrum pixels</a></li>
						</ul>
					</div>
				</section>
				@endif
				{{-- <a href="#" class="btn btn-info btn-cons btn-large btn-module btn-module-inverse"><i class="fa fa-envelope"></i> &nbsp;Houd mij op de hoogte</a> --}}
			</div>
		</div>
		<div class="col-md-6">
			<div class="module-header-image-holder rollIn">
				<img src="{{ url($module->image) }}" alt="{{ $module->title }}" class="module-header-image">
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="content">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="module-content col-md-12">
				<ul class="module-episodes">
					@foreach($module->episodes as $episode)
					<div class="col-md-4">
						<li class="module-episode {!! $episode->isCompleted($episode->id, $user->id) ? 'completed' : '' !!}">
							<a href="{!! route('member.get.episode',[$module->cursus_id, $module->slug, $episode->id]) !!}" title="{!! $episode->title !!}">
								<div class="module-episode-image">
									<img src="{!! $episode->poster !!}" alt="{!! $episode->title !!}">
								</div>
								<div class="module-top-episode-content">
									<span class="episode-count"><i class="fa fa-eye"></i> {!! $user->getEpisodeCount($episode->id) !!}</span>
								</div>
								<div class="module-episode-content">
									{{-- <h3 class="module-episode-title">{!! $episode->title !!}</h3> --}}
									<span class="module-status" style="{!! $episode->isCompleted($episode->id, $user->id) ? 'color:#cfcfcf;' : 'display:none;' !!}"><i class="fa fa-check-circle"></i> {!! $episode->isCompleted($episode->id, $user->id) ? 'Voltooid' : '' !!}</span>
									<span class="episode-favorite" style="{!! $episode->isFavorite($episode->id, $user->id) ? 'color:#cfcfcf;' : 'display:none;' !!}"><i class="fa fa-heart-o"></i> {!! $episode->isFavorite($episode->id, $user->id) ? 'Favoriet' : '' !!}</span>
									<span class="episode-later-bekijken" style="{!! $episode->isWatchLater($episode->id, $user->id) ? 'color:#cfcfcf;' : 'display:none;' !!}"><i class="fa fa-clock-o"></i> {!! $episode->isWatchLater($episode->id, $user->id) ? 'Later bekijken' : '' !!}</span>
								</div>
								<div class="module-overlay">
									<i class="fa fa-play-circle-o fa-6x"></i>
								</div>
								{{-- <div class="module-episode-image-overlay"></div> --}}
							</a>
						</li>
					</div>
					@endforeach
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
$(document).ready(function(){
	setTimeout(function(){
		$(".de-content div").each(function(index){
			$(this).delay(300*index).animate({opacity:1});
		});
		$('.de-content hr').delay(600).animate({opacity:1});
		$("li.module-episode").each(function(index) {
		    $(this).delay(400*index).fadeIn(300);
		    $(this).animate({'margin-top':'0'},300)
		});
		$('.rollIn').css({'visibility':'visible'});
	}, 900);
	if($('.btn-module').hasClass('open')) {
		$(this).children('ul.module').slideDown(2000);
	} else {
		$(this).children('ul.module').slideUp(1000);

	}
	// $('a.dropdown-toggle').on('click', function(){
	// 	$(this).next('ul.module').slideToggle();
	// });
});
</script>
@stop