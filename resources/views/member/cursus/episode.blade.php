@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/webarch/messenger.min.css') }}">
<link rel="stylesheet" href="{{ url('/css/webarch/messenger-theme-flat.css') }}">
<link rel="stylesheet" href="{{ url('/css/video/videojs.css') }}">
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1225789587495155";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="de-episode" style="background:black;">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 episode-holder" style="padding:0;">
				<!-- <video 
					id="my-video"
					poster="{!! $episode->poster !!}"
					class="video-js vjs-big-play-centered episode"
					data-episode-id="{!! $episode->id !!}"
					controls
				>
					<source
						src="{!! $episode->src !!}"
						type="video/mp4"
						data-quality="HD"
						label="HD"
						data-default="true"
					>
					<p class="vjs-no-js">
						To view this video please enable JavaScript, and consider upgrading
						to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
					</p>
				</video> -->
				<div class="episode-frame">
					<iframe style="position:absolute;top:0;left:0;width:100%;height:100%;z-index:3;border:0 none;" src="{!! $episode->src !!}" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				@if(isset($previous_episode))
				<a href="{!! route('member.get.episode', [$module->cursus_id, $module->slug, $previous_episode->id]) !!}" class="previous previous-episode" title="Vorige Les" data-toggle="tooltip" data-original-title="Vorige les."><i class="fa fa-chevron-left"></i></a>
				@else
				<a href="{!! route('member.get.module.episodes', [$module->cursus_id, $module->slug]) !!}" class="previous previous-module" title="Vorige Module" data-toggle="tooltip" data-original-title="Vorige module"><i class="fa fa-chevron-circle-left"></i></a>
				@endif
				@if(isset($next_episode))
				<a href="{!! route('member.get.episode', [$module->cursus_id, $module->slug, $next_episode->id]) !!}" class="next next-episode" title="Volgende Les" data-toggle="tooltip" data-original-title="Volgende les"><i class="fa fa-chevron-right"></i></a>
				@else
				@if($module->nextModule($module->id))
				<a href="{!! route('member.get.module.episodes', [$module->cursus_id, $module->nextModule($module->id)->slug]) !!}" class="next next-module" title="Volgende Module" data-toggle="tooltip" data-original-title="Volgende module"><i class="fa fa-chevron-circle-right"></i></a>
				@endif
				@endif		
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
@if($user->id === 1)
<div class="module-tools" style="background-color:#231f20;">
	<div class="container">
		<div class="col-md-12">
			<ul class="nav nav-tools toggler closed">
				<li class="tool-label" data-toggle="tooltip" data-original-title="Klik om de module tools te bekijken">
					<i class="fa fa-cogs"></i>
					<span class="open">Tools</span>
					<span class="closed"><i class="fa fa-chevron-right"></i></span>
				</li>
			</ul>
			<ul class="nav nav-tools collapse">
				<li><a href="#">Worksheets</a></li>
				<li><a href="#">Pixeltool</a></li>
				<li><a href="#">Advertentiebeheer</a></li>
				<li><a href="#">Voorbeeld pixel</a></li>
				<li><a href="#">FB Pixel helper</a></li>
				<li><a href="#">FB Helpcentrum pixels</a></li>
			</ul>
		</div>
	</div>
</div>
@endif
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-offset-2">
				<div class="col-md-12">
					<div class="tiles white">
						<div class="tiles-body episode-buttons-box">
							<ul class="episode-assets-list">
								<li>
									<div class="lesson-status {{ $user->episodeIsComplete($episode->id) ? 'episode-completed' : '' }}" id="lesson-complete-toggle" data-toggle="tooltip" data-original-title="Markeer de les als voltooid">
										{!! Form::open(array('id'=>'episode-complete', 'route'=>'set.completed.video')) !!}
										{!! Form::hidden('episode_id',$episode->id) !!}
										{!! Form::hidden('user_id', $user->id) !!}
										<button id="lesson-complete-tiggle-button" class="btn btn-link btn-with-icon">
											<span class="lesson-status-message">
												<i class="fa fa-check-circle"></i>
												<span>{{ $user->episodeIsComplete($episode->id) ? 'Voltooid' : 'Niet Voltooid' }}</span>
											</span>
										</button>
										{!! Form::close() !!}
									</div>
								</li>
								{{-- <li class="watch-count-item">
									<button class="btn btn-cons btn-with-icon">
										<i class="fa fa-eye"></i>
										<span>Les {!! $user->getEpisodeCount($episode->id) !!}x bekeken</span>
									</button>
								</li> --}}
								<li class="watch-later-item {{ $user->episodeIsWatchLater($episode->id) ? 'episode-watch-later' : '' }}" data-toggle="tooltip" data-original-title="Markeer les als later-bekijken.">
									{!! Form::open(array('id'=>'episode-watch-later', 'route'=>'set.watchlater.video')) !!}
									{!! Form::hidden('episode_id', $episode->id) !!}
									{!! Form::hidden('user_id', $user->id) !!}
									<button class="btn btn-cons btn-with-icon" title="Watch Later">
										<i class="fa fa-clock-o"></i>
										<span>Later Bekijken</span>
									</button>
									{!! Form::close() !!}
								</li>
								
								<li class="favorite-item {{ $user->episodeIsFavorite($episode->id) ? 'favorite-episode' : '' }}" data-toggle="tooltip" data-original-title="Markeer les als favoriet.">
									{!! Form::open(array('route'=>'set.favorite.video', 'id'=>'add-episode-favorite')) !!}
									{!! Form::hidden('episode_id', $episode->id) !!}
									{!! Form::hidden('user_id', $user->id) !!}
									<button class="btn btn-cons btn-with-icon" title="Toevoegen aan favorieten">
										<i class="fa fa-heart"></i>
										<span class="favorite-text">{{ $user->episodeIsFavorite($episode->id) ? 'Favoriet!' : 'Toevoegen aan favorieten'}}</span>
									</button>
									{!! Form::close() !!}
								</li>
								
								{{-- <li>
									<a href="#" class="btn btn-cons btn-with-icon">
										<i class="fa fa-download"></i>
										<span>Download Worksheet</span>
									</a>
								</li> --}}
								{{-- <li>
									<a href="#" class="btn btn-cons btn-with-icon">
										<i class="fa fa-comments-o"></i>
										<span>Bespreek de video</span>
									</a>
								</li> --}}
								<li data-toggle="tooltip" data-original-title="Ga naar module overzicht.">
									<a href="{!! route('member.get.module.episodes', [$module->cursus_id, $module->slug]) !!}" class="btn btn-cons btn-with-icon">
										<i class="fa fa-bars"></i>
										<span>Module Overzicht</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="col-md-12">
					<div class="tiles white">
						<div class="tiles-body">
							<h3><span class="semi-bold">{!! $module->title !!}</span>: {!! $episode->title !!}</h3>
							<p><span class="bold"><i>{!! $module->body !!}</i></span></p>
							{{-- <p>{!! $episode->body !!}</p> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row row-seperator"></div>
	{{-- <div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h5>Whats up next?</h5>
			<h3>Continue learning</h3>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-2 col-md-offset-2">
			<p>Leer meer over de cursus door meer te kijken en bla bla bla</p>
		</div>
		<div class="col-md-6">
			<ul>
				@if(isset($next_episode))
				<li>{!! $next_episode->title !!}</li>
				@endif
			</ul>
		</div>
	</div> --}}
	<div class="row">
		<div id="disquss-holder">
			<div class="col-md-8 col-md-offset-2">
				<h2>MENG JE IN</h2>
				<h3>het gesprek</h3>
				<hr>
				<div class="clearfix"></div>
				<div id="disquss">
					<style>
						div.fb-comments,
						div.fb-comments span,
						div.fb-comments span iframe {
							width:100% !important;
						}
					</style>
					<div class="fb-comments" data-href="https://www.facebook.com/groups/SocialpreneursCommunity/permalink/{!! $module->id !!}{!! $episode->id !!}" data-colorscheme="light" data-mobile=""></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{ url('js/webarch/messenger.min.js') }}"></script>
<script src="{{ url('js/webarch/messenger-theme-flat.min.js') }}"></script>
{{-- <script src="{{ url('js/video/video.js') }}"></script> --}}
<script src="https://vjs.zencdn.net/5.8.8/video.js"></script>
<script src="{{ url('js/video/vimeo.js') }}"></script>
<script>
var episode = videojs('my-video');
episode.on('play', function() {
	var episodeId = $('input[type=hidden][name=episode_id]').val();
	var userId = $('input[type=hidden][name=user_id]').val();
	url = '/member/episode/'+episodeId+'/'+userId;
	$.ajax({
		type: 'POST',
		url: url,
		success: function(data) {
			console.log(data);
			// alert('is volgens mij goed gegaan.');
		},
	});
});
episode.on('ended', function() {
	var url = $('form#episode-complete').attr('action');

	$.ajax({
		type: 'POST',
		url: url,
		data: $('form#episode-complete').serialize(),
		beforeSend: function() {
            $('div.ebook-errors').hide();
        },
		success: function(data){
			if(data.success === false) {
				// episode is not completed
				$('div.lesson-status').removeClass('episode-completed');
				$('span.lesson-status-message > span').html('Niet Voltooid');
				Messenger().post({
					message: 'Les ongedaan',
					type: 'error',
				});
			} else {
				// episode is completed
				$('div.lesson-status').addClass('episode-completed');
				$('span.lesson-status-message > span').html('Voltooid');
				Messenger().post('Les voltooid');
			}
		},
    });
});



</script>
<script>
// Set proper iframe width and height
$(document).ready(function(){
	$('#layout-condensed-toggle').click();
	// $('video#my-video_html5_api').css({'height':($('div.episode-holder').width() * 0.57)});
	$('div.episode-frame').css({'height':($('div.episode-holder').width() * 0.56)});
	$(window).resize(function(){
		// $('video#my-video_html5_api').css({'height':($('div.episode-holder').width() * 0.57)});
		$('div.episode-frame').css({'height':($('div.episode-holder').width() * 0.56)});
	});
	$('div.de-episode').css({'padding-top':$('div.header').height()});
	$('ul.nav-tools.toggler li.tool-label').on('click', function(){
		$('ul.nav-tools.toggler').toggleClass('closed');
		$('ul.nav-tools.collapse').fadeToggle();
	});
});

// Ajax call to mark episode as complete or incomplete
$('form#episode-complete').on('submit',function(e){
	e.preventDefault();
	var url = $('form#episode-complete').attr('action');

	$.ajax({
		type: 'POST',
		url: url,
		data: $('form#episode-complete').serialize(),
		beforeSend: function() {
            $('div.ebook-errors').hide();
        },
		success: function(data){
			if(data.success === false) {
				// episode is not completed
				$('div.lesson-status').removeClass('episode-completed');
				$('span.lesson-status-message > span').html('Niet Gekeken');
				Messenger().post({
					message: 'Les ongedaan',
					type: 'error',
				});
			} else {
				// episode is completed
				$('div.lesson-status').addClass('episode-completed');
				$('span.lesson-status-message > span').html('Voltooid');
				Messenger().post('Les voltooid');
			}
		},
    });
});

// Ajax call to makr episode as favorite or not
$('form#add-episode-favorite').on('submit', function(e) {
	e.preventDefault();
	var url = $('form#add-episode-favorite').attr('action');

	$.ajax({
		type: 'POST',
		url: url,
		data: $('form#add-episode-favorite').serialize(),
		beforeSend: function() {

		},
		success: function(data) {
			if(data.success === false) {
				// episode is not a favorite
				$('li.favorite-item').removeClass('favorite-episode');
				$('li.favorite-item button span').text('Toevoegen aan favorieten');
				Messenger().post({
					message: 'Les verwijdert van favorieten!',
					type: 'error',
				});
			} else {
				// episode is a favorite
				$('li.favorite-item').addClass('favorite-episode');
				$('li.favorite-item button span').text('Favoriet');
				Messenger().post('Les toegevoegd aan favorieten!');
			}
		}
	})
})

// Ajax call to add episode to watch later, or remove it
$('form#episode-watch-later').on('submit', function(e) {
	e.preventDefault();
	var url = $('form#episode-watch-later').attr('action');

	$.ajax({
		type: 'POST',
		url: url,
		data: $('form#episode-watch-later').serialize(),
		beforeSend: function() {

		},
		success: function(data) {
			if(data.success === false) {
				// Episode is not added to watch later
				$('li.watch-later-item').removeClass('episode-watch-later');
				Messenger().post({
					message: 'Les verwijdert van later bekijken!',
					type: 'error',
				});
			} else {
				// Episode is added to watch later
				$('li.watch-later-item').addClass('episode-watch-later');
				Messenger().post('Les toegevoegd aan later bekijken!');
			}
		}
	});
});
</script>
@stop