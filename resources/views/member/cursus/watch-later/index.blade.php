@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
<style>
	.module{opacity:0;margin-top:25px;}
	.cursus {
		opacity:0;
	}
	.content {
		padding-top: 30px !important;
	}
</style>
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')
<div id="page-header" class="later-bekijken">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-6 de-cursus-content cursus">
			<div class="top-content">
				<h1>Later bekijken</h1>
			</div>
			<div class="mid-content">
				<p>Hier vind je een overzicht van de lessen die je hebt gemarkeerd met 'later bekijken'.</p>
			</div>
			<div class="bottom-content">
				{{-- <div class="cursus-buttons">
					<a href="#" class="btn btn-success btn-cons btn-large btn-module"><i class="fa fa-play-circle-o"></i> Begin met kijken!</a> 
				</div> --}}
				@include('includes.member.social')
			</div>
		</div>
		<div class="col-md-6 rollIn later-bekijken-header-image-holder">
			<i class="fa fa-clock-o"></i>
			{{-- <img src="{{ url(config('app.image')) }}" alt="{{ config('app.name') }}" style="max-width:100%;"> --}}
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="content">
	<div class="row">
		<div class="col-md-12">
			@if(count($episodes) > 0)
				@foreach($episodes as $key => $value)
				<div class="row">
					<div class="col-md-3 module">
						<div class="module-content-wrapper">
							{{-- <a href="#"> --}}
								<img src="{!! url($module->find($key)->image) !!}" alt="{!! $module->find($key)->title !!}" class="module-image">
								{{-- <div class="module-overlay">
									<i class="fa fa-play-circle-o fa-6x"></i>
								</div> --}}
								<div class="module-de-overlay">
									<div class="module-episodes">
										<i class="fa fa-play-circle-o"></i> <span class="bold">{!! count($value) !!}</span>
									</div>
									<div class="module-title">
										<h4>{!! $module->find($key)->title !!}</h4>
									</div>
									<div class="clearfix"></div>
								</div>
							{{-- </a> --}}
						</div>
					</div>
					<div class="col-md-9">
						<ul class="module-episodes">
						@foreach($value as $episode)
							<div class="col-md-4">
								<li class="module-episode {!! $episode->isCompleted($episode->id, $user->id) ? 'completed' : '' !!}">
									<a href="{!! route('member.get.episode',[$module->find($episode->module_id)->cursus_id, $module->find($episode->module_id)->slug, $episode->id]) !!}" title="{!! $episode->title !!}" id="w-l-toggle" data-ep-id="{!! $episode->id !!}">
										<div class="module-episode-image">
											<img src="{!! $episode->poster !!}" alt="{!! $episode->title !!}">
										</div>
										<div class="module-episode-content">
											{{-- <h3 class="module-episode-title">{!! $episode->title !!}</h3> --}}
											<span class="module-status" style="{!! $episode->isCompleted($episode->id, $user->id) ? 'color:#07bd5c;' : 'color:#cfcfcf' !!}"><i class="fa fa-check-circle"></i> {!! $episode->isCompleted($episode->id, $user->id) ? 'Voltooid' : 'Niet Voltooid' !!}</span>
										</div>
										<div class="module-overlay">
											<i class="fa fa-play-circle-o fa-6x"></i>
										</div>
										{{-- <div class="module-episode-image-overlay"></div> --}}
									</a>
								</li>
							</div>
						@endforeach
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<hr>
				@endforeach
			@else
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Nog geen lessen toegevoegd aan later bekijken..</h2>
						</div>
						<div class="panel-body">
							<p>Je hebt nog lessen toegevoegd aan je lijst met lessen die je later wilt bekijken..</p>
							<p>Ga lessen bekijken en voeg ze toe aan je later bekijken lijst!</p>
							<a href="#" class="btn btn-success btn-cons btn-large btn-module"><i class="fa fa-play-circle-o"></i> Begin met kijken!</a>
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
@stop

@section('scripts')
<form id="watch-later-form"	action="{{ route('remove.watchlater.video') }}" method="POST" style="display: none;">
	{{ csrf_field() }}
	<input type="hidden" value="{!! $user->id !!}" name="user_id">
	<input type="hidden" name="episode_id">
</form>
<script>
$('a#w-l-toggle').on('click', function(){
	var episodeId = $(this).data('ep-id');
	$('input[type=hidden][name=episode_id]').val(episodeId);
	$('form#watch-later-form').submit();
});

$('form#watch-later-form').on('submit',function(e){
	e.preventDefault();
	var url = $('form#watch-later-form').attr('action');

	$.ajax({
		type: 'POST',
		url: url,
		data: $('form#watch-later-form').serialize(),
		success: function(data){
			// if(data.success = true) {
			// 	window.location.href = data.route;
			// }
			// console.log('er is iets misgegaan..');
		},
	});
});
$(document).ready(function(){
	$(".module").each(function(index) {
	    $(this).delay(400*index).animate({opacity: 1});
	    $(this).animate({'margin-top':'0'});
	});
	$('.cursus').each(function(index) {
		$(this).delay(500*index).animate({opacity:1});
	});
	setTimeout(function(){
		$(".de-content div").each(function(index){
			$(this).delay(300*index).animate({opacity:1});
		});
		$('.de-content hr').delay(600).animate({opacity:1});
		$("li.module-episode").each(function(index) {
		    $(this).delay(400*index).fadeIn(300);
		    $(this).animate({'margin-top':'0'},300)
		});
		$('.rollIn').css({'visibility':'visible'});
	}, 900);

});
</script>
@stop