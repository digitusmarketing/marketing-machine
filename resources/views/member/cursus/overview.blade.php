@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
<style>
	.module{opacity:0;margin-top:25px;}
	.cursus {
		opacity:0;
	}
</style>
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')

{{-- @foreach($cursussen as $cursus) --}}
<div id="page-header" class="header-cursus">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-6 de-cursus-content cursus">
			<div class="top-content">
				<h1>{!! $cursussen->title !!}</h1>
				{{-- <h1>Facebook als Marketing Machine</h1> --}}
			</div>
			<div class="mid-content">
				{{-- {!! $cursus->body !!} --}}
				<p>In de 10-delige videocursus ontdek je stap voor stap hoe jij Facebook inzet als Marketing Machine voor jouw business.</p>
				<p>Je leert welke strategieën, trucs en technieken wij voor onszelf en onze klanten inzetten om zichtbaarheid, klanten en omzet te realiseren.</p>
			</div>
			<div class="bottom-content">
				<div class="cursus-buttons">
					<a href="{!! route('member.get.all.modules',[$cursussen->id]) !!}" class="btn btn-success btn-cons btn-large btn-module"><i class="fa fa-play-circle-o"></i> Begin met kijken!</a> 
				</div>
				@include('includes.member.social')
			</div>
		</div>
		<div class="col-md-6 cursus-image cursus">
			<img src="{!! url('/images/site/cursus/Facebook-als-marketing-machine-40-DVD-box.png') !!}" alt="{{ config('app.name') }}" style="max-width:100%;">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
{{-- @endforeach --}}
@stop

@section('scripts')
<script>
	
$(document).ready(function(){
	$(".module").each(function(index) {
	    $(this).delay(400*index).animate({opacity: 1});
	    $(this).animate({'margin-top':'0'});
	});
	$('.cursus').each(function(index) {
		$(this).delay(500*index).animate({opacity:1});
	});
});
</script>
@stop