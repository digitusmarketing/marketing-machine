@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
<style>
	.module{opacity:0;margin-top:25px;}
	.cursus {
		opacity:0;
	}
</style>
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')

<div id="page-header" class="header-cursus">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-6 de-cursus-content cursus">
			<div class="top-content">
				<h1>{!! $cursus->title !!}</h1>
				{{-- <h1>Facebook als Marketing Machine</h1> --}}
			</div>
			<div class="mid-content">
				{!! $cursus->body !!}
				{{-- <p>In de 10-delige videocursus ontdek je stap voor stap hoe jij Facebook inzet als Marketing Machine voor jouw business.</p>
				<p>Je leert welke strategieën, trucs en technieken wij voor onszelf en onze klanten inzetten om zichtbaarheid, klanten en omzet te realiseren.</p> --}}
			</div>
			<div class="bottom-content">
				<div class="cursus-buttons">
					<a href="{!! route('member.get.module.episodes',[$cursus->id, $cursus->modules->first()->slug]) !!}" class="btn btn-success btn-cons btn-large btn-module"><i class="fa fa-play-circle-o"></i> Begin met kijken!</a> 
				</div>
				@include('includes.member.social')
			</div>
		</div>
		<div class="col-md-6 cursus-image cursus">
			<img src="{!! url('/images/site/cursus/Facebook-als-marketing-machine-40-DVD-box.png') !!}" alt="{{ config('app.name') }}" style="max-width:100%;">
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="content" style="padding-top:40px;padding-left:25px;padding-right:25px;">
	@foreach ($modules as $module) 
	    @if (!$module->episodes)
	    	module has been completed
	    @else
	    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 single-colored-widget m-b-30 module">
			<div class="module-content-wrapper">
				<a href="{!! route('member.get.module.episodes',[$cursus->id, $module->slug]) !!}">
					{{-- <img src="https://i.ytimg.com/vi/rb8Y38eilRM/maxresdefault.jpg" alt="" class="module-image"> --}}
					<img src="{{ url($module->image)}}" alt="" class="module-image">
					<div class="module-overlay">
						<i class="fa fa-play-circle-o fa-6x"></i>
					</div>
					<div class="module-de-overlay">
						<div class="module-episodes">
							{{-- <span class="small-text muted">Videos</span><br/> --}}
							<i class="fa fa-play-circle-o"></i>
							<span class="bold">{!! count($module->episodes) !!}</span>
						</div>
						<div class="module-title">
							<h4><span class="semi-bold">Module {!! $module->id !!}</span>: {!! $module->title !!}</h4>
							{{-- <p>{!! $module->body !!}</p> --}}
						</div>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
			{{-- <div class="heading">
			</div> --}}
		</div>
	    @endif
	@endforeach
{{-- 	@foreach($cursus->modules as $module)
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 single-colored-widget m-b-30 module">
			<div class="module-content-wrapper">
				<a href="{!! route('member.get.module.episodes',$module->slug) !!}">
					<img src="https://i.ytimg.com/vi/rb8Y38eilRM/maxresdefault.jpg" alt="" class="module-image">
					<div class="module-overlay">
						<i class="fa fa-play-circle-o fa-6x"></i>
					</div>
				</a>
			</div>
			<div class="heading">
				<div class="pull-left">
					<h4>{!! $module->title !!}</h4>
					<p>{!! $module->body !!}</p>
				</div>
				<div class="pull-right">
					<span class="small-text muted">Videos</span><br/>
					<i class="fa fa-film"></i>
					<span class="badge badge-inverse">{!! count($module->episodes) !!}</span>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	@endforeach --}}
</div>

@stop

@section('scripts')
<script>
	
$(document).ready(function(){
	$(".module").each(function(index) {
	    $(this).delay(400*index).animate({opacity: 1});
	    $(this).animate({'margin-top':'0'});
	});
	$('.cursus').each(function(index) {
		$(this).delay(500*index).animate({opacity:1});
	});
});
</script>
@stop