@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
<link rel="stylesheet" href="{{ url('/css/webarch/messenger.min.css') }}">
<link rel="stylesheet" href="{{ url('/css/webarch/messenger-theme-flat.css') }}">
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1225789587495155";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="page-header" class="affiliate">
	<div class="col-md-10 col-md-offset-1">
		<div class="col-md-6 de-cursus-content cursus">
			<div class="top-content">
				<h1>Affiliatepaneel</h1>
			</div>
			<div class="mid-content">
				<p>Hier vind je een overzicht van alle statistieken van je affiliate account.</p>
				<p><i class="fa fa-info-circle"></i> Ontvang 30% korting op je abonnement per lid die zich via jouw referentie-link inschrijft.<br/>
				Je kunt tot 90% korting ontvangen op je abonnement door 3 nieuwe leden in te schrijven.</p>
			</div>
			<div class="bottom-content">
				@if($user->isValidatedAffiliate())
				<div class="cursus-buttons">
					<a href="https://www.facebook.com/sharer.php?u={{ route('base.index') }}?a={{ $user->affiliate->code }}" target="_window" class="btn btn-success btn-cons btn-large btn-module"><i class="fa fa-play-circle-o"></i> Begin met verkopen!</a> 
				</div>
				@endif
				@include('includes.member.social')
			</div>
		</div>
		<div class="col-md-6 popIn later-bekijken-header-image-holder">
			{{-- <img src="https://www.marketingmachine.io/images/site/cursus/Facebook-als-marketing-machine-40-DVD-box.png" alt="{{ config('app.name') }}" style="max-width:100%;"> --}}
			<i class="fa fa-magic"></i>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="content">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			@if($user->isValidatedAffiliate())
				<div class="module-content col-md-12">
					<ul class="module-episodes">
						<div class="col-lg-4 col-md-4">
							<div class="tiles green m-b-10">
								<div class="tiles-body">
									<div class="controller">
										<a href="javascript:;" class="reload"></a>
										<a href="javascript:;" class="remove"></a>
									</div>
									<div class="tiles-title text-black">Jouw Affiliate Statistieken </div>
									<div class="widget-stats">
										<div class="wrapper transparent">
											<span class="item-title">Klikken op link</span> <span class="item-count animate-number semi-bold" data-value="{!! $user->affiliate->watch_count !!}" data-animation-duration="700">0</span>
										</div>
									</div>
									<div class="widget-stats">
										<div class="wrapper transparent">
											<span class="item-title">Affiliate sales</span> <span class="item-count animate-number semi-bold" data-value="{!! $user->affiliate->countActiveSalesUsers() !!}" data-animation-duration="700">0</span>
										</div>
									</div>
									<div class="widget-stats ">
										<div class="wrapper last">
											<span class="item-title">Commissie</span> <span class="item-count animate-number semi-bold" data-value="{!! $user->affiliate->getSalesAmount() !!}" data-animation-duration="700">0 EUR</span>
										</div>
									</div>
									<div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
										@if($user->affiliate->countActiveSalesUsers() == '1') {
											<?php $percentage = '33.33'; ?>
										}
										@elseif($user->affiliate->countActiveSalesUsers() == '2') {
											<?php $percentage = '66.66'; ?>
										}
										@elseif($user->affiliate->countActiveSalesUsers() == '3') {
											<?php $percentage = '100'; ?>
										}
										@else {
											<?php $percentage = '0.00'; ?>
										}
										@endif
										<div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="{!! $percentage !!}%"></div>
									</div>
								</div>
								@if(count($user->affiliate->sales) > 0)
								<div class="tiles white">
									<div class="tiles-body">
										<h5 class="semi-bold">LAST SALES</h5>
										<table class="table no-more-tables m-t-20 m-l-20 m-b-30">
											<thead style="display:none">
												<tr>
													<th style="width:9%">Referentie</th>
													<th style="width:22%">Betaalmethode</th>
													<th style="width:6%">Commissie</th>
													<th style="width:1%"> </th>
												</tr>
											</thead>
											<tbody>
												@foreach($user->affiliate->sales as $sale)
												<tr>
													<td class="v-align-middle bold text-success">{!! $sale->order->order_id !!}</td>
													<td class="v-align-middle"><span class="muted"><span class="label label-primary">{!! $sale->order->paymentmethod !!}</span></span> </td>
													<td><span class="muted bold text-success">&euro; {!! number_format($sale->order->price * config('affiliate.normal.commissie'), 2, '.', ',') !!}</span> </td>
													<td class="v-align-middle"></td>
												</tr>
												@endforeach

											</tbody>
										</table>
										<div id="sales-graph"> </div>
									</div>
								</div>
								@else

								@endif
							</div>
							<div class="tiles white m-b-10">
								<div class="tiles-body">
									
									<div class="tiles-title text-black">Referentielink</div>
									
										<div class="row">
											<div class="col-md-12">
												<div class="input-group">
													<input type="text" value="{{ route('base.index') }}?a={{ $user->affiliate->code }}" placeholder="Jouw Affiliatelink.." readonly id="affiliatelink" class="form-control">
													<span class="input-group-addon success" onclick="copyToClipboard('affiliatelink')">
														<span class="arrow"></span>
														<i class="fa fa-link"></i>
													</span>
												</div>
											<div class="clearfix"></div>
											</div>
										</div>
									
								</div>
							</div>

							{{-- <div class="tiles white m-b-10">
								<div class="tiles-body">
									
									<div class="tiles-title text-black">Facebook webinar - <small>8 &amp; 10 Maart 2017</small></div>
									
										<div class="row">
											<div class="col-md-12">
												<div class="input-group">
													<input type="text" value="{{ route('base.webinar') }}?a={{ $user->affiliate->code }}" readonly id="webinar" class="form-control">
													<span class="input-group-addon success" onclick="copyToClipboard('webinar')">
														<span class="arrow"></span>
														<i class="fa fa-link"></i>
													</span>
												</div>
											<div class="clearfix"></div>
											</div>
										</div>
									
								</div>
							</div> --}}
						</div>
						@if($user->id === 1)
						<div class="col-lg-8 col-md-8">
							<div class="tiles white">
								<div class="tiles-body">
									<div class="tiles-title text-black">Sjablonen</div>
									<hr>
									<p class="text-black">Klik op één van de buttons om een sjabloon te openen en/of bewerken.</p>
									<ul class="nav nav-tabs" role="tablist">
										<li>
											<a href="#facebook-text" role="tab" data-toggle="tab" aria-expanded="false">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										{{-- <li class="dropdown" data-toggle="tooltip" data-original-title="Email sjablonen">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-envelope"></i>
												<i class="fa fa-caret-down"></i>
											</a>
											<ul class="dropdown-menu">
												<li>
													<a href="#text-1-1" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-envelope"></i> Versie 1
													</a>
												</li>
												<li>
													<a href="#text-1-2" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-envelope"></i> Versie 2
													</a>
												</li>
											</ul>
										</li> --}}
										{{-- <li class="dropdown" data-toggle="tooltip" data-original-title="Advertentie sjablonen">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-bullhorn"></i>
												<i class="fa fa-caret-down"></i>
											</a>
											<ul class="dropdown-menu">
												<li>
													<a href="#text-2-1" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-bullhorn"></i> Versie 1
													</a>
												</li>
												<li>
													<a href="#text-2-2" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-bullhorn"></i> Versie 2
													</a>
												</li>
											</ul>
										</li> --}}
										{{-- <li class="dropdown" data-toggle="tooltip" data-original-title="Sociale Netwerken">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
												<i class="fa fa-share-alt"></i>
												<i class="fa fa-caret-down"></i>
											</a>
											<ul class="dropdown-menu">
												<li>
													<a href="#text-3" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-whatsapp"></i> Whatsapp
													</a>
												</li>
												<li>
													<a href="#text-4" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-facebook"></i> Facebook
													</a>
												</li>
												<li>
													<a href="#text-5" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-twitter"></i> Twitter
													</a>
												</li>
												<li>
													<a href="#text-6" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-instagram"></i> Instagram
													</a>
												</li>
												<li>
													<a href="#text-7" role="tab" data-toggle="tab" aria-expanded="false">
														<i class="fa fa-linkedin"></i> LinkedIn
													</a>
												</li>
											</ul>
										</li> --}}
									</ul>
									<div class="well well-sm" style="margin-bottom: 10px;padding-top:0">
										
										<div class="tab-content" style="margin-bottom:0;">
											<div class="tab-pane" id="facebook-text">
												<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
												<p id="kopieermijaub">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam illo minus qui pariatur inventore blanditiis reprehenderit, quia consequuntur libero quae quas, cumque aspernatur possimus nostrum natus tempora sit, atque excepturi.</p>
												<button class="btn btn-success" id="shareBtn"><i class="fa fa-share"></i></button>
											</div>
											{{-- <div class="tab-pane" id="text-1-1">
												<h2>Titel Email v1</h2>
												<p>Text voor email v1</p>
											</div>
											<div class="tab-pane" id="text-1-2">
												<h2>Titel Email v2</h2>
												<p>Text voor email v2</p>
											</div>
											<div class="tab-pane" id="text-2-1">
												<h2>Titel Advertentie v1</h2>
												<p>Text voor advertentie v1</p>
											</div>
											<div class="tab-pane" id="text-2-2">
												<h2>Titel Advertentie v2</h2>
												<p>Text voor advertentie v2</p>
											</div>
											<div class="tab-pane" id="text-3">
												<h2>Titel Whatsapp</h2>
												<p>Text voor Whatsapp</p>
											</div>
											<div class="tab-pane" id="text-4">
												<h2>Titel Facebook</h2>
												<p>Text voor Facebook</p>
											</div>
											<div class="tab-pane" id="text-5">
												<h2>Titel Twitter</h2>
												<p>Text voor Twitter</p>
											</div>
											<div class="tab-pane" id="text-6">
												<h2>Titel Instagram</h2>
												<p>Text voor Instagram</p>
											</div>
											<div class="tab-pane" id="text-7">
												<h2>Titel LinkedIn</h2>
												<p>Text voor LinkedIn</p>
											</div> --}}
										</div>
									</div>
								</div>
							</div>
						</div>
						@endif
					</ul>
				</div>
				<div class="clearfix"></div>
				@foreach($user->affiliate->sales as $sale)
				<div class="row">
					{!! $sale->order->price !!}
				</div>
				@endforeach
			@else
				<div class="module-content col-md-12">
					<div class="grid simple">
						<div class="grid-title">
							<h3>Je affiliate account is nog niet goedgekeurd</h3>
						</div>
						<div class="grid-body">
							@if($user->affiliate->status == 0)
							<p>Om je commissie over te boeken naar je bankrekening, dienen wij over de volgende gegevens te beschikken.</p>
							<button id="vul-aan" class="btn btn-success btn-cons btn-large">Bekijk mijn gegevens</button>
							<div class="company-credentials-form" style="display:none;">
								
							@if($errors)
								@foreach($errors as $error)
									{!! $error !!}
								@endforeach
							@endif
							<div class="col-md-12">
								<h2>Bedrijfsgegevens</h2>
							</div>
							<div class="clearfix"></div>
							<hr>
							{!! Form::open(array('route'=>'member.company.update')) !!}
							{!! Form::hidden('user_id',$user->id) !!}
							<div class="form-group col-md-12 {{ $errors->has('bedrijfsnaam') ? 'has-error' : '' }}">
								{!! Form::label('bedrijfsnaam', 'Bedrijfsnaam:') !!}
								{!! Form::text('bedrijfsnaam', old('bedrijfsnaam',$user->company->company_name), array('class'=>'form-control', 'placeholder'=>'Vul je bedrijfsnaam in..')) !!}
								@if($errors->has('bedrijfsnaam'))
								<p class="alert alert-danger">{!! $errors->first('bedrijfsnaam') !!}</p>
								@endif
							</div>
							<div class="col-md-2 form-group {{ $errors->has('aanhef') ? 'has-error' : '' }}">
								{!! Form::label('aanhef', 'Aanhef:') !!}
								{!! Form::select('aanhef', array('m'=>'Dhr.', 'f'=>'Mevr.'), old('aanhef',$user->company->title), array('class'=>'form-control')) !!}
								@if($errors->has('aanhef'))
								<p class="alert alert-danger">{!! $errors->first('aanhef') !!}</p>
								@endif
							</div>
							<div class="form-group col-md-5 {{ $errors->has('voornaam') ? 'has-error' : '' }}">
								{!! Form::label('voornaam', 'Voornaam:') !!}
								{!! Form::text('voornaam', old('voornaam',$user->first_name), array('class'=>'form-control', 'id'=>'disabledInput', 'readonly')) !!}
								@if($errors->has('voornaam'))
								<p class="alert alert-danger">{!! $errors->first('voornaam') !!}</p>
								@endif
							</div>
							<div class="form-group col-md-5 {{ $errors->has('achternaam') ? 'has-error' : '' }}">
								{!! Form::label('achternaam', 'Achternaam:') !!}
								{!! Form::text('achternaam', old('achternaam',$user->last_name), array('class'=>'form-control', 'id'=>'disabledInput', 'readonly')) !!}
								@if($errors->has('achternaam'))
								<p class="alert alert-danger">{!! $errors->first('achternaam') !!}</p>
								@endif
							</div>
							<div class="form-group col-md-6 {{ $errors->has('business_email') ? 'has-error' : '' }}">
								{!! Form::label('business_email', 'Emailadres:') !!}
								{!! Form::email('business_email', old('business_email',$user->company->business_email), array('class'=>'form-control', 'placeholder'=>'Vul het email adres in waar je facturen op wenst te verkrijgen..')) !!}
								@if($errors->has('business_email'))
								<p class="alert alert-danger">{!! $errors->first('business_email') !!}</p>
								@endif
							</div>
							<div class="col-md-6 form-group">
								{!! Form::label('kvk', 'KvK of Handelsnummer:') !!}
								{!! Form::text('kvk', old('kvk',$user->company->kvk), array('class'=>'form-control', 'placeholder'=>'Vul je KvK of Handelsnummer in..')) !!}
								@if($errors->has('kvk'))
								<p class="alert alert-danger">{!! $errors->first('kvk') !!}</p>
								@endif
							</div>
							<div class="form-group col-md-6 {{ $errors->has('telefoonnummer') ? 'has-error' : '' }}">
								{!! Form::label('telefoonnummer', 'Telefoonnummer:') !!}
								{!! Form::input('tel', 'telefoonnummer', old('telefoonnummer',$user->company->phone), array('class'=>'form-control', 'placeholder'=>'Vul je telefoonnummer in..')) !!}
								@if($errors->has('telefoonnummer'))
								<p class="alert alert-danger">{!! $errors->first('telefoonnummer') !!}</p>
								@endif
							</div>
							<div class="form-group col-md-6">
								{!! Form::label('mobielnummer', 'Mobiel:') !!}
								{!! Form::input('tel', 'mobielnummer', old('mobielnummer',$user->company->mobile_phone), array('class'=>'form-control', 'placeholder'=>'Vul je mobielenummer in..')) !!}
							</div>
							<div class="form-group col-md-4 {{ $errors->has('adres') ? 'has-error' : '' }}">
								{!! Form::label('adres', 'Adres:') !!}
								{!! Form::text('adres', old('adres',$user->company->address), array('class'=>'form-control', 'placeholder'=>'Vul je adres in..')) !!}
								@if($errors->has('adres'))
								<p class="alert alert-danger">{!! $errors->first('adres') !!}</p>
								@endif
							</div>
							<div class="form-group col-md-2 {{ $errors->has('postcode') ? 'has-error' : '' }}">
								{!! Form::label('postcode', 'Postcode:') !!}
								{!! Form::text('postcode', old('postcode',$user->company->postal), array('class'=>'form-control', 'placeholder'=>'Vul je postcode in..')) !!}
								@if($errors->has('postcode'))
								<p class="alert alert-danger">{!! $errors->first('postcode') !!}</p>
								@endif
							</div>
							<div class="form-group col-md-4 {{ $errors->has('woonplaats') ? 'has-error' : '' }}">
								{!! Form::label('woonplaats', 'Woonplaats:') !!}
								{!! Form::text('woonplaats', old('woonplaats',$user->company->recidence), array('class'=>'form-control', 'placeholder'=>'Vul de vestigingsplaats in..')) !!}
								@if($errors->has('woonplaats'))
								<p class="alert alert-danger">{!! $errors->first('woonplaats') !!}</p>
								@endif
							</div>
							<div class="form-group col-md-2 {{ $errors->has('land') ? 'has-error' : '' }}">
								{!! Form::label('land', 'Land:') !!}
								{!! Form::select('land', array(null=>'Kies je land..', 'AW' =>'Aruba', 'BE' =>'België', 'CW' =>'Curaçao', 'DK' =>'Denemarken', 'DE' =>'Duitsland', 'FI' =>'Finland', 'FR' =>'Frankrijk', 'IE' =>'Ierland', 'IT' =>'Italië', 'NL' =>'Nederland', 'NO' =>'Noorwegen', 'PT' =>'Portugal', 'ES' =>'Spanje', 'SR' =>'Suriname', 'GB' =>'Verenigd Koninkrijk', 'ZA' =>'Zuid-Afrika', 'SE' =>'Zweden',), old('land',$user->company->country_code), array('class'=>'form-control')) !!}
								{!! Form::hidden('land_full', old('land_full',$user->company->country_full)) !!}
								@if($errors->has('land'))
								<p class="alert alert-danger">{!! $errors->first('land') !!}</p>
								@endif
							</div>
							<div class="col-md-12 form-group btw-nummer {{ $errors->has('btwnummer') ? 'has-error' : '' }}" style="{{ $errors->has('btwnummer') ? 'display:block' : 'display:none' }} {{ old('land',$user->company->country_code) == 'NL' || old('land',$user->company->country_code) == null || old('land',$user->company->country_code) == '' ? '' : 'display:block;' }}">
								{!! Form::label('btwnummer', 'BTW nummer:') !!} <small>(Voer je BTW nummer in zonder leestekens)</small>
								{!! Form::text('btwnummer', old('btwnummer',$user->company->btw), array('class'=>'form-control','placeholder'=>'Vul het BTW nummer in zonder leestekens..')) !!}
								@if($errors->has('btwnummer'))
								<p class="alert alert-danger">{!! $errors->first('btwnummer') !!}</p>
								@endif
							</div>
							<div class="clearfix"></div>
							<hr>
							<div class="col-md-12">
								<h2>Bankgegevens:</h2>
							</div>
							<div class="clearfix"></div>
							<hr>
							<div class="col-md-4 form-group">
								{!! Form::label('tenaamstelling', 'Tennaamstelling:') !!}
								{!! Form::text('tenaamstelling', old('tenaamstelling',$user->company->appellation), array('class'=>'form-control', 'placeholder'=>'Ten name van..')) !!}
								@if($errors->has('tenaamstelling'))
								<p class="alert alert-danger">{!! $errors->first('tenaamstelling') !!}</p>
								@endif
							</div>
							<div class="col-md-4 form-group">
								{!! Form::label('iban', 'IBAN:') !!}
								{!! Form::text('iban', old('iban',$user->company->iban), array('class'=>'form-control', 'placeholder'=>'IBAN..')) !!}
								@if($errors->has('iban'))
								<p class="alert alert-danger">{!! $errors->first('iban') !!}</p>
								@endif
							</div>
							<div class="col-md-4 form-group">
								{!! Form::label('bic', 'BIC:') !!}
								{!! Form::text('bic', old('bic',$user->company->bic), array('class'=>'form-control', 'placeholder'=>'BIC..')) !!}
								@if($errors->has('bic'))
								<p class="alert alert-danger">{!! $errors->first('bic') !!}</p>
								@endif
							</div>
							<div class="clearfix"></div>
							<hr>
							<div class="col-md-12">
								{!! Form::reset('Reset..', array('class'=>'btn btn-danger btn-cons btn-large')) !!}
								{!! Form::submit('Vul mijn gegevens aan..', array('class'=>'btn btn-success btn-cons btn-large')) !!}
							</div>
							{!! Form::close() !!}
							</div>
							@elseif($user->affiliate->status == 1)
							<p>Je gegevens zijn succesvol ontvangen. We kijken je gegevens na en laten je per mail weten of we je aanvraag hebben goedgekeurd of niet.</p>
							@else
							@endif
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="{{ url('js/webarch/messenger.min.js') }}"></script>
<script src="{{ url('js/webarch/messenger-theme-flat.min.js') }}"></script>
<script>
	$(document).ready(function(){
		$('button#vul-aan').on('click', function(){
			$('div.company-credentials-form').slideToggle();
		});
		$('select[name=land]').on('change', function(){
			$('input[type=hidden][name=land_full]').val($('select[name=land] option:selected').text());
			if($(this).val() != 'NL') {
				$('div.col-md-12.form-group.btw-nummer').fadeIn();
			} else {
				$('div.col-md-12.form-group.btw-nummer').fadeOut();
			}
		});
	});
	function copyToClipboard(element) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($('#'+element).val()).select();
		document.execCommand("copy");
		$temp.remove();
		Messenger().post({
			message: 'Referentielink is gekopieerd naar je klembord!',
			loc: top,
			location: top,
			showCloseButton: true
		});
	}
</script>
<script>
document.getElementById('shareBtn').onclick = function() {
  FB.ui({
    method: 'share',
    mobile_iframe: true,
    href: 'https://www.socialpreneurs.io',
  }, function(response){});
}
</script>
@stop