@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')
<div id="page-header" class="social-intro">
  <div class="container">
    <div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 social-intro-content">
      <img src="{!! config('app.images.white') !!}" alt="{!! config('app.officialname') !!}" class="logo slideOutTop" data-class-in="slideInTop" data-class-out="slideOutTop" >
      <h2 class="slogan slideOutBottom" data-class-in="slideInRight" data-class-out="slideOutRight">{!! config('app.slogan') !!}</h2>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<div class="content">
          <!-- BEGIN PAGE TITLE -->
          {{-- <div class="page-title">
            <h3>Master Page</h3>
          </div> --}}
          <!-- END PAGE TITLE -->
          <!-- BEGIN PlACE PAGE CONTENT HERE -->

            @if($user->active == 1 || $user->active == 2)
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-colored-widget m-b-30">
              <div class="content-wrapper green">
                <div class="pull-left">
                  <h3 class="text-white">Cursussen</h3>
                </div>
                <div class="pull-right">
                  <i class="fa fa-play-circle-o fa-6x"></i>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="heading">
                <div class="">
                  {{-- <h4>&nbsp;</h4> --}}
                  <p><span class="label label-success">{!! $episodecount !!}</span> videos nog niet bekeken</p>
                </div>
                <div class="">
                  {{-- <span class="small-text muted">&nbsp;</span><br/> --}}
                  <a class="btn btn-white btn-cons" type="button" href="{{ route('member.get.cursussen') }}"><i class="fa fa-play"></i> &nbsp;Bekijken</a>
                </div>
              </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-colored-widget m-b-30">
              <div class="content-wrapper blue">
                <div class="pull-left">
                  <h3 class="text-white">Later bekijken</h3>
                </div>
                <div class="pull-right">
                  <i class="fa fa-clock-o fa-6x"></i>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="heading">
                <div class="">
                  {{-- <h4>&nbsp;</h4> --}}
                  <p><span class="label label-info">{!! $user->countWatchLaterEpisodes() !!}</span> videos later bekijken</p>
                </div>
                <div class="">
                  {{-- <span class="small-text muted">&nbsp;</span><br/> --}}
                  <a class="btn btn-white btn-cons" type="button" href="{{ route('get.watchlater.videos') }}"><i class="fa fa-play"></i> &nbsp;Bekijken</a>
                </div>
              </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-colored-widget m-b-30">
              <div class="content-wrapper red">
                <div class="pull-left">
                  <h3 class="text-white">Favoriete videos</h3>
                </div>
                <div class="pull-right">
                  <i class="fa fa-heart-o fa-6x"></i>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="heading">
                <div class="">
                  {{-- <h4>&nbsp;</h4> --}}
                  <p><span class="label label-danger">{!! $user->countFavoriteEpisodes() !!}</span> videos als favoriet</p>
                </div>
                <div class="">
                  {{-- <span class="small-text muted">&nbsp;</span><br/> --}}
                  <a class="btn btn-white btn-cons" type="button" href="{{ route('get.favorite.videos') }}"><i class="fa fa-play"></i> &nbsp;Bekijken</a>
                </div>
              </div>
            </div>
            @endif
            {{-- <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-colored-widget m-b-30">
              <div class="content-wrapper purple">
                <div class="pull-left">
                  <h3 class="text-white">Affiliate</h3>
                </div>
                <div class="pull-right">
                  <i class="fa fa-magic fa-6x"></i>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="heading">
                <div class="">
                  <p><span class="label label-purple">@if($user->isAffiliate()){!! $user->affiliate->countActiveSalesUsers() !!}@endif</span> affiliate sales</p>
                </div>
                <div class="">
                  <a class="btn btn-white btn-cons" type="button" href="{{ route('member.affiliate.index') }}"><i class="fa fa-users"></i> &nbsp;Bekijken</a>
                </div>
              </div>
            </div> --}}

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-colored-widget m-b-30">
              <div class="content-wrapper orange">
                <div class="pull-left">
                  <h3 class="text-white">Transacties</h3>
                </div>
                <div class="pull-right">
                  <i class="fa fa-exchange fa-6x"></i>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="heading">
                <div class="">
                  {{-- <h4>&nbsp;</h4> --}}
                  <p><span class="label label-warning">{!! $user->countOrders() !!}</span> transacties</p>
                </div>
                <div class="">
                  {{-- <span class="small-text muted">&nbsp;</span><br/> --}}
                  <a class="btn btn-white btn-cons" type="button" href="{{ route('get.member.orders') }}"><i class="fa fa-exchange"></i> &nbsp;Bekijken</a>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 single-colored-widget m-b-30">
              <div class="content-wrapper teal">
                <div class="pull-left">
                  <h3 class="text-white">Profiel</h3>
                </div>
                <div class="pull-right">
                  <i class="fa fa-user fa-6x"></i>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="heading">
                <div class="">
                  {{-- <h4>&nbsp;</h4> --}}
                  <p>Wijzig je gegevens</p>
                </div>
                <div class="">
                  {{-- <span class="small-text muted">&nbsp;</span><br/> --}}
                  <a class="btn btn-white btn-cons" type="button" href="{{ route('member.profile') }}"><i class="fa fa-edit"></i> &nbsp;Bewerken</a>
                </div>
              </div>
            </div>

          <!-- END PLACE PAGE CONTENT HERE -->
        </div>
@stop