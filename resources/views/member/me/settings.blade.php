@extends('layouts.back-end')

@section('css')
@stop

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="tabbable tabs-left">
				<ul class="" role="tablist">
					<p>Profiel</p>
					<li class="active">
						<a href="#tab1" role="tab" data-toggle="tab" aria-expanded="true">Tab 1</a>
					</li>
					<p>Subscription</p>
					<li>
						<a href="#tab2" role="tab" data-toggle="tab" aria-expanded="true">Tab 2</a>
					</li>
					<p>Billing</p>
					<li>
						<a href="#tab3" role="tab" data-toggle="tab" aria-expanded="true">Tab 3</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="tab1">
						<div class="row">
							<div class="col-md-12">
								<h1>Tab 1</h1>
								{!! Form::open(['route'=>['delete.member.subscription',$user->id], 'method'=>'DELETE']) !!}
								{!! Form::submit('jan') !!}
								{!! Form::close() !!}
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tab2">
						<div class="row">
							<div class="col-md-12">
								<h1>Tab 2</h1>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tab3">
						<div class="row">
							<div class="col-md-12">
								<h1>Tab 3</h1>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('footer')
@stop