@extends('layouts.back-end')

@section('css')
@stop

@section('menubar')
@include('includes.member.headerbar')
@stop
@section('sidebar')
@include('includes.member.sidebar')
@stop

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="tiles white col-md-12 no-padding">
				<div class="tiles green cover-pic-wrapper">
					{{-- <div class="overlayer bottom-right">
						<div class="overlayer-wrapper">
							<div class="padding-10 hidden-xs">
								<button class="btn btn-primary btn-small">
									<i class="fa fa-edit"></i> &nbsp;&nbsp; Bewerken
								</button>
							</div>
						</div>
					</div> --}}
					@if($user->cover) 
						<div class="user-cover-photo" style="
						background-image: url('{!! $user->cover !!}'); background-repeat: no-repeat; background-size: cover; background-position: center center; width: 100%;display:block;max-height:250px;">
							<img class="user-cover-photo" src="{!! $user->cover !!}" alt="Facebook Coverphoto {!! $user->first_name !!} {!! $user->last_name !!}" style="width:100%;opacity:0;">
						</div>
					@else 
						<div class="user-cover-photo" style="
						background-image: url('/images/site/bg/email-bg.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center; width: 100%;display:block;max-height:250px;">
							<img class="user-cover-photo" src="{{ url('/images/site/bg/email-bg.jpg') }}" alt="Socialpreneurs.io Coverphoto" style="width:100%;opacity:0;">
						</div>
					@endif
				</div>
				<div class="tiles white">
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div class="user-profile-pic">
								<img width="69" height="69" data-src-retina="{{ url($user->profile_image) }}" data-src="{{ url($user->profile_image) }}" src="{{ url($user->profile_image) }}" alt="{!! $user->first_name !!} {!! $user->last_name !!}">
							</div>
						</div>
						<div class="col-md-8 user-description-box col-sm-8">
							{{-- <div class="user-mini-description" style="padding-top:20px;"> --}}
							<h4 class="semi-bold no-margin">{!! $user->first_name !!} {!! $user->last_name !!}</h4>
							<h6 class="no-margin">{!! $user->company->company_name !!}</h6>
							<br/>
							<p>
								<i class="fa fa-envelope"></i>
								{!! $user->company->business_email !!} <i class="fa fa-info-circle" style="width:auto;color:#1d71b8; font-size:17px;" data-toggle="tooltip" data-original-title="De facturen worden naar dit emailadres verstuurd."></i>
							</p>
							<div class="clearfix"></div>
							<hr>
							<h5>Lid sinds:
								<span class="text-success semi-bold">
									{!! $user->created_at !!}
								</span>
							</h5>
							{{-- <h5>Affiliate: 
								<a href="{!! route('member.affiliate.index') !!}">
									
								@if($user->affiliate->status == 0)
		                        <span class="status status-inactive" data-toggle="tooltip" data-original-title="Je affiliate account is nog niet goedgekeurd. Vul de gegevens in op de affiliate pagina."></span>
		                        @elseif($user->affiliate->status == 1)
		                        <span class="status status-ending" data-toggle="tooltip" data-original-title="Je affiliate account is aangevraagd. Je ontvangt spoedig bericht van ons!"></span>
		                        @else
		                        <span class="status status-active" data-toggle="tooltip" data-original-title="Je affiliate account is actief! Laten we van start gaan!"></span>
		                        @endif
								</a>
							</h5> --}}
								
								
							{{-- </div> --}}
						</div>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="col-md-12 col-sm-12">
						<div class="closed-accord">
							<h3 class="normal">Bedrijfsgegevens: <button class="btn btn-primary btn-toggle" data-toggle="company-credentials"><span class="toggle">Open</span></button></h3>
						</div>
						<div class="company-credentials">
							<div class="col-md-7 user-description-box col-sm-7" style="margin:0;">
								<p>
									<i class="fa fa-user"></i>
									@if($user->company->title == 'm') Dhr. @else Mevr. @endif {!! $user->first_name !!} {!! $user->last_name !!}
								</p>
								<p>
									<i class="fa fa-envelope"></i>
									{!! $user->company->business_email !!}
								</p>
								<p>
									<i class="fa fa-map-marker"></i>
									{!! $user->company->address !!}, {!! $user->company->postal !!}, {!! $user->company->recidence !!}, {!! $user->company->country_full !!}
								</p>
								<p>
									<i class="fa fa-phone"></i>
									{!! $user->company->phone !!}
								</p>
								@if($user->company->mobile_phone)
								<p>
									<i class="fa fa-mobile-phone"></i>
									{!! $user->company->mobile_phone !!}
								</p>
								@endif
							</div>
							<div class="col-md-5 col-sm-5">
								<h3 class="normal">&nbsp;</h3>
								<h5>Klant ID: {!! $user->mollie_customer_id !!}</h5>
								<h5>Klantnummer: {!! $user->wefact_debtor_id !!}</h5>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
					@if($user->hasSubscription())
					<hr>
					<div class="col-md-12 col-sm-12" style="padding-bottom:15px;">
						<div class="closed-accord">
							<h3 class="normal">Abonnement: @if($user->hasSubscription())<i class="fa fa-check-circle" style="color:green;"></i>@else<i class="fa fa-times-circle" style="color:red;"></i>@endif <button class="btn btn-primary btn-toggle" data-toggle="abonnement-credentials"><span class="toggle">Open</span></button></h3>
						</div>
						<div class="abonnement-credentials">
							<div class="col-md-7 col-sm-7">
								<h5>Abonnement ID: {!! $user->mollie_subscription_id !!}</h5>
								<h5>Op het moment betaal je: &euro; <span class="text-success semi-bold">{!! $user->subscriptionPrice() !!}</span></h5>
								<h5>Eerst volgende betaling: {!! $user->nextSubscriptionPayment() !!}</h5>
							</div>
							<div class="col-md-5 col-sm-5">
								{{-- <h3 class="normal">&nbsp;</h3>
								<p></p> --}}
								<button class="btn btn-large btn-danger btn-cons pull-right" data-toggle="modal" data-target="#cancel-subscription">Stopzetten</button>
								<div class="modal fade" id="cancel-subscription" tabindex="-1" role="dialog" aria-labelledby="myMydalLabel" aria-hidden="true" style="display:none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
												<br>
												<h4 id="myModalLabel" class="semi-bold">Abonnement stopzetten</h4>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default btn-large btn-cons" data-dismiss="modal">Ik ben van gedachten veranderd..</button>
												{!! Form::open(['route'=>['delete.member.subscription',$user->id], 'method'=>'DELETE', 'class'=>'pull-right']) !!}
												{!! Form::submit('Ja, ik weet het zeker', array('class'=>'btn btn-danger btn-large btn-cons')) !!}
												{!! Form::close() !!}
											</div>

										</div>
									</div>
								</div>
								<br/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
					@elseif($user->plan_id != '3')
					<hr>
					<div class="col-md-12 col-sm-12" style="padding-bottom:15px;">
						<div class="closed-accord">
							<h3 class="normal">Abonnement: @if($user->hasSubscription())<i class="fa fa-check-circle" style="color:green;"></i>@else<i class="fa fa-times-circle" style="color:red;"></i>@endif <button class="btn btn-primary btn-toggle" data-toggle="abonnement-credentials"><span class="toggle">Open</span></button></h3>
						</div>
						<div class="abonnement-credentials">
							<div class="col-md-7 col-sm-7">
								<h5 class="alert alert-warning">Je hebt je abonnement stopgezet op: <span class="text-success semi-bold">{!! $user->getSubscriptionCancelDate() !!}</span></h5>
								<h5 class="alert alert-info">Je account verloopt {!! $user->getAccountEndDate() !!}</h5>
							</div>
							<div class="col-md-5 col-sm-5">
								<h3 class="normal">Opnieuw starten:</h3>
								<p></p>
								<button class="btn btn-large btn-success btn-cons pull-right" data-toggle="modal" data-target="#myModal">Plan kiezen</button>
								<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myMydalLabel" aria-hidden="true" style="display:none;">
									<div class="modal-dialog">
										<div class="modal-content">
											{!! Form::open(['route'=>'restart.member.subscription']) !!}
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
												<br>
												<h4 id="myModalLabel" class="semi-bold">Nieuw abonnement {!! config('app.officialname') !!}</h4>
											</div>
											<div class="modal-body">
												{!! Form::hidden('user_id',$user->id) !!}
												{{-- {!! Form::select('plan_id', array(null=>'Kies je plan..', '1' =>'Maandelijks - (&euro;47,- p/m) ex. BTW', '2' =>'Jaarlijks - (&euro;470,- p/j) ex. BTW'), old('plan_id'), array('class'=>'form-control')) !!} --}}
												{!! Form::select('plan_id', array(null=>'Kies je plan..', '1' =>'Maandelijks - (&euro;47,- p/m) ex. BTW'), old('plan_id'), array('class'=>'form-control')) !!}
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default btn-large btn-cons" data-dismiss="modal">Ik ben van gedachten veranderd..</button>
												{!! Form::submit('Ja, ik weet het zeker', array('class'=>'btn btn-success btn-large btn-cons')) !!}
											</div>
											{!! Form::close() !!}

										</div>
									</div>
								</div>
								<br/>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
					@else
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
<script>
	$(document).ready(function(){
		$('.btn-toggle').on('click', function() {
			var toggle = $(this).data('toggle');
			$('.'+toggle).slideToggle();
			var text = $(this).text();
			$(this).text(text == "Open" ? "Close" : "Open");
		});
	});
</script>
@stop