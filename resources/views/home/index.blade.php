@extends('layouts.front-end')

@section('css')
<link href="https://fonts.googleapis.com/css?family=Secular+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
@stop

@section('robots')
@stop

@section('fb_pixel')
	@include('includes.pixels.fb_pixel')
@stop

@section('nav')

{{-- <header id="header">
	<div class="container">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 logo-holder">
			<a href="{{ route('base.index') }}" class="logo">
				<img src="{{ url(config('app.images.normal')) }}" alt="{{ config('app.name') }}" class="logo">
			</a>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6 navbar-holder">
			<nav id="navbar" class="navbar pull-right">
				@if (Auth::guest())
					<a href="{{ url('/login') }}" 
					class="btn btn-login">
						<i class="fa fa-sign-in"></i> 
						Inloggen
					</a>
                @else
                	<a href="{{ route('member.index') }}"
                	class="btn btn-success">
                		<i class="fa fa-dashcube"></i>
                		Dashboard
                	</a>
					<a href="{{ url('/logout') }}" 
					class="btn btn-logout" 
					onclick="event.preventDefault();document.getElementById('logout-form').submit();">
						<i class="fa fa-sign-out"></i> 
						Uitloggen
					</a>
					<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
            	@endif
			</nav>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</header>

<section id="head-spacer"></section> --}}

@stop

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1928920830673076";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="login-wrapper">
	<div class="login-holder">
		@if (Auth::guest())
		<a class="the-lock" href="{{ url('/login') }}" title="Inloggen">
			<span>
					<i class="fa fa-sign-in"></i> 
			</span>
		</a>
		@else
		<a class="the-lock" href="{{ route('member.index') }}" title="Dashboard">
			<span>				
					<i class="fa fa-dashcube"></i>
			</span>
		</a>
		@endif
	</div>
</div>
{{-- <section id="floater">
	<div class="bonus-intro">
		<div class="container">
			<div class="pull-right mobiel"><button type="button" class="close" onclick="dismissFloater();" style="z-index:50;color:white;opacity:1;"><i class="fa fa-times"></i></button></div>
			<div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12" style="color:white;text-align:center;">
				<h2>13 Maart 2017 - Facebook Challenge!</h2>
				<hr class="desktop">
			</div>
			
			<div class="clearfix"></div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 desktop">
				<img src="/images/site/cursus/bonus/start-facebook-challenge.png" alt="13 Maart 2017 - Start de Facebook Challenge">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="color:white !important">
				<p class="desktop">Bouw samen met Facebook experts en gelijkgestemde ondernemers een winstgevende Facebook campagne in 10 dagen. Ontvang snel antwoord op je vragen en voorkom dat je onzeker start met adverteren op Facebook!</p>
				<ul>
					<li><span class="holder"><i class="fa fa-check"></i></span> Elke dag een heldere opdracht (±1 uur)</li>
					<li class="desktop"><span class="holder"><i class="fa fa-check"></i></span> Elke dag (60 min) LIVE op Facebook (+opnames)</li>
					<li><span class="holder"><i class="fa fa-check"></i></span> Krijg LIVE antwoord op jouw vragen</li>
					<li><span class="holder"><i class="fa fa-check"></i></span> Ontvang feedback van anderen in de Facebook groep</li>
					<li class="desktop"><span class="holder"><i class="fa fa-check"></i></span> Brainstormen met gelijkgestemde ondernemers</li>
					<li><span class="holder"><i class="fa fa-check"></i></span> [NIEUW] Facebook advertentie funnels</li>
				</ul>
				<p class="mobiel" style="margin-top:10px;">Word nu lid voor slechts &euro;47 p/m en transformeer je business binnen no-time.</p>
				<p class="desktop" style="margin-top:10px;">Word nu lid voor slechts &euro;47 p/m en transformeer je business binnen no-time, zodat je naast je business ook een bijdrage kunt leveren aan een mooiere wereld van morgen.</p>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12 desktop">
				<hr>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 timer-stuff" style="color:white;text-align:center;">
				<h4 class="mobiel">Je hebt nog:</h4>
				<h4 class="desktop">De Facebook Challenge voor Socialpreneurs leden start over:</h4>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-lg-8 col-md-8 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12">
						<div class="timer"></div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="floater-buttons">
					<button class="btn btn-cons floater-button no-ty desktop">Nee, dank je</button>
					<button class="btn btn-cons floater-button yes-ty">Yes ik wil knallen</button>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="bonus-intro-toggle">
		<div class="container">
			<div class="bonus-intro-toggle toggle-close">
				<div class="toggle-close">
					<i class="fa fa-caret-up"></i>
				</div>
				<div class="toggle-open" style="display:none;">
					<i class="fa fa-caret-down"></i>
				</div>
			</div>
		</div>
	</div>
</section> --}}
<section id="intro">
	<div class="container">
		<div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 intro-content">
			<img src="{!! config('app.images.normal') !!}" alt="{!! config('app.officialname') !!}" class="logo slideOutTop" data-class-in="slideInTop" data-class-out="slideOutTop" >
			<h1 class="slogan slideOutBottom" data-class-in="slideInRight" data-class-out="slideOutRight">{!! config('app.slogan') !!}</h1>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="focus">
	<div class="container">
		<div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 focus-content">
			<h2 class="slideOutLeft" data-class-in="slideInLeft" data-class-out="slideOutRight">Krijg jij er ook een energieboost van wanneer je een positieve impact hebt op iemands leven? </h2>
			<h4 class="slideOutRight" data-class-in="slideInRight" data-class-out="slideOutLeft">Wat zou het met jou doen als je dit vaker en op grotere schaal doet? Op zo'n grote schaal dat, als jij er niet meer bent, jouw acties nog steeds impact hebben. Dat is wat de ondernemers in de Socialpreneurs Community bezighoudt!</h4>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="call-to-action" class="green">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 call-to-action-content">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<h2 data-class-in="slideInLeft" data-class-out="slideOutLeft">Ik Wil Deel Uitmaken Van Deze Community</h2>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<a href="#aftik-form" class="btn btn-white btn-cons call-to-action" data-class-in="popIn" data-class-out="popOut">Lid Worden</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<!--<section id="voorwoord">
	<div class="container">
		<div class="col-lg-5 col-md-5 col-lg-offset-7 col-md-offset-7 col-sm-12 col-xs-12 voorwoord-box" data-class-in="slideInLeft" data-class-out="slideOutLeft">
			<h2>Voorwoord</h2>
			<p>Het is onze missie om minstens 10.000 inspirerende ondernemers, die hun steentje willen bijdragen aan een mooiere wereld, te helpen met hun marketing. Wij zijn steeds op zoek naar manieren hoe wij zelf een bijdrage kunnen leveren aan de wereld. Er zijn ook vele andere ondernemers die op hun manier binnen eigen omgeving, op een regionale, nationale of misschien zelfs op internationale schaal wat willen betekenen. Ondanks dat we natuurlijk nog geen idee hebben welke manieren hiervoor op ons pad zullen komen, weten we wel één manier hoe wij een bijdrage kunnen leveren. Dat is namelijk door het helpen met de marketing van inspirerende ondernemers. Wij komen te vaak enthousiaste ondernemers tegen die (vanwege andere specialisaties) niet de kennis in huis hebben om (de juiste) klanten aan te trekken waardoor zij zich niet kunnen focussen op hun corebusiness en de impact die ze willen maken. Dat is enorm zonde als je het ons vraagt. Wij willen graag ons steentje bijdragen door alles wat we in huis hebben qua marketingkennis door te geven aan deze community.</p>
		</div>
		<div class="clearfix"></div>
	</div>
</section> -->
<section id="voor-wie" data-class-in="slideInLeft" data-class-out="slideOutLeft">
	<div class="container">
		<div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 voor-wie-content"  >
			<h2>Voor Wie Is De Socialpreneurs Community?</h2>
			<h4>Ondernemers die het leven van hun doelgroep beter, leuke of eenvoudiger maken
&amp; andere ambitieuze ondernemers die wat willen betekenen voor hun omgeving
of zelfs op een regionale, nationale of zelfs internationale schaal een impact willen maken
</h4>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="verwachting">
	<div class="container">
		<div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 verwachting-content">
			<h2 data-class-in="fadeIn" data-class-out="fadeOut">Wat Je Kunt Verwachten Van De SocialPreneurs Community</h2>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 verwachting-box">
				<ul>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 icon-holder">
							<div class="icon">
								<i class="fa fa-check"></i>
							</div>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-holder">
							<h4>de 'Messenger Marketing' cursus</h4>
							<p>Zolang je lid bent van de community heb je toegang tot deze cursus. Hierin leer je hoe je Facebook advertenties op de automatische piloot laat draaien en doorlopend nieuwe klanten zich laten aanmelden voor jouw product of dienst. Zie hieronder meer over de inhoud van de cursus.</p>
						</div>
					</li>
					<li data-class-in="slideInRight" data-class-out="slideOutRight">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 icon-holder">
							<div class="icon">
								<i class="fa fa-check"></i>
							</div>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-holder">
							<h4>Bot flowchart-templates en tekst-templates</h4>
							<p>Dit is al het extra materiaal wat je nodig hebt voor een perfecte bots strategie. Ook krijg je inzicht welke tekst je moet gebruiken om alles uit jouw bot te halen.</p>
						</div>
					</li>
					<li data-class-in="slideInRight" data-class-out="slideOutRight">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 icon-holder">
							<div class="icon">
								<i class="fa fa-check"></i>
							</div>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-holder">
							<h4>besloten facebook groep</h4>
							<p>Je bent lid van ons netwerk van ambitieuze ondernemers die ook geïnteresseerd zijn in het automatiseren van hun marketing en hun steentje bij willen dragen aan een betere wereld van morgen. Deze groep dient de volgende doelen: het delen van kennis, meningen en feedback. Hier wordt gepraat over resultaten, progressie, marketingideeën en natuurlijk positieve verhalen en brainstormsessies.</p>
						</div>
					</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 icon-holder">
							<div class="icon">
								<i class="fa fa-check"></i>
							</div>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-holder">
							<h4>onweerstaanbare marketing boodschap</h4>
							<p>Oftewel copywriting, het schrijven van krachtige en overtuigende teksten die jouw doelgroep doen bewegen. Veel ondernemers zijn ont-zet-tend goed in wat ze doen, maar krijgen niet de aandacht van hun doelgroep die ze verdienen. Wij helpen jou aan de hand van de eenvoudige stappen in ons ‘Onweerstaanbare Marketing Boodschap’ model om telkens weer krachtige en overtuigende teksten te schrijven. Dit is toe te passen voor landingspagina’s, salespagina’s, advertenties, e-books en video’s.</p>
						</div>
					</li>
					<li data-class-in="slideInRight" data-class-out="slideOutRight">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 icon-holder">
							<div class="icon">
								<i class="fa fa-check"></i>
							</div>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-holder">
							<h4>toegang tot al ons ‘achter de schermen’ materiaal</h4>
							<p>Alle kennis die wij in huis hebben wordt met jou, achter gesloten deuren, gedeeld. Hierbij kun je denken aan (niet gelimiteerd tot): Nieuwste marketingstrategieën en -ontwikkelingen, Facebook live sessies, besloten webinars, hot-seats, interviews met inspirerende ondernemers, Facebook advertentietrainingen, marketing-automatisering, verkooppsychologie en inzage in Socialpreneurs strategieën achter de schermen. Deze informatie wordt alleen met de Socialpreneurs community gedeeld. We zullen alles geven en niets achterhouden!</p>
						</div>
					</li>
					<li data-class-in="slideInRight" data-class-out="slideOutRight">
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 icon-holder">
							<div class="icon">
								<i class="fa fa-check"></i>
							</div>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 text-holder">
							<h4>BONUS: Facebook Mastermind Cursus</h4>
							<p>Je krijgt alle ins en outs voor Facebook Marketing. </p>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="call-to-action" class="orange">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 call-to-action-content">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<h2 data-class-in="slideInLeft" data-class-out="slideOutLeft">Ik Wil Deel Uitmaken Van Deze Community</h2>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<a href="#aftik-form" class="btn btn-white btn-cons call-to-action" data-class-in="popIn" data-class-out="popOut">Lid Worden</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="modules">
	<div class="container">
		<div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 modules-content">
			<h2 data-class-in="slideInTop" data-class-out="slideOutTop">Cursus 'Messenger Marketing Machine' - De Modules</h2>
			<!--<img src="/images/site/cursus/Facebook-als-marketing-machine-40-DVD-box.png" alt="Facebook Als Marketing Machine Cursus" data-class-in="popIn" data-class-out="popOut">-->
			<?php
			$count = 0;
			$index = 1;
			?>
			@foreach($modules as $module)
			<div class="col-md-6 slideOutLeft">
				<h3>Module {!! $index !!} - {!! $module->title !!}</h3>
				<p>{!! $module->body !!}</p>
			</div>
			<?php $count++ ?>
			@if($count === 2)
				<div class="clearfix"></div>
				<div class="introductie"></div>
				<div class="clearfix"></div>
				<?php $count = 0; ?>
			@endif
			<?php $index++; ?>
			@endforeach
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="call-to-action" class="green">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 call-to-action-content">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<h2 data-class-in="slideInLeft" data-class-out="slideOutLeft">Ik Wil Deel Uitmaken Van Deze Community</h2>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<a href="#aftik-form" class="btn btn-white btn-cons call-to-action" data-class-in="popIn" data-class-out="popOut">Lid Worden</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="bonus">
	<div class="container">
		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 besloten-content">
			<h2 data-class-in="slideInBottom" data-class-out="slideOutBottom">De Besloten Facebook Groep</h2>
			<h4 data-class-in="slideInLeft" data-class-out="slideOutLeft">Bij de community hoort natuurlijk een besloten Facebookgroep. In deze groep vind je alleen ambitieuze ondernemers waarmee je kunt brainstormen en ideeën kunt uitwisselen.</h4>
		</div>
		<div class="clearfix"></div>
		<div class="bonus-content">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 bonus-info">
				<h4 data-class-in="slideInLeft" data-class-out="slideOutLeft">Een Online Community Waar Je:</h4>
				<ul class="blue-check">
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Om feedback kunt vragen zodat je zelfverzekerd verder kunt met het toepassen van wat je hebt geleerd (snel antwoord)</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Geïnspireerd raakt door creatieve ideeën van mede-ondernemers en communitymanagers</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Op de hoogte blijft van nieuwe ontwikkelingen en marketing van de toekomst</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Gemotiveerd wordt door ons team en enthousiaste mede-ondernemers die ook aan het knallen zijn</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Ontdekt hoe je naast marketing op Facebook ook andere marketingactiviteiten kunt automatiseren</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Geïnspireerd wordt door onderwerpen waarvan wij ons afvragen, "waarom we dat nooit op school hebben geleerd"</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Op de hoogte blijft van exclusieve events voor leden</li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 bonus-box" data-class-in="slideInLeft" data-class-out="slideOutLeft">
				<h4 data-class-in="slideInLeft" data-class-out="slideOutLeft">BONUS:</h4>
				<p data-class-in="slideInLeft" data-class-out="slideOutLeft">Toegang tot wekelijkse exclusieve webinars/live-sessies, video’s en blogs/artikelen over:</p>
				<ul class="white-check">
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Facebook Marketing (Uiteraard :D )</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Marketing Automatisering</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Verkoop Psychologie</li>
					<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Persoonlijke Ontwikkeling</li>
					<!--<li data-class-in="slideInLeft" data-class-out="slideOutLeft"><span class="holder"><i class="fa fa-check"></i></span> Beïnvloed 'Instituties' (terugkerende gedragspatronen)</li>-->
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<section id="recensies">
	<div class="container">
		<div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 recensies-content">
			<h2 data-class-in="slideInTop" data-class-out="slideOutTop">Recensies</h2>
			@if($reviews)
			<?php
				$c = 0;
			?>
			@foreach($reviews as $review)
			<div class="col-md-6 review">
				@if($review->video_link != null)
				@else
				<div class="col-md-12 review-name" data-class-in="slideInLeft" data-class-out="slideOutLeft">
					<h3 class="review-of">{!! $review->review_name !!} - <small><a>{!! $review->company !!}</a></small></h3>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-4 col-sm-4 col-xs-12 review-photo" data-class-in="popIn" data-class-out="popOut">
					<img src="{!! url($review->image) !!}" alt="{!! $review->review_name !!}" class="review-photo">
				</div>
				<div class="col-md-8 col-sm-8 col-xs-12 review-body" data-class-in="fadeIn" data-class-out="fadeOut">
					{!! $review->body !!}
				</div>
				@endif
			</div>
			<?php $c++ ?>
			@if($c === 2)
				<div class="clearfix"></div>
				<?php $c = 0; ?>
			@endif
			@endforeach
			@endif
			<div class="clearfix"></div>
		
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<!--<section id="video-recensies">
	<div class="container">
		<div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 video-recensies-content">
			@if($reviews)
			@foreach($reviews as $review)
			@if($review->video_link != null)
			<h2 data-class-in="slideInTop" data-class-out="slideOutTop">{!! $review->body !!}</h2>
			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 recensie-content-video">
				<h2 data-class-in="slideInLeft" data-class-out="slideOutLeft">{!! $review->review_name !!} - <small><a>{!! $review->company !!}</a></small></h2>
				<div class="review-video-holder" data-class-in="popIn" data-class-out="popOut">
					{!! $review->video_link !!}
				</div>
			</div>
			<div class="clearfix"></div>
			@else
			@endif
			@endforeach
			@endif
		</div>
		<div class="clearfix"></div>
	</div>
</section>-->
<section id="call-to-action" class="orange">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 call-to-action-content">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<h2 data-class-in="slideInLeft" data-class-out="slideOutLeft">Ik Wil Deel Uitmaken Van Deze Community</h2>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<a href="#aftik-form" class="btn btn-white btn-cons call-to-action" data-class-in="popIn" data-class-out="popOut">Lid Worden</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="FAQ">
	<div class="container">
		<div class="col-lg-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-12 col-xs-12 faq-content">
			<h2 data-class-in="slideInTop" data-class-out="slideOutTop">Mogelijke Vragen Die Je Nog Hebt</h2>
			<div class="faq-box" data-class-in="slideInLeft" data-class-out="slideOutLeft">
				<div class="question-content">
					<h4 class="question-title">
						<a>
							Wat nou als ik er niets van snap?
							<span class="holder">
								<div class="minus-1"></div>
								<div class="minus-2"></div>
							</span>
						</a>
					</h4>
				</div>
				<div class="answer-collapse">
					<div class="answer-content">
						<p>
							Deze cursus is volledig gericht op eenvoud. Je leert technieken zoals experts ze toepassen, maar dan in jip en janneke taal, stap voor stap uitgelegd.
						</p>
					</div>
				</div>
			</div>
			<div class="faq-box" data-class-in="slideInLeft" data-class-out="slideOutLeft">
				<div class="question-content">
					<h4 class="question-title">
						<a>
							Hoe ontvang ik de video's en cursus?
							<span class="holder">
								<div class="minus-1"></div>
								<div class="minus-2"></div>
							</span>
						</a>
					</h4>
				</div>
				<div class="answer-collapse">
					<div class="answer-content">
						<p>
							De opdrachten van de cursus staan online in ons besloten systeem. Je krijgt jouw eigen inloggegevens waarmee je altijd en overal kunt inloggen. Je houdt levenslang toegang tot de cursus inclusief alle updates.
						</p>
					</div>
				</div>
			</div>
			<div class="faq-box" data-class-in="slideInLeft" data-class-out="slideOutLeft">
				<div class="question-content">
					<h4 class="question-title">
						<a>
							Heb ik hier wel tijd voor?
							<span class="holder">
								<div class="minus-1"></div>
								<div class="minus-2"></div>
							</span>
						</a>
					</h4>
				</div>
				<div class="answer-collapse">
					<div class="answer-content">
						<p>
							Dit is een van de meest gestelde vragen. Juist daarom hebben we de cursus zo opgezet dat je binnen 10 dagen jouw strategie tot in de puntjes hebt uitgewerkt.
						</p>
					</div>
				</div>
			</div>
			<div class="faq-box" data-class-in="slideInLeft" data-class-out="slideOutLeft">
				<div class="question-content">
					<h4 class="question-title">
						<a>
							Wat als ik niet tevreden ben?
							<span class="holder">
								<div class="minus-1"></div>
								<div class="minus-2"></div>
							</span>
						</a>
					</h4>
				</div>
				<div class="answer-collapse">
					<div class="answer-content">
						<p>
							Wij zijn ons ontzettend goed bewust van wat er qua aanbod is op het gebied van omzet genereren door middel van Facebook marketing.  Mocht jij ergens anders een cursus vinden die gericht is op meer omzet binnenhalen door het inzetten van Facebook Marketing, en je kunt dit aantonen, dan krijg jij jouw volledige investering terug gestort op jouw rekening. Dit geldt binnen de eerste 30 dagen nadat je toegang hebt tot de cursus.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="investering">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 investering-content">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 investering-text">
				<h2 data-class-in="slideInTop" data-class-out="slideOutTop">Jouw Investering In De Community</h2>
				<p data-class-in="slideInLeft" data-class-out="slideOutLeft">Om voor jou tot een interessante en schappelijke investering te komen hebben we avonden gebrainstormd. Ook hebben we in ons netwerk en aan onze klanten gevraagd wat zij een reële investering zouden vinden. We kregen de meest uiteenlopende antwoorden terug. Van eenmalige investeringen van €2000 tot maandelijkse investeringen van €100 à €200 per maand. Wij begrijpen absoluut dat dit enorme investeringen zijn, dat wilden we juist voorkomen. Om er een no-brainer van te maken, en er tegelijkertijd voor te zorgen om alleen maar enthousiaste en gemotiveerde ondernemers in de community te houden, hebben we besloten om de investering vast te zetten op €47,- per maand.</p>
			</div>
			<div class="col-lg-4 col-md-4 col-lg-offset-2 col-md-offset-2 col-sm-12 col-xs-12 prijs-image">
				<div class="prijs-image-holder">
					<img src="/images/site/cursus/de-prijs.png" alt="">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="call-to-action" class="blue">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 call-to-action-content">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				<h2 data-class-in="slideInLeft" data-class-out="slideOutLeft">Ik Wil Deel Uitmaken Van Deze Community</h2>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<a href="#aftik-form" class="btn btn-white btn-cons call-to-action" data-class-in="popIn" data-class-out="popOut">Lid Worden</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="aftik-form">
	<div id="facebook-login-modal" style="display:none;">
		<div class="facebook-modal">
			<div class="tiles white col-lg-4 col-md-4 col-xs-12 col-sm-12 no-padding">
				<div class="p-t-30 p-b-30">
					<img src="{{ url(config('app.images.normal')) }}" title="{{ config('app.name') }}" alt="{{ config('app.name') }}" class="app-image">
				</div>
				
				<div class="tiles grey p-l-40 p-t-20 p-b-30">
					<h2 style="color:#231f20;">Inloggen met Facebook</h2>
					<h5 style="color:#231f20;"><b>Belangrijk:</b> zet de toestemming voor email a.u.b. <u>niet</u> uit!<br>Dit is nodig om later in te loggen.</h5>
					{{-- <span class="small-text muted">Dit is nodig voor een later stadium</span> --}}
					<fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>
				</div>
			</div>
		</div>
	</div>

	<div class="container plan-selection">
		<div class="col-md-8 col-md-offset-2 select-plan">
			<h2>Laten we van start gaan!</h2>
			<h4>Aanmelden kost minder dan een minuut en je kan direct aan de slag. Als je nog twijfelt, we hebben een "maandelijks" abonnement, en het is niet zoals een telefoonabonnement - je kan het abonnement op elk moment stopzetten.</h4>
		@foreach($plans as $plan)
		<div class="col-md-6">
			<div class="plan {!! $plan->type !!}-plan">
				<div class="plan-type">
					<h3>{!! $plan->title !!}</h3>
				</div>
				<div class="plan-price">
					<span class="dollar">&euro;</span>
					<span>{{ number_format($plan->price, 0) }}</span>
					<span class="btw">(ex. BTW)</span>
					{{-- <h2>&euro; {{ number_format($plan->price, 0) }},-</h2>
					<h5>per {!! $plan->type !!}</h5> --}}
				</div>
				<div class="plan-description text">
					<p>{!! $plan->show_description !!}</p>
				</div>
				<div class="plan-button">
					<button class="btn select-plan-button select-plan-{{ $plan->id }}" data-plan-name="{!! $plan->title !!}" data-plan-type="{!! $plan->type !!}" data-plan-id="{!! $plan->id !!}" data-plan-price="{!! number_format($plan->price, 0) !!}">Ik wil direct aan de slag</button>
				</div>
			</div>
		</div>
		@endforeach
		</div>
		<div class="clearfix"></div>
		<div style="height: 100px;"></div>
		<div id="status"></div>
		<div id="checkout-form" style="display:none;">
		<hr>
			{!! Form::open(array('route'=>'base.store')) !!}
			@if(count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{!! $error !!}</li>
						@endforeach
					</ul>
				</div>
			@endif
			@include('includes.forms.aftikform')
			{!! Form::submit('Bestellen', array('class'=>'btn btn-success')) !!}
			{!! Form::close() !!}
		</div>
	</div>
</section>




@stop

@section('footer')
<footer id="footer">
	<div class="container">
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 footer-logo">
			<img src="{!! config('app.images.whiteimage') !!}" alt="{!! config('app.officialname') !!}" data-class-in="slideInLeft" data-class-out="slideOutLeft">
		</div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 footer-content">
			<h2 data-class-in="fadeIn" data-class-out="fadeOut">Disclaimer</h2>
			<p data-class-in="fadeIn" data-class-out="fadeOut">Op deze pagina vertellen we hoe sommige community leden een aantal succesvolle marketing campagnes hebben opgezet. Sommige van deze leden benoemen specifieke resultaten die ze behaald hebben met de kennis uit het platform, de ‘Messenger Marketing' cursus en de community. Alles bij elkaar opgeteld loopt dit op tot in de honderdduizenden euro’s.</p>
			<p data-class-in="fadeIn" data-class-out="fadeOut">Houd er rekening mee dat we absoluut niet claimen dat deze resultaten standaard zijn en dat jij deze gegarandeerd zult halen. Het zou belachelijk zijn om zoiets te claimen. Helemaal omdat we elkaar (waarschijnlijk) nooit ontmoet hebben, en omdat we niets weten van jouw product, jouw business, prijsklasse of wat je nog meer doet.</p>
			<p data-class-in="fadeIn" data-class-out="fadeOut">Alleen met het investeren in deze community ga je geen honderdduizenden euro’s verdienen. Alleen het video materiaal kijken is niet voldoende. Serieuze keiharde inzet en uitvoering is nodig van jouw kant. Alleen JIJ kan dat doen!</p>
			<p data-class-in="fadeIn" data-class-out="fadeOut">Dat gezegd hebbende, wat we onthullen, met je delen en jou leren binnen het platform en de community heeft voor ons en de leden enorme resultaten opgeleverd. Dat is waarom we jou de kans geven om onze methodes in jouw business te TESTEN, en je de mogelijkheid geven om je lidmaatschap maandelijks op te zeggen. We zijn ons er van bewust dat er hierdoor ook ondernemers zullen zijn die proberen alle waardevolle kennis tot zich te nemen in de 1e maand en daarna het lidmaatschap stopzetten. Wij nemen dit risico bewust voor eigen rekening, zodat er alleen ambitieuze, enthousiaste en inspirerende ondernemers in de community blijven.</p>
			<p data-class-in="fadeIn" data-class-out="fadeOut">Kortom: je hebt niets te verliezen en zo enorm veel winst te behalen. We hopen je in de community te zien, en hopen natuurlijk ook dat je jouw verbluffende resultaten met ons deelt.</p>
		</div>
		<div class="clearfix"></div>
		<hr class="footer">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer-digitus">
			<span class="onderdeel">
				<p>{!! config('app.officialname') !!}</p>
			</span>
		</div>
	</div>
</footer>
<!--<div id="fb-chat" class="loading">
	<div class="chat-label">
		<h4 class="desktop"><div><span class="pull-left"><i class="fa fa-comment"></i></span> Heb je een vraag? </div></h4>
		<h4 class="mobiel"> <div class="comment"><span><i class="fa fa-comment"></i></span></div></h4>
		<h4 class="mobiel"><div class="times"><span><i class="fa fa-times"></i></span></div></h4>
	</div>
	<div class="chat-box">
		<div class="fb-page" data-href="https://www.facebook.com/digitusmarketing" data-tabs="messages" data-width="296" data-height="296" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/digitusmarketing" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/digitusmarketing">Digitus Marketing</a></blockquote></div>
	</div>
</div>-->
@stop

@section('footerscript')
{{-- {!! Form::open(array('route'=>'my.affiliate.link', 'id'=>'a-form', 'style'=>'display:hidden')) !!}
{!! Form::hidden('affiliate_code') !!}
{!! Form::close() !!} --}}
{{-- <form action="{!! route('my.affiliate.link') !!}" id="a-form" style="display:hidden;">
	<input type="hidden" name="affiliate_code">
</form> --}}
{{-- <script src="{!! url('js/jquery.inview.js') !!}"></script> --}}
<script src="{!! url('js/jquery.countdown.js') !!}"></script>
@if(count($errors) > 0)
<script>
	setTimeout(function(){
		// checkLoginState();
		$('section#aftik-form').slideDown();
		var body = $('html,body');
		body.stop().animate({scrollTop: $('section#aftik-form').offset().top}, 1000);
		if($('.paymentmethod').hasClass('selected')) {
			$('.paymentmethod.selected').click();
		}
		var plan_id = {{ old('plan_id') }};
		if(plan_id) {
			setTimeout(function() {
				$('.select-plan-button.select-plan-'+plan_id).click();
			}, 1200);
		}
	},1000);
</script>
@endif
{{-- <script>
	$(".timer")
    .countdown('2017-03-13 13:00:00', function(event) {
      $(this).html(
        event.strftime('<div class="dagen"><p class="cijfer">%-D</p><p class="letter">dagen</p></div><div class="uren"><p class="cijfer">%-H</p><p class="letter">uur</p></div><div class="minuten"><p class="cijfer">%-M</p><p class="letter">minuten</p></div><div class="seconden"><p class="cijfer">%-S</p><p class="letter">seconden</p></div>'));
    });

</script> --}}
<script>
function dismissFloater() {
	$('.bonus-intro-toggle').click();
}

$(document).ready(function() {

	$('a').on('click', function(event){
		// var target = $(this.getAttribute('href'));
		// if(target.length){
		if(this.hash !== "") {
			event.preventDefault();
			var hash = this.hash;
			$(hash).slideDown();
			console.log($(hash));
			console.log($(hash).offset().top);
			console.log($('html, body').animate({
				// scrollTop: target.offset().top
				scrollTop: $(hash).offset().top
			}, 1000));
			// target.slideDown();
			// console.log(target);
			// console.log(target.offset().top);
			$('html').animate({
				// scrollTop: target.offset().top
				scrollTop: $(hash).offset().top
			}, 1000);
			$('body').animate({
				// scrollTop: target.offset().top
				scrollTop: $(hash).offset().top
			}, 1000);
		}
		// console.log(this);
		// console.log($('section#aftik-form'));
		// $('section#aftik-form').slideDown();
		// console.log($('section#aftik-form').offset().top);
		// var body = $('html,body');
		// $('html,body').stop().animate({scrollTop: $('section#aftik-form').offset().top}, 1000);
		// $('html,body').animate({scrollTop: $('section#aftik-form').offset().top}, 1000);
		// body.stop().animate({scrollTop: $('section#aftik-form').offset().top}, 1000);
		// body.animate({scrollTop: $('section#aftik-form').offset().top}, 1000);
	});
	$('button.floater-button.yes-ty').on('click', function(){
		$('.bonus-intro-toggle').click();
		$('section#aftik-form').slideDown();
		var body = $('html,body');
		body.stop().animate({scrollTop: $('section#aftik-form').offset().top}, 1000);
	});
	$('button.floater-button.no-ty').on('click', function(e) {
		e.preventDefault();
		$('.bonus-intro-toggle').click();
	});
	// setTimeout(function() {
	// 	$('section#floater').animate({'top':'0'}, "slow").addClass('opened');
	// }, 4000);
	// $('.bonus-intro-toggle').on('click', function(){
	// 	var height = $('div.bonus-intro').innerHeight();
	// 	if($('.bonus-intro-toggle').hasClass('toggle-close')) {
	// 		$('section#floater').animate({'top':-height+'px'}).removeClass('opened');
	// 		$(this).removeClass('toggle-close').addClass('toggle-open');
	// 		$(this).children('.toggle-open').show();
	// 		$(this).children('.toggle-close').hide();
	// 	} else if($('.bonus-intro-toggle').hasClass('toggle-open')) {
	// 		$('section#floater').animate({'top':'0px'}).addClass('opened');
	// 		$(this).removeClass('toggle-open').addClass('toggle-close');
	// 		$(this).children('.toggle-open').hide();
	// 		$(this).children('.toggle-close').show();
	// 	}
	// });

	// setTimeout(function(){
	// 	$('div#fb-chat').removeClass('loading');
	// }, 10000);
	// setTimeout(function(){
	// 	$('div.login-wrapper').animate({'right':'5px'});
	// }, 5000);
	// $('div#fb-chat').on('click', function(){
	// 	$(this).toggleClass('open');
	// 	// $(this).children('.chat-box').slideToggle('fast');
	// });

	$('.plan.jaar-plan').addClass('disabled');
	$('.plan.jaar-plan div.plan-button button.select-plan-button').addClass('disabled').attr('disabled');
	$('.plan.jaar-plan div.plan-button button').attr('data-plan-name','').attr('data-plan-type','').attr('data-plan-id','').attr('data-plan-price','');
	// var header_height = $('header#header').height();
	// $('section#head-spacer').css('height', header_height);
	// $(window).resize(function(){
	// 	var header_height = $('header#header').height();
	// 	$('section#head-spacer').css('height', header_height);
	// });

	// Set proper paymentmethod
	$('div.paymentmethod').on('click', function() {
	  if($('div.paymentmethod').hasClass('selected')) {
	    $('div.paymentmethod.selected > input').prop('checked',false);
	    $('div.paymentmethod.selected > p.label').removeClass('label-success').addClass('label-info');
	    $('div.paymentmethod.selected').removeClass('selected');
	  }
	  $(this).addClass('selected');
	  $(this).children('input').prop('checked', true);
	  $(this).children('p.label').removeClass('label-info');
	  $(this).children('p.label').addClass('label-success');
	  $('span.de-betaalmethode').removeClass('label-danger').addClass('label-success');
	  $('span.de-betaalmethode').text($('input.paymentmethod:checked').data('title'));

	  // If paying with iDEAL, show issuers select input
	  if($('input[name=betaalmethode]:checked').val() == 'ideal') {
	    $('.if-ideal').fadeIn();
	  } else {
	    $('.if-ideal').fadeOut();
	  }
	  $('input[name=paymentmethod]').val($('input[name=betaalmethode]:checked').val());

	});

	$('select[name=land]').on('change', function(){
		$('input[type=hidden][name=land_full]').val($('select[name=land] option:selected').text());
		if($(this).val() != 'NL') {
			$('div.col-md-12.form-group.btw-nummer').fadeIn();
		} else {
			$('div.col-md-12.form-group.btw-nummer').fadeOut();
		}
	});

	$('input[name=btwnummer]').on('blur',function(){
		// console.log(length);
		$(this).val($(this).val().replace(/\./g,'').replace(/,/g,'').replace(/ /g,'').replace(/\s/g,''));
		// $(this).val($(this).val().replace(' ','').replace(',','').replace('.',''));
	});

	$('.select-plan-button').on('click', function() {
		if($(this).hasClass('disabled')) {

		} else {
			var plan_id = $(this).data('plan-id');
			var plan_price = $(this).data('plan-price');
			var plan_name = $(this).data('plan-name');
			var plan_type = $(this).data('plan-type');
			if($('.plan').hasClass('selected-plan') === true) {
				$('.plan').removeClass('selected-plan');
				$('.plan').removeClass('disabled-plan');
			}
			$(this).parents('.plan').addClass('selected-plan');
			if($('.plan').hasClass('selected-plan') === false) {
				$('.plan').addClass('disabled-plan');
			}
			$('input[type=hidden][name=plan_id]').val(plan_id);
			$('input[type=hidden][name=bedrag]').val(plan_price);
			$('div.selected-plan').show();
			$('div.selected-plan div.plan-type > h3').text(plan_name);
			$('div.selected-plan div.plan-price span.plan-price').text(plan_price);
			// $('span.plan-type').text(plan_type);
			checkLoginState();
			$('#facebook-login-modal').fadeIn();
		}
		
	});

	$('.faq-box').on('click', function(){
		$(this).toggleClass('open');
		$(this).children('.answer-collapse').slideToggle();
	});

	setTimeout(function(){
		$('.rollIn').css({'visibility':'visible'});
		$('#intro div div *').each(function(index){
			$(this).delay(300*index).removeClass('slideOutTop').removeClass('slideOutBottom');
		});
		setTimeout(function(){
			$('#focus div div *').each(function(index){
				$('h2').delay(300*index).removeClass('slideOutLeft')
				$('h4').delay(600*index).removeClass('slideOutRight');
			});
		}, 500);
	}, 400);


	


	$('.recensie-content-video').each(function(){
		$(this).children('div.review-video-holder').children('iframe.iframe-top').css({'height':($(this).width() * 0.56)});
		$(window).resize(function(){
			$(this).children('div.review-video-holder').children('iframe.iframe-top').css({'height':($(this).width() * 0.56)});
		});
	});

	var getQueryString = function ( field, url ) {
	    var href = url ? url : window.location.href;
	    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
	    var string = reg.exec(href);
	    return string ? string[1] : null;
	};

	var affiliate_link = getQueryString('a');
	if(affiliate_link != null) {

		$('form#a-form input[name=affiliate_code][type=hidden]').val(affiliate_link);
		var url = $('form#a-form').attr('action');

		$.ajax({
			type: 'POST',
			url: url,
			data: $('form#a-form').serialize(),
			success: function(data){
				if(data.success === false) {
					// episode is not completed
					console.log('false');
				} else {
					// episode is completed
					console.log('true');
					var href = window.location.href,
					newUrl = href.substring(0, href.indexOf('?'))
					window.history.replaceState({}, '', newUrl);
				}
			},
	    });
	    
		console.log(affiliate_link);

	}

});
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {

    // console.log('statusChangeCallback');

    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '1928920830673076',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use graph api version 2.8
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  // FB.getLoginStatus(function(response) {
  //   statusChangeCallback(response);
  // });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/nl_NL/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=name,first_name,last_name,email,gender,id,cover', function(response) {
    	console.log(response);
		// console.log('Successful login for: ' + response.name);
		// document.getElementById('status').innerHTML =
		// 'Thanks for logging in, ' + response.first_name + ' ' + response.last_name + '!';
		var voornaam = response.first_name;
		var achternaam = response.last_name;
		var email = response.email;
		var gender = response.gender;
		var fb_id = response.id;
		if(response.cover) {
			var cover = response.cover.source;
		} else {
			var cover = '#';
		}
		$('#facebook-login-modal').hide();
		$('#checkout-form').show();
		$('input[name=voornaam]').val(voornaam);
		$('input[name=achternaam]').val(achternaam);
		$('input[name=email]').val(email);
		$('input[name=fb_u_id]').val(fb_id);
		$('input[name=cover]').val(cover);
		// console.log(gender);
    });
    FB.api('/me/picture', function(response) {
        console.log(response);
        // console.log('Successful login for: ' + response.name);
        // document.getElementById('status').innerHTML =
        // 'Thanks for logging in, ' + response.first_name + ' ' + response.last_name + '!';
        if(response.data) {
	        var picture_url = response.data.url;
        } else {
        	var picture_url = '#';
        }
        $('input[name=picture_url]').val(picture_url);
        // console.log(gender);
    });
    setTimeout(function(){
		var body = $('html,body');
		body.stop().animate({scrollTop: $('div#checkout-form').offset().top}, 1000);
	}, 1000);
  }
</script>
@stop