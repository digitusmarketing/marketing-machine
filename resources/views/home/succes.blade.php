@extends('layouts.front-end')

@section('title')
{{ $sitetitel }} | 
@stop

@section('css')
<link href="https://fonts.googleapis.com/css?family=Secular+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
@stop

@section('robots')
@include('includes.noindexnofollow')
@stop

@section('fb_pixel')
<?php $extra = "fbq('track', 'Lead');"; ?>
	@include('includes.pixels.fb_pixel', array('extra'=>$extra))
@stop

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1928920830673076";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<section id="intro">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 intro-content">
			<h1 class="slogan">{!! $sitetitel !!}</h1>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section id="thankyou">
	<div class="container">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 thankyou-content popOut popIn">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 popOut popIn">
					{!! $icon !!}
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 thankyou-tekst">
					<h2 class="slideOutLeft slideInLeft">{!! $titel !!}</h2>
					<p class="slideOutLeft slideInLeft">{!! $message !!}</p>
					@if(isset($fb_groep))
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 slideOutLeft slideInLeft">
							<p>Je kunt nu lid worden van de Facebook groep en inloggen op het platform:</p>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<a href="{!! $fb_groep !!}" target="_blank" class="btn btn-white btn-cons ty-page blue popOut popIn"><i class="fa fa-facebook"></i>&nbsp;  Lid worden</a>
							<a href="{!! url('/login') !!}" class="btn btn-white btn-cons ty-page green popOut popIn"><i class="fa fa-sign-in"></i>&nbsp;  Inloggen</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
					@endif
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>

		</div>
		<div class="clearfix"></div>
	</div>
</section>
@stop

@section('footer')
<footer id="footer">
	<div class="container">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3 thankyou-footer-content">
			<img src="{!! config('app.images.white') !!}" alt="{!! config('app.officialname') !!}">
		</div>
		<div class="clearfix"></div>
		<hr class="footer">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer-digitus">
			<span class="onderdeel">
				<p>{!! config('app.officialname') !!} is een onderdeel van <a href="https://www.digitusmarketing.nl" target="_blank">Digitus Marketing</a></p>
			</span>
		</div>
	</div>
</footer>
<div id="fb-chat" class="loading">
	<div class="chat-label">
		<h4 class="desktop"><div><span class="pull-left"><i class="fa fa-comment"></i></span> Heb je een vraag? </div></h4>
		<h4 class="mobiel"> <div class="comment"><span><i class="fa fa-comment"></i></span></div></h4>
		<h4 class="mobiel"><div class="times"><span><i class="fa fa-times"></i></span></div></h4>
	</div>
	<div class="chat-box">
		<div class="fb-page" data-href="https://www.facebook.com/digitusmarketing" data-tabs="messages" data-width="296" data-height="296" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/digitusmarketing" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/digitusmarketing">Digitus Marketing</a></blockquote></div>
	</div>
</div>
@stop

@section('footerscript')
<script>
$(document).ready(function(){

	setTimeout(function(){
		$('div#fb-chat').removeClass('loading');
	}, 10000);
	$('div#fb-chat').on('click', function(){
		$(this).toggleClass('open');
		// $(this).children('.chat-box').slideToggle('fast');
	});

	$('.thankyou-content.popIn').removeClass('popOut');

	setTimeout(function() {
		$('div.popIn').each(function(){
			$(this).removeClass('popOut');
		});
	}, 800);
	setTimeout(function() {
		$('h2.slideInLeft').each(function(){
			$(this).removeClass('slideOutLeft');
		});
		$('p.slideInLeft').each(function(index){
			$(this).delay(500*index).removeClass('slideOutLeft');
		});
		$('div.slideInLeft').each(function(){
			$(this).removeClass('slideOutLeft');
		});
	}, 1200);
	setTimeout(function() {
		$('a.popIn').each(function(index){
			$(this).delay(500*index).removeClass('popOut');
		});
	}, 1800)

});
</script>
@stop