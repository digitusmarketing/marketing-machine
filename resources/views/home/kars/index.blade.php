@extends('layouts.front-end')

@section('css')
<link href="https://fonts.googleapis.com/css?family=Secular+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<style>
	body{
		background:#eee;
	}
	header#header-betalen {
		position:relative;
		background: #1d71b8;
		padding:0;
		padding-top:6%;
	}
	section#content {
		padding-bottom: 6%;
	}
	.content-holder{
		background:#FFFFFF;
		float:left;
	}
	.header-holder{
		background:#eee;
		padding:15px;
	}
	.extra-holder {
	    background: rgba(237,236,111,0.5);
	    float: right;
	    padding-top: 40px;
	}
	.extra-holder .wyg {
		width:100%;
	}
	.extra-holder .wyg h3 {
		margin-top:0;
		text-align:center;
		margin-bottom:40px;
	}
	.extra-holder .wyg p {
	    text-align: center;
	    margin: 20px 0 40px;
	    font-weight: bold;
	    font-style: italic;
	}
	.extra-holder .wyg ul.checkmark-green {
		list-style-image: url('/images/site/checkmark-green.png');
	}
	.extra-holder .wyg ul.checkmark-green li {
		padding: 10px;
	}
	.extra-bottom-holder {
		background: #f6f6f6; /* For browsers that do not support gradients */
	    background: -webkit-linear-gradient(#f6f6f6, #ebebeb); /* For Safari 5.1 to 6.0 */
	    background: -o-linear-gradient(#f6f6f6, #ebebeb); /* For Opera 11.1 to 12.0 */
	    background: -moz-linear-gradient(#f6f6f6, #ebebeb); /* For Firefox 3.6 to 15 */
	    background: linear-gradient(#f6f6f6, #ebebeb); /* Standard syntax */
		display: flex;
	}
	.form-holder {
		padding: 40px 15px 20px;
	}
	.form-holder h2 {
		margin-top: 0;
	}
	.bottom-holder {
		border-width: 2px 1px 1px;
		border-style: solid;
		text-align: center;
	}
	.bottom-holder > p {
	    font-size: 12px;
	    font-style: italic;
	    color: #231f20;
	}
	.guarantee-holder {
		border-color: #1d71b8 #eee;
	}
	.privacy-holder {
		border-color: #3aaa35 #eee;
	}
	.secure-holder {
		border-color: #e94e1b #eee;
	}
	span.asterisk {
		color: orange;
		font-weight:bold;
	}
	.pay-btn,
	.pay-btn:active,
	.pay-btn:focus {
		margin-top: -65px;
		font-size:16px;
		background: #1a71b8 !important;
		background-color: #1a71b8 !important;
		border: 4px solid #1a71b8;
		color:white;
	    text-transform: uppercase;
	    padding: 12px 41px !important;
	    -webkit-transition: all 0.2s ease 0s;
	    -moz-transition: all 0.2s ease 0s;
	    transition: all 0.2s ease 0s;
	}
	.pay-btn:hover {
		background: white !important;
		background-color: white !important;
		color: #1d71b8 !important;
		border: 4px solid #1a71b8;
		-webkit-transition: all 0.2s ease 0s;
		-moz-transition: all 0.2s ease 0s;
		transition: all 0.2s ease 0s;
	}
</style>
@stop

@section('robots')
@stop

@section('fb_pixel')
	@include('includes.pixels.fb_pixel')
@stop

@section('content')

	<header id="header-betalen">
		<div class="container">
			<div class="row">
				<div class="col-md-12 header-holder">
					<div class="col-md-2">
						<img src="{!! url('/images/site/bot-image.png') !!}" alt="cursus image" class="cursus">
					</div>
					<div class="col-md-8">
						<h1>Messenger Marketing Cursus</h1>
						<p>Je staat op het punt om een geweldige dimensie aan je business te geven.</p>
						<p>Als 'early adopter' ben jij je concurrent een grote stap voor. Het in contact komen met jouw potentiële klanten gaat je enorm veel tijd besparen.</p>
					</div>
					<div class="col-md-2">
						<img src="{!! url('/images/site/logo-official.png') !!}" alt="socialpreneurs image" class="socialpreneurs">
					</div>
				</div>
			</div>
		</div>
	</header>
	<section id="content">
		<div class="container">
			<div class="row content-holder">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-4 extra-holder">
							<div class="wyg">
								<h3>Jouw cursus bevat:</h3>
								<ul class="checkmark-green">
									<li>Volledige 'Messenger Marketing' Cursus'</li>
									<li>Bot Flowchart-Templates En Tekst-Templates</li>
									<li>Besloten Facebook Groep</li>
									<li>Onweerstaanbare Marketing Boodschap</li>
									<li>Toegang Tot Al Ons ‘Achter De Schermen’ Materiaal</li>
									<li>Bonus: Facebook Mastermind Cursus</li>
								</ul>
								<p style="text-align:center;margin:">...Tot aan de andere kant!</p>
							</div>
							{{-- <div class="recensie">
								<p>Korte snelle recensie</p>
							</div> --}}
							<div class="row extra-bottom-holder">
								<div class="col-md-4 bottom-holder guarantee-holder">
									<img src="{!! url('/images/site/guarantee.png') !!}" alt="garantie image" class="garantie">
									<p class="garantie">30-dagen-geld-terug garantie</p>
								</div>
								<div class="col-md-4 bottom-holder privacy-holder">
									<img src="{!! url('/images/site/privacy.png') !!}" alt="privacy image" class="privacy">
									<p class="privacy">Socialpreneurs is 100% privacy-safe</p>
								</div>
								<div class="col-md-4 bottom-holder secure-holder">
									<img src="{!! url('/images/site/secure.png') !!}" alt="secure image" class="secure">
									<p class="secure">Veilig,  betrouwbaar en transparant</p>
								</div>
							</div>
						</div>
						<div class="col-md-8 form-holder">
							<h2>Contact Informatie</h2>
							<small>Velden gemarkeerd met een <span class="asterisk">*</span> zijn verplicht</small>
							<hr>
							{!! Form::open() !!}
							@include('includes.forms.aftikform2')
							<div class="clearfix"></div>
							<div class="form-group col-md-12">
								{!! Form::submit('Bestellen', array('class'=>'btn pull-right pay-btn')) !!}
							</div>
							<div class="clearfix"></div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer id="footer">
		<p class="copyrights">Copyright Socialpreneurs.io - All Rights Reserved</p>
	</footer>

@stop

@section('footer')

@stop

@section('footerscript')
<script>
	$(document).ready(function(){
		$('select[name=land]').on('change', function(){
			$('input[type=hidden][name=land_full]').val($('select[name=land] option:selected').text());
			if($(this).val() != 'NL') {
				$('div.col-md-12.form-group.btw-nummer').fadeIn();
			} else {
				$('div.col-md-12.form-group.btw-nummer').fadeOut();
			}
		});
		$('input[name=btwnummer]').on('blur',function(){
			$(this).val($(this).val().replace(/\./g,'').replace(/,/g,'').replace(/ /g,'').replace(/\s/g,''));
		});
	});
</script>
@stop