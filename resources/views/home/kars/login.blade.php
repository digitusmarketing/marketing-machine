@extends('layouts.auth-end')

@section('css')
<style>
	.white-bg {
		background:white;
		padding:15px 15px 1px;
	}
	.btn-login {
		background: #1a71b8 !important;
		background-color: #1a71b8 !important;
		border: 4px solid #1a71b8;
		color:white;
	    text-transform: uppercase;
	    padding: 12px 41px !important;
	    -webkit-transition: all 0.2s ease 0s;
	    -moz-transition: all 0.2s ease 0s;
	    transition: all 0.2s ease 0s;
	    font-weight: 600 !important;
	}
	.btn-login:hover {
		background: white !important;
		background-color: white !important;
		color: #1d71b8 !important;
		border: 4px solid #1a71b8;
		-webkit-transition: all 0.2s ease 0s;
		-moz-transition: all 0.2s ease 0s;
		transition: all 0.2s ease 0s;
	}
</style>
@stop

@section('content')
<div class="lockscreen-wrapper animated flipInX">
    <div class="row">

        <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 no-padding">
        	<div class="logo">
	            <img src="{{ url(config('app.images.normal')) }}" alt="{{ config('app.name') }}" class="app-image" style="width:100%;">
        	</div>
	        <div class="clearfix"></div>
	        <div class="p-t-25 p-b-25">&nbsp;</div>
	        <div class="clearfix"></div>
	        <div class="login-form white-bg">
	        	<h2>Inloggen</h2>
	        	<hr>
	        	<div class="clearfix"></div>
	        	@if(count($errors) > 0)
				    <div class="alert alert-danger">
				        <ul>
				            @foreach($errors->all() as $error)
				            <li>{!! $error !!}</li>
				            @endforeach
				        </ul>
				    </div>
				    <hr>
				    <div class="clearfix"></div>
				@endif
		        {!! Form::open(array('url'=>'kars')) !!}
				<div class="form-group">
			        {!! Form::label('email', 'Emailadres:') !!}
			        {!! Form::email('email', old('email'), array('class'=>'form-control')) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password', 'Wachtwoord:') !!}
					{!! Form::password('password', array('class'=>'form-control')) !!}
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-md-6">
							<a class="btn btn-link" href="{{ url('/password/reset') }}">
			                    Wachtwoord vergeten?
			                </a>
						</div>
						<div class="col-md-6">
							{!! Form::submit('Inloggen', array('class'=>'btn btn-login btn-cons btn-fetch')) !!}
						</div>
					</div>
				</div>
				{!! Form::close() !!}
	        </div>
        </div>

        
    </div>
</div>
<div id="push"></div>
@stop

@section('scripts')

@stop