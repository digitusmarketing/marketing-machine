<!DOCTYPE html>

<html lang="nl-NL">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
</head>
<body>
	<div class="rcmBody" style="border:0; margin:0; padding:0; min-width:100%;">

		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td bgcolor="f9fafa" style="border:0; margin:0; padding:0;">
					<table style="background-color: #1d71b8" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td align="center" style="border: 0; margin: 0; padding: 0">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td align="center" style="border: 0; margin: 0; padding: 0">
												<table align="center" border="0" cellpadding="0" cellspacing="0" class="width" width="500">
													<tbody>
														<tr>
															<td align="center" height="20" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #1e2124; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif">
																Affiliate Notificatie - Goedgekeurd!.
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
								<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
									<tbody>
										<tr>
											<td height="7" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
												<div class="clear" style="height: 7px; width: 1px">&nbsp;</div>
											</td>
										</tr>
										<tr>
											<td class="banner" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tbody><tr>
														<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
															<div class="clear" style="height: 1px; width: 20px"></div>
														</td>
														<td style="border: 0; margin: 0; padding: 0" width="100%">
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tbody><tr>
																	<td align="center" class="icon" height="72" style="border: 0; margin: 0; padding: 0">
																		<a href="https://www.socialpreneurs.io" style="border: 0; margin: 0; padding: 0" rel="noreferrer" target="_blank">
																			<span class="retina">
																				<img height="72" src="https://www.socialpreneurs.io/images/site/logo-white.png" style="border: 0; margin: 0; padding: 0" width="129">
																			</span>
																		</a>
																	</td>
																</tr>
																<tr>
																	<td height="22" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<div class="clear" style="height: 22px; width: 1px">&nbsp;</div>
																	</td>
																</tr>
																<tr>
																	<td align="center" class="title" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px; text-shadow: 0 1px 1px #1b1d20">
																		<span class="apple-override" style="color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px">Affiliate</span> <span class="apple-override-header" style="color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px; text-decoration: none">Notificatie</span>
																	</td>
																</tr>


																<tr>
																	<td height="13" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<div class="clear" style="height: 13px; width: 1px">&nbsp;</div>
																	</td>
																</tr>
																<tr>
																	<td align="center" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<table border="0" cellpadding="0" cellspacing="0" class="card card-mastercard">
																			<tbody>
																				<tr class="card-light">
																					<td class="card-type" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly" valign="top">
																						<span style="color: #ffffff; font-family: 'Lucida Console', monospace; font-size: 14px; line-height: 14px">
																							Goedgekeurd!
																						</span>
																					</td>
																					<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="6">
																						<div class="clear" style="height: 1px; width: 6px">&nbsp;</div>
																					</td>
																					<td class="card-digits" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Lucida Console', monospace; font-size: 14px; line-height: 14px; text-shadow: 0 1px 1px #1b1d20; vertical-align: middle" valign="middle">
																						<span style="color: #ffffff; font-family: 'Lucida Console', monospace; font-size: 14px; line-height: 14px">&nbsp;</span>
																					</td>
																				</tr>
																			</tbody>
																		</table>

																	</td>
																</tr>


															</tbody></table>
														</td>
														<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
															<div class="clear" style="height: 1px; width: 20px"></div>
														</td>
													</tr>
												</tbody></table>
											</td>
										</tr>
										<tr>
											<td height="27" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
												<div class="clear" style="height: 27px; width: 1px">&nbsp;</div>
											</td>
										</tr>
									</tbody>
								</table>
								<table bgcolor="#1b1d20" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td align="center" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
												<table class="width" border="0" cellpadding="0" cellspacing="0" width="500">
													<tbody>
														<tr>
															<td colspan="4" height="8" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																<div class="clear" style="height: 8px; width: 1px">&nbsp;</div>
															</td>
														</tr>
														<tr>
															<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
																<div class="clear" style="height: 1px 20px"></div>
															</td>
															<td align="left" class="subbanner-item font-small" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px; text-shadow: 0 1px 1px #1b1d20" width="230">
																<!-- <span class="apple-override-header" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px; text-shadow: 0 1px 1px #1b1d20">
																	
																</span> -->
															</td>
															<td align="right" class="subbanner-item font-small" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px; text-shadow: 0 1px 1px #1b1d20" width="230">
																<!-- <span class="apple-override-header" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px; text-shadow: 0 1px 1px #1b1d20"></span> -->
															</td>
															<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
																<div class="clear" style="height: 1px 20px"></div>
															</td>
														</tr>
														<tr>
															<td colspan="4" height="8" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																<div class="clear" style="height: 8px; width: 1px">&nbsp;</div>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td align="center" style="border: 0; margin: 0; padding: 0">
									<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
										<tbody>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="left" class="font-large" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #515f66; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21px">

													<p>Hoi {!! $first_name !!},</p>
													<p>Welkom bij de SocialPreneurs Community Affiliates!</p>
													<p>We hebben je affiliate account goedgekeurd. Vanaf nu kun je nieuwe leden aanbrengen. 
													Per nieuw lid dat zich aanmeldt via jouw referentielink ontvang je €14,10 (ex. BTW) aan commissie..  per maand! Ja, dat lees je goed. Als jij goed voor ons bent zijn wij dat natuurlijk ook voor jou.</p>
													<p>Met deze korting groeit de Community niet alleen sneller, zo hebben wij met zijn allen ook een grotere impact en creëren we een betere wereld voor morgen.</p>
													<p>Mooi toch?</p>
													<p>Je kunt nu meteen al je ambitieuze en ondernemende vrienden en kennissen jouw persoonlijke affiliatelink sturen: <a href="{!! $affiliate_link !!}" target="_blank">{!! $affiliate_link !!}</a></p>

													<p>Wanneer zij zich via deze link aanmelden, het moet wel echt via deze link zijn en niet via de website, krijg jij direct korting. We houden je per e-mail direct op de hoogte van de aanmeldingen die binnenkomen. </p>
													<p>Veel succes {!! $first_name !!}!</p>

													<p>Team Digitus Marketing</p>
												</td>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td colspan="3" align="center" height="1" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="200">
														<tbody>
															<tr>
																<td bgcolor="edeff0" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																	<div class="clear" style="height: 1px; width: 200px">&nbsp;</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td align="center" style="border: 0; margin: 0; padding: 0">
									<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
										<tbody>
											<tr>
												<td colspan="3" height="28" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 28px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="20">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="center" class="font-small" style="border: 0; margin: 0; padding: 0; color: #959fa5; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px">
													Je ontvangt deze email omdat je een affiliate aanvraag hebt gedaan op <a href="https://www.socialpreneurs.io" style="border: 0; margin: 0; padding: 0; color: #008cdd; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text-decoration: none" rel="noreferrer" target="_blank"><span style="border: 0; margin: 0; padding: 0; color: #008cdd; text-decoration: none">Socialpreneurs.io</span></a>.
												</td>
												<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="20">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="28" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 28px; width: 1px">&nbsp;</div>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
	</div>

</body>
</html>