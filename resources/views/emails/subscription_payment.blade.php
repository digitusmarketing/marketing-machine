<!DOCTYPE html>

<html lang="nl-NL">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
</head>
<body>
	<div class="rcmBody" style="border:0; margin:0; padding:0; min-width:100%;">

		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td bgcolor="f9fafa" style="border:0; margin:0; padding:0;">
					<table style="background-color: #1d71b8" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td align="center" style="border: 0; margin: 0; padding: 0">
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td align="center" style="border: 0; margin: 0; padding: 0">
												<table align="center" border="0" cellpadding="0" cellspacing="0" class="width" width="500">
													<tbody>
														<tr>
															<td align="center" height="20" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #1e2124; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif">
																Bedankt voor je &euro; {!! $bedrag !!} betaling aan <span class="apple-override-hidden" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #1e2124; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif">Socialpreneurs.io</span>.
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
								<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
									<tbody>
										<tr>
											<td height="7" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
												<div class="clear" style="height: 7px; width: 1px">&nbsp;</div>
											</td>
										</tr>
										<tr>
											<td class="banner" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" valign="middle">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tbody><tr>
														<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
															<div class="clear" style="height: 1px; width: 20px"></div>
														</td>
														<td style="border: 0; margin: 0; padding: 0" width="100%">
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tbody><tr>
																	<td align="center" class="icon" height="72" style="border: 0; margin: 0; padding: 0">
																		<a href="https://www.socialpreneurs.io" style="border: 0; margin: 0; padding: 0" rel="noreferrer" target="_blank">
																			<span class="retina">
																				<img height="72" src="https://www.socialpreneurs.io/images/site/logo-white.png" style="border: 0; margin: 0; padding: 0" width="129">
																			</span>
																		</a>
																	</td>
																</tr>
																<tr>
																	<td height="22" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<div class="clear" style="height: 22px; width: 1px">&nbsp;</div>
																	</td>
																</tr>
																<tr>
																	<td align="center" class="title" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px; text-shadow: 0 1px 1px #1b1d20">
																		<span class="apple-override" style="color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px">&euro; {!! $bedrag !!}</span> <span style="color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px; opacity: 0.75">aan</span> <a href="https://www.socialpreneurs.io" style="color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px; text-decoration: none" rel="noreferrer" target="_blank"><span class="apple-override-header" style="color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 25px; text-decoration: none">Socialpreneurs.io</span></a>
																	</td>
																</tr>


																<tr>
																	<td height="13" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<div class="clear" style="height: 13px; width: 1px">&nbsp;</div>
																	</td>
																</tr>
																<tr>
																	<td align="center" height="1" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<table align="center" border="0" cellpadding="0" cellspacing="0" width="200">
																			<tbody>
																				<tr>
																					<td bgcolor="#1b1d20" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																						<div class="clear" style="height: 1px; width: 200px">&nbsp;</div>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td height="18" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<div class="clear" style="height: 18px; width: 1px">&nbsp;</div>
																	</td>
																</tr>
																<tr>
																	<td align="center" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																		<table border="0" cellpadding="0" cellspacing="0" class="card card-mastercard">
																			<tbody>
																				<tr class="card-light">
																					<td class="card-type" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly" valign="top">
																						<span style="color: #ffffff; font-family: 'Lucida Console', monospace; font-size: 14px; line-height: 14px">
																							Factuurnummer:
																						</span>
																					</td>
																					<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="6">
																						<div class="clear" style="height: 1px; width: 6px">&nbsp;</div>
																					</td>
																					<td class="card-digits" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Lucida Console', monospace; font-size: 14px; line-height: 14px; text-shadow: 0 1px 1px #1b1d20; vertical-align: middle" valign="middle">
																						<span style="color: #ffffff; font-family: 'Lucida Console', monospace; font-size: 14px; line-height: 14px">{!! $factuurnummer !!}</span>
																					</td>
																				</tr>
																			</tbody>
																		</table>

																	</td>
																</tr>


															</tbody></table>
														</td>
														<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
															<div class="clear" style="height: 1px; width: 20px"></div>
														</td>
													</tr>
												</tbody></table>
											</td>
										</tr>
										<tr>
											<td height="27" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
												<div class="clear" style="height: 27px; width: 1px">&nbsp;</div>
											</td>
										</tr>
									</tbody>
								</table>
								<table bgcolor="#1b1d20" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td align="center" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
												<table class="width" border="0" cellpadding="0" cellspacing="0" width="500">
													<tbody>
														<tr>
															<td colspan="4" height="8" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																<div class="clear" style="height: 8px; width: 1px">&nbsp;</div>
															</td>
														</tr>
														<tr>
															<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
																<div class="clear" style="height: 1px 20px"></div>
															</td>
															<td align="left" class="subbanner-item font-small" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px; text-shadow: 0 1px 1px #1b1d20" width="230">
																<span class="apple-override-header" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px; text-shadow: 0 1px 1px #1b1d20">
																	{!! $payment_date !!}
																</span>
															</td>
															<td align="right" class="subbanner-item font-small" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px; text-shadow: 0 1px 1px #1b1d20" width="230">
																<span class="apple-override-header" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; color: #ffffff; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px; text-shadow: 0 1px 1px #1b1d20">{!! $referentie !!}</span>
															</td>
															<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="20">
																<div class="clear" style="height: 1px 20px"></div>
															</td>
														</tr>
														<tr>
															<td colspan="4" height="8" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly" width="100%">
																<div class="clear" style="height: 8px; width: 1px">&nbsp;</div>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
					{{-- <table bgcolor="ffffff" border="0" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #e4e6e8" width="100%">
						<tbody>
							<tr>
								<td align="center" style="border: 0; margin: 0; padding: 0">
									<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
										<tbody>
											<tr>
												<td class="temp-padding" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly; font-size: 1px; line-height: 1px">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly; font-size: 1px; line-height: 1px" width="460">
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tbody>


															<tr>
																<td colspan="5" height="12" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly; font-size: 1px; line-height: 1px">
																	<div class="clear" style="height: 12px; width: 1px">&nbsp;</div>
																</td>
															</tr>
															<tr class="summary-item">
																<td class="summary-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="1">
																	<div class="clear" style="height: 1px; width: 1px"></div>
																</td>
																<td align="left" class="font-small" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #77858c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 20px" width="100%">
																Omschrijving
																</td>
																<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="10">
																	<div class="clear" style="height: 1px; width: 10px"></div>
																</td>
																<td align="right" class="font-small" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #77858c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 20px" width="120">
																	Bedrag
																</td>
																<td class="summary-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="1">
																	<div class="clear" style="height: 1px; width: 1px"></div>
																</td>
															</tr>
															<tr>
																<td colspan="5" height="12" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; border-bottom: 1px solid #eaeff2">
																	<div class="clear" style="height: 12px; width: 1px">&nbsp;</div>
																</td>
															</tr>



															<tr>
																<td colspan="5" height="11" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; border-top: 1px solid #eaeff2">
																	<div class="clear" style="height: 11px; width: 1px">&nbsp;</div>
																</td>
															</tr>
															<tr class="summary-item">
																<td class="summary-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="1">
																	<div class="clear" style="height: 1px; width: 1px"></div>
																</td>
																<td align="left" class="font-medium" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #292e31; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 17px" width="100%">
																	<span class="apple-override-dark" style="border: 0; margin: 0; padding: 0">{!! substr($description,0 ,-10)  !!}</span>
																</td>
																<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="10">
																	<div class="clear" style="height: 1px; width: 10px"></div>
																</td>
																<td align="right" class="font-medium" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #292e31; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 17px" width="120">
																	&euro; {!! $bedrag !!}
																</td>
																<td class="summary-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="1">
																	<div class="clear" style="height: 1px; width: 1px"></div>
																</td>
															</tr>
															<tr>
																<td colspan="5" height="12" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																	<div class="clear" style="height: 12px; width: 1px">&nbsp;</div>
																</td>
															</tr>
															<tr>
																<td colspan="5" align="right" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; border-top: 2px solid #eaeff2">
																	<table border="0" cellpadding="0" cellspacing="0" class="width" width="320">
																		<tbody>
																			<tr>
																				<td colspan="5" height="11" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																					<div class="clear" style="height: 11px; width: 1px">&nbsp;</div>
																				</td>
																			</tr>
																			<tr class="summary-item">
																				<td class="summary-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="1">
																					<div class="clear" style="height: 1px; width: 1px"></div>
																				</td>
																				<td align="left" class="font-small" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #77858c; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 17px" width="100">
																					Totaal
																				</td>
																				<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="10">
																					<div class="clear" style="height: 1px; width: 10px"></div>
																				</td>
																				<td align="right" class="font-medium width" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #292e31; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: normal; line-height: 17px" width="208">
																					&euro; {!! $bedrag !!}
																				</td>
																				<td class="summary-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="1">
																					<div class="clear" style="height: 1px; width: 1px"></div>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="5" height="12" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly; border-bottom: 1px solid #eaeff2">
																					<div class="clear" style="height: 12px; width: 1px">&nbsp;</div>
																				</td>
																			</tr>






																			<tr>
																				<td colspan="5" height="11" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																					<div class="clear" style="height: 11px; width: 1px">&nbsp;</div>
																				</td>
																			</tr>
																			<tr class="summary-item">
																				<td class="summary-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="1">
																					<div class="clear" style="height: 1px; width: 1px"></div>
																				</td>
																				<td align="left" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #292e31; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 17px" width="100">
																					Betaald
																				</td>
																				<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="10">
																					<div class="clear" style="height: 1px; width: 10px"></div>
																				</td>
																				<td align="right" class="summary-total width" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #292e31; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; font-weight: bold; line-height: 17px" width="208">
																					&euro; {!! $bedrag !!}
																				</td>
																				<td class="summary-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="1">
																					<div class="clear" style="height: 1px; width: 1px"></div>
																				</td>
																			</tr>
																			<tr>
																				<td colspan="5" height="12" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																					<div class="clear" style="height: 12px; width: 1px">&nbsp;</div>
																				</td>
																			</tr>


																		</tbody>
																	</table>
																</td>
															</tr>


														</tbody>
													</table>
												</td>
												<td class="temp-padding" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly; font-size: 1px; line-height: 1px">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table> --}}
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td align="center" style="border: 0; margin: 0; padding: 0">
									<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
										<tbody>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="center" class="font-large" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #515f66; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21px">

												Je factuur staat klaar in het <a href="https://www.socialpreneurs.io/member/transacties" style="border: 0; margin: 0; padding: 0; color: #515f66; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text-decoration: none" rel="noreferrer" target="_blank"><span style="border: 0; margin: 0; padding: 0; color: #008cdd; text-decoration: none">transactieoverzicht</span></a>.
												</td>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="center" class="font-large" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; color: #515f66; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 15px; line-height: 21px">

												De volgende automatische betaling zal plaatsvinden op {!! $next_payment !!}.
												</td>
												<td style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="37" style="border: 0; margin: 0; padding: 0; mso-line-height-rule: exactly">
													<div class="clear" style="height: 37px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td colspan="3" align="center" height="1" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<table align="center" border="0" cellpadding="0" cellspacing="0" width="200">
														<tbody>
															<tr>
																<td bgcolor="edeff0" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
																	<div class="clear" style="height: 1px; width: 200px">&nbsp;</div>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td align="center" style="border: 0; margin: 0; padding: 0">
									<table border="0" cellpadding="0" cellspacing="0" class="width" width="500">
										<tbody>
											<tr>
												<td colspan="3" height="28" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 28px; width: 1px">&nbsp;</div>
												</td>
											</tr>
											<tr>
												<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="20">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
												<td align="center" class="font-small" style="border: 0; margin: 0; padding: 0; color: #959fa5; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 17px">
													Je ontvangt deze email omdat je een aankoop hebt gedaan op <a href="https://www.socialpreneurs.io" style="border: 0; margin: 0; padding: 0; color: #008cdd; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; text-decoration: none" rel="noreferrer" target="_blank"><span style="border: 0; margin: 0; padding: 0; color: #008cdd; text-decoration: none">Socialpreneurs.io</span></a>.
												</td>
												<td class="perm-padding" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px" width="20">
													<div class="clear" style="height: 1px; width: 20px"></div>
												</td>
											</tr>
											<tr>
												<td colspan="3" height="28" style="border: 0; margin: 0; padding: 0; font-size: 1px; line-height: 1px; mso-line-height-rule: exactly">
													<div class="clear" style="height: 28px; width: 1px">&nbsp;</div>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
	</div>

</body>
</html>