@extends('layouts.front-end')

@section('title')
  Bedankt!
@stop

@section('css')
{!! Html::style('/css/funnel/funnel.css') !!}
<style>
html, body {
  height:100%;
}
.timer > div {
    width: 33.3333% !important;
}
</style>


@stop

@section('robots')
<meta property="og:image" content="{{ url($funnel->image) }}">
<meta property="og:url" content="{{ action('Funnel\FunnelController@index', array($funnel->slug)) }}">
<meta property="og:title" content="{!! $funnel->facebook_share_titel !!}">
<meta property="og:description" content="{!! $funnel->slogan !!}">
<meta property="og:site_name" content="socialpreneurs.io">
@stop

@section('fb_pixel')
  @include('includes.pixels.fb_pixel')
@stop

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.4&appId=387796737964493";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="page-container row-fluid">
  <div class="container" id="intro">
    <div class="col-md-10 col-md-offset-1">
      <div class="col-md-12">
        <h1 style="padding-bottom:25px;" class="page-titel">{!! $funnel->thankyou_titel !!}</h1>
        <div class="col-md-12">
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            {!! $funnel->thankyou_video !!}
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('footerscript')
<script>
  // Set proper iframe width and height
  $('iframe').css({'height':($('iframe.iframe-top').width() * 0.57)});
  $(window).resize(function(){
    $('iframe').css({'height':($('iframe.iframe-top').width() * 0.57)});
  });
</script>
@stop