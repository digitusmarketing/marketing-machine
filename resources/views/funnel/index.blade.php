@extends('layouts.front-end')

@section('title')
  @if($funnel)
    {!! $funnel->titel !!}
  @endif
@stop

@section('css')
{!! Html::style('/css/funnel/funnel.css') !!}
<style>
.timer > div {
    width: 33.3333% !important;
}
iframe{width:100%;}
</style>


@stop

@section('robots')
<meta property="og:image" content="{{ url($funnel->image) }}">
<meta property="og:url" content="{{ action('Funnel\FunnelController@index', array($funnel->slug)) }}">
<meta property="og:title" content="{!! $funnel->facebook_share_titel !!}">
<meta property="og:description" content="{!! $funnel->slogan !!}">
<meta property="og:site_name" content="socialpreneurs.io">
@stop

@section('fb_pixel')
  @include('includes.pixels.fb_pixel')
@stop

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.4&appId=387796737964493";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="page-container row-fluid">
  <div class="container" id="intro">
    <div class="col-md-10 col-md-offset-1">
      
      <div class="col-md-12">
        <h1 style="border-bottom:1px dashed white; padding-bottom:25px;" class="offer-h1">{!! $funnel->titel !!}</h1>
        <h5 style="border-bottom:1px dashed white; padding-bottom:25px;" class="desktop">{!! $funnel->slogan !!}</h5>
        <div class="col-md-12" style="padding-top:15px;">
          <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 mobiel">
            <img src="{!! url($funnel->image) !!}" alt="{!! $funnel->titel !!}">
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 mobiel mobiel-show-form">
            <h5 style="margin-bottom:15px;">{!! $funnel->pre_button_text !!}</h5>
            <button class="btn demo-button mobiel-show-form show-form">{!! $funnel->text !!}</button>
          </div>
          <div class="clearfix mobiel"></div>
          @if(isset($funnel->video_link))
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            {!! $funnel->video_link !!}
          </div>
          <div class="clearfix"></div>
          @else
          <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 desktop">
            <img src="{!! url($funnel->image) !!}" alt="{!! $funnel->titel !!}">
          </div>
          @endif
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-show top desktop">
              <h5 style="margin-bottom:15px;">{!! $funnel->pre_button_text !!}</h5>
              <button class="btn demo-button show-form">{!! $funnel->button_text !!}</button>
            </div>
            <div class="form-hidden top" style="display:none;margin-top:15px">
              {!! Form::open(array('class'=>'animated fadeIn', 'id'=>'top-form', 'action'=>['Funnel\FunnelController@post',$funnel->slug])) !!}
                {!! Form::hidden('formaction',action('Funnel\FunnelController@post',$funnel->slug)) !!}

                <div class="errors-top alert alert-danger" style="display:none;">
                  <p>Er is wat fout gegaan..</p>
                  <p>Probeer het opnieuw of stuur ons een <a href="mailto:info@digitusmarketing.nl?subject=Ik wil de 7 geheimen downloaden maar er gaat iets mis." target="_blank">email</a>.</p>
                </div>
                
                <div class="col-md-4 col-sm-12 form-group">
                  <div id="voornaam-error" style="display:none;">
                    <p class="alert alert-danger"></p>
                  </div>
                  {!! Form::text('naam', old('naam'), array('class'=>'form-control', 'placeholder'=>'Naam..')) !!}
                </div>
                <div class="col-md-4 col-sm-12 form-group">
                  <div id="email-error" style="display:none;">
                    <p class="alert alert-danger"></p>
                  </div>
                  {!! Form::email('email', old('email'), array('class'=>'form-control', 'placeholder'=>'Email..')) !!}
                </div>
                <?php
                $encrypter = app('Illuminate\Encryption\Encrypter');
                $encrypted_token = $encrypter->encrypt(csrf_token());
                ?>
                <input type="hidden" id="token-download" value="{!! $encrypted_token !!}">
                <div class="col-md-4 col-sm-12 form-group">
                  {!! Form::submit($funnel->form_button_text, array('class'=>'btn demo-button pull-right request-button', 'style'=>'margin:0;width:100%;padding:9px !important; font-size:14pt !important;')) !!}
                </div>
                
                <div class="form-group col-md-12">
                  <p class="pull-left" style="color:white"><i class="fa fa-lock" style="margin-right:5px;"></i> Je gegevens zijn veilig</p>
                </div>
                <div class="clearfix"></div>
              {!! Form::close() !!}
            </div>
          </div>
          

          
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="page">

  <div class="container">
    <div class="row login-container animated fadeInUp"> 
      <div class="col-md-10 col-md-offset-1 tiles white" style="padding:25px">
        <h2 style="text-align:center;">{!! $funnel->content_titel !!}</h2>
        <p>{!! $funnel->content_body !!}</p>
      </div>
      <div class="col-md-10 col-md-offset-1">
        <div class="form-show">
          <button class="btn demo-button show-form bottom">{!! $funnel->button_2_text !!}</button>
        </div>
        <div class="form-hidden" style="display:none;">
          <h4>{!! $funnel->titel !!} aanvragen</h4>
          {!! Form::open(array('class'=>'animated fadeIn', 'id'=>'bottom-form', 'action'=>['Funnel\FunnelController@post',$funnel->slug])) !!}
            {!! Form::hidden('formaction',action('Funnel\FunnelController@post',$funnel->slug)) !!}
            <div class="errors-bot alert alert-danger" style="display:none;">
              <p style="color:#e76e70 !important;">Er is wat fout gegaan..</p>
              <p style="color:#e76e70 !important;">Probeer het opnieuw of stuur ons een <a href="mailto:info@digitusmarketing.nl?subject=Ik wil de 7 geheimen downloaden maar er gaat iets mis." target="_blank">email</a>.</p>
            </div>
            
            <div class="col-md-4 col-sm-12 form-group">
              <div id="voornaam-error" style="display:none;">
                <p class="alert alert-danger"></p>
              </div>
              {!! Form::text('naam', old('naam'), array('class'=>'form-control', 'placeholder'=>'Naam..')) !!}
            </div>
            <div class="col-md-4 col-sm-12 form-group">
              <div id="email-error" style="display:none;">
                <p class="alert alert-danger"></p>
              </div>
              {!! Form::email('email', old('email'), array('class'=>'form-control', 'placeholder'=>'Email..')) !!}
            </div>
            <?php
            $encrypter = app('Illuminate\Encryption\Encrypter');
            $encrypted_token = $encrypter->encrypt(csrf_token());
            ?>
            <input type="hidden" id="token-download" value="{!! $encrypted_token !!}">
            <div class="col-md-4 col-sm-12 form-group">
              {!! Form::submit($funnel->form_button_2_text, array('class'=>'btn demo-button pull-right request-button', 'style'=>'margin:0;width:100%;padding:9px !important; font-size:14pt !important;')) !!}
            </div>
            
            <div class="form-group col-md-12">
              <p class="pull-left"><i class="fa fa-lock" style="margin-right:5px;"></i> Je gegevens zijn veilig</p>
              <div style="position: absolute; left: -5000px;">
              </div>
            <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            
          {!! Form::close() !!}
        </div>
      </div>  
    </div>
  </div>
@stop

@section('footerscript')
  <script>
  $(document).ready(function(){
    $('button.show-form').on('click', function(){
      $('div.mobiel-show-form').hide();
      $(this).parent('div.form-show').hide();
      if($(this).hasClass('bottom')) {
        $(this).parent().parent('div.col-md-10.col-md-offset-1').addClass('download-bar');
      }
      $(this).parent().next('.form-hidden').slideDown('2500');
    });
    $('button.mobiel-show-form').on('click', function(){
      $('div.mobiel-show-form').hide();
      $('div.form-show.top').hide();
      $('div.form-hidden.top').slideDown('2500');
      $('html, body').animate({
        scrollTop: $('div.form-hidden.top').offset().top - 100+'px'
      }, 1000);
    });
  });

  // Set proper iframe width and height
  $('iframe').css({'height':($('iframe.iframe-top').width() * 0.57)});
  $(window).resize(function(){
    $('iframe').css({'height':($('iframe.iframe-top').width() * 0.57)});
  });

  var href = window.location.href,
  newUrl = href.substring(0, href.indexOf('?'))
  window.history.replaceState({}, '', newUrl);

  function pad(number, length){
    var str = "" + number
    while (str.length < length) {
        str = '0'+str
    }
    return str
}

var offset = new Date().getTimezoneOffset();
offset = ((offset<0? '+':'-')+ // Note the reversed sign!
          pad(parseInt(Math.abs(offset/60)), 1)+
          pad(Math.abs(offset%60), 1));


$('input[type=hidden][name=timezone]').val(offset);
  </script>
@stop