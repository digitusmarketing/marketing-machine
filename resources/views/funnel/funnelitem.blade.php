@extends('layouts.front-end')

@section('titel')
  @if(isset($funnelitem))
  {!! $funnelitem->titel !!}
  @endif
@stop

@section('robots')
<meta property="og:image" content="{{ url($funnel->image) }}">
<meta property="og:url" content="{{ action('Funnel\FunnelController@show', array($funnel->slug, $funnelitem->slug)) }}">
<meta property="og:title" content='{!! $funnel->titel !!}'>
<meta property="og:description" content="Een video serie waarin Parsifal je laat zien hoe je op de automatische piloot relevante klanten aantrekt, terwijl jij je bezig houdt met je core-business!">

<meta property="og:site_name" content="socialpreneurs.io">
@stop
@section('css')
{!! Html::style('/css/funnel/funnel.css') !!}
<style>
body{
  background-image:url("//www.digitusmarketing.nl/images/site/bg/blauw-bg-pattern.jpg");
}
.timer > div {
  width: 33.3333% !important;
}
div.timer p.letter {
  color:white !important;
}
div.social-share {
  padding: 10px;
  margin:15px 0;
}
h3.social-share {
  color:white;
}
.page-container {
  padding-top:2% !important;
}
.download-button-bar {
  margin:15px 0;
  border-top: 1px dashed white;
  border-bottom: 1px dashed white;
  padding:15px 0;
}

@media only screen and (min-width:220px) and (max-width:680px) {
  h3.social-share {
    font-size: 18px;
    text-align: center !important;
    float: none !important;
  }
  .demo-button{
    font-size:14pt !important;
  }
}
.episode-holder,
.video-js {
  max-width: 100%;
  width:100%;
}
.f-items {
  display: block;
}
.f-items .col-md-12.col-sm-4.col-xs-4 {
  margin: 10px 0;
}
.f-items div:first-child {
  margin-top:0 !important;
}
.f-items div:last-child {
  margin-bottom:0 !important;
}

.item {
  position:relative;
}
.item img {
  border: 1px solid black;
  box-shadow: 1px 2px 3px 0 rgba(0, 0, 0, 0.35);
}
.item.current::before {
    content: '';
    display: block;
    position: absolute;
    left: -10px;
    top: 50%;
    height: 10px;
    width: 10px;
    /* background: white; */
    margin-top: -5px;
    border-right: 10px solid black;
    border-top: 10px solid transparent;
    border-bottom: 10px solid transparent;
}
.disabled-item.overlay,
.item span,
.item a {
  position:absolute;
  top:0;
  right:0;
  bottom:0;
  left:0;
  display:flex;
}
.item span {
  align-items:center;
  justify-content: center;
}
.item a {
  align-items:center;
  justify-content: center;
  flex-direction:column;
}
.overlay {
  background:rgba(0,0,0,0.7);
  opacity:1;
  transition: all 0.3s;
}
.overlay:hover {
  background:rgba(0,0,0,0.25);
  transition: all 0.4s;
}
.overlay:hover a,
.overlay:hover span {
  text-decoration: none;
  transform: scale(1);
  opacity:1;
  transition: all 0.4s;
}
.overlay a,
.overlay span {
  color:white;
  font-size: 50pt;
  transform: scale(0.2);
  opacity: 0;
  transition: all 0.3s;
}
/*.video-js .vjs-progress-control * {
  display: none;
}*/
@media only screen and (max-width: 768px) {
  .f-items {
    margin: 15px 0;
  }
  .f-items .col-md-12.col-sm-4.col-xs-4 {
    margin:0;
    padding:0 20px;
  }
  .item.current::before {
    content: '';
    display: block;
    position: absolute;
    left: 50%;
    top: -5px;
    height: 10px;
    width: 10px;
    /* background: white; */
    margin-left: -10px;
    border-right: 10px solid transparent;
    border-top: 0;
    border-bottom: 10px solid black;
    border-left: 10px solid transparent;
}
}
</style>
@stop

@section('fb_pixel')
  @if($funnelitem->id === '1') {
  <?php 
    $extra = "setTimeout(function(){
      fbq('trackCustom', 'Video1-7Min', {content_name: 'video 1 7 minuten bekeken', content_category: '3-videoserie'});
    }, ".$funnelitem->call_to_action_delay.");";  
  ?>
  @elseif($funnelitem->id === '2') {
  <?php 
    $extra = "setTimeout(function(){
      fbq('trackCustom', 'Video2-20Min', {content_name: 'video 2 20 minuten bekeken', content_category: '3-videoserie'});
    }, ".$funnelitem->call_to_action_delay.");";  
  ?>
  }
  @elseif($funnelitem->id === '3') {
  <?php 
    $extra = "setTimeout(function(){
      fbq('trackCustom', 'Video3-20Min', {content_name: 'video 3 20 minuten bekeken', content_category: '3-videoserie'});
    }, ".$funnelitem->call_to_action_delay.");";  
  ?>
  }
  @endif
  @if(isset($extra))
    @include('includes.pixels.fb_pixel', array('extra'=>$extra))
    {{-- @include('includes.pixels.fb_audience', array('extra'=>$extra)) --}}
  @else
  @include('includes.pixels.fb_pixel')
  @endif
@stop

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v2.4&appId=387796737964493";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

  <div class="container" id="intro">
    <div class="col-md-12">
      
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <h1><i class="fa fa-play-circle-o"></i> &nbsp;{!! $funnelitem->titel !!}</h1>
          <h5 style="margin-top:15px;border-bottom:1px dashed white; padding-bottom:25px;margin-bottom:25px;font-style:italic">{!! $funnelitem->slogan !!}</h5>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-9 col-sm-12 col-xs-12 video-holder">
          {{-- <div class="embed-responsive embed-responsive-16by9 episode-holder iframe-top"> --}}
            {{-- <video 
              id="my-video"
              poster="{!! url($video->poster) !!}"
              class="video-js vjs-big-play-centered episode"
              controls
            >
              <source
                src="{!! $video->video_link !!}"
                type="video/mp4"
                data-quality="HD"
                label="HD"
                data-default="true"
              >
              <p class="vjs-no-js">
                To view this video please enable JavaScript, and consider upgrading
                to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
              </p>
            </video> --}}
          	{{-- {!! $video->video_link !!} --}}
          {{-- </div> --}}
          {!! $funnelitem->item_link !!}
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12 f-items">
          @foreach($funnel->funnelitems as $item)
          <div class="col-md-12 col-sm-4 col-xs-4">
            <div class="row item {{ $item->id === $funnelitem->id ? 'current' : 'disabled' }}">
              @if($item->id < $funnelitem->id || $item->id > $funnelitem->id)
              <div class="disabled-item overlay">
                @if($item->id < $funnelitem->id)
                <a href="{!! route('funnel.show',[$funnel->slug,$item->slug]) !!}">
                  <i class="fa fa-play-circle-o"></i>
                  <h5>{!! $item->titel !!}</h5>
                </a>
                @else
                <span>
                  <i class="fa fa-times-circle-o"></i>
                  <h5>{!! $item->titel !!}</h5>
                </span>
                @endif
              </div>
              @endif
              <img src="{!! url($item->item_poster) !!}" class="item-image">
            </div>
          </div>
          @endforeach
        </div>
        <div class="clearfix"></div>
        <div class="col-md-10 col-md-offset-1">
          <div class="social-share">
            <h3 class="pull-left social-share" style="margin: 0;">Deel deze video serie:</h3>
            
            <div class="col-md-4 col-sm-6 col-xs-6 fb-share">
              <div class="fb-share-button" data-href="{!! route('funnel.index',$funnel->slug) !!}" data-layout="button" data-size="large" data-mobile-iframe="false">
                {{-- <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.digitusmarketing.nl%2Fto%2F3-videoserie&amp;src=sdkpreparse">Delen</a> --}}
              </div>
            </div>  
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 download-button-bar" style="display:none;">
            <h3 style="color:white;margin:20px 0 10px">{!! $funnelitem->call_to_action_titel !!}</h3>
            <a href="{!! url($funnel->end_point) !!}" target="_blank" class="btn demo-button" style="margin:20px auto; float:none;">{!! $funnelitem->call_to_action_button_text !!}</a>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 fb-commentbar" style="display:block;margin:50px auto 0;">
          <h2 style="color:white;margin:25px auto;">Heb je vragen, opmerkingen of feedback?</h2>
          <div style="padding:15px 15px 15px 0; background: white;">
            <div class="fb-comments" data-href="{{ route('funnel.index',$funnel->slug)}}" data-colorscheme="light" data-mobile="true" data-width="830" style="background:white;padding:15px;width:100%;box-shadow: 2px 2px 4px 0 rgba(0,0,0,0.4);"></div>
          </div>
        </div>
        {{-- <div class="col-md-6 col-md-offset-3">
          <h2 style="text-align:center;color:white;">{!! $video->offer_expires_text !!}</h2>
          <div class="timer">
            <div class="timer">
                <div class="uren">
                  <p class="cijfer"></p>
                  <p class="letter"></p>
                </div>
                <div class="minuten">
                  <p class="cijfer"></p>
                  <p class="letter"></p>
                </div>
                <div class="seconden">
                  <p class="cijfer"></p>
                  <p class="letter"></p>
                </div>
              </div>   
          </div>
        </div>
      </div> --}}
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
@stop

@section('footerscript')
  <script>
    // Set proper iframe width and height
    // $('iframe').css({'height':($('.iframe-top').width() * 0.57)});
    $('iframe').each(function(){
      $(this).css({'height':($(this).width() * 0.57)});
    });
    $(window).resize(function(){
      $('iframe').each(function(){
        $(this).css({'height':($(this).width() * 0.57)});
      });
    });

    $(document).ready(function(){
      setTimeout(function(){
        $('.download-button-bar').slideDown();
      }, '<?php echo $funnelitem->call_to_action_delay; ?>');
      // var video_height = $('.video-holder').height();
      // $('.f-items').css({'height':video_height});
    });
  </script>
  
@stop