<div class="modal fade" id="approve-affiliate" tabindex="-1" role="dialog" aria-labelledby="myMydalLabel" aria-hidden="true" style="display:none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<br>
				<h4 id="myModalLabel" class="semi-bold">Affiliate goed / -afkeuren</h4>
			</div>
			<div class="modal-body">
				{{-- <div class="row form-row">
					<div class="col-md-12">
						<h2>Bedrijfsgegevens:</h2>
					</div>
				</div> --}}
				<div class="row form-row">
					<div class="col-md-12">
						<h2>{!! $member->company->company_name !!}</h2>
					</div>
				</div>
				<div class="row form-row">
					<div class="col-md-6">
						<h4 class="semi-bold">Contactpersoon</h4>
						<p><i class="fa fa-user"></i> @if($member->company->title == 'm') Dhr. @else Mevr. @endif {!! $member->first_name !!} {!! $member->last_name !!}</p>
					</div>
					<div class="col-md-6">
						<h4 class="semi-bold">Email:</h4>
						<p><i class="fa fa-envelope"></i> {!! $member->company->business_email !!}</p>
					</div>
				</div>
				<div class="row form-row">
					<div class="col-md-6">
						<h4 class="semi-bold">Adres:</h4>
						<p><i class="fa fa-map-marker"></i> {!! $member->company->address !!}, {!! $member->company->postal !!}, {!! $member->company->recidence !!}, {!! $member->company->country_full !!}</p>
					</div>
					<div class="col-md-6">
						<h4 class="semi-bold">KvK:</h4>
						<p>
							<i class="fa fa-certificate"></i> 
							{!! $member->company->kvk !!} 
							<a href="https://www.kvk.nl/handelsregister/archief/" class="pull-right badge badge-info" target="_blank"  data-toggle="tooltip" data-original-title="KvK nummer controleren.">
								<i class="fa fa-external-link"></i>
							</a> 
							<a href="https://graydon.be/" class="pull-right badge badge-warning" target="_blank"  data-toggle="tooltip" data-original-title="Ondernemingsnummer controleren." style="margin-right:15px;">
								<i class="fa fa-external-link"></i>
							</a>
						</p>
					</div>
				</div>
				<div class="row form-row">
					<div class="col-md-6">
						<h4 class="semi-bold">Tel:</h4>
						<p><i class="fa fa-phone"></i> {!! $member->company->phone !!}</p>
					</div>
					<div class="col-md-6">
						<h4 class="semi-bold">Mob:</h4>
						<p><i class="fa fa-mobile-phone"></i> {!! $member->company->mobile_phone !!}</p>
					</div>
				</div>
				<hr>
				<div class="row form-row">
					<div class="col-md-12">
						<h2>Bankgegevens:</h2>
					</div>
				</div>
				<div class="row form-row">
					<div class="col-md-12">
						<h4 class="semi-bold">Tenaamstelling:</h4>
						<p><i class="fa fa-bullhorn"></i> {!! $member->company->appellation !!}</p>
					</div>
				</div>
				<div class="row form-row">
					<div class="col-md-6">
						<h4 class="semi-bold">IBAN:</h4>
						<p><i class="fa fa-money"></i> {!! $member->company->iban !!}</p>
					</div>
					<div class="col-md-6">
						<h4 class="semi-bold">BTW:</h4>
						<p>
							<i class="fa fa-euro"></i> 
							{!! $member->company->btw !!} 
							<a href="http://ec.europa.eu/taxation_customs/vies/vieshome.do?locale=nl" class="pull-right badge badge-info" target="_blank"  data-toggle="tooltip" data-original-title="BTW nummer controleren.">
								<i class="fa fa-external-link"></i>
							</a>
						</p>
						<p  data-toggle="tooltip" data-original-title="Digitus Marketing BTW nummer"><span class="label label-info">NL850525469B01</span></p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-large btn-cons" data-dismiss="modal" data-toggle="modal" data-target="#deny-affiliate"><i class="fa fa-times-circle"></i> Afkeuren..</button>
				{{-- <button type="button" class="btn btn-primary btn-large btn-cons" data-dismiss="modal"><i class="fa fa-check-circle"></i> Goedkeuren..</button> --}}

				{!! Form::open(['route'=>'jandje.affiliate.approve', 'class'=>'pull-right']) !!}
				{!! Form::hidden('affiliate_id',$member->affiliate->id) !!}
				<button type="submit" class="btn btn-primary btn-large btn-cons"><i class="fa fa-check-circle"></i> Goedkeuren..</button>
				{{-- {!! Form::submit('Ja, ik weet het zeker', array('class'=>'btn btn-danger btn-large btn-cons')) !!} --}}
				{!! Form::close() !!}
			</div>

		</div>
	</div>
</div>