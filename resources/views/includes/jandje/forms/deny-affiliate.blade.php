<div class="modal fade" id="deny-affiliate" tabindex="-1" role="dialog" aria-labelledby="myMydalLabel" aria-hidden="true" style="display:none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<br>
				<h4 id="myModalLabel" class="semi-bold">Affiliate afkeuren</h4>
			</div>
			{!! Form::open(['route'=>'jandje.affiliate.decline']) !!}
			<div class="modal-body">
				{!! Form::hidden('affiliate_id',$member->affiliate->id) !!}
				{!! Form::label('decline_bericht', 'Reden afwijzing affiliate:') !!}
				{!! Form::textarea('decline_bericht', old('decline_bericht'), array('class'=>'form-control')) !!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning btn-large btn-cons" data-dismiss="modal"><i class="fa fa-minus-circle"></i> Laat maar zitten..</button>
				<button type="submit" class="btn btn-danger btn-large btn-cons pull-right"><i class="fa fa-times-circle"></i> Afkeuren...</button>
			</div>
			{!! Form::close() !!}

		</div>
	</div>
</div>