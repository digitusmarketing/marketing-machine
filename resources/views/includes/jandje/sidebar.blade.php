<div class="page-sidebar " id="main-menu">
        <!-- BEGIN MINI-PROFILE -->
        <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
          <div class="user-info-wrapper sm">
            <div class="profile-wrapper sm">
              <img src="{{ url($user->profile_image) }}" alt="{!! $user->first_name !!} {!! $user->last_name !!}" data-src="{{ url($user->profile_image) }}" data-src-retina="{{ url($user->profile_image) }}" width="69" height="69" />
              <div class="availability-bubble online"></div>
            </div>
            <div class="user-info sm">
              <div class="username">{!! $user->first_name !!} <span class="semi-bold">{!! $user->last_name !!}</span></div>
              <div class="status">{!! $user->username !!}</div>
            </div>
          </div>
          <!-- END MINI-PROFILE -->
          <!-- BEGIN SIDEBAR MENU -->

          <p class="menu-title sm">Menu <span class="pull-right"><a href="javascript:;"><i class="material-icons">refresh</i></a></span></p>

          <ul>
            <li class="start {{ Request::is('jandje') ? 'active' : '' }} "> <a href="{{ route('jandje.index') }}"><i class="fa fa-home"></i> <span class="title">Dashboard</span> <span class="selected"></span> </a>
            </li>
            {{-- <li class="{{ Request::is('jandje/series') || Request::is('jandje/series/*') || Request::is('jandje/series/*/*') ? 'active' : '' }}">
              <a href="{{ route('jandje.modules.index') }}">
                <i class="fa fa-play-circle-o"></i>
                <span class="title">Modules</span>
              </a>
            </li> --}}
            <p class="menu-title">Gebruikers</p>
            <li class="{{ Request::is('jandje/members') ? 'active' : '' }}">
              <a href="{{ route('jandje.members.index') }}">
                <i class="fa fa-users"></i>
                <span class="title">Members</span>
              </a>
            </li>
            {{-- <li class="{{ Request::is('jandje/affiliates') }}">
              <a href="{{ route('jandje.affiliates.index') }}">
                <i class="fa fa-magic"></i>
                <span class="title">Affiliates</span>
              </a>
            </li> --}}
            <p class="menu-title">Member</p>
            <li><a href="{!! route('member.index') !!}"><i class="fa fa-fire"></i> <span class="title">Member</span></a></li>
          </ul>
          <div class="clearfix"></div>
          <!-- END SIDEBAR MENU -->
        </div>
      </div>
      <a href="#" class="scrollup">Scroll</a>
      <div class="footer-widget">
        <div class="pull-right">
          <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <i class="fa fa-power-off"></i></a>
              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
        </div>
      </div>