<hr>
@if($user->isAffiliate())
@if($user->affiliate->isValidated())
<p class="social-share">Deel deze community op: 
	<span class="social-icon"><a href="https://www.facebook.com/sharer.php?u={{ route('base.index') }}?a={{ $user->affiliate->code }}" target="_blank" title="Deel de community op Facebook. Je referentie-link is automatisch toegevoegd!"><i class="fa fa-facebook-square"></i></a></span> 
	{{-- <span class="social-icon"><a href="#" target="_blank" title="Delen op Twitter"><i class="fa fa-twitter-square"></i></a></span> --}}
</p>
@else
@endif
@endif