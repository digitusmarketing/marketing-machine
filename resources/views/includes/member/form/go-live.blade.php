<div class="modal fade" id="go-live" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="go-live-title" style="display:none;">
	<div class="modal-dialog">
		<div class="modal-content report-form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<br>
				<h4 id="report-a-bug-title" class="semi-bold">Go Live on Facebook!</h4>
			</div>
			<div class="modal-body">
				<p>Via hier kun je Live gaan op Facebook via je computer of laptop m.b.v. een <a href="https://www.google.nl/#q=broadcaster+software" target="_blank">broadcaster software</a> zoals o.a. <a href="https://obsproject.com/" target="_blank">OBS</a>. Klik op de "Go Live" button om te starten!</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning btn-large btn-cons" data-dismiss="modal" style="margin-bottom:0;">Ik wil toch niet live gaan..</button>
				<button type="button" class="btn btn-primary btn-large btn-cons" id="goLiveButton" data-dismiss="modal">Go Live!</button>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>