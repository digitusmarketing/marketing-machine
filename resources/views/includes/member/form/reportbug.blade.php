<div class="modal fade" id="report-a-bug" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="report-a-bug-title" style="display:none;">
	<div class="modal-dialog">
		<div class="modal-content report-form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<br>
				<h4 id="report-a-bug-title" class="semi-bold">Meld een bug!</h4>
			</div>
			{!! Form::open(['route'=>'report.bug', 'id'=>'report-a-bug-form']) !!}
			<div class="modal-body">
				{!! Form::hidden('user_id',$user->id) !!}
				{!! Form::textarea('bericht', old('bericht'), array('class'=>'form-control')) !!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-large btn-cons" data-dismiss="modal">Ik wil toch niks melden..</button>
				{!! Form::submit('Verstuur', array('class'=>'btn btn-primary btn-large btn-cons pull-right')) !!}
				<div class="clearfix"></div>
			</div>
			{!! Form::close() !!}
		</div>
		<div class="modal-content succes-form" style="display:none;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<br>
				<h4 id="report-a-bug-title" class="semi-bold">Succes!</h4>
			</div>
			<div class="modal-body">
				<p>We hebben je bericht ontvangen, bedankt!</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-large btn-cons" data-dismiss="modal">Super!</button>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('form#report-a-bug-form').on('submit',function(e){
			e.preventDefault();
			var url = $('form#report-a-bug-form').attr('action');

			$.ajax({
				type: 'POST',
				url: url,
				data: $('form#report-a-bug-form').serialize(),
				success: function(data){
					if(data.success === true) {
						// bericht is ontvangen
						$('.report-form').hide();
						$('.succes-form').show();
					} 
				},
		    });
		});
		$('a#report-bug').on('click', function(){
			var naam = "{!! $user->first_name !!} {!! $user->last_name !!}";
			$('textarea[name=bericht]').append('Hey Jeroen!\n');
			$('textarea[name=bericht]').append('\n');
			$('textarea[name=bericht]').append('Ik ben op dit moment op de pagina: '+window.location.href+' en ik ben volgens mij een bug tegengekomen.\n');
			$('textarea[name=bericht]').append('\n');
			$('textarea[name=bericht]').append('[[ TYPE HIER JE BERICHT ]]\n');
			$('textarea[name=bericht]').append('Graag breng ik jou op de hoogte van deze bug!\n');
			$('textarea[name=bericht]').append('\n');
			$('textarea[name=bericht]').append('Met vriendelijke groet,\n');
			$('textarea[name=bericht]').append(naam);
		});
	});
</script>