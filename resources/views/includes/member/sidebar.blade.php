<div class="page-sidebar " id="main-menu">
        <!-- BEGIN MINI-PROFILE -->
        <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
          <div class="user-info-wrapper sm">
            <div class="profile-wrapper sm">
              <img src="{{ url($user->profile_image) }}" alt="{!! $user->first_name !!} {!! $user->last_name !!}" data-src="{{ url($user->profile_image) }}" data-src-retina="{{ url($user->profile_image) }}" width="69" height="69" />
              <div class="availability-bubble online"></div>
            </div>
            <div class="user-info sm">
              <div class="username">{!! $user->first_name !!} <span class="semi-bold">{!! $user->last_name !!}</span></div>
              <div class="status">{!! $user->username !!} &nbsp;</div>
            </div>
          </div>
          <!-- END MINI-PROFILE -->
          <!-- BEGIN SIDEBAR MENU -->

          <p class="menu-title sm">Menu <span class="pull-right"><a href="javascript:;"><i class="material-icons">refresh</i></a></span></p>

          <ul>
            <li class="start {{ Request::is('member') ? 'active' : '' }} "> <a href="{{ route('member.index') }}"><i class="fa fa-home"></i> <span class="title">Dashboard</span> <span class="selected"></span> </a>
            </li>
            @if($user->active())
            <li class="{{ Request::is('member/cursus') ? 'active' : '' }}">
              <a href="{{ route('member.get.cursussen') }}"> <i class="fa fa-play-circle-o"></i> <span class="title">Cursussen</span> </a>
            </li>
            {{-- <li class="{{ Request::is('member/series') || Request::is('member/episodes/favorites') || Request::is('member/episodes/later-bekijken') ? 'active' : '' }}">
              <a href="javascript:;"> <i class="fa fa-play-circle-o"></i> <span class="title">Cursus</span> <span class=" arrow {{ Request::is('member/series') || Request::is('member/episodes/favorites') || Request::is('member/episodes/later-bekijken') ? 'open' : '' }}"></span> </a>
              <ul class="sub-menu" style="{{ Request::is('member/series') || Request::is('member/episodes/favorites') || Request::is('member/episodes/later-bekijken') ? 'display:block' : '' }}">
                <li class="{{ Request::is('member/series') ? 'active' : '' }}"> <a href="{{ route('member.get.all.modules') }}"> <i class="fa fa-bars"></i> Alle modules </a> </li>
                <li class="{{ Request::is('member/episodes/favorites') ? 'active' : '' }}"> <a href="{{ route('get.favorite.videos') }}"> <i class="fa fa-heart"></i> Favoriete videos <span class="badge badge-danger animated bounceIn">{!! $user->countFavoriteEpisodes() !!}</span></a></li>
                <li class="{{ Request::is('member/episodes/later-bekijken') ? 'active' : '' }}"> <a href="{{ route('get.watchlater.videos') }}"> <i class="fa fa-clock-o"></i> Later bekijken <span class="badge badge-info animated bounceIn">{!! $user->countWatchLaterEpisodes() !!}</span></a></li>
              </ul>
            </li> --}}
            @endif
            {{-- <li class="disabled">
              <a href="javascript:;"> <i class="fa fa-group"></i> <span class="title">Extra's</span> <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li class="disabled"> <i class="fa fa-lightbulb-o"></i> <span class="title">Interviews</span></li>
                <li class="disabled"> <i class="fa fa-laptop"></i> <span class="title">Webinars</span></li>
              </ul>
            </li> --}}
            <p class="menu-title">Profiel</p>
            <li class="{{  Request::is('member/transacties') ? 'active' : ''  }}"><a href="{{ route('get.member.orders') }}"> <i class="fa fa-exchange"></i> <span class="title">Transacties</span></a></li>
            {{-- <li class="{{ Request::is('member/affiliate') ? 'active' : '' }}"><a href="{{ route('member.affiliate.index') }}"> <i class="fa fa-magic"></i> <span class="title">Affiliate</span></a></li> --}}
            <li class="{{ Request::is('member/me') ? 'active' : '' }}"><a href="{{ route('member.profile') }}"> <i class="fa fa-user"></i> <span class="title">Profiel</span></a></li>
            @if($user->active())
            <p class="menu-title">Extra</p>
            <li>
              <a href="javascript:;"> <i class="fa fa-facebook"></i> <span class="title">Facebook</span> <span class="arrow"></span></a>
              <ul class="sub-menu">
                <li><a href="{!! config('app.fb_group') !!}" target="_blank"> <i class="fa fa-users"></i> <span class="title">Facebook groep</span></a></li>
                <li><a href="#"  data-toggle="modal" data-target="#go-live"> <i class="fa fa-video-camera"></i> <span class="title">Go Live</span></a></li>
              </ul>
            </li>
            @endif
            @if($user->isAdmin())
            <p class="menu-title">Admin</p>
            <li><a href="{!! route('jandje.index') !!}"><i class="fa fa-rocket"></i> <span class="title">Jandje</span></a></li>
            @endif
          </ul>

          <!-- <div class="side-bar-widgets">
            <p class="menu-title sm">FOLDER <span class="pull-right"><a href="#" class="create-folder"> <i class="material-icons">add</i></a></span></p>
            <ul class="folders">
              <li>
                <a href="#">
                  <div class="status-icon green"></div>
                  My quick tasks </a>
              </li>
            </ul>
            <p class="menu-title">PROJECTS </p>
            <div class="status-widget">
              <div class="status-widget-wrapper">
                <div class="title">Freelancer<a href="#" class="remove-widget"><i class="material-icons">close</i></a></div>
                <p>Redesign home page</p>
              </div>
            </div>
          </div> -->


          <div class="clearfix"></div>
          <!-- END SIDEBAR MENU -->
        </div>
      </div>
      <a href="#" class="scrollup">Scroll</a>
      <div class="footer-widget">
        <div class="pull-left footer-widget-icons">
          <a href="#" title="Meld een bug!" id="report-bug" data-toggle="modal" data-target="#report-a-bug">
            <i class="fa fa-bug"></i>
          </a>
        </div>
        <div class="pull-right">
          <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <i class="fa fa-power-off"></i></a>
              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
        </div>
        <div class="clearfix"></div>
      </div>