<div class="col-md-12">
	<h2>Persoonsgegevens</h2>
</div>
<div class="col-md-2 form-group {{ $errors->has('aanhef') ? 'has-error' : '' }}">
	{!! Form::label('aanhef', 'Aanhef:') !!}
	{!! Form::select('aanhef', array('m'=>'Dhr.', 'f'=>'Mevr.'), '', array('class'=>'form-control')) !!}
	@if($errors->has('aanhef'))
	<p class="alert alert-danger">{!! $errors->first('aanhef') !!}</p>
	@endif
</div>
<div class="form-group col-md-5 {{ $errors->has('voornaam') ? 'has-error' : '' }}">
	{!! Form::label('voornaam', 'Voornaam:') !!} <span class="asterisk">*</span>
	{!! Form::text('voornaam', old('voornaam'), array('class'=>'form-control')) !!}
	@if($errors->has('voornaam'))
	<p class="alert alert-danger">{!! $errors->first('voornaam') !!}</p>
	@endif
</div>
<div class="form-group col-md-5 {{ $errors->has('achternaam') ? 'has-error' : '' }}">
	{!! Form::label('achternaam', 'Achternaam:') !!} <span class="asterisk">*</span>
	{!! Form::text('achternaam', old('achternaam'), array('class'=>'form-control')) !!}
	@if($errors->has('achternaam'))
	<p class="alert alert-danger">{!! $errors->first('achternaam') !!}</p>
	@endif
</div>
<div class="form-group col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
	{!! Form::label('email', 'Emailadres:') !!} <span class="asterisk">*</span> <i data-toggle="tooltip" data-placement="top" title="Dit emailadres wordt gebruikt om in te loggen." class="fa fa-info-circle" style="color:#1d71b8;"></i>
	{!! Form::email('email', old('email'), array('class'=>'form-control')) !!}
	@if($errors->has('email'))
	<p class="alert alert-danger">{!! $errors->first('email') !!}</p>
	@endif
</div>
<div class="form-group col-md-6 {{ $errors->has('telefoonnummer') ? 'has-error' : '' }}">
	{!! Form::label('telefoonnummer', 'Telefoonnummer:') !!}
	{!! Form::input('tel', 'telefoonnummer', old('telefoonnummer'), array('class'=>'form-control')) !!}
	@if($errors->has('telefoonnummer'))
	<p class="alert alert-danger">{!! $errors->first('telefoonnummer') !!}</p>
	@endif
</div>
<div class="form-group col-md-6">
	{!! Form::label('mobielnummer', 'Mobiel:') !!}
	{!! Form::input('tel', 'mobielnummer', old('mobielnummer'), array('class'=>'form-control')) !!}
</div>
<div class="clearfix"></div>
<hr>
<div class="col-md-12">
	<h2>Bedrijfsgegevens</h2>
</div>
<div class="form-group col-md-5 {{ $errors->has('bedrijfsnaam') ? 'has-error' : '' }}">
	{!! Form::label('bedrijfsnaam', 'Bedrijfsnaam:') !!} <span class="asterisk">*</span>
	{!! Form::text('bedrijfsnaam', old('bedrijfsnaam'), array('class'=>'form-control')) !!}
	@if($errors->has('bedrijfsnaam'))
	<p class="alert alert-danger">{!! $errors->first('bedrijfsnaam') !!}</p>
	@endif
</div>
<div class="form-group col-md-7 {{ $errors->has('zakelijk_email') ? 'has-error' : '' }}">
	{!! Form::label('zakelijk_email', 'Zakelijk emailadres:') !!} <span class="asterisk">*</span> <i data-toggle="tooltip" data-placement="top" title="Naar dit emailadres worden facturen verstuurd." class="fa fa-info-circle" style="color:#1d71b8;"></i>
	{!! Form::email('zakelijk_email', old('zakelijk_email'), array('class'=>'form-control')) !!}
	@if($errors->has('zakelijk_email'))
	<p class="alert alert-danger">{!! $errors->first('zakelijk_email') !!}</p>
	@endif
</div>
<div class="form-group col-md-4 {{ $errors->has('adres') ? 'has-error' : '' }}">
	{!! Form::label('adres', 'Adres:') !!}<span class="asterisk">*</span>
	{!! Form::text('adres', old('adres'), array('class'=>'form-control')) !!}
	@if($errors->has('adres'))
	<p class="alert alert-danger">{!! $errors->first('adres') !!}</p>
	@endif
</div>
<div class="form-group col-md-2 {{ $errors->has('postcode') ? 'has-error' : '' }}">
	{!! Form::label('postcode', 'Postcode:') !!} <span class="asterisk">*</span>
	{!! Form::text('postcode', old('postcode'), array('class'=>'form-control')) !!}
	@if($errors->has('postcode'))
	<p class="alert alert-danger">{!! $errors->first('postcode') !!}</p>
	@endif
</div>
<div class="form-group col-md-4 {{ $errors->has('woonplaats') ? 'has-error' : '' }}">
	{!! Form::label('woonplaats', 'Vestigingplaats:') !!} <span class="asterisk">*</span>
	{!! Form::text('woonplaats', old('woonplaats'), array('class'=>'form-control')) !!}
	@if($errors->has('woonplaats'))
	<p class="alert alert-danger">{!! $errors->first('woonplaats') !!}</p>
	@endif
</div>
<div class="form-group col-md-2 {{ $errors->has('land') ? 'has-error' : '' }}">
	{!! Form::label('land', 'Land:') !!} <span class="asterisk">*</span>
	{!! Form::select('land', array(null=>'Kies je land..', 'AW' =>'Aruba', 'BE' =>'België', 'CW' =>'Curaçao', 'DK' =>'Denemarken', 'DE' =>'Duitsland', 'FI' =>'Finland', 'FR' =>'Frankrijk', 'IE' =>'Ierland', 'IT' =>'Italië', 'NL' =>'Nederland', 'NO' =>'Noorwegen', 'PT' =>'Portugal', 'ES' =>'Spanje', 'SR' =>'Suriname', 'GB' =>'Verenigd Koninkrijk', 'ZA' =>'Zuid-Afrika', 'SE' =>'Zweden',), old('land'), array('class'=>'form-control')) !!}
	{!! Form::hidden('land_full', old('land_full')) !!}
	@if($errors->has('land'))
	<p class="alert alert-danger">{!! $errors->first('land') !!}</p>
	@endif
</div>
<div class="col-md-12 form-group btw-nummer {{ $errors->has('btwnummer') ? 'has-error' : '' }}" style="{{ $errors->has('btwnummer') ? 'display:block' : 'display:none' }} {{ old('land') == 'NL' || old('land') == null ? '' : 'display:block;' }}">
	{!! Form::label('btwnummer', 'BTW nummer:') !!} <span class="asterisk">*</span><br/><small>(Voer je BTW nummer in zonder leestekens)</small>
	{!! Form::text('btwnummer', old('btwnummer'), array('class'=>'form-control')) !!}
	@if($errors->has('btwnummer'))
	<p class="alert alert-danger">{!! $errors->first('btwnummer') !!}</p>
	@endif
</div>
<div class="clearfix"></div>
<hr>
<div class="form-group col-md-6 group-paymentmethod {{ $errors->has('paymentmethod') ? 'has-error' : '' }}">
	<h2>Selecteer betaalmethode:</h2>
	<p class="alert alert-danger alert-betaalmethode" style="display:none;"><span class="fa fa-warning"></span> Selecteer een betaalmethode</p>
	<select name="betaalmethode" id="betaalmethode" class="form-control">
		@foreach($methods as $method)
		<option value="{!! $method->id !!}" name="betaalmethode">{!! $method->description !!}</option>
		@endforeach
	</select>
	<div class="clearfix"></div>
</div>
{{-- <div class="form-group col-md-6">
	<div class="row price-bar">
		<div class="check">
			<i class="fa fa-check-square-o" style="color:green;"></i>
		</div>
		<div class="da-price">
			<span class="euro">&euro;</span>
			<span class="price">47,-</span>
			<small class="ex-btw">(ex BTW.)</small>
		</div>
	</div>
</div> --}}
<div class="form-group hidden-form-fields">
	{!! Form::hidden('bedrag', '47.00') !!}
	{!! Form::hidden('issuer', old('issuer')) !!}
	{!! Form::hidden('paymentmethod', 'ideal') !!}
	<?php
	$encrypter = app('Illuminate\Encryption\Encrypter');
	$encrypted_token = $encrypter->encrypt(csrf_token());
	?>
	<input type="hidden" id="token-download" value="{!! $encrypted_token !!}">
</div>