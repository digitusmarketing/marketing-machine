<div class="col-md-12">
	<h2>Persoonsgegevens</h2>
</div>
<div class="col-md-2 form-group {{ $errors->has('aanhef') ? 'has-error' : '' }}">
	{!! Form::label('aanhef', 'Aanhef:') !!}
	{!! Form::select('aanhef', array('m'=>'Dhr.', 'f'=>'Mevr.'), '', array('class'=>'form-control')) !!}
	@if($errors->has('aanhef'))
	<p class="alert alert-danger">{!! $errors->first('aanhef') !!}</p>
	@endif
</div>
<div class="form-group col-md-5 {{ $errors->has('voornaam') ? 'has-error' : '' }}">
	{!! Form::label('voornaam', 'Voornaam:') !!}
	{!! Form::text('voornaam', old('voornaam'), array('class'=>'form-control')) !!}
	@if($errors->has('voornaam'))
	<p class="alert alert-danger">{!! $errors->first('voornaam') !!}</p>
	@endif
</div>
<div class="form-group col-md-5 {{ $errors->has('achternaam') ? 'has-error' : '' }}">
	{!! Form::label('achternaam', 'Achternaam:') !!}
	{!! Form::text('achternaam', old('achternaam'), array('class'=>'form-control')) !!}
	@if($errors->has('achternaam'))
	<p class="alert alert-danger">{!! $errors->first('achternaam') !!}</p>
	@endif
</div>
<div class="form-group col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
	{!! Form::label('email', 'Emailadres:') !!}
	{!! Form::email('email', old('email'), array('class'=>'form-control', 'id'=>'disabledInput', 'readonly')) !!}
	@if($errors->has('email'))
	<p class="alert alert-danger">{!! $errors->first('email') !!}</p>
	@endif
</div>
<div class="form-group col-md-6 {{ $errors->has('telefoonnummer') ? 'has-error' : '' }}">
	{!! Form::label('telefoonnummer', 'Telefoonnummer:') !!}
	{!! Form::input('tel', 'telefoonnummer', old('telefoonnummer'), array('class'=>'form-control')) !!}
	@if($errors->has('telefoonnummer'))
	<p class="alert alert-danger">{!! $errors->first('telefoonnummer') !!}</p>
	@endif
</div>
<div class="form-group col-md-6">
	{!! Form::label('mobielnummer', 'Mobiel:') !!}
	{!! Form::input('tel', 'mobielnummer', old('mobielnummer'), array('class'=>'form-control')) !!}
</div>
<div class="clearfix"></div>
<hr>
<div class="col-md-12">
	<h2>Bedrijfsgegevens</h2>
</div>
<div class="form-group col-md-5 {{ $errors->has('bedrijfsnaam') ? 'has-error' : '' }}">
	{!! Form::label('bedrijfsnaam', 'Bedrijfsnaam:') !!}
	{!! Form::text('bedrijfsnaam', old('bedrijfsnaam'), array('class'=>'form-control')) !!}
	@if($errors->has('bedrijfsnaam'))
	<p class="alert alert-danger">{!! $errors->first('bedrijfsnaam') !!}</p>
	@endif
</div>
<div class="form-group col-md-7 {{ $errors->has('zakelijk_email') ? 'has-error' : '' }}">
	{!! Form::label('zakelijk_email', 'Zakelijk emailadres:') !!} <small>(Voer hier het emailadres in waar je facturen op wenst te verkrijgen)</small>
	{!! Form::email('zakelijk_email', old('zakelijk_email'), array('class'=>'form-control')) !!}
	@if($errors->has('zakelijk_email'))
	<p class="alert alert-danger">{!! $errors->first('zakelijk_email') !!}</p>
	@endif
</div>
<div class="form-group col-md-4 {{ $errors->has('adres') ? 'has-error' : '' }}">
	{!! Form::label('adres', 'Adres:') !!}
	{!! Form::text('adres', old('adres'), array('class'=>'form-control')) !!}
	@if($errors->has('adres'))
	<p class="alert alert-danger">{!! $errors->first('adres') !!}</p>
	@endif
</div>
<div class="form-group col-md-2 {{ $errors->has('postcode') ? 'has-error' : '' }}">
	{!! Form::label('postcode', 'Postcode:') !!}
	{!! Form::text('postcode', old('postcode'), array('class'=>'form-control')) !!}
	@if($errors->has('postcode'))
	<p class="alert alert-danger">{!! $errors->first('postcode') !!}</p>
	@endif
</div>
<div class="form-group col-md-4 {{ $errors->has('woonplaats') ? 'has-error' : '' }}">
	{!! Form::label('woonplaats', 'Woonplaats:') !!}
	{!! Form::text('woonplaats', old('woonplaats'), array('class'=>'form-control')) !!}
	@if($errors->has('woonplaats'))
	<p class="alert alert-danger">{!! $errors->first('woonplaats') !!}</p>
	@endif
</div>
<div class="form-group col-md-2 {{ $errors->has('land') ? 'has-error' : '' }}">
	{!! Form::label('land', 'Land:') !!}
	{!! Form::select('land', array(null=>'Kies je land..', 'AW' =>'Aruba', 'BE' =>'België', 'CW' =>'Curaçao', 'DK' =>'Denemarken', 'DE' =>'Duitsland', 'FI' =>'Finland', 'FR' =>'Frankrijk', 'IE' =>'Ierland', 'IT' =>'Italië', 'NL' =>'Nederland', 'NO' =>'Noorwegen', 'PT' =>'Portugal', 'ES' =>'Spanje', 'SR' =>'Suriname', 'GB' =>'Verenigd Koninkrijk', 'ZA' =>'Zuid-Afrika', 'SE' =>'Zweden',), old('land'), array('class'=>'form-control')) !!}
	{!! Form::hidden('land_full', old('land_full')) !!}
	@if($errors->has('land'))
	<p class="alert alert-danger">{!! $errors->first('land') !!}</p>
	@endif
</div>
<div class="col-md-12 form-group btw-nummer {{ $errors->has('btwnummer') ? 'has-error' : '' }}" style="{{ $errors->has('btwnummer') ? 'display:block' : 'display:none' }} {{ old('land') == 'NL' || old('land') == null ? '' : 'display:block;' }}">
	{!! Form::label('btwnummer', 'BTW nummer:') !!} <small>(Voer je BTW nummer in zonder leestekens)</small>
	{!! Form::text('btwnummer', old('btwnummer'), array('class'=>'form-control')) !!}
	@if($errors->has('btwnummer'))
	<p class="alert alert-danger">{!! $errors->first('btwnummer') !!}</p>
	@endif
</div>
<div class="clearfix"></div>
<hr>
<div class="form-group col-md-6 group-paymentmethod {{ $errors->has('paymentmethod') ? 'has-error' : '' }}">
	<h2>Selecteer betaalmethode:</h2>
	<p class="alert alert-danger alert-betaalmethode" style="display:none;"><span class="fa fa-warning"></span> Selecteer een betaalmethode</p>
	@foreach($methods as $method)
	<div class="paymentmethod {{ old('paymentmethod') == $method->id ? 'selected' : '' }}">
		<img id="paymentmethod" class="{{$method->id}}" src="{{ url($method->image->normal) }}" data-id="{{$method->id}}" data-title="{{$method->description}}" title="{{$method->description}}">
		<p class="label label-info" style="display:block;margin:0;border-radius:0;">
			@if($method->id == 'mistercash') {!! ucfirst($method->id) !!} @elseif($method->id == 'kbc') KBC/CBC  @else {!! $method->description !!} @endif
		</p>
		{!! Form::radio('betaalmethode', $method->id, '', array('id'=>$method->id,'class'=>'paymentmethod', 'data-title'=>$method->description)) !!}
	</div>
	@endforeach
	<div class="clearfix"></div>
</div>
<div class="form-group col-md-3 {{ $errors->has('bedrag') ? 'has-error' : '' }}">
	<h2>Betaalmethode:</h2>
	<span class="de-betaalmethode label label-danger">Geen betaalmethode geselecteerd</span>
	@if($errors->has('paymentmethod') || $errors->has('bedrag'))
		@if($errors->has('paymentmethod'))
			<p class="alert alert-danger">Selecteer een betaalmethode a.u.b.</p>
		@endif
		@if($errors->has('bedrag'))
			<p class="alert alert-danger">Selecteer een betaalmethode a.u.b.</p>
		@endif
	@endif
</div>
<div class="if-ideal form-group col-md-3 group-issuer" style="display:none;">
	<h2>Selecteer je bank:</h2>
	<select name="issuer" id="issuer" class="form-control">
		@foreach($issuers as $issuer)
		<option value="{!! $issuer->id !!}" name="issuer">{!! $issuer->name !!}</option>
		@endforeach
	</select>
	<div class="clearfix"></div>
</div>
<div class="form-group col-md-6 selected-plan" style="display:none;">
	<h2>Geselecteerde betaalplan:</h2>
	<div class="plan selected">
		<div class="plan-selected plan-type">
			<h3></h3>
		</div>
		<div class="plan-selected plan-price" style="display:flex !important;flex: initial !important;">
			<span class="dollar">&euro;</span>
			<span class="plan-price"></span>
			<span class="btw">(ex. BTW)</span>
			{{-- <p class="plan-per">per&nbsp;<span class="plan-type"></span></p> --}}
		</div>
	</div>
</div>
<div class="clearfix"></div>
<hr>
<div class="form-group hidden-form-fields">
	{!! Form::hidden('bedrag', old('bedrag')) !!}
	{!! Form::hidden('paymentmethod', old('paymentmethod')) !!}
	{!! Form::hidden('plan_id', old('plan_id')) !!}
	{!! Form::hidden('issuer', old('issuer')) !!}
	{!! Form::hidden('omschrijving', old('omschrijving')) !!}
	{!! Form::hidden('fb_u_id', old('fb_u_id')) !!}
	{!! Form::hidden('picture_url', old('picture_url')) !!}
	{!! Form::hidden('cover', old('cover')) !!}
	<?php
	$encrypter = app('Illuminate\Encryption\Encrypter');
	$encrypted_token = $encrypter->encrypt(csrf_token());
	?>
	<input type="hidden" id="token-download" value="{!! $encrypted_token !!}">
</div>