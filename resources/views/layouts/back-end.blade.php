<!DOCTYPE html>
<html @yield('ng-app')>
  <head>
    <title>{{ config('app.name') }}</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <meta property="og:locale" content="nl_NL" />
    <meta property="og:title" content="Mijn affiliate shizzle | {!! config('app.officialname') !!}" />
    <meta property="og:description" content="{!! config('app.slogan') !!}" />
    <meta property="og:url" content="{!! config('app.url') !!}" />
    <meta property="og:site_name" content="{!! config('app.name') !!}" />
    <meta property="og:image" content="{!! url(config('app.images.shareimage')) !!}" />
    <meta property="fb:app_id" content="{!! config('services.facebook.client_id') !!}" />

    <!-- BEGIN PLUGIN CSS -->
    <link href="/css/webarch/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/webarch/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/webarch/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/webarch/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/webarch/jquery.scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/webarch/webarch.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
    <!-- BEGIN CUSTOM CSS -->
    @yield('css')
    <!-- EIND CUSTOM CSS -->
    @include('includes.pixels.fb_pixel')
  </head>
  <body class="">
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1928920830673076";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
    <!-- BEGIN HEADER -->
    @yield('menubar')
    <!-- END HEADER -->
    <!-- BEGIN CONTENT -->
    <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      @yield('sidebar')
      


      <!-- <div class="footer-widget">
        <div class="progress transparent progress-small no-radius no-margin">
          <div class="progress-bar progress-bar-success animate-progress-bar" data-percentage="79%" style="width: 79%;"></div>
        </div>
        <div class="pull-right">
          <div class="details-status"> <span class="animate-number" data-value="86" data-animation-duration="560">86</span>% </div>
          <a href="lockscreen.html"><i class="material-icons">power_settings_new</i></a></div>
      </div> -->


      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        @yield('content')
      </div>
      <!-- END PAGE CONTAINER -->
      
    </div>
    <!-- END CONTENT -->

    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="/js/webarch/pace.min.js" type="text/javascript"></script>
    <!-- BEGIN JS DEPENDECENCIES-->
    <script src="/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="/js/webarch/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/webarch/jqueryblockui.min.js" type="text/javascript"></script>
    <script src="/js/webarch/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="/js/webarch/jquery.scrollbar.min.js" type="text/javascript"></script>
    <script src="/js/webarch/jquery.animateNumbers.js" type="text/javascript"></script>
    <script src="/js/webarch/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/js/webarch/select2.min.js" type="text/javascript"></script>
    <!-- END CORE JS DEPENDECENCIES-->
    
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="/js/webarch/webarch.js" type="text/javascript"></script>
    <script src="/js/webarch/chat.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS -->

    <!-- BEGIN FORMS -->
    @include('includes.member.form.reportbug')
    @include('includes.member.form.go-live')
    <!-- END FORMS -->

    <!-- BEGIN CUSTOM JS -->
    @yield('scripts')
    <script src="/js/go-live.js"></script>
    <!-- END CUSTOM JS -->
  </body>
</html>