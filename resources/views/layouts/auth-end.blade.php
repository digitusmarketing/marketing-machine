<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>{{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN PLUGIN CSS -->
    <link href="/css/webarch/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/css/webarch/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/webarch/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/css/webarch/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/webarch/jquery.scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="/css/webarch/webarch.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
    <!-- BEGIN CUSTOM CSS -->
    @yield('css')
    <!-- END CUSTOM CSS -->
    @include('includes.pixels.fb_pixel')
  </head>
  <!-- END HEAD -->
  <!-- BEGIN BODY -->
  @if(isset($bg))
  <body class="error-body no-top lazy" data-original="{{ $bg }}" style="background-image: url('{{ $bg }}');">
  @else
  <body class="error-body no-top lazy" data-original="/images/site/bg/reset-bg.jpg" style="background-image: url('/images/site/bg/reset-bg.jpg')">
  @endif
    <div class="container">
      @yield('content')
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="/js/webarch/pace.min.js" type="text/javascript"></script>
    <!-- BEGIN JS DEPENDECENCIES-->
    <script src="/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="/js/webarch/bootstrap.min.js" type="text/javascript"></script>
    <script src="/js/webarch/jqueryblockui.min.js" type="text/javascript"></script>
    <script src="/js/webarch/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="/js/webarch/jquery.scrollbar.min.js" type="text/javascript"></script>
    <script src="/js/webarch/jquery.animateNumbers.js" type="text/javascript"></script>
    <script src="/js/webarch/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/js/webarch/select2.min.js" type="text/javascript"></script>
    <!-- END CORE JS DEPENDECENCIES-->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="/js/webarch/webarch.js" type="text/javascript"></script>
    <script src="/js/webarch/chat.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN CUSTOM JS -->
    @yield('scripts')
    <!-- END CUSTOM JS -->
  </body>
</html>