<!DOCTYPE html>
<html lang="nl-NL">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="alternate" href="{{ config('app.url') }}" hreflang="nl" />
	<link rel="alternate" href="{{ config('app.url') }}" hreflang="nl-be" />

	<meta name="description" content="{!! config('app.slogan') !!}"/>

	<link rel="publisher" href="https://plus.google.com/+DigitusmarketingNl">

	<meta property="og:locale" content="nl_NL" />
	<meta property="og:title" content='{!! config('app.name') !!} - Voor ondernemers die hun grootste dromen waarmaken' />
	<meta property="og:description" content='{!! config('app.slogan') !!}' />
	<meta property="og:url" content="{!! config('app.url') !!}" />
	<meta property="og:site_name" content="{!! config('app.name') !!}" />
	<meta property="og:image" content="{!! url(config('app.images.shareimage')) !!}" />
	<meta property="fb:app_id" content="{!! config('services.facebook.client_id') !!}" />

	@yield('robots')

	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title'){{ config('app.name') }}</title>

	<!-- Styling -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="{!! url('css/font-awesome.css') !!}">
	<link rel="stylesheet" href="{!! url('css/main.css') !!}">
	@yield('css')

	<link rel="canonical" href="{{ url()->current() }}">

	<link rel="icon" href="{!! url('favicon.ico') !!}" type="image/ico" />
	<link rel="shortcut icon" href="{!! url('favicon.ico') !!}" type="image/ico" />

	

    @yield('fb_pixel')
</head>
<body itemscope itemtype="http://schema.org/WebPage">
@include('includes.ontraport_pixel')
@include('includes.google_analytics')

	@yield('nav')

	@yield('content')

	@yield('footer')
	<!-- Footer Scripts -->
	<script type="text/javascript" src="{!! url('js/jquery-3.1.1.min.js') !!}"></script>
	<script type="text/javascript" src="{!! url('js/main.js') !!}"></script>
	@yield('footerscript')
</body>
</html>