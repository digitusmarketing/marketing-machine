@extends('layouts.back-end')

@section('css')
@stop

@section('menubar')
@include('includes.jandje.headerbar')
@stop
@section('sidebar')
@include('includes.jandje.sidebar')
@stop

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="tiles white col-md-12 no-padding">
				<div class="tiles green cover-pic-wrapper">
					<div class="overlayer bottom-right">
						<div class="overlayer-wrapper">
							<div class="padding-10 hidden-xs">
								{{-- <button class="btn btn-primary btn-small">
									<i class="fa fa-edit"></i> &nbsp;&nbsp; Bewerken
								</button> --}}
								@if($user->id === 1)<a href="{!! route('jandje.member.new.payment',$member->id) !!}" class="btn btn-white btn-large btn-control"><i class="fa fa-money"></i></a>@endif
								<a href="{!! route('jandje.affiliate.show.sales',$member->affiliate->id) !!}" class="btn btn-danger btn-large btn-control"><i class="fa fa-magic"></i></a>
								<a href="{!! route('jandje.show.member.orders',$member->id) !!}" class="btn btn-warning btn-large btn-control"><i class="fa fa-exchange"></i></a>
								<a href="{!! route('jandje.member.edit',$member->id) !!}" class="btn btn-primary btn-large btn-control"><i class="fa fa-edit"></i></a>
								<a href="{!! route('jandje.member.login.as',$member->id) !!}" class="btn btn-info btn-large btn-control"><i class="fa fa-sign-in"></i></a>
							</div>
						</div>
					</div>
					@if($member->cover) 
						<div class="user-cover-photo" style="
						background-image: url('{!! $member->cover !!}'); background-repeat: no-repeat; background-size: cover; background-position: center center; width: 100%;display:block;max-height:250px;">
							<img class="user-cover-photo" src="{!! $member->cover !!}" alt="Facebook Coverphoto {!! $member->first_name !!} {!! $member->last_name !!}" style="width:100%;opacity:0;">
						</div>
					@else 
						<div class="user-cover-photo" style="
						background-image: url('/images/site/bg/email-bg.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center; width: 100%;display:block;max-height:250px;">
							<img class="user-cover-photo" src="{{ url('/images/site/bg/email-bg.jpg') }}" alt="Socialpreneurs.io Coverphoto" style="width:100%;opacity:0;">
						</div>
					@endif
				</div>
				<div class="tiles white">
					<div class="row">
						<div class="col-md-3 col-sm-3">
							<div class="user-profile-pic">
								<img width="69" height="69" data-src-retina="{{ url($member->profile_image) }}" data-src="{{ url($member->profile_image) }}" src="{{ url($member->profile_image) }}" alt="{!! $member->first_name !!} {!! $member->last_name !!}">
							</div>
						</div>

						<div class="col-md-8 user-description-box col-sm-8">
							{{-- <div class="member-mini-description" style="padding-top:20px;"> --}}
							<h4 class="semi-bold no-margin">{!! $member->first_name !!} {!! $member->last_name !!}</h4>
							<h6 class="no-margin">{!! $member->company->company_name !!}</h6>
							<br/>
							<p>
								<i class="fa fa-envelope"></i>
								{!! $member->company->business_email !!} <i class="fa fa-info-circle" style="width:auto;color:#1d71b8; font-size:17px;" data-toggle="tooltip" data-original-title="De facturen worden naar dit emailadres verstuurd."></i>
							</p>
							<div class="clearfix"></div>
							<hr>
							<h5>Lid sinds:
								<span class="text-success semi-bold">
									{!! $member->created_at !!}
								</span>
							</h5>
							<h5>Last login:
								<span class="text-success semi-bold">
									{!! $member->last_login !!}
								</span>
							</h5>
							<h5>Affiliate: 
								{{-- <a href="{!! route('member.affiliate.index') !!}"> --}}
									
								@if($member->affiliate->status == 0)
		                        <span class="status status-inactive" data-toggle="tooltip" data-original-title="Je affiliate account is nog niet goedgekeurd. Vul de gegevens in op de affiliate pagina."></span>
		                        @elseif($member->affiliate->status == 1)
		                        <span class="status status-ending" data-toggle="tooltip" data-original-title="Je affiliate account is aangevraagd. Je ontvangt spoedig bericht van ons!"></span>
		                        @else
		                        <span class="status status-active" data-toggle="tooltip" data-original-title="Je affiliate account is actief! Laten we van start gaan!"></span>
		                        @endif
								{{-- </a> --}}
							</h5>
								
								
							{{-- </div> --}}
						</div>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="col-md-12 col-sm-12" style="padding-bottom:15px;">
						@if($member->affiliate->isValidated() < 3)
						<button class="btn btn-large btn-success btn-cons pull-right" data-toggle="modal" data-target="#approve-affiliate">Check Affiliate</button>
						@else
						<button class="btn btn-large btn-primary btn-cons pull-right" data-toggle="modal" data-target="#show-affiliate">Show Affiliate</button>
						@endif
					</div>
					{{-- <div class="col-md-12 col-sm-12" style="padding-bottom:15px;">
						<div class="closed-accord">
							<h3 class="normal">Affiliate: <button class="btn btn-primary btn-toggle" data-toggle="company-credentials"><span class="toggle">Open</span></button></h3>
						</div>
						<div class="company-credentials">
							<div class="col-md-7 user-description-box col-sm-7" style="margin:0;">
								<p>
									<i class="fa fa-user"></i>
									@if($member->company->title == 'm') Dhr. @else Mevr. @endif {!! $member->first_name !!} {!! $member->last_name !!}
								</p>
								<p>
									<i class="fa fa-envelope"></i>
									{!! $member->company->business_email !!}
								</p>
								<p>
									<i class="fa fa-map-marker"></i>
									{!! $member->company->address !!}, {!! $member->company->postal !!}, {!! $member->company->recidence !!}, {!! $member->company->country_full !!}
								</p>
								<p>
									<i class="fa fa-phone"></i>
									{!! $member->company->phone !!}
								</p>
								@if($member->company->mobile_phone)
								<p>
									<i class="fa fa-mobile-phone"></i>
									{!! $member->company->mobile_phone !!}
								</p>
								@endif
							</div>
							<div class="col-md-5 col-sm-5">
								<h3 class="normal">&nbsp;</h3>
								<h5>KvK: {!! $member->company->kvk !!}</h5>
								<h5>IBAN: {!! $member->company->iban !!}</h5>
								<h5>BTW nummer: {!! $member->company->btw !!}</h5>
								<h5>Tenaamstelling: {!! $member->company->appellation !!}</h5>
								<h5>Klant ID: {!! $member->mollie_customer_id !!}</h5>
								<h5>Klantnummer: {!! $member->wefact_debtor_id !!}</h5>
							</div>
							<div class="clearfix"></div>
						</div>
					</div> --}}
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')
@include('jandje.affiliate.show')
@include('includes.jandje.forms.approve-affiliate')
@include('includes.jandje.forms.deny-affiliate')
<script>
	$(document).ready(function(){
		$('.btn-toggle').on('click', function() {
			var toggle = $(this).data('toggle');
			$('.'+toggle).slideToggle();
			var text = $(this).text();
			$(this).text(text == "Open" ? "Close" : "Open");
		});
	});
</script>
@stop