@extends('layouts.back-end')

@section('ng-app')
ng-app="myApp"
@stop

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
@stop

@section('menubar')
@include('includes.jandje.headerbar')
@stop
@section('sidebar')
@include('includes.jandje.sidebar')
@stop

@section('content')
<div id="page-header" class="social-intro">
  <div class="container">
    <div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 social-intro-content">
      <img src="{!! config('app.images.white') !!}" alt="{!! config('app.officialname') !!}" class="logo slideOutTop" data-class-in="slideInTop" data-class-out="slideOutTop" >
      <h2 class="slogan slideOutBottom" data-class-in="slideInRight" data-class-out="slideOutRight">{!! config('app.slogan') !!}</h2>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<div class="content">
  {{-- <div ng-controller="memberController">
    <div ng-repeat="data in datas">
      <div ng-init="data">
        <p ng-bind="data"></p>
      </div>
    </div>
  </div> --}}
          <!-- BEGIN PAGE TITLE -->
          {{-- <div class="page-title">
            <h3>Master Page</h3>
          </div> --}}
          <!-- END PAGE TITLE -->
          <!-- BEGIN PlACE PAGE CONTENT HERE -->
          <div class="row-fluid" ng-controller="memberController">
            <div class="span12">
              <div class="grid simple">
                <div class="grid-title">
                  <h4>
                    Members 
                    {{-- <span ng-click="limit = 10">10</span>
                    <span ng-click="limit = 25">25</span>
                    <span ng-click="limit = 50">50</span>
                    <span ng-click="limit = 100">100</span> --}}
                    <select name="limit" ng-model="limit">
                      <option value="5">5</option>
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                    </select>
                  </h4>
                  <div class="input-prepend inside search-form no-boarder col-md-4 pull-right">
                    <span class="add-on">
                      <i class="fa fa-search"></i>
                    </span>
                    <input type="text" ng-model="search" placeholder="Zoeken.." class="no-boarder" style="width:400px;">
                    
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="grid-body">
                  <table class="table-hover table dataTable" id="example">
                    <thead>
                      <tr>
                        <th width="5%" align="center"></th>
                        <th width="5%" align="center"></th>
                        <th width="15%" ng-click="sortType = 'first_name'; sortReverse = !sortReverse">
                          Naam
                          <span class="fa fa-caret-down" ng-show="sortType == 'first_name' && !sortReverse"></span>
                          <span class="fa fa-caret-up" ng-show="sortType == 'first_name' && sortReverse"></span>
                        </th>
                        <th width="8%"  style="text-align:center; border-left:1px solid rgb(232,237,241); border-right:1px solid rgb(232, 237, 241);">
                          <span class="status show-status reset" ng-click="sortStatus = {};sortType = ['id','-affiliatestatus']; sortReverse = !sortReverse"></span>
                          <span class="status show-status active" ng-class="{2:'status-active'}[sortStatus.affiliatestatus]" ng-click="sortStatus = {affiliatestatus: 2};sortType = ['first_name',sortStatus.affiliatestatus]; sortReverse = !sortReverse"></span>
                          <span class="status show-status ending" ng-class="{1:'status-ending'}[sortStatus.affiliatestatus]" ng-click="sortStatus = {affiliatestatus: 1};sortType = ['first_name',sortStatus.affiliatestatus]; sortReverse = !sortReverse"></span>
                          <span class="status show-status inactive" ng-class="{0:'status-inactive'}[sortStatus.affiliatestatus]" ng-click="sortStatus = {affiliatestatus: 0};sortType = ['first_name',sortStatus.affiliatestatus]; sortReverse = !sortReverse"></span>
                        </th>
                        <th>Bedrijfsnaam</th>
                        <th width="10%" ng-click="sortType = 'created_at'; sortReverse = !sortReverse">
                          Lid sinds
                          <span class="fa fa-caret-down" ng-show="sortType == 'created_at' && !sortReverse"></span>
                          <span class="fa fa-caret-up" ng-show="sortType == 'created_at' && sortReverse"></span>
                        </th>
                        <th width="10%" ng-click="sortType = 'last_login'; sortReverse = !sortReverse" style="border-right:1px solid rgb(232, 237, 241);">
                          Last active
                          <span class="fa fa-caret-down" ng-show="sortType == 'last_login' && !sortReverse"></span>
                          <span class="fa fa-caret-up" ng-show="sortType == 'last_login' && sortReverse"></span>
                        </th>
                        <th width="7%">Controls</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="member in members | orderBy:['-affiliatestatus',sortType]:sortReverse | filter:search | filter:sortStatus | limitTo: limit">
                          <td align="center">
                            <a href="/jandje/members/@{{ member.id }}">
                              <span class="status @{{ member.active == 0 ? 'status-inactive' : member.active == 1 ? 'status-active' : 'status-ending' }}"></span>
                            </a>
                          </td>
                          <td align="center">
                            <a href="/jandje/members/@{{ member.id }}">
                              <div class="user-profile-pic" style="margin:0;text-align:left;">
                                <img src="@{{ member.profile_image }}" alt="@{{ member.first_name }} @{{ member.last_name }}" class="profile-image" style="height:50px;width:50px;border-width:3px;">
                              </div>
                            </a>
                          </td>
                          <td>
                              <b>@{{ member.first_name }} <span class="semi-bold">@{{ member.last_name }}</span></b>
                          </td>
                          <td align="center" style="border-left:1px solid rgb(232,237,241); border-right:1px solid rgb(232, 237, 241);">
                              <span class="status @{{ member.affiliatestatus == 0 ? 'status-inactive' : member.affiliatestatus == 1 ? 'status-ending' : 'status-active' }}"></span>
                          {{-- member.affiliate.status == 0 ? 'statis-inactive' : member.affiliate.status == 1 ? 'status-ending' : 'status-active'
                          data.sender == 'system' ? data.receiver : data.sender == 'mail' ? data.receiver : data.sender --}}
                          {{-- @if($member->affiliate)
                          @if($member->affiliate->isValidated() == 0)
                          <span class="status status-inactive"></span>
                          @elseif($member->affiliate->isValidated() == 1)
                          <span class="status status-ending"></span>
                          @else
                          <span class="status status-active"></span>
                          @endif
                          @endif
                          <span class="status">@{{ member.affiliate.id }}</span> --}}
                          
                          </td>
                          <td>
                              <span class="label label-info"><i class="fa fa-suitcase"></i> @{{ member.company.company_name }}</span>
                          </td>
                          <td>
                              @{{ member.created_at || date }}
                          </td>
                          <td style="border-right:1px solid rgb(232, 237, 241);">
                              @{{ member.last_login || date }}
                          </td>
                        <td align="center">
                          <a href="/jandje/members/@{{ member.id }}" class="btn btn-success btn-large btn-control"><i class="fa fa-eye"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          {{-- <div class="row-fluid">
            <div class="span12">
              <div class="grid simple">
                <div class="grid-title">
                  <h4>Members</h4>
                </div>
                <div class="grid-body">
                  <table class="table-hover table dataTable" id="example">
                    <thead>
                      <tr>
                        <th width="5%" align="center"></th>
                        <th width="5%" align="center"></th>
                        <th width="20%">Naam</th>
                        <th width="5%" align="center"></th>
                        <th width="45%">Bedrijfsnaam</th>
                        <th width="20%">Controls</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($members as $member) 
                      <tr>
                        <td align="center">
                        @if($member->active == 0)
                        <span class="status status-inactive"></span>
                        @elseif($member->active == 1)
                        <span class="status status-active"></span>
                        @else
                        <span class="status status-ending"></span>
                        @endif
                        </td>
                        <td align="center">
                          <div class="user-profile-pic" style="margin:0;text-align:left;">
                            <img src="{!! $member->profile_image !!}" alt="{!! $member->first_name !!} {!! $member->last_name !!}" class="profile-image" style="height:50px;width:50px;border-width:3px;">
                          </div>
                        </td>
                        <td><b>{!! $member->first_name !!} <span class="semi-bold">{!! $member->last_name !!}</span></b></td>
                        <td align="center">
                        @if($member->affiliate)
                        @if($member->affiliate->isValidated() == 0)
                        <span class="status status-inactive"></span>
                        @elseif($member->affiliate->isValidated() == 1)
                        <span class="status status-ending"></span>
                        @else
                        <span class="status status-active"></span>
                        @endif
                        @endif
                        </td>
                        <td><span class="label label-info"><i class="fa fa-suitcase"></i> {!! $member->company->company_name !!}</span></td>
                        <td>
                          <a href="{!! route('jandje.member.show',$member->id) !!}" class="btn btn-success btn-large btn-control"><i class="fa fa-eye"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div> --}}

          <!-- END PLACE PAGE CONTENT HERE -->
        </div>
@stop

@section('scripts')
<script src="/js/angular.min.js"></script>
<script>
var app = angular.module('myApp', []);
app.controller('memberController', 
  ['$scope', '$http', 
  function($scope, $http) {
    $scope.sortStatus = function (item) {
      return item;
    };
    $scope.limit = '25';
    $http.get('https://www.socialpreneurs.io/jandje/members/all')
    .then(function(response) {
      $scope.members = response.data;
      var b = 0;
      $.each($scope.members, function(key, value){
        $.each(value, function(key, value) {
          if(key == 'affiliate') {
            $scope.members[b]['affiliatestatus'] = value.status;
          }
        });
        b++;
      });
      console.log($scope.members);
      // console.log($scope.sortStatus);
    });
  }]
);
// app.filter('sortStatus', function () {
//   return function (item) {
//     return item.affiliatestatus < 2;
//   }
// });
</script>
@stop