@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
@stop

@section('menubar')
@include('includes.jandje.headerbar')
@stop
@section('sidebar')
@include('includes.jandje.sidebar')
@stop

@section('content')
<div id="page-header" class="social-intro">
  <div class="container">
    <div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 social-intro-content">
      <img src="{!! config('app.images.white') !!}" alt="{!! config('app.officialname') !!}" class="logo slideOutTop" data-class-in="slideInTop" data-class-out="slideOutTop" >
      <h2 class="slogan slideOutBottom" data-class-in="slideInRight" data-class-out="slideOutRight">{!! config('app.slogan') !!}</h2>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<div class="content">
          <!-- BEGIN PAGE TITLE -->
          {{-- <div class="page-title">
            <h3>Master Page</h3>
          </div> --}}
          <!-- END PAGE TITLE -->
          <!-- BEGIN PlACE PAGE CONTENT HERE -->

          <div class="row-fluid">
            <div class="span12">
              <div class="grid simple">
                <div class="grid-title">
                  <h4>Affiliates</h4>
                </div>
                <div class="grid-body">
                  <table class="table-hover table dataTable" id="example">
                    <thead>
                      <tr>
                        <th width="5%" align="center">Status</th>
                        <th width="5%" align="center">#</th>
                        <th width="20%">Naam</th>
                        <th width="50%">Bedrijfsnaam</th>
                        <th width="20%">Controls</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($affiliates as $affiliate) 
                      <tr>
                        <td align="center">
                        @if($affiliate->isValidated() == 0)
                        <span class="status status-inactive"></span>
                        @elseif($affiliate->isValidated() == 1)
                        <span class="status status-ending"></span>
                        @else
                        <span class="status status-active"></span>
                        @endif
                        </td>
                        <td align="center">
                          <div class="user-profile-pic" style="margin:0;text-align:left;">
                            <img src="{!! $affiliate->user->profile_image !!}" alt="{!! $affiliate->user->first_name !!} {!! $affiliate->user->last_name !!}" class="profile-image" style="height:50px;width:50px;border-width:3px;">
                          </div>
                        </td>
                        <td><b>{!! $affiliate->user->first_name !!} <span class="semi-bold">{!! $affiliate->user->last_name !!}</span></b></td>
                        <td><span class="label label-info"><i class="fa fa-suitcase"></i> {!! $affiliate->user->company->company_name !!}</span></td>
                        <td>
                          <a href="{!! route('jandje.affiliate.edit',$affiliate->id) !!}" class="btn btn-primary btn-large btn-control"><i class="fa fa-edit"></i></a>
                          <a href="{!! route('jandje.affiliate.show',$affiliate->id) !!}" class="btn btn-success btn-large btn-control"><i class="fa fa-eye"></i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <!-- END PLACE PAGE CONTENT HERE -->
        </div>
@stop