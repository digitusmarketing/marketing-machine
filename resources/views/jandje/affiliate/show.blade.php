<div class="modal fade" id="show-affiliate" tabindex="-1" role="dialog" aria-labelledby="myMydalLabel" aria-hidden="true" style="display:none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
				<br>
				<h4 id="myModalLabel" class="semi-bold">Affiliate {!! $member->first_name !!} <span class="semi-bold">{!! $member->last_name !!}</span></h4>
			</div>
			<div class="modal-body">
				<div class="row m-b-15">
					<div class="col-md-6">
						<div class="tiles blue ">
							<div class="tiles-body">
								<div class="controller">
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
								<div class="tiles-title"> KLIKKEN OP LINK </div>
								<div class="heading"> <span class="animate-number" data-value="{!! $member->affiliate->watch_count !!}" data-animation-duration="1200">0</span> </div>
								{{-- <div class="progress transparent progress-small no-radius">
									<div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>
								</div> --}}
								{{-- <div class="description"><i class="icon-custom-up"></i><span class="text-white mini-description ">&nbsp; 4% higher <span class="blend">than last month</span></span>
								</div> --}}
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="tiles blue ">
							<div class="tiles-body">
								<div class="controller">
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>
								<div class="tiles-title"> COMMISSIE </div>
								<div class="heading">&euro; <span class="animate-number" data-value="{!! $member->affiliate->getSalesAmount() !!}" data-animation-duration="1200">0</span> </div>
								{{-- <div class="progress transparent progress-small no-radius">
									<div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="26.8%"></div>
								</div>
								<div class="description"><i class="icon-custom-up"></i><span class="text-white mini-description ">&nbsp; 4% higher <span class="blend">than last month</span></span>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
				@if($member->affiliate->sales)
				<div class="row">
					<div class="col-md-12">
						<h2>Sales <span class="badge badge-primary">{!! $member->affiliate->countActiveSalesUsers() !!}</span></h2>
					</div>
					<div class="clearfix"></div>
					@foreach($member->affiliate->sales as $sale)
						<div class="tiles white ">
							<div class="p-t-20 p-b-15 b-b b-grey">
								<div class="post overlap-left-10">
									<div class="user-profile-pic-wrapper">
										<div class="user-profile-pic-2x white-border">
											<img width="45" height="45" src="{!! $sale->order->user->profile_image !!}" data-src="{!! $sale->order->user->profile_image !!}" data-src-retina="{!! $sale->order->user->profile_image !!}" alt="">
										</div>
									</div>
									<div class="info-wrapper small-width inline">
										<div class="info text-black ">
											<p>{!! $sale->order->user->first_name !!} {!! $sale->order->user->last_name !!}</p>
											<p class="muted small-text"> {!! $sale->order->created_at !!} - <b>{!! $sale->order->order_id !!}</b></p>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="inline pull-right">
										<div class="tiles text-white p-t-5 p-l-5 p-b-5 p-r-5 inline"> <i class="fa fa-eye fa-lg"></i> </div>
										<div class="tiles white p-t-5 p-l-5 p-b-5 p-r-5 inline"> <i class="fa fa-comment-o fa-lg"></i> </div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				@endif
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-large btn-cons" data-dismiss="modal"><i class="fa fa-times-circle"></i> close..</button>
				{{-- <button type="button" class="btn btn-primary btn-large btn-cons" data-dismiss="modal"><i class="fa fa-check-circle"></i> Goedkeuren..</button> --}}

				{{-- {!! Form::open(['route'=>'jandje.affiliate.approve', 'class'=>'pull-right']) !!}
				{!! Form::hidden('affiliate_id',$member->affiliate->id) !!}
				<button type="submit" class="btn btn-primary btn-large btn-cons"><i class="fa fa-check-circle"></i> Goedkeuren..</button>
				{!! Form::submit('Ja, ik weet het zeker', array('class'=>'btn btn-danger btn-large btn-cons')) !!}
				{!! Form::close() !!} --}}
			</div>

		</div>
	</div>
</div>