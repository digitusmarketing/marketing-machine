@extends('layouts.back-end')

@section('css')
<link rel="stylesheet" href="{{ url('/css/video/module.css') }}">
<link rel="stylesheet" href="{{ url('/css/charts/morris.css') }}">
@stop

@section('menubar')
@include('includes.jandje.headerbar')
@stop
@section('sidebar')
@include('includes.jandje.sidebar')
@stop

@section('content')
<div id="page-header" class="social-intro">
  <div class="container">
    <div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 social-intro-content">
      <img src="{!! config('app.images.white') !!}" alt="{!! config('app.officialname') !!}" class="logo slideOutTop" data-class-in="slideInTop" data-class-out="slideOutTop" >
      <h2 class="slogan slideOutBottom" data-class-in="slideInRight" data-class-out="slideOutRight">{!! config('app.slogan') !!}</h2>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<div class="content">
          <!-- BEGIN PAGE TITLE -->
          {{-- <div class="page-title">
            <h3>Master Page</h3>
          </div> --}}
          <!-- END PAGE TITLE -->
          <!-- BEGIN PlACE PAGE CONTENT HERE -->

            {{-- @foreach($users as $user)
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 m-b-10">
              <div class="widget-item">
                <div class="tiles green overflow-hidden full-height">
                  <div class="overlayer bottom-right fullwidth">
                    <div class="overlayer-wrapper">
                      <div class="tiles gradient-black p-l-20 p-r-20 p-b-20 p-t-20">
                        <div class="pull-right">
                          <a class="hashtags transparent"><i class="fa fa-suitcase"></i> @if($user->company){!! $user->company->company_name !!}@endif</a>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <img src="{!! $user->cover !!}" alt="" class="lazy hover-effect-img image-responsive-width" style="max-height:250px;min-height:250px;">
                </div>
                <div class="tiles white">
                  <div class="tiles-body">
                    <div class="row">
                      <div class="user-profile-pic text-left">
                        <img src="{!! url($user->profile_image) !!}" width="69" height="69" alt="">
                        <div class="pull-right m-r-20 m-t-35">
                          <span class="bold text-black small-text">
                            @if($user->created_at)
                            {!! $user->created_at !!}
                            @endif
                          </span>
                        </div>
                      </div>
                      <div class="col-md-7 no-padding">
                        <div class="user-comment-wrapper">
                          <div class="comment">
                            <div class="user-name text-black bold">
                              {!! $user->first_name !!}
                              <span class="semi-bold">{!! $user->last_name !!}</span>
                            </div>
                            <div class="preview-wrapper">{!! $user->username !!}</div>                            
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="col-md-5 no-padding">
                        <div class="clearfix"></div>
                        <button class="btn">Bekijk de user</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            @endforeach --}}
            <div class="col-lg-4 col-md-4">
              <div class="tiles purple">
                <div class="tiles-body">
                  <div class="tiles-title">Vorige maand omzet:</div>
                  <?php $last_month_percentage = number_format((($last_month_revenue - $current_month_revenue) / $current_month_revenue) * 100, 2, '.',' '); ?>
                  <?php $last_percentage = number_format(($last_month_revenue/2000) * 100, 0, '.',' '); ?>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Sales</span>
                      <span class="item-count animate-number semi-bold" data-value="{!! count($filtered_last_month) !!}" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="heading">
                    &euro; <span class="animate-number" data-value="{!! $last_month_revenue !!}" data-animation-duration="700">0</span>
                  </div>
                  <div>
                      <p class="pull-right">Doel: &euro;2000,-</p>
                    <div class="progress transparent progress-white progress-small no-radius">
                      <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="{{ $last_percentage }}%" style="width: {{ $last_percentage }}"></div>
                    </div>
                  </div>
                  <div class="description">
                    @if(substr($last_month_percentage,0,1) == '-' )
                    <i class="fa fa-arrow-circle-o-down"></i>
                    @else
                    <i class="fa fa-arrow-circle-o-up"></i>
                    @endif
                    <span class="text-white mini-description">
                      &nbsp; <span class="animate-number" data-value="{{ $last_month_percentage }}" data-animation-duration="1400">0</span>% 
                      @if(substr($last_month_percentage,0,1) == '-')
                      lager 
                      @else
                      hoger 
                      @endif <span class="blend">dan de huidige maand</span>
                    </span>
                  </div>
                </div>
                <div class="tiles white">
                  <div class="tiles-body">
                    <h5 class="semi-bold">LAST SALES</h5>
                    <table class="table no-more-tables m-t-20 m-l-20 m-b-30">
                      <thead style="display:none">
                        <tr>
                          <th style="width:9%">Referentie</th>
                          <th style="width:22%">Betaalmethode</th>
                          <th style="width:6%">Commissie</th>
                          <th style="width:1%"> </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($filtered_last_month as $sale)
                        <tr>
                          <td class="v-align-middle bold text-success">{!! $sale->order_id !!}</td>
                          <td class="v-align-middle"><span class="muted"><span class="label label-primary">{!! $sale->paymentmethod !!}</span></span> </td>
                          <td><span class="muted bold text-success">&euro; {!! number_format($sale->price, 2, '.', ',') !!}</span> </td>
                          <td class="v-align-middle"></td>
                          <td>{!! $sale->created_at !!}</td>
                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                    <div id="sales-graph"> </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="tiles green">
                <div class="tiles-body">
                  <div class="tiles-title">Deze maand omzet:</div>
                  <?php $current_month_percentage = number_format((($current_month_revenue - $last_month_revenue) / $last_month_revenue) * 100, 2, '.',' '); ?>
                  <?php $current_percentage = number_format(($current_month_revenue/2000) * 100, 0, '.',' '); ?>
                  <div class="heading">
                    &euro; <span class="animate-number" data-value="{!! $current_month_revenue !!}" data-animation-duration="1400">0</span>
                  </div>
                  <div>
                    <p class="pull-right">Doel: &euro;2000,-</p>
                    <div class="progress transparent progress-white progress-small no-radius">
                      <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="{{ $current_percentage }}%" style="width: {{ $current_percentage }}"></div>
                    </div>
                  </div>
                  <div class="description">
                    @if(substr($current_month_percentage,0,1) == '-' )
                    <i class="fa fa-arrow-circle-o-down"></i>
                    @else
                    <i class="fa fa-arrow-circle-o-up"></i>
                    @endif
                    <span class="text-white mini-description">
                      &nbsp; <span class="animate-number" data-value="{{ $current_month_percentage }}" data-animation-duration="1400">0</span>% 
                      @if(substr($current_month_percentage,0,1) == '-')
                      lager 
                      @else
                      hoger 
                      @endif <span class="blend">dan vorige maand</span>
                    </span>
                  </div>
                </div>
                <div class="tiles white">
                  <div class="tiles-body">
                    <h5 class="semi-bold">LAST SALES</h5>
                    <table class="table no-more-tables m-t-20 m-l-20 m-b-30">
                      <thead style="display:none">
                        <tr>
                          <th style="width:9%">Referentie</th>
                          <th style="width:22%">Betaalmethode</th>
                          <th style="width:6%">Commissie</th>
                          <th style="width:1%"> </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($filtered_current_month as $sale)
                        <tr>
                          <td class="v-align-middle bold text-success">{!! $sale->order_id !!}</td>
                          <td class="v-align-middle"><span class="muted"><span class="label label-primary">{!! $sale->paymentmethod !!}</span></span> </td>
                          <td><span class="muted bold text-success">&euro; {!! number_format($sale->price, 2, '.', ',') !!}</span> </td>
                          <td class="v-align-middle"></td>
                          <td>{!! $sale->created_at !!}</td>
                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                    <div id="sales-graph"> </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4">
              <div class="tiles blue">
                <div class="tiles-body">
                  <div class="tiles-title">Totale omzet:</div>
                  <?php $total_revenue = number_format($current_month_revenue + $last_month_revenue, 2, '.',''); ?>
                  <div class="heading">
                    &euro; <span class="animate-number" data-value="{!! $total_revenue !!}" data-animation-duration="1400">0</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12">
              <div id="line-example"> </div>
            </div>
          <!-- END PLACE PAGE CONTENT HERE -->
        </div>
@stop

@section('scripts')
<script src="/js/charts/morris.min.js"></script>
<script src="/js/charts/jquery.flot.js"></script>
<script src="/js/charts/jquery.flot.animator.min.js"></script>
<script src="/js/charts/jquery-sparkline.js"></script>
<script src="/js/charts/d3.v2.js"></script>
<script src="/js/charts/rickshaw.min.js"></script>
<script src="/js/charts/raphael-min.js"></script>
<script>
$.ajax({
  type: 'GET',
  url: 'https://www.socialpreneurs.io/jandje/orders',
  success: function(data) {
    console.log(data);
    var array = '';
    $.each(data, function(index, value) {
      array += "{ y: '"+index+"', a: "+value+" },";
      console.log("{ y: '"+index+"', a: "+value+" },");
      
    });
    $(document).ready(function(){

      Morris.Line({
        element: 'line-example',
        data: [
          array
        ],
        xkey: 'y',
        ykeys: ['a'],
        xLabels: 'month',
        labels: ['Abonnementen'],
        lineColors:['#0aa699'],
          
          // axes: false,
      });
  });
  },
});

</script>
@stop