<?php

use Illuminate\Database\Seeder;

class CursusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cursussen')->insert([
            'title'=>'Facebook als Marketing Machine',
            'body'=>'test test test'
        ]);
        DB::table('cursussen')->insert([
            'title'=>'De Perfect Facebook Ad',
            'body'=>'gewoon nogmaals testen'
        ]);
    }
}
