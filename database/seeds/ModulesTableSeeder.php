<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'title' => 'Strategie',
            'slug' => 'strategie',
            'image' => 'images/site/cursus/modules/strategie.png',
            'body' => 'In deze videos nemen we jou aan de hand om jouw strategie te bepalen. Wat zet je in om in contact te komen met jouw potentiële klanten? Hoe ga je potentiële klanten converteren naar betalende klanten? En hoe ga je Facebook hiervoor inzetten?',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Doelgroep',
            'slug' => 'doelgroep',
            'image' => 'images/site/cursus/modules/doelgroep.png',
            'body' => 'In deze videos laten we jou zien hoe je eenvoudig de locatie, leeftijd, geslacht en andere demografische gegevens achterhaalt van jouw huidige Facebook fans. Ook laten we jou zien hoe je eenvoudig onderzoekt welke (sub)doelgroepen nog meer uiterst interessant zijn voor jouw business om te targeten.',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Onweerstaanbare Marketing Boodschap',
            'slug' => 'onweerstaanbare-marketing-boodschap',
            'image' => 'images/site/cursus/modules/onweerstaanbare-marketing-boodschap.png',
            'body' => 'Nadat je de doelgroepen die je wil gaan bereiken goed in kaart hebt gebracht, ga je voor jouw business ontzettend veel relevante Facebook gebruikers bereiken. Het is van belang dat jouw advertenties en landingspagina overtuigend zijn om jouw doelgroep de actie uit te laten voeren die jij wil. Wij laten jou zien hoe je dit overtuigend doet zonder te “verkopen”, en zodat je jouw teksten altijd dichtbij jezelf houdt.',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Facebook Pixels',
            'slug' => 'facebook-pixels',
            'image' => 'images/site/cursus/modules/facebook-pixels.png',
            'body' => 'Wanneer je advertentie tegoed besteed op Facebook wil je er natuurlijk zeker van zijn dat je hier het maximale uit haalt. Het is belangrijk om jouw Facebook pixels eenmalig goed te plaatsen, zodat je altijd een duidelijk overzic ht hebt van de resultaten en aan de hand daarvan kunt optimaliseren indien nodig. Daarnaast is het van belang dat je bezoekers van jouw website of specifieke webpagina’s opnieuw kunt bereiken door middel van remarketing.',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Aangepaste & Vergelijkbare Doelgroepen',
            'slug' => 'aangepaste-vergelijkbare-doelgroepen',
            'image' => 'images/site/cursus/modules/aangepaste-en-vergelijkbare-doelgroepen.png',
            'body' => 'Het bereiken van jouw website bezoekers, personen op jouw mailinglijst of jouw huidige klantenbestand is ontzettend belangrijk, deze personen kennen en vertrouwen jou al. Wij laten je zien hoe je deze opnieuw bereikt met aangepaste doelgroepen. Daarnaast is het natuurlijk super interessant om nieuwe personen te bereiken die overeenkomen met jouw website bezoekers, personen op jouw mailinglijst en/of jouw huidige klantenbestand. Wij laten jou zien hoe je vergelijkbare doelgroepen maakt van personen die hetzelfde gedrag vertonen, dezelfde interesses hebben en een vergelijkbaar profiel hebben met jouw huidige klanten.',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Opgeslagen Doelgroepen',
            'slug' => 'opgeslagen-doelgroepen',
            'image' => 'images/site/cursus/modules/opgeslagen-doelgroepen.png',
            'body' => 'Nu je alle relevante (sub)doelgroepen goed in kaart hebt, ga je hier opgeslagen doelgroepen van maken. Zodat je bij jouw campagnes jouw opgeslagen doelgroep eenvoudig met 2 klikken toevoegt en hier niet meer naar om hoeft te kijken. Deze doelgroepen kun je ook voor alle toekomstige campagnes in gaan zetten.',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Advertentie Bericht',
            'slug' => 'advertentie-bericht',
            'image' => 'images/site/cursus/modules/advertentie-bericht.png',
            'body' => 'In deze video laten we jou zien hoe je effectieve advertenties opzet. Waar moet je op letten bij het kiezen van je advertentie type? Hoe maak/kies je een goede advertentie afbeelding? Aan welke specificaties moet jouw advertentie voldoen zodat Facebook hem niet afkeurt? Ook laten we jou zien hoe je ervoor zorgt dat alle reacties, shares en likes van verschillende doelgroepen onder dezelfde advertentie komen.',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Power Editor & Campagne opbouw',
            'slug' => 'power-editor-campagne-opbouw',
            'image' => 'images/site/cursus/modules/power-editor-en-campagne-opbouw.png',
            'body' => 'Nu je alle voorbereidingen hebt getroffen laten we jou zien hoe je jouw campagne in Facebook’s Power Editor bouwt en hoe je te allen tijde overzicht houdt in jouw campagne structuur. Ook leer je wat verstandig is qua budget en looptijd van je campagne en hoe je dit instelt voor maximale resultaten. Na deze video heb je jouw volledige campagne tot in de puntjes uitgewerkt en ga je jouw campagne aanzetten.',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Onderhoud & Optimalisatie',
            'slug' => 'onderhoud-optimalisatie',
            'image' => 'images/site/cursus/modules/onderhoud-en-optimalisatie.png',
            'body' => 'Na het uitvoeren van alle voorgaande opdrachten heb je een ijzersterke campagne staan. De meeste ondernemers focussen zich op de verkeerde statistieken, wij laten jou zien welke statistieken relevant zijn om gemakkelijk inzicht te krijgen in welke doelgroepen en advertenties het best voor jou presteren. Wij vinden het zelf altijd super tof om de kosten per gewenst resultaat te pushen tot het uiterste. We laten jou zien hoe wij dit doen en hoe ook jij de laagste kosten voor het beste resultaat behaalt.',
            'cursus_id' => 1,
        ]);
        DB::table('modules')->insert([
            'title' => 'Afsluiting',
            'slug' => 'afsluiting',
            'image' => 'images/site/cursus/modules/afsluiting.png',
            'body' => 'Afsluitvideo. <br><br>

"<i style="font-style:italic;">Afscheid nemen bestaat niet, ik ga wel weg maar verlaat je niet.</i>"<br>
- Marco Borsato',
            'cursus_id' => 1,
        ]);
    }
}
