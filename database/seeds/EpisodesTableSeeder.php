<?php

use Illuminate\Database\Seeder;

class EpisodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('episodes')->insert([
            'title' => 'Specifiek product / dienst',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180718690.hd.mp4?s=0c58af10be973ceeec4c0204db646aacf9b65f5c&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589479279.jpg',
            'module_id' => 1,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Waarde bieden',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180718854.hd.mp4?s=7ecc4c2ec4479e82f7e2855631a788af749eebf7&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589479407.jpg',
            'module_id' => 1,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Strategie bepalen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180718883.hd.mp4?s=07de21f26d24aa15d01de15ac086a569bb043f7c&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589479553.jpg',
            'module_id' => 1,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Opdrachten module 1',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/181160896.hd.mp4?s=ba954ba273bf3718436cabd63ca76142d6e4a86b&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589634828.jpg',
            'module_id' => 1,
       ]);

        DB::table('episodes')->insert([
            'title' => 'Specifieke doelgroep',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180719069.hd.mp4?s=925d5130b6c861079820cf73ce4d6bc35d98c72a&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589479797.jpg',
            'module_id' => 2,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Basis doelgroep bepalen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180719111.hd.mp4?s=7de2a90b7af5e3569d39eecc27a3db976ccb12d8&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589481838.jpg',
            'module_id' => 2,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Doelgroep onderzoek',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180719134.hd.mp4?s=d657ff47fa9ee1985a5eb5a0007f1dccd59780b4&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589482030.jpg',
            'module_id' => 2,
       ]);
        DB::table('episodes')->insert([
            'title' => '[Extra] Advertentie account instellingen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180719168.hd.mp4?s=c89e49e3085292ebd7b12c0c84f064124fb6e340&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589482276.jpg',
            'module_id' => 2,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Opdrachten module 2',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/181160894.hd.mp4?s=fcfcd3bec39c100f28ef39c4d1227487d8606873&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589635964.jpg',
            'module_id' => 2,
       ]);

        DB::table('episodes')->insert([
            'title' => 'Introductie',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180924777.hd.mp4?s=94097df179da13ad9b710b9f4591b99b9f531e20&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589482860.jpg',
            'module_id' => 3,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Hook & Promise',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180924776.hd.mp4?s=c58d2750b1f4e2dc5c9c0a97088183181fdffdba&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589483241.jpg',
            'module_id' => 3,
       ]);
        DB::table('episodes')->insert([
            'title' => 'AIDA Model',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180924775.hd.mp4?s=f5ea868c5726fbaa120759f98008222f64666472&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589483522.jpg',
            'module_id' => 3,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Opdrachten module 3',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/181164045.hd.mp4?s=861efab6e4ae7205abfd0e8a5df46877cc891e86&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/611307498.jpg',
            'module_id' => 3,
       ]);

        DB::table('episodes')->insert([
            'title' => 'Wat zijn Facebook pixels',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180719219.hd.mp4?s=9107d5712c83f80ca184ca2363d094ab84df66b5&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589483725.jpg',
            'module_id' => 4,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Facebook pixel maken en plaatsen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180719250.hd.mp4?s=b5374065e04f0e3e755d3ce3572633aa75d47a41&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589483922.jpg',
            'module_id' => 4,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Facebook pixel controleren',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180719295.hd.mp4?s=96a7a96e79585187d9a3a3b6935af5d269d5d9a5&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589484250.jpg',
            'module_id' => 4,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Opdrachten module 4',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/181160898.hd.mp4?s=bfab402a71bbc80a6735d306695015d20cbca178&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589636203.jpg',
            'module_id' => 4,
       ]);

        DB::table('episodes')->insert([
            'title' => 'Wat zijn aangepaste- en vergelijkbare doelgroepen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925163.hd.mp4?s=18c39ade98cd392c8a08b4a66ceea6369d767b61&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589485274.jpg',
            'module_id' => 5,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Hoe maak ik aangepaste doelgroepen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180924965.hd.mp4?s=8b7091388be7200ec3579af4894b0b2ad35cbf24&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589485739.jpg',
            'module_id' => 5,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Hoe maak ik vergelijkbare doelgroepen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180924962.hd.mp4?s=fecb9f4e9cd5ed00f00319e6a92bfece61d676c7&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589485938.jpg',
            'module_id' => 5,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Opdrachten module 5',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/181160899.hd.mp4?s=358b16e2daaa54b7857e3a653e563ad3e8241d8f&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589636308.jpg',
            'module_id' => 5,
       ]);

        DB::table('episodes')->insert([
            'title' => 'Wat zijn opgeslagen doelgroepen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925221.hd.mp4?s=dcccb5c70cd273e17ca71afa8ddc98c343976ac4&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589486259.jpg',
            'module_id' => 6,
       ]);
        DB::table('episodes')->insert([
            'title' => 'Aangepaste doelgroepen toevoegen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925223.hd.mp4?s=06adc42748d75867980529cbe9a51ec84a255173&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589486598.jpg',
            'module_id' => 6,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Basis doelgroep selecteren',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925225.hd.mp4?s=f744f86d9cac6340e14c211362bec1e2aacffb3d&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589486833.jpg',
            'module_id' => 6,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Gedetaileerde doelgroepen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925226.hd.mp4?s=23aaa546b40ee30225e26b225644f97f21beadb7&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589487067.jpg',
            'module_id' => 6,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Connecties',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925370.hd.mp4?s=2bc2590c004c46c4d246c78e217481abeb1af0ab&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589487287.jpg',
            'module_id' => 6,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Opdrachten module 6',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/181164091.hd.mp4?s=06d08f922c6af47ada1c419efb4705d503d1a501&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589636439.jpg',
            'module_id' => 6,
       ]);

       DB::table('episodes')->insert([
            'title' => 'Facebook Darkpost',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925427.hd.mp4?s=25bae7265f979aa162344240678a30291157de48&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589487630.jpg',
            'module_id' => 7,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Advertentie type en onderdelen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925428.hd.mp4?s=03d4750502bcfcc490e44e23a71fe9e6d01f2f48&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589487838.jpg',
            'module_id' => 7,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Advertentie afbeelding',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925429.hd.mp4?s=9b7f19c02de522ce760e3f75631b182af6e5b0e7&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589488040.jpg',
            'module_id' => 7,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Advertentie samenstellen en controleren',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925425.hd.mp4?s=ef4eb04998a7376f3f4b9e567e9d8be1b2557fd4&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589488249.jpg',
            'module_id' => 7,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Opdrachten module 7',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/181160901.hd.mp4?s=10aa0080bc5600a3443728eeac58deabbc275e27&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589636526.jpg',
            'module_id' => 7,
       ]);

       DB::table('episodes')->insert([
            'title' => 'Het geraamte van jouw Facebook campagne',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925612.hd.mp4?s=9bcfcacb4a0027219113f55f8e5167f01b029aa4&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589488487.jpg',
            'module_id' => 8,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Geraamte bouwen in de Facebook power editor',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925613.hd.mp4?s=96b5ab5b9e89c4e488aa1b7f632c9212b6dea1e9&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589488765.jpg',
            'module_id' => 8,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Advertentie-set instellen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925614.hd.mp4?s=f30a26b6efa25122f394d032b4fb49ce82dce850&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589488958.jpg',
            'module_id' => 8,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Advertenties instellen',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925615.hd.mp4?s=7f14dc0bca93b8ba28ea138390921cdd6eb377e0&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589489171.jpg',
            'module_id' => 8,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Campagne dupliceren en hergebruiken',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925616.hd.mp4?s=a209a9579a0c0a36807af6d3e59775215855679d&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589489394.jpg',
            'module_id' => 8,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Opdrachten module 8',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/181160895.hd.mp4?s=98cf9d3cdb98d459e86df9e88094dfd3e37bd6e5&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589636626.jpg',
            'module_id' => 8,
       ]);

       DB::table('episodes')->insert([
            'title' => 'Optimaliseren op Advertentie-set niveau',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925755.hd.mp4?s=0e2c290b1d694df62328705dd8c95e6a0fdff71c&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589489632.jpg',
            'module_id' => 9,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Optimaliseren op Advertentie niveau',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925756.hd.mp4?s=2d90624ec460788e4ead24584a08b611a828a609&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589489854.jpg',
            'module_id' => 9,
       ]);
       DB::table('episodes')->insert([
            'title' => 'Conversieratio Landingspagina',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925749.hd.mp4?s=ab5a2f95a3b24824d475559ddbff075b8c41d01c&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589490067.jpg',
            'module_id' => 9,
       ]);

       DB::table('episodes')->insert([
            'title' => 'Onze cursus aanraden',
            'body' => 'Een korte samenvatting van de video',
            'src' => 'https://player.vimeo.com/external/180925753.hd.mp4?s=135a33256ed475c01ef7068269b5c1a486b1ab22&profile_id=119',
            'poster' => 'https://i.vimeocdn.com/video/589490279.jpg',
            'module_id' => 10,
       ]);
    }
}
