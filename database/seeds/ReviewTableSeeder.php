<?php

use Illuminate\Database\Seeder;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reviews')->insert([
            'review_name' => 'René Taling',
            'company' => 'Dojo Burning Heart',
            'image' => 'https://www.digitusmarketing.nl/images/digitus/recensies/rene-taling.jpg',
            'video_link' => null,
            'body' => '<p>Mijn naam is Rene Taling en ben 25 jaar werkzaam geweest bij Dagbladen en Reclamebureaus. Sinds 5 jaar werk ik nu als allround marketing medewerker bij sportscholen. De laatste 2 en 1/2 jaar bij Sportschool Dojo Burning Heart in Hilversum. Sinds 1 oktober hebben we drie vestigingen. Inmiddels weer volop bezig met ontwerpen. Wat nieuw voor mij is het beheer en onderhoud websites en Facebookadvertenties. Het onderhouden en ontwikkelen van campagnes voor Facebook is voor mij best lastig omdat het een totaal andere wereld is dan vormgeven/ontwerpen.</p>

<p>Zoals ik al beschreef kwam ik ruimschoots tekort bij het maken van campagnes en doordat ik nogal visueel ben ingesteld, leek een videocursus mij een heel goed idee. De resultaten zijn nogal indrukwekkend. 1x campagne gestart "The Burning Four". Mochten de eerste 20 inschrijvingen 4 weken lang Gratis kickboksen. Was in 2 of 3 dagen vol. Laatste campagne "Open dag Dansschool" en "Ladies Only" dames sportschool, ging om inschrijvingen realiseren voor de opening van de nieuwe locatie. 33 inschrijvingen tijdens de Open Dag (gemiddeld €37,50 p/m).</p>

<p>Ik heb in een korte tijd heel goed begrepen dat het van groot belang is om een goede campagne te voeren op Facebook. Je kunt echt niet zonder als je resultaat wil halen, tenminste.</p>

<p>Wat ik wil zeggen tegen ondernemers die nog twijfelen om met deze cursus aan de slag te gaan: ‘<b>Stop waiting for something that is never going to happen and instead get going!</b>’ Om het dicht bij de sportschool te houden, dit is een uit spraak van Sylvester Stallone (Rocky). Als je ook maar een beetje twijfelt ga ervoor, het is echt de moeite waard!!</p>',
        ]);
        DB::table('reviews')->insert([
            'review_name' => 'Sascha Hillman',
            'company' => 'Kapsalon Inn Hair',
            'image' => 'https://www.digitusmarketing.nl/images/digitus/recensies/Sascha-Hillmann.jpg',
            'video_link' => null,
            'body' => '<p>Ik ben al bijna 30 jaar kapper en heb sinds 1992 een eigen zaak. Op dit moment werk ik weer alleen. Maar door het succes van de cursus moet ik toch weer personeel erbij gaan nemen.</p>

<p>Sinds 1 jaar ben ik me gaan specialiseren in bepaalde kleurtechnieken en op die manier probeer ik me als kleurexpert te onderscheiden tussen alle andere kapsalons in de buurt. Ik ben al eerder bezeig geweest met FB marketing. Maar dit was meer op de manier van content marketing. Dit kost te veel tijd en levert te weinig resultaat op. Jullie video cursus sprak me aan omdat jullie een ander aspect van FB marketing behandelen. Namelijk de ads. En dit levert snellere resultaten. Daarnaast was het persoonlijke contact die ik met jullie had voor de start van de cursus ook voor mij bepalend. Ben nou eenmaal een gevoelsmens :-) Ik zie bij jullie dezelfde passie voor jullie vak als dat ik heb voor mijn vak. En dat is mooi!!</p>

<p>De resultaten zijn uitstekend te noemen. Ik ben namelijk op dit moment aan het overwegen om de campagne stop te zetten, omdat ik de aanmeldingen bijna niet meer krijg ingepland. Op dit moment geïnvesteerd een bedrag van rond de €257 en voor minimaal €3250 aan afspraken ingepland.</p>

<p>Grootste inzicht dat ik heb gekregen is dat het echt werkt om gericht op een doelgroep te adverteren. Mits je je goed hebt verdiept in je ideale klant.</p>

<p>Wat ik wil zeggen tegen ondernemers die nog twijfelen om met deze cursus aan de slag te gaan: Begin er nu aan voordat je concurrenten ook hiermee aan de slag gaan :-)</p>',
        ]);

        DB::table('reviews')->insert([
            'review_name' => 'Lars Blokdijk',
            'company' => 'Coworkparadise',
            'image' => null,
            'video_link' => '<iframe src="https://player.vimeo.com/video/188794760" class="iframe-top" width="640" height="480" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" style="height: 328.97px;"></iframe>',
            'body' => null,
        ]);
        DB::table('reviews')->insert([
            'review_name' => 'Linda Even',
            'company' => 'Menukaartmeisje.nl',
            'image' => null,
            'video_link' => '<iframe src="https://player.vimeo.com/video/188817102" class="iframe-top" width="640" height="360" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" style="height: 328.97px;"></iframe>',
            'body' => null,
        ]);

        DB::table('reviews')->insert([
            'review_name' => 'Heinrich von Esch',
            'company' => 'Droog Trainen Academie',
            'image' => 'https://www.digitusmarketing.nl/images/digitus/recensies/Heinrich-von-esch.jpg',
            'video_link' => null,
            'body' => '<p>Ik ben al een jaar of 2 bezig met Facebook marketing. Ik verkoop een eBook pakket en mijn bedrijf is voor een groot deel afhankelijk van Facebook. Ik kwam bij de heren van digitus marketing terecht via een Facebook ad die adverteerde voor een webinar over de Facebook marketing machine(goede targetting ;-)). De webinar zat goed in elkaar en Parsifal Tritsch en Rogier van Voorst leken te weten waar ze het over hadden. Ik was in eerste instantie wat sceptisch over de cursus van de heren van digitus marketing, omdat ik zelf al langere tijd met FB marketing bezig was (hoewel de conversies de laatste maanden wat teruggelopen waren). Maar er zat een 30 dagen niet goed geld terug garantie bij dus ik dacht, why not. Ik heb de cursus binnen drie dagen gevolgd en ben meteen begonnen met het toepassen van stof uit de cursus. Ik moet zeggen dat ik onder de indruk ben van het resultaat, De bezoekers van Facebook zijn verdubbeld met dezelfde kosten die ik had vòôr de cursus, en dat met de eerste campagne. Ik ben er van overtuigd dat ik nog betere resultaten behaal als ik de campagnes nog verder finetune. Deze cursus is zeker een aanrader! Bedankt heren</p>',
        ]);
        DB::table('reviews')->insert([
            'review_name' => 'Martin Salet',
            'company' => 'DavidMartinBags.com',
            'image' => 'https://www.digitusmarketing.nl/images/digitus/recensies/Martin-salet.jpg',
            'video_link' => null,
            'body' => '<p>Mijn naam is Martin Salet, partner bij Designstudio AstridDavidse.com en we hebben sinds augustus 2016 een webshop met eigen design reistassen: DavidMartinBags.com. Ik ben met de Marketing Machine cursus gestart omdat het me goede, nuchtere informatie leek met een goed gevoel voor de menselijke maat en een betaalbare prijs.</p>

<p>De eerste resultaten die ik heb geboekt zijn van 0 naar 100% en meer gegaan. Na de eerste verkoopgolf aan vrienden en bekenden kwam de verkoop aan “echte klanten” pas los na de eerste serieuze Facebook campagne. Totaal geïnvesteerd €250,- en een omzet van 1500,- in 2 weken.</p>

<p>Het grootste inzicht dat ik heb gekregen is dat Facebook marketing serious business is. Ik weet zeker dat er nog veel meer potentie inzit voor mijn webshop. Aan iedereen die twijfelt om met deze cursus aan de slag te gaan wil ik zeggen; investeer in jezelf, start met Digitus en leg jezelf vast aan 10 dagen Facebook training, elk uur dat je investeert is het dubbel en dwars waard.</p>',
        ]);
        DB::table('reviews')->insert([
            'review_name' => 'Erna Basten',
            'company' => 'Praktijk Erna Basten',
            'image' => 'https://www.digitusmarketing.nl/images/digitus/recensies/erna-basten.jpg',
            'video_link' => null,
            'body' => '<p>Ik zou de video training aanraden aan iedereen. Voor het bereik van een groter publiek maar ook voor het genereren van meer klanten. Je kunt het zelf leren en daardoor heel makkelijk promoties maken. Ik sta versteld hoe deze akties werken en er is niets wat sneller werkt.</p>

<p>Hou er wel rekening mee dat wanneer je gaat werken met een facebookactie je het druk krijgt. Zorg dat je een goede planning maakt want het wordt zo druk. De eerste keer heb ik er even van bij moeten komen (:</p>',
        ]);
        DB::table('reviews')->insert([
            'review_name' => 'Judith Strating',
            'company' => "Judith's Dance Point",
            'image' => 'https://www.digitusmarketing.nl/images/recensies/21/Judith-strating.jpg',
            'video_link' => null,
            'body' => '<p>Ik had mij al een beetje vediept in Facebook advertenties. Toen ik hoorde over de videocursus van Digitus, dacht ik dit is voor mij dé oplossing! “Ik heb mijn volledige investering binnen de eerste 2 weken al terug verdiend! Los van het aantal inschrijvingen dat ik nu via Facebook binnenhaal, merk ik dat er veel meer over mijn dansschool gepraat wordt. Ook merk ik dat ik meer likes krijg en dat mijn berichten veel meer mensen bereiken. Kortom, het levert mij veel meer op dan alleen inschrijvingen! Bedankt Digitus!”</p>',
        ]);
        DB::table('reviews')->insert([
            'review_name' => 'Maurice Krabbenborg',
            'company' => 'kaartjeposten.nl',
            'image' => 'https://www.digitusmarketing.nl/images/digitus/recensies/maurice-krabbenborg.jpg',
            'video_link' => null,
            'body' => '<p>Na eerdere (zelf opgezette)campagnes op Facebook die nogal matig presteerden, toch (op advies van een bevriende ondernemer) eens gaan luisteren naar de jongens van Digitus Marketing.</p>

<p>Onder hun begeleiding in korte tijd veel te weten gekomen over de vele mogelijkheden vwb adverteren op Facebook. Helder verhaal en snelle feedback op vragen.</p>',
        ]);
        DB::table('reviews')->insert([
            'review_name' => 'Anouk Bleekemolen',
            'company' => 'laatjecoachen.nu',
            'image' => 'https://www.digitusmarketing.nl/images/digitus/recensies/anouk-bleekemolen.jpg',
            'video_link' => null,
            'body' => '<p>Ik had in het verleden wel eens een advertentie op fb gemaakt. Dan via het scherm ‘promoot bericht’. Nu dus een cursus gevolgd om betere advertenties te maken.</p>

<p>Met de tools die ik nu gekregen heb kan ik veel gerichter mijn doelgroep bereiken en dus met minder advertentiekosten betere resultaten halen.</p>

<p>Ik weet hoe ik verschillende doelgroepen kan aanmaken, verder kan verfijnen en daarna kan sturen. Het doel is natuurlijk uiteindelijk met de minste kosten het hoogste resultaat halen.</p>

<p>Ik ben niet bepaald technisch maar ik vind het nu echt leuk om te doen. Het zorgen dat alle verschillende benoemde doelgroepen reageren op dezelfde advertentie is echt van grote meerwaarde.</p>

<p>Nu kan ik zelf fbcampagnes voeren die echt effectief zijn. Mijn blog waarmee ik tijdens de challenge bezig was om te promoten was binnen een week meer dan 1200 keer gedeeld op fb vanaf mijn website. Fantastisch!</p>',
        ]);
    }
}
