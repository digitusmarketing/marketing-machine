<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            'title' => 'Maandelijks',
            'pay_description' => 'Maandelijks abonnement Socialpreneurs.io',
            'show_description' => 'Nog niet zeker? Begin met een maandelijks abonnement dat je kunt stoppen in 10 seconden.',
            'price' => '47.00',
            'type' => 'maand',
            'interval' => '1',
            'cursus_id' => 1,
        ]);
        DB::table('plans')->insert([
            'title' => 'Jaarlijks',
            'pay_description' => 'Jaarlijks abonnement Socialpreneurs.io',
            'show_description' => 'We hebben het over je business.. Het is alles of niet, ontvang 2 maanden gratis!',
            'price' => '470.00',
            'type' => 'jaar',
            'interval' => '12',
            'cursus_id' => 1,
        ]);
        DB::table('plans')->insert([
            'title' => 'Forever',
            'pay_description'   => 'Forever on Socialpreneurs.io',
            'show_description'  => 'Word niet geshowed',
            'price' =>  '0.00',
            'type'  => 'forever',
            'interval'  => '0',
            'cursus_id' => 1,
        ]);
    }
}
