<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_id');
            $table->string('status');
            $table->string('type')->nullable();
            $table->decimal('price',6,2);
            $table->text('description');
            $table->string('paymentmethod');
            $table->string('order_id');
            $table->integer('user_id')->unsigned();
            $table->integer('plan_id');
            $table->integer('cursus_id');
            $table->string('wefact_invoice_code')->nullable();
            $table->boolean('wefact')->default(0);
            $table->timestamp('payment_date');
            $table->timestamps();
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('orders');
    }
}
