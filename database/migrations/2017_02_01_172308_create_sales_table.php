<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('affiliate_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->boolean('paid_out')->default(0);
            $table->timestamp('paid_at')->nullable();
            $table->timestamp('proximate_payment')->nullable();
            $table->timestamps();
        });
        Schema::table('sales', function (Blueprint $table) {
            $table->foreign('affiliate_id')->references('id')->on('affiliates');
        });
        Schema::table('sales', function (Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales', function (Blueprint $table) {
            $table->dropForeign(['affiliate_id']);
        });
        Schema::table('sales', function (Blueprint $table) {
            $table->dropForeign(['order_id']);
        });
        Schema::dropIfExists('sales');
    }
}
