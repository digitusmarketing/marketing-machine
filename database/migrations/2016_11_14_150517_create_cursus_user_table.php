<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursusUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursus_user', function (Blueprint $table) {
            $table->integer('cursus_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->boolean('completed')->default(0);
            $table->timestamps();
        });
        Schema::table('cursus_user', function (Blueprint $table) {  
            $table->foreign('cursus_id')->references('id')->on('cursussen');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cursus_user', function (Blueprint $table) {
            $table->dropForeign(['cursus_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('cursus_user');
    }
}
