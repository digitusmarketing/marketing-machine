<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('pay_description');
            $table->text('show_description');
            $table->decimal('price',6,2);
            $table->string('type');
            $table->string('interval')->nullable();
            $table->integer('cursus_id')->nullable()->unsigned();
            $table->timestamps();
        });
        Schema::table('plans', function (Blueprint $table) {
            $table->foreign('cursus_id')->references('id')->on('cursussen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->dropForeign(['cursus_id']);
        });
        Schema::dropIfExists('plans');
    }
}
