<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunnelitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funnelitems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('titel');
            $table->string('slogan');
            $table->string('item_link');
            $table->string('call_to_action_titel');
            $table->string('call_to_action_button_text');
            $table->bigInteger('call_to_action_delay');
            $table->integer('funnel_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('funnelitems', function (Blueprint $table) {
            $table->dropForeign(['funnel_id']);
        });
        Schema::dropIfExists('funnelitems');
    }
}
