<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditFunnelitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('funnelitems', function (Blueprint $table) {
            $table->foreign('funnel_id')->references('id')->on('funnels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('funnelitems', function (Blueprint $table) {
            $table->dropForeign(['funnel_id']);
        });
    }
}
