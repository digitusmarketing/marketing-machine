<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('image');
            $table->text('body');
            $table->integer('cursus_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('modules', function (Blueprint $table) {  
            $table->foreign('cursus_id')->references('id')->on('cursussen');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modules', function (Blueprint $table) {
            $table->dropForeign(['cursus_id']);
        });
        Schema::dropIfExists('modules');
    }
}
