<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunnelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funnels', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(0);
            $table->string('slug');
            $table->string('titel');
            $table->string('slogan');
            $table->text('body');
            $table->string('end_point');
            $table->string('pre_button_text');
            $table->string('button_text');
            $table->string('form_button_text');
            $table->string('pre_button_2_text')->nullable();
            $table->string('button_2_text')->nullable();
            $table->string('form_button_2_text')->nullable();
            $table->string('video_link')->nullable();
            $table->string('image')->nullable();
            $table->string('content_titel');
            $table->text('content_body');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funnels');
    }
}
