<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episode_user', function (Blueprint $table) {
            $table->integer('episode_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->boolean('completed')->default(0);
            $table->boolean('favorite')->default(0);
            $table->boolean('watch_later')->default(0);
            $table->smallInteger('watch_count')->default(0);
            $table->timestamps();
        });
        Schema::table('episode_user', function (Blueprint $table) {  
            $table->foreign('episode_id')->references('id')->on('episodes');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('episode_user', function (Blueprint $table) {
            $table->dropForeign(['episode_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('episode_user');
    }
}
