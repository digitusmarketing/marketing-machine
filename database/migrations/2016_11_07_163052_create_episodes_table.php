<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('body');
            $table->string('src');
            $table->string('poster');
            $table->integer('module_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('episodes', function (Blueprint $table) {  
            $table->foreign('module_id')->references('id')->on('modules');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('episodes', function (Blueprint $table) {
            $table->dropForeign(['module_id']);
        });
        Schema::dropIfExists('episodes');
    }
}
