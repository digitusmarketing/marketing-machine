<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditFunnels2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('funnels', function (Blueprint $table) {
            $table->string('thankyou_titel')->nullable();
            $table->string('thankyou_body')->nullable();
            $table->string('thankyou_link')->nullable();
            $table->string('thankyou_video')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('funnels', function (Blueprint $table) {
        //     //
        // });
    }
}
